/**
 * Gestionnaire pour les cases à cocher iCheck
 * 
 * Ce fichier contient les fonctionnalités pour gérer les cases à cocher
 * transformées par iCheck dans l'interface d'administration.
 */

$(document).ready(function() {
    console.log('Initialisation du gestionnaire pour les cases à cocher');
    
    // Écouter les clics sur les conteneurs iCheck
    $(document).on('click', '.icheckbox_square-blue', function(e) {
        console.log('Clic détecté sur un conteneur iCheck');
        var checkbox = $(this).find('.boolean-checkbox');
        if (checkbox.length) {
            var parameterId = checkbox.data('parameter-id');
            if (!parameterId) {
                parameterId = checkbox.closest('.boolean-parameter').data('id');
            }
            
            // Vérifier si la case est cochée après le clic (avec un petit délai pour laisser iCheck mettre à jour l'état)
            setTimeout(function() {
                var isChecked = $(e.currentTarget).hasClass('checked');
                var isDisabled = $(e.currentTarget).closest('.icheckbox_square-blue').hasClass('disabled');
                var newValue = isChecked ? 'true' : 'false';
                console.log('Mise à jour du paramètre après clic:', parameterId, 'avec la valeur:', newValue);
                if (isDisabled) {
                    console.log('Le paramètre est déactivé, aucune mise à jour n est effectuée');
                    return;
                }
                updateParameterValue(parameterId, newValue);
            }, 50);
        }
    });
    
    // Écouter également les événements ifChanged pour les cases à cocher
    $(document).on('ifChanged', '.boolean-checkbox', function(event) {
        console.log('Événement ifChanged détecté');
        var checkbox = $(this);
        var parameterId = checkbox.data('parameter-id');
        if (!parameterId) {
            parameterId = checkbox.closest('.boolean-parameter').data('id');
        }
        
        var isChecked = checkbox.closest('.icheckbox_square-blue').hasClass('checked');
        var newValue = isChecked ? 'true' : 'false';
        console.log('Mise à jour du paramètre via ifChanged:', parameterId, 'avec la valeur:', newValue);
        if ($(this).prop('disabled') === true) {
            return;
        }
        updateParameterValue(parameterId, newValue);
    });
    
    // Fonction pour mettre à jour la valeur d'un paramètre via AJAX
    function updateParameterValue(id, value) {
        if (!id) {
            console.error('Erreur: ID du paramètre manquant');
            return;
        }
        
        console.log('Appel AJAX pour mettre à jour le paramètre:', id);
        
        // Récupérer le token CSRF
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        if (!csrfToken) {
            var csrfTokenInput = $('input[name="_token"]');
            if (csrfTokenInput.length) {
                csrfToken = csrfTokenInput.val();
            }
        }
        
        $.ajax({
            url: '/admin/global-parameter/' + id + '/update-value',
            method: 'POST',
            data: {
                value: value,
                _token: csrfToken
            },
            dataType: 'json',
            success: function(data) {
                console.log('Réponse du serveur:', data);
                if (data.success) {
                    // Afficher un message de succès
                    var alertDiv = $('<div>')
                        .addClass('alert alert-success')
                        .text('Paramètre mis à jour avec succès')
                        .css({
                            position: 'fixed',
                            top: '20px',
                            right: '20px',
                            zIndex: '9999'
                        });
                    
                    $('body').append(alertDiv);
                    
                    setTimeout(function() {
                        alertDiv.remove();
                    }, 3000);
                    
                    // Déclencher des événements spécifiques pour les paramètres de carte
                    var checkbox = $('#checkbox_' + id);
                    var parameterRow = checkbox.closest('.parameter-row');
                    var paramName = parameterRow.find('.parameter-description').text().trim();
                    
                    if (paramName.includes('MAP_CENTER') || paramName.toLowerCase().includes('centre de la carte')) {
                        $('#koh_map_center').trigger('map:updated');
                    } else if (paramName.includes('MAP_ZOOM') || paramName.toLowerCase().includes('zoom de la carte')) {
                        $('#koh_map_zoom').trigger('map:updated');
                    }
                } else {
                    throw new Error(data.message || 'Erreur lors de la mise à jour du paramètre');
                }
            },
            error: function(xhr, status, error) {
                console.error('Erreur AJAX:', error);
                console.error('Statut:', status);
                console.error('Réponse:', xhr.responseText);
                
                var alertDiv = $('<div>')
                    .addClass('alert alert-danger')
                    .text('Erreur lors de la mise à jour du paramètre')
                    .css({
                        position: 'fixed',
                        top: '20px',
                        right: '20px',
                        zIndex: '9999'
                    });
                
                $('body').append(alertDiv);
                
                setTimeout(function() {
                    alertDiv.remove();
                }, 3000);
            }
        });
    }
});
