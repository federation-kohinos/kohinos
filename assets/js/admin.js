import '../bootstrap';

// any CSS you require will output into a single css file (admin.css in this case)
require('../css/admin.css');
require('bootstrap');
require('../css/common.css');

require('../js/geoloc.js');
require('../js/flash-messages.js');
require('../js/map-preview.js');
require('../js/global-parameters.js');
require('../js/icheck-handler.js');

$('#flash-messages').flashNotification('init');

$(document).ready(function() {

	loadDynamicChart();
	function loadDynamicChart() {
		$('.loadDynamicChart').each(function() {
		    var self = $(this);
		    $.ajax({
		        url : self.data('url'),
		        type: 'get',
		        success: function(data) {
		            if(data.success == true && data.html != null) {
	                    self.parent().html(data.html);
	                    self.addClass('label-success');
		            } else {
		                self.addClass('label-error');
		            }
		        },
		        error: function(data) {
		        	self.addClass('label-error');
		        }
		    });
		});

	}
	/*
	 * On user edit page, on the rolesgroup checkbox:
	 *		- display 'group' select if "Gestionnaire de groupe" is checked
	 *		- display 'comptoir' select if "Comptoir" is checked
	*/
	function possiblegroups_display_selects(e) {
		var label = $(this).closest('li').find('.control-label__text')[0].textContent.trim().toLowerCase()
		var isChecked = $(this).closest('li').find('.checked').length > 0

		if (label.includes('comptoir')) {
			if (isChecked) {
				$('.comptoirsgeres_select').removeClass('hide')
			} else {
				$('.comptoirsgeres_select').addClass('hide')
			}
		} else if (label.includes('groupe')) {
			if (isChecked) {
				$('.groupesgeres_select').removeClass('hide')
			} else {
				$('.groupesgeres_select').addClass('hide')
			}
		} else if (label.includes('contact')) {
			if (isChecked) {
				$('.groupecontactsgeres_select').removeClass('hide')
			} else {
				$('.groupecontactsgeres_select').addClass('hide')
			}
		} else if (label.includes('trésorier')) {
			if (isChecked) {
				$('.groupetresoriersgeres_select').removeClass('hide')
			} else {
				$('.groupetresoriersgeres_select').addClass('hide')
			}
		}
	}

	$('.possible_group_cblist li .checkbox label').on('click', possiblegroups_display_selects);
	$('.possible_group_cblist li .checkbox label ins').on('click', possiblegroups_display_selects);


	$('.graphStatsButton').on('click', function (e) {
		e.preventDefault();
		var self = $(this);
		$.ajax({
			url : self.data('url'),
			type: 'get',
			success: function(data) {
				if(data.success == true && data.html != null) {
					$(self.data('target')).html(data.html);
					loadDynamicChart()
				} else {
					self.addClass('label-error');
				}
			},
			error: function(data) {
				self.addClass('label-error');
			}
		});
	});


	$('.editableboolean').on('click', function (e) {
	    e.preventDefault();
	    var self = $(this);
	    $.ajax({
	        url : self.data('url'),
	        type: 'post',
	        data : {'value' : (self.data('value') == 'true')},
	        success: function(data) {
	            if(data.status == 'success' && !data.messages) {
	                if (data.newvalue == 'false') {
	                    self.text('non');
	                    self.data('value', 'false');
	                    self.addClass('label-danger');
	                    self.removeClass('label-success');
	                } else {
	                    self.data('value', 'true');
	                    self.text('oui');
	                    self.removeClass('label-danger');
	                    self.addClass('label-success');
	                }
	            } else {
	                // $(this).addClass('error');
	            }
	        }
	    });
	});
});
