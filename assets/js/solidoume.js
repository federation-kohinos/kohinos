$(document).ready(function() {
  $('#formSolidoumeItem_amount, #formSolidoumeItem_isRecurrent').on('change', function(e) {
    $('#formSolidoumeItem_save').attr('disabled', false);
    var form = this.closest('form')
    var balance = $('#' + form.name + '_balance').val()
    var amount = $('#' + form.name + '_amount').val()
    var isrecurrent = $('#' + form.name + '_isRecurrent').is(':checked')

    if (parseFloat(balance) < parseFloat(amount)) {
      $('#formSolidoumeItem_save').attr('disabled', true);
      $('#formSolidoumeItem_error').attr('style', 'display:block;');
    } else {
      $('#formSolidoumeItem_error').attr('style', 'display:none;');
    }
  });
});