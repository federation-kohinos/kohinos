/**
 * Gestion des paramètres globaux dans l'interface d'administration
 * 
 * Ce fichier contient les fonctionnalités JavaScript pour la gestion des paramètres globaux
 * dans l'interface d'administration de Kohinos.
 */

$(document).ready(function() {
    console.log('Initialisation du script de gestion des paramètres globaux');
    
    // Récupérer le token CSRF
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    if (!csrfToken) {
        // Fallback pour récupérer le token depuis un champ caché
        var csrfTokenInput = $('input[name="_token"]');
        if (csrfTokenInput.length) {
            csrfToken = csrfTokenInput.val();
            console.log('Token CSRF récupéré depuis le champ caché');
        }
    }
    
    // Stocker le token CSRF dans une variable globale
    window.csrfToken = csrfToken;
    console.log('Token CSRF récupéré:', csrfToken ? 'Oui' : 'Non');
    
    // Configurer les en-têtes par défaut pour toutes les requêtes AJAX
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': csrfToken
        }
    });
    
    // Gestion des valeurs textuelles
    $('.editable-value').on('click', function() {
        if ($(this).hasClass('editing')) return;
        
        $(this).addClass('editing');
        var originalValue = $(this).data('original-value');
        var parameterId = $(this).data('id');
        
        var input = $('<input>')
            .attr('type', 'text')
            .addClass('form-control')
            .val(originalValue);
        
        $(this).empty().append(input);
        
        input.focus();
        
        function saveEdit() {
            var newValue = input.val();
            var element = $('.editable-value.editing');
            element.removeClass('editing');
            element.html(newValue || '(vide)');
            element.data('original-value', newValue);
            element.attr('title', newValue);
            
            // Mettre à jour les champs cachés pour la carte si nécessaire
            if (element.hasClass('map-param')) {
                if (element.closest('.parameter-value').prev().text().trim().includes('MAP_CENTER')) {
                    $('#koh_map_center').val(newValue);
                    $('#koh_map_center').trigger('map:updated');
                } else if (element.closest('.parameter-value').prev().text().trim().includes('MAP_ZOOM')) {
                    $('#koh_map_zoom').val(newValue);
                    $('#koh_map_zoom').trigger('map:updated');
                }
            }
            
            updateParameter(parameterId, newValue);
        }
        
        input.on('blur', saveEdit);
        input.on('keydown', function(e) {
            if (e.key === 'Enter') {
                saveEdit();
            } else if (e.key === 'Escape') {
                var element = $('.editable-value.editing');
                element.removeClass('editing');
                element.html(originalValue || '(vide)');
            }
        });
    });
    
    function updateParameter(id, value) {
        console.log('Appel AJAX pour mettre à jour le paramètre:', id);
        
        if (!id) {
            console.error('Erreur: ID du paramètre manquant');
            return;
        }
        
        $.ajax({
            url: '/admin/global-parameter/' + id + '/update-value',
            method: 'POST',
            data: {
                value: value,
                _token: window.csrfToken
            },
            dataType: 'json',
            success: function(data) {
                console.log('Réponse du serveur:', data);
                if (data.success) {
                    // Afficher un message de succès
                    var alertDiv = $('<div>')
                        .addClass('alert alert-success')
                        .text('Paramètre mis à jour avec succès')
                        .css({
                            position: 'fixed',
                            top: '20px',
                            right: '20px',
                            zIndex: '9999'
                        });
                    
                    $('body').append(alertDiv);
                    
                    setTimeout(function() {
                        alertDiv.remove();
                    }, 3000);
                } else {
                    throw new Error(data.message || 'Erreur lors de la mise à jour du paramètre');
                }
            },
            error: function(xhr, status, error) {
                console.error('Erreur AJAX:', error);
                console.error('Statut:', status);
                console.error('Réponse:', xhr.responseText);
                
                var alertDiv = $('<div>')
                    .addClass('alert alert-danger')
                    .text('Erreur lors de la mise à jour du paramètre')
                    .css({
                        position: 'fixed',
                        top: '20px',
                        right: '20px',
                        zIndex: '9999'
                    });
                
                $('body').append(alertDiv);
                
                setTimeout(function() {
                    alertDiv.remove();
                }, 3000);
            }
        });
    }
    
    // Nous n'initialisons plus la carte ici car elle est déjà initialisée dans map-preview.js
    // À la place, nous nous assurons que les événements de mise à jour sont correctement gérés
});
