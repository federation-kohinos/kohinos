require('../leaflet/leaflet.js');
require('../js/flash-messages.js');

$(document).ready(function() {
	var mapMarkers = [];
	$(document).on('click', '.searchLatLon', function (e) {
		e.preventDefault();
	    var self = $(this);
	    $.ajax({
	        url : self.data('url'),
	        type: 'post',
	        data : {'adresse' : $('#'+(self.attr('id').replace('_geoloc_search', ''))+'_geoloc_adresse').val(), 'cpostal' : $('#'+(self.attr('id').replace('_geoloc_search', ''))+'_geoloc_cpostal').val(), 'ville' : $('#'+(self.attr('id').replace('_geoloc_search', ''))+'_geoloc_ville').val()},
	        success: function(response) {
	        	var divid = self.attr('id').replace('_geoloc_search', '');
	        	$('#'+divid+'_message').remove();
	        	$('#'+divid+'_map').remove();
	        	var marker_1 = null;
	            if(response.status == 'success' && response.data && response.data.lat != '' && response.data.lon != '') {
	            	$('#'+divid+'_geoloc_lat').val(response.data.lat);
	            	$('#'+divid+'_geoloc_lon').val(response.data.lon);
	                $('#'+divid+'_geoloc_lat').addClass('inputsuccess');
	                $('#'+divid+'_geoloc_lon').addClass('inputsuccess');
	                $('<p id="'+(divid+'_message')+'">Cliquez sur la carte si vous voulez affiner la géolocalisation</p><div id="'+(divid+'_map')+'" style="height: 250px;margin-top: 10px;"></div>').insertAfter('#'+self.attr('id'));
	                var latlon = L.latLng(parseFloat(response.data.lat), parseFloat(response.data.lon));
	                var mymap = L.map(divid+'_map').setView(latlon, 13);
					L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {maxZoom: 18, attribution: '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(mymap);
					var marker_1 = L.marker(latlon).addTo(mymap);
	            } else {
	                $(this).addClass('inputerror');
	                $('<p id="'+(divid+'_message')+'">ADRESSE INCONNUE : Cliquez sur la carte pour définir une adresse</p><div id="'+(divid+'_map')+'" style="height: 250px;margin-top: 10px;"></div>').insertAfter('#'+self.attr('id'));
	                var mymap = L.map(divid+'_map').setView(JSON.parse($('#koh_map_center').val()), parseInt($('#koh_map_zoom').val()));
	                L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {maxZoom: 18, attribution: '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(mymap);
				}
				mymap.on('click', function(e){
					if (marker_1 != null) {
						mymap.removeLayer(marker_1);
					}
					for(var i = 0; i < mapMarkers.length; i++){
					    mymap.removeLayer(mapMarkers[i]);
					}
				    // Add marker to map at click location; add popup window
				    var newMarker = new L.marker(e.latlng);
				    newMarker.addTo(mymap);
				    mapMarkers.push(newMarker);
				    $('#'+divid+'_geoloc_lat').val(e.latlng.lat);
				    $('#'+divid+'_geoloc_lon').val(e.latlng.lng);
				    $('#'+divid+'_geoloc_lat').addClass('inputsuccess');
				    $('#'+divid+'_geoloc_lon').addClass('inputsuccess');
				});
	        }
	    });
	});
});