// leaftlet : for openstreetmap
require('../leaflet/leaflet.js');

jQuery(document).ready(function() {
    // Vérifier si l'élément map-preview existe
    if (!$('#map-preview').length) {
        return;
    }
    
    let mymap = null;
    
    // Fonction pour initialiser ou mettre à jour la carte
    function initOrUpdateMap() {
        try {
            // Récupérer et traiter les paramètres de la carte
            const mapCenterVal = $('#koh_map_center').val() || '[45.7811,3.0927]';
            const mapZoomVal = $('#koh_map_zoom').val() || '9';
            
            // Parser le centre de la carte
            let mapCenter = [45.7811, 3.0927]; // Valeur par défaut
            
            try {
                // Essayer d'abord de parser comme JSON
                mapCenter = JSON.parse(mapCenterVal);
            } catch (e) {
                // Si échec, extraire les coordonnées manuellement
                if (mapCenterVal.includes(',')) {
                    // Nettoyer la chaîne et extraire les nombres
                    const cleanVal = mapCenterVal.replace(/[^\d.,\[\]]/g, '');
                    const parts = cleanVal.replace(/[\[\]]/g, '').split(',');
                    
                    if (parts.length >= 2) {
                        mapCenter = [parseFloat(parts[0]), parseFloat(parts[1])];
                    }
                }
            }
            
            // Parser le zoom
            const mapZoom = parseInt(mapZoomVal) || 9;
            
            // Configurer la carte
            $('#map-preview').css('height', '300px');
            
            if (mymap === null) {
                // Initialiser une nouvelle carte
                mymap = L.map('map-preview').setView(mapCenter, mapZoom);
                
                // Ajouter la couche de tuiles OpenStreetMap
                L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: '&copy; Openstreetmap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(mymap);
            } else {
                // Mettre à jour la vue de la carte existante
                mymap.setView(mapCenter, mapZoom);
                
                // Supprimer les marqueurs existants
                mymap.eachLayer(function(layer) {
                    if (layer instanceof L.Marker) {
                        mymap.removeLayer(layer);
                    }
                });
            }
            
            // Ajouter un marqueur au centre
            L.marker(mapCenter).addTo(mymap);
            
            // Forcer un redimensionnement de la carte
            setTimeout(() => mymap.invalidateSize(), 500);
        } catch (e) {
            console.error('Erreur lors de l\'initialisation/mise à jour de la carte:', e);
        }
    }
    
    // Initialiser la carte au chargement
    initOrUpdateMap();
    
    // Écouter les événements de mise à jour des paramètres
    $('#koh_map_center, #koh_map_zoom').on('map:updated', initOrUpdateMap);
    $('.map-param').on('change', initOrUpdateMap);
});
