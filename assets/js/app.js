/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)

// start the Stimulus application
import '../bootstrap';

// UTILISER LES FONT AWESOME POUR L'ICONOGRAPHIE
require('../../public/fontawesome/css/all.min.css');
// THEME BOOTSTRAP / BOOTSWATCH + CONFIGURATION GLOBALE (COULEURS, FONTS...)
require('../css/global.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// require jQuery normally
const $ = require('jquery');

// create global $ and jQuery variables
global.$ = global.jQuery = $;

// JS is equivalent to  the normal "bootstrap" package
// no need to set this to a variable, just require it
// require('popper.js/dist/popper.js');
require('bootstrap');

require('../css/common.css');
// BOOTSTRAP plugins
require('bootstrap-slider')
require('bootstrap-slider/dist/css/bootstrap-slider.min.css')

// @TODO : make boostrap-datepicker works !!
// require('bootstrap-datepicker-webpack')
// require('bootstrap-datepicker-webpack/dist/css/bootstrap-datepicker3.min.css')
// require('../../public/bundles/sonatacore/vendor/moment/min/moment-with-locales.min.js');
// require('../../public/bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
// require('../../public/bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');

// leaftlet : for openstreetmap
require('../leaflet/leaflet.js');
// for flash message notification
require('../js/flash-messages.js');
require('../js/solidoume.js');

require('../js/geoloc.js');

require('select2/dist/js/select2.full');
require('select2/dist/js/i18n/fr');

// CSS DU KOHINOS
require('../css/app.css');

$('#flash-messages').flashNotification('init');

var $collectionHolder;
// setup an "add a groupe presta (marché amap" link
var $addGroupeButton = $('<button type="button" class="btn btn-secondary add_groupe_link"><i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter AMAP / Marché</button>');
var $newLinkLi = $('<p class="row mx-2"></p>').append($addGroupeButton);
var $addGeolocButton = $('<button type="button" class="btn btn-secondary add_groupe_link"><i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter une adresse</button>');
var $newLinkLiGeoloc = $('<p class="row mx-2"></p>').append($addGeolocButton);

/* */
function addGroupeForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a groupe" link li
    var $newFormLi = $('<p class="row mx-2"></p>').append(newForm);
    $newLinkLi.before($newFormLi);

    // add a delete link to the new form
    addGroupeFormDeleteLink($newFormLi);
}

function addGroupeFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button type="button" class="col-3 btn btn-warning">Supprimer</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the tag form
        $tagFormLi.remove();
    });
}


/* */
function addGroupeFormGeoloc($collectionHolderGeoloc, $newLinkLiGeoloc) {
    // Get the data-prototype explained earlier
    var prototypeGeoloc = $collectionHolderGeoloc.data('prototype');

    // get the new index
    var indexGeoloc = $collectionHolderGeoloc.find('.form-group').length;

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newFormGeoloc = prototypeGeoloc.replace(/__name__/g, indexGeoloc);

    // increase the index with one for the next item
    $collectionHolderGeoloc.data('index', indexGeoloc + 1);

    // Display the form in the page in an li, before the "Add a groupe" link li
    var $newFormLiGeoloc = $('<p></p>').append(newFormGeoloc);
    $newLinkLiGeoloc.before($newFormLiGeoloc);

    // add a delete link to the new form
    addGroupeFormDeleteLinkGeoloc($newFormLiGeoloc);
}

function addGroupeFormDeleteLinkGeoloc($tagFormLiGeoloc) {
    var $removeFormButtonGeoloc = $('<button type="button" class="btn btn-warning">Supprimer</button>');
    $tagFormLiGeoloc.append($removeFormButtonGeoloc);

    $removeFormButtonGeoloc.on('click', function(e) {
        // remove the li for the tag form
        $tagFormLiGeoloc.remove();
    });
}

function showConfirmTransactionModal(div, form, montant, don, destinataire = null) {
  // Get modal
  var modal = $('#confirmTransactionModal')

  // Get relevant confirmation message div
  var message = modal.find(div)

  // Set data in modal
  message.find('.montant_transaction').text(montant)
  if (parseFloat(don) > 0) {
    message.find('.montant_don').text(don)
  } else {
    message.find('.div_don').html('')
  }
  if (destinataire != null) {
    message.find('.nom_destinataire').text(destinataire)
  }

  // Show modal and relevant confirmation message
  modal.find(div).show()
  modal.modal('show')

  // Bind modal validation button with form submition
  $('#confirmTransactionModal #confirmTransactionButton').off()
  $('#confirmTransactionModal #confirmTransactionButton').on('click', function(e){
    $(this).attr('disabled', true)
    $(this).html('<span class="spinner-border spinner-border-sm ms-4 me-4" role="status" aria-hidden="true"></span>')
    form.submit()
  });
}

jQuery(document).ready(function() {
	// Get the ul that holds the collection of groupes
	$collectionHolder = $('.groupeprestas');

  $('.select2').select2({
    language: 'fr',
    width: '100%',
  });
  $('.select2adh').select2({
    minimumInputLength: 3,
    language: 'fr',
    width: '100%',
    ajax: {
      delay: 350,
      cache: true,
      url: '/search/adherent',
      dataType: 'json'
    }
  });

	// add a delete link to all of the existing tag form li elements
    $collectionHolder.find('div.form-group').each(function() {
    	$(this).addClass('row mx-2');
        addGroupeFormDeleteLink($(this));
    });

	// add the "add a groupe" anchor and li to the groupes ul
	$collectionHolder.append($newLinkLi);

	// count the current form inputs we have (e.g. 2), use that as the new
	// index when inserting a new item (e.g. 2)
	$collectionHolder.data('index', $collectionHolder.find(':input').length);

	$addGroupeButton.on('click', function(e) {
	    // add a new groupe form (see next code block)
	    addGroupeForm($collectionHolder, $newLinkLi);
	});

  if ($('.geolocs').length) {        
  	// Get the ul that holds the collection of groupes
  	$collectionHolderGeoloc = $('.geolocs');

  	// add a delete link to all of the existing tag form li elements
      $collectionHolderGeoloc.find('li').each(function() {
          addGroupeFormDeleteLinkGeoloc($(this));
      });

  	// add the "add a groupe" anchor and li to the groupes ul
  	$collectionHolderGeoloc.append($newLinkLiGeoloc);

  	// count the current form inputs we have (e.g. 2), use that as the new
  	// index when inserting a new item (e.g. 2)
  	$collectionHolderGeoloc.data('index', $collectionHolderGeoloc.find(':input').length);

  	$addGeolocButton.on('click', function(e) {
  	    // add a new groupe form (see next code block)
  	    addGroupeFormGeoloc($collectionHolderGeoloc, $newLinkLiGeoloc);
  	});
  }

	// BOOTSTRAP TOOLTIPS
	$('[data-toggle="tooltip"]').tooltip()

  $("input:text[name='formAchatMonnaieAdherent[don][montant]']").change(function() {
    var valuetotal = parseFloat($("span.achat_monnaie_montant_choisi").text().replace(" €", "")) + parseFloat($("input:text[name='formAchatMonnaieAdherent[don][montant]']").val().replace(",", "."))
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  $("input:text[name='formAchatMonnaieAConfirmerAdherent[don][montant]']").change(function() {
    var valuetotal = parseFloat($("span.achat_monnaie_montant_choisi").text().replace(" €", "")) + parseFloat($("input:text[name='formAchatMonnaieAConfirmerAdherent[don][montant]']").val().replace(",", "."))
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  $("input:radio[name='formAchatMonnaieAdherent[montantradio]']").change(function() {
    var mySlider = $("input.achatmonnaie-montant-slider").slider();

    // Set slider value like radio when radio changes
  	mySlider.slider('setValue', this.value)

    $("span.achat_monnaie_montant_choisi").text(this.value + ' €')
    if ($("input:text[name='formAchatMonnaieAdherent[don][montant]']").length) {
      var valuetotal = parseFloat(this.value) + parseFloat($("input:text[name='formAchatMonnaieAdherent[don][montant]']").val().replace(",", "."))
    } else {
      var valuetotal = parseFloat(this.value)
    }
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  $("input:radio[name='formAchatMonnaieAConfirmerAdherent[montantradio]']").change(function() {
    var mySlider = $("input.achatmonnaie-montant-slider").slider();

    // Set slider value like radio when radio changes
    mySlider.slider('setValue', this.value)

    $("span.achat_monnaie_montant_choisi").text(this.value + ' €')
    if ($("input:text[name='formAchatMonnaieAConfirmerAdherent[don][montant]']").length) {
      var valuetotal = parseFloat(this.value) + parseFloat($("input:text[name='formAchatMonnaieAConfirmerAdherent[don][montant]']").val().replace(",", "."))
    } else {
      var valuetotal = parseFloat(this.value)
    }
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  function debugFormElements(formName) {
    var form = $("form[name='" + formName + "']");
    console.group('Form Elements Debug - ' + formName);
    console.log('Form found:', form.length > 0);
    
    // Debug montant field
    var montantField = form.find('input[name="' + formName + '[montant]"]');
    console.log('Montant field:', {
      found: montantField.length > 0,
      value: montantField.val(),
      visible: !montantField.hasClass('d-none')
    });

    // Debug radio buttons
    var radioButtons = form.find('input[name="' + formName + '[montantradio]"]');
    console.log('Radio buttons:', {
      found: radioButtons.length > 0,
      count: radioButtons.length,
      checkedValue: radioButtons.filter(':checked').val(),
      visible: !radioButtons.closest('.achatmonnaie-montant-radio').hasClass('d-none')
    });

    // Debug slider
    var slider = form.find('.achatmonnaie-montant-slider');
    console.log('Slider:', {
      found: slider.length > 0,
      value: slider.val(),
      visible: !slider.hasClass('d-none')
    });

    console.groupEnd();
  }

  function updateMontantTotal(formName, source) {
    console.group('Updating Montant - ' + formName);
    console.log('Update triggered by:', source);

    var form = $("form[name='" + formName + "']");
    if (!form.length) {
      console.warn('Form not found:', formName);
      console.groupEnd();
      return;
    }

    var montant = 0;
    var montantSource = '';

    // Try to get value from montant field first
    var montantField = form.find('input[name="' + formName + '[montant]"]');
    if (montantField.length && montantField.val()) {
      montant = parseFloat(montantField.val().replace(',', '.')) || 0;
      montantSource = 'montant field';
    }

    // If source is radio, update from radio
    var checkedRadio = form.find('input[name="' + formName + '[montantradio]"]:checked');
    if (source === 'radio' && checkedRadio.length) {
      montant = parseFloat(checkedRadio.val() || 0);
      montantSource = 'radio';
    }

    // If source is slider, update from slider
    var slider = form.find('.achatmonnaie-montant-slider');
    if (source === 'slider' && slider.length) {
      montant = parseFloat(slider.val() || 0);
      montantSource = 'slider';
    }

    console.log('Current montant:', {
      value: montant,
      source: montantSource
    });

    // Update all inputs
    if (montantField.length) {
      montantField.val(montant);
      console.log('Updated montant field to:', montant);
    }

    // Update radio if it exists
    var matchingRadio = form.find('input[name="' + formName + '[montantradio]"][value="' + montant + '"]');
    if (matchingRadio.length) {
      matchingRadio.prop('checked', true);
      console.log('Updated radio button to:', montant);
    } else {
      form.find('input[name="' + formName + '[montantradio]"]').prop('checked', false);
      console.log('Unchecked all radio buttons as no matching value found');
    }

    // Update slider if it exists
    if (slider.length) {
      try {
        slider.slider('setValue', montant);
        console.log('Updated slider to:', montant);
      } catch (e) {
        console.error('Error updating slider:', e);
      }
    }

    // Update display
    form.find('.achat_monnaie_montant_choisi').text(montant + ' €');

    // Calculate total with donation
    var donInput = form.find('input[name="' + formName + '[don][montant]"]');
    var don = 0;
    if (donInput.length && donInput.val()) {
      don = parseFloat(donInput.val().replace(',', '.')) || 0;
    }
    var total = montant + don;
    form.find('.achat_monnaie_montant_total').text(total + ' €');

    console.log('Final values:', {
      montant: montant,
      don: don,
      total: total
    });
    console.groupEnd();
  }

  // Handle form events for both adherent and confirmation forms
  ['formAchatMonnaieAdherent', 'formAchatMonnaieAConfirmerAdherent'].forEach(function(formName) {
    var form = $("form[name='" + formName + "']");
    if (!form.length) {
      console.log('Form not found:', formName);
      return;
    }

    console.log('Initializing form:', formName);
    debugFormElements(formName);

    // Radio button change
    form.find('input[name="' + formName + '[montantradio]"]').change(function() {
      console.log('Radio changed:', $(this).val());
      updateMontantTotal(formName, 'radio');
    });

    // Slider change
    var slider = form.find('.achatmonnaie-montant-slider');
    if (slider.length) {
      try {
        slider.slider().on('change', function(event) {
          console.log('Slider changed:', event.value.newValue);
          updateMontantTotal(formName, 'slider');
        });
      } catch (e) {
        console.error('Error initializing slider:', e);
      }
    }

    // Direct montant input change
    form.find('input[name="' + formName + '[montant]"]').on('input', function() {
      console.log('Montant input changed:', $(this).val());
      updateMontantTotal(formName, 'montant');
    });

    // Don amount change
    form.find('input[name="' + formName + '[don][montant]"]').on('input', function() {
      console.log('Don changed:', $(this).val());
      updateMontantTotal(formName, 'don');
    });

    // Initial update
    updateMontantTotal(formName, 'init');
  });

  $("input:text[name='formAchatMonnaiePrestataire[don][montant]']").change(function() {
    var valuetotal = parseFloat($('input.achatmonnaie-montant-slider').slider().val()) + parseFloat($("input:text[name='formAchatMonnaiePrestataire[don][montant]']").val().replace(",", "."))
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  $("input:text[name='formAchatMonnaieAConfirmerPrestataire[don][montant]']").change(function() {
    var valuetotal = parseFloat($('input.achatmonnaie-montant-slider').slider().val()) + parseFloat($("input:text[name='formAchatMonnaieAConfirmerPrestataire[don][montant]']").val().replace(",", "."))
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  $("input:radio[name='formAchatMonnaiePrestataire[montantradio]']").change(function() {
    var mySlider = $("input.achatmonnaie-montant-slider").slider();

    // Set slider value like radio when radio changes
    mySlider.slider('setValue', this.value)

    $("span.achat_monnaie_montant_choisi").text(this.value + ' €')
    if ($("input:text[name='formAchatMonnaiePrestataire[don][montant]']").length) {
      var valuetotal = parseFloat(this.value) + parseFloat($("input:text[name='formAchatMonnaiePrestataire[don][montant]']").val().replace(",", "."))
    } else {
      var valuetotal = parseFloat(this.value)
    }
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  $("input:radio[name='formAchatMonnaieAConfirmerPrestataire[montantradio]']").change(function() {
    var mySlider = $("input.achatmonnaie-montant-slider").slider();

    // Set slider value like radio when radio changes
    mySlider.slider('setValue', this.value)

    $("span.achat_monnaie_montant_choisi").text(this.value + ' €')
    if ($("input:text[name='formAchatMonnaieAConfirmerPrestataire[don][montant]']").length) {
      var valuetotal = parseFloat(this.value) + parseFloat($("input:text[name='formAchatMonnaieAConfirmerPrestataire[don][montant]']").val().replace(",", "."))
    } else {
      var valuetotal = parseFloat(this.value)
    }
    $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });

  $("form[name='formAchatMonnaiePrestataire'] input.achatmonnaie-montant-slider").slider().on('change', function(event){
      var value = event.value.newValue;

      $("span.achat_monnaie_montant_choisi").text(value + ' €')
      if ($("input:text[name='formAchatMonnaiePrestataire[don][montant]']").length) {
        var valuetotal = parseFloat(value) + parseFloat($("input:text[name='formAchatMonnaiePrestataire[don][montant]']").val().replace(",", "."))
      } else {
        var valuetotal = parseFloat(value)
      }
      $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });
  
  $("form[name='formAchatMonnaieAConfirmerPrestataire'] input.achatmonnaie-montant-slider").slider().on('change', function(event){
      var value = event.value.newValue;

      $("span.achat_monnaie_montant_choisi").text(value + ' €')
      if ($("input:text[name='formAchatMonnaieAConfirmerPrestataire[don][montant]']").length) {
        var valuetotal = parseFloat(value) + parseFloat($("input:text[name='formAchatMonnaieAConfirmerPrestataire[don][montant]']").val().replace(",", "."))
      } else {
        var valuetotal = parseFloat(value)
      }
      $("span.achat_monnaie_montant_total").text(valuetotal + ' €')
  });


  $('.transactionSubmit').on('click', function(e){
      // Stop form submition
      e.preventDefault();
      $(this).attr('disabled', true);

      var form = this.closest('form')
      if (form.checkValidity()) {
        // Get destinataire type : presta or adherent
        var destinataire_type = $('#' + form.name + '_destinataireType')[0].value
        if (destinataire_type == 'prestataire') {
          var div = '.confirmTransactionPrestataire'
        } else {
          var div = '.confirmTransactionAdherent'
        }

        // Get destinataire
        var destinataire_select = $('#' + form.name + '_destinataire')[0]
        if (destinataire_select.length) {
          var destinataire_name = destinataire_select.options[destinataire_select.selectedIndex].text
        }

        // Get montant
        var montant_field = $('#' + form.name + '_montant')[0]
        var montant_value = montant_field.value

        // Get don montant
        var don_value = 0;
        if ($('#' + form.name + '_don_montant').length) {
          var don_field = $('#' + form.name + '_don_montant')[0]
          var don_value = don_field.value
        }

        showConfirmTransactionModal(div, form, montant_value, don_value, destinataire_name)
        $(this).attr('disabled', false);
      } else {
        // Use symfony validation
        form.submit()
      }
  });

  $('.fluxSubmit').on('click', function(e){
      // Stop form submition
      e.preventDefault();
      $(this).attr('disabled', true);

      var form = this.closest('form')
      form.submit()
  });

  $('.cotisationMLCSubmit').on('click', function(e){
      // Stop form submition
      e.preventDefault();
      $(this).attr('disabled', true);

      var form = this.closest('form')

      // Set form moyen
      $('#' + form.name + '_moyen')[0].value = 'emlc'
      $('#' + form.name + '_don_moyen')[0].value = 'emlc'

      if (form.checkValidity()) {
        var div = '.confirmCotisation'

        // Get montant
        var montant_field = $('#' + form.name + '_montant')[0]
        var montant_value = montant_field.value
    
        // Get don montant
        var don_value = 0;
        if ($('#' + form.name + '_don_montant').length) {        
          var don_field = $('#' + form.name + '_don_montant')[0]
          var don_value = don_field.value
        }

        showConfirmTransactionModal(div, form, montant_value, don_value)
        $(this).attr('disabled', false);
      } else {
        // Use symfony validation
        form.submit()
      }
  });
  $('.achatCBSubmit').on('click', function(e){
    // Empêcher la soumission du formulaire pour pouvoir définir les valeurs
    e.preventDefault();
    
    var form = this.closest('form');

    // Set form moyen
    $('#' + form.name + '_moyen')[0].value = 'cb';
    if ($('#' + form.name + '_don_moyen').length) {
      $('#' + form.name + '_don_moyen')[0].value = 'cb';
    }
    
    // Soumettre le formulaire manuellement
    form.submit();
  });
  $('.achatHelloAssoSubmit').on('click', function(e){
      var form = this.closest('form')

      // Set form moyen
      $('#' + form.name + '_moyen')[0].value = 'helloasso'
      if ($('#' + form.name + '_don_moyen').length) {
        $('#' + form.name + '_don_moyen')[0].value = 'helloasso'
      }
  });
  $('.cotisationCBSubmit').on('click', function(e){
      var form = this.closest('form')

      // Set form moyen
      $('#' + form.name + '_moyen')[0].value = 'cb'
      if ($('#' + form.name + '_don_moyen').length) {
        $('#' + form.name + '_don_moyen')[0].value = 'cb'
      }
  });
  $('.cotisationHelloAssoSubmit').on('click', function(e){
      var form = this.closest('form')

      // Set form moyen
      $('#' + form.name + '_moyen')[0].value = 'helloasso'
      if ($('#' + form.name + '_don_moyen').length) {
        $('#' + form.name + '_don_moyen')[0].value = 'helloasso'
      }
  });

  $("input:radio[name='formAchatMonnaieAConfirmerAdherent[moyen]']").change(function() {
    if ($("#demande_achat_text_"+this.value).length) {
      $(".demande_achat_text").hide();
      $("#demande_achat_text_"+this.value).show();
    }
  });

  $("input:radio[name='formAchatMonnaieAConfirmerPrestataire[moyen]']").change(function() {
    if ($("#demande_achat_text_"+this.value).length) {
      $(".demande_achat_text").hide();
      $("#demande_achat_text_"+this.value).show();
    }
  });

  $(".paiementDate").toggle($('#formSolidoumeItem_isRecurrent').is(':checked'))
  $('#formSolidoumeItem_amount').slider()
  $('#formSolidoumeItem_isRecurrent').click(function() {
    $(".paiementDate").toggle(this.checked);
  });

  // $('.js-datepicker').datepicker({
  //     closeText: 'Fermer',
  //     prevText: '&#x3c;Préc',
  //     nextText: 'Suiv&#x3e;',
  //     currentText: 'Aujourd\'hui',
  //     monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
  //     'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
  //     monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
  //     'Jul','Aou','Sep','Oct','Nov','Dec'],
  //     dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
  //     dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
  //     dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
  //     weekHeader: 'Sm',
  //     dateFormat: 'yy-mm-dd',
  //     format: 'yyyy-mm-dd',
  //     firstDay: 1,
  //     isRTL: false,
  //     showMonthAfterYear: false,
  //     yearSuffix: '',
  //     minDate: 0,
  //     maxDate: '+12M +0D',
  //     numberOfMonths: 2,
  //     showButtonPanel: true
  // });

});