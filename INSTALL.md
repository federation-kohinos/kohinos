Installation de Kohinos
**PROCEDURE D'INSTALLATION EN PRODUCTION :**


Apache >= 2

PHP >= 7.4

Extensions :

    intl
    ctype
    iconv
    mysql

Installer composer si besoin

$ curl -sS https://getcomposer.org/installer | php

Cloner le projet dans le dossier nomdudossier

    Sur o2switch, l'accès au clone via SSH est impossible, utilisez plutôt le mode HTTP !

SSH : **$ git clone git@gitlab.com:federation-kohinos/kohinos.git nomdudossier**

ou

HTTPS : $ git clone https://gitlab.com/federation-kohinos/kohinos.git nomdudossier

=> Taper login et mot de passe GITLAB

Aller à nomdudossier

$ cd nomdudossier

Installer les dépendances via composer

$ php ../composer.phar install --optimize-autoloader

ou

**$ composer install --optimize-autoloader**

Modifier les variables d'environnement

Copier le fichier .env.dist en .env et configurer :

    l'accès à la base de données (DATABASE_URL)

    bien faire attention à copier la Version du serveur Mysql (visible sur la page d'accueil de phpmyadmin "Version du serveur :" 
    (toute la valeur avant ' - ') à la fin de la variable DATABASE_URL après "?serverVersion=". 
    Exemple : Sur Mysql on a : Version du serveur : 10.3.31-MariaDB - MariaDB Server 
    On ajoute à DATABASE_URL : => ?serverVersion=10.3.31-MariaDB

- APP_ENV=dev et APP_DEBUG=1
- l'envoi de mail (MAILER_URL)
- la variable APP_SECRET (variable secrète que vous pouvez générer à partir de cette url : http://nux.net/secret

Si vous utilisez Payzen comme moyen de paiement par CB :

1. - PAYZEN_SITE_ID=VotreConfigPayzen
2. - PAYZEN_CERTIFICATE=VotreConfigPayzen
3. - PAYZEN_CTX_MODE=TEST
4. - PAYZEN_DEBUG=true


Lancer la création de la base de données et des données de base du kohinos

Création de la base de données, si ce n'est déjà fait avec les accès dans le .ENV :

**$ php bin/console doctrine:database:create**

Création des tables et des contraintes :

**$ php bin/console doctrine:migrations:migrate --no-interaction**

Charger les fixtures standards :

**$ php bin/console hautelook:fixtures:load --purge-with-truncate --env=pro --no-interaction**

Vous obtiendrez cette erreur ci dessous, c'est normal !

In MediaEventSubscriber.php line 96:
There is no main category related to context: rubrique

**$ php bin/console sonata:media:fix-media-context**

**$ php bin/console hautelook:fixtures:load --append --env=pro**

Supprimer le cache (si besoin)

**$ php bin/console cache:clear**

ou

$ rm -rf var/cache/* 
(supprimes tous les dossiers de cache, plus rapide que clear:cache et plus efficace en cas de changement de structure de base de données)

Installer les assets :

**$ php bin/console assets:install**
 
Générer les fichiers et la BDD des traductions (pour pouvoir les modifier via l'interface d'administration notamment)

!!! NE FONCTIONNE PLUS POUR LE MOMENT !!!

$ php bin/console translation:update --force fr --prefix=""

$ php bin/console translation:update --force en --prefix=""

$ php bin/console lexik:translations:import --case-insensitive --force -c


Si besoin, modifier le htaccess (si apache < 2.4, il y a le .htaccessOLD qui peut être comptatible) (pas nécessaire sur o2switch)

$ vi .htaccess

INSTALLATION SUR LE SERVEUR TERMINÉE

Configuration & test du Paiement Payzen

Une fois Payzen configuré sur le Kohinos, il faut le configurer sur le Back Office de Payzen :

    Paramétrage -> Boutique : configurer les 2 "URL de retour de la boutique" avec la page d'accueil du site.
    Paramétrage -> Règles de notification -> URL de notification à la fin du paiement : dans le 2ème encadré, renseigner les 2 champs avec [url_du_site]/payment/done

Une fois l'installation et la configuration du Kohinos terminées, il est conseillé de réaliser un premier paiement afin de tester le bon fonctionnement de la communication avec Payzen en environnement de production.

Pour cela, payez une cotisation par exemple que vous pourrez rembourser dans le Back Office de Payzen et invalider dans l'admin du Kohinos (reçu = false).

CONFIGURATION DU KOHINOS POUR VOTRE MONNAIE LOCALE

Naviguer vers l'url de votre installation du Kohinos et vous accéderez à l'écran d'installation et de configuration du nom de la monnaie, du super admin, email et mot de passe, ainsi que le nom du siège, du premier groupe local, du solde de monnaie du siège, du centre de la carte etc...

EN CAS D'ERREUR 500 :

- Vérifier les logs présents dans var/log/, en général, si l'environnement est PROD, on peut aller voir le fichier prod.critical-DATE-DU-JOUR.log
Pour cela on peut utiliser la commande suivante : 
**tail -f var/log/prod.xxx.log | grep CRITICAL**