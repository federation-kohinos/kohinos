var indexSectionsWithContent =
{
  0: "$_abcdefghiklmnoprstuv",
  1: "abcdefghiklmnoprstuv",
  2: "d",
  3: "_abcdefghilmoprstuv",
  4: "$",
  5: "dilru"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fonctions",
  4: "Variables",
  5: "Pages"
};

