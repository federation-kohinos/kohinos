var class_app_1_1_entity_1_1_change =
[
    [ "getAllOperations", "class_app_1_1_entity_1_1_change.html#a518726603a9e025dd4a281d8fc34605e", null ],
    [ "getParenttype", "class_app_1_1_entity_1_1_change.html#aa489da46e407e2d648bac8296e4191f5", null ],
    [ "getType", "class_app_1_1_entity_1_1_change.html#a616cb2a9a227f607baae1f3de734beb6", null ],
    [ "getUsersToNotify", "class_app_1_1_entity_1_1_change.html#a279afe19c7f0c2cd57025b55dbc118f1", null ],
    [ "operate", "class_app_1_1_entity_1_1_change.html#a27a3ca995643525ddd794faf8aa3eabf", null ],
    [ "$destinataire", "class_app_1_1_entity_1_1_change.html#a3aebd047c9a3859d54841caefdc500dc", null ],
    [ "TYPE_CHANGE_ADHERENT_COMPTOIR", "class_app_1_1_entity_1_1_change.html#a780e2241b2c4c4ddfe866f50a48d2f15", null ],
    [ "TYPE_CHANGE_PRESTATAIRE_COMPTOIR", "class_app_1_1_entity_1_1_change.html#afdfb5d20979de9fd9a7749ebb7219a4f", null ]
];