var class_app_1_1_admin_1_1_solidoume_param_admin =
[
    [ "configureListFields", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a98650dc6188e0660a0ce5e0cd0fc1e6d", null ],
    [ "configureRoutes", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a8a90f9604673190d272dfe302ac120c1", null ],
    [ "configureSideMenu", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a4435c6bf2e0ad9c286fee45c52103b84", null ],
    [ "getBatchActions", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a8353438ab5d9b4823257ba232aca5234", null ],
    [ "getTemplate", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a4d5c4f0391b0313071bcb51b61b38248", null ],
    [ "$baseRouteName", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a5084c1136b07ee067e2ad9f6ae69b422", null ],
    [ "$baseRoutePattern", "class_app_1_1_admin_1_1_solidoume_param_admin.html#ab00a31caccd23b7f7740a339d5324b44", null ],
    [ "$security", "class_app_1_1_admin_1_1_solidoume_param_admin.html#a9e6352c3b7282ba8709ecf05793ad3db", null ]
];