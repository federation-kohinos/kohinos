var class_app_1_1_admin_1_1_user_admin =
[
    [ "configure", "class_app_1_1_admin_1_1_user_admin.html#af89de5ddf0e9bccb1149f03dbfc75e23", null ],
    [ "configureDatagridFilters", "class_app_1_1_admin_1_1_user_admin.html#ac25fb2cd103b246e665468c97cd8a61c", null ],
    [ "configureFormFields", "class_app_1_1_admin_1_1_user_admin.html#a72e6480340d81d5552e41c5af5069ea9", null ],
    [ "configureListFields", "class_app_1_1_admin_1_1_user_admin.html#ad37d8c1afbc7ca849351e041f52a68e6", null ],
    [ "configureRoutes", "class_app_1_1_admin_1_1_user_admin.html#abb8ee6712905ab044b4d576f18b5985d", null ],
    [ "$baseRouteName", "class_app_1_1_admin_1_1_user_admin.html#aca9dfeba045f3f6830bea1358bc6140a", null ],
    [ "$baseRoutePattern", "class_app_1_1_admin_1_1_user_admin.html#a41959dd826a6c9ab52ed0c4893adc874", null ],
    [ "$datagridValues", "class_app_1_1_admin_1_1_user_admin.html#a14f6e59442dab7e7855f99cf238fc7f8", null ]
];