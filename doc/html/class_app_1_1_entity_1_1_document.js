var class_app_1_1_entity_1_1_document =
[
    [ "__toString", "class_app_1_1_entity_1_1_document.html#a48a54ffcd317ad0159e84f5f8ddeda52", null ],
    [ "getId", "class_app_1_1_entity_1_1_document.html#aae61e290ba6a2550d0fedb08eb5d05b0", null ],
    [ "getMedia", "class_app_1_1_entity_1_1_document.html#ada9b5fbb1101bc0b0c89b303584620ac", null ],
    [ "getUser", "class_app_1_1_entity_1_1_document.html#a1a4c07c5a3ebb21575fd11a3704ab641", null ],
    [ "setMedia", "class_app_1_1_entity_1_1_document.html#a20f9387fe5f155ef6ff3066b55c2402f", null ],
    [ "setUser", "class_app_1_1_entity_1_1_document.html#a8e2597e1b0e6700669655cb7f5b55ec3", null ],
    [ "$id", "class_app_1_1_entity_1_1_document.html#adeb4abd7fd59b5a9b4aaca6d51bc22a5", null ],
    [ "$media", "class_app_1_1_entity_1_1_document.html#a752865c0725c4b22d9745be412bacd41", null ]
];