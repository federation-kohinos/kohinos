var class_app_1_1_entity_1_1_email_token =
[
    [ "__construct", "class_app_1_1_entity_1_1_email_token.html#a81012bbc1436f8b8195c993bcf09e430", null ],
    [ "getExpiredAt", "class_app_1_1_entity_1_1_email_token.html#a51366eb0e9102971222f8378914985a2", null ],
    [ "getToken", "class_app_1_1_entity_1_1_email_token.html#a90fadeb8656dca058dd7665227f66f78", null ],
    [ "getUser", "class_app_1_1_entity_1_1_email_token.html#a7580eff8fe11ef6ea76d97255906c4e7", null ],
    [ "isValid", "class_app_1_1_entity_1_1_email_token.html#a680ff1edb3e8e92cc951bf69d81a80b0", null ],
    [ "setExpiredAt", "class_app_1_1_entity_1_1_email_token.html#a7457bb6b6a3d3d4efb88b9d1917af545", null ],
    [ "setToken", "class_app_1_1_entity_1_1_email_token.html#a07f87c6706f8c37d103769ef9df2c0b7", null ],
    [ "setUser", "class_app_1_1_entity_1_1_email_token.html#a3ff90a07a9df2e7d89dd3adf1a4ff62b", null ],
    [ "$id", "class_app_1_1_entity_1_1_email_token.html#ac52b1c671266d1c5377d2221f87ee9ce", null ]
];