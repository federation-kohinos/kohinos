var class_app_1_1_admin_1_1_page_admin =
[
    [ "configure", "class_app_1_1_admin_1_1_page_admin.html#a4441fafc4ee582916311a92b8437ed63", null ],
    [ "configureDatagridFilters", "class_app_1_1_admin_1_1_page_admin.html#a5f3c8cf47501ca9b51a21764405a4af5", null ],
    [ "configureFormFields", "class_app_1_1_admin_1_1_page_admin.html#aa467d0ea576eac77ca7f8af2adda0f9b", null ],
    [ "configureListFields", "class_app_1_1_admin_1_1_page_admin.html#a20c6a9237ea9646c0a8587a6c4e6a405", null ],
    [ "configureRoutes", "class_app_1_1_admin_1_1_page_admin.html#a2a6cc976caadfa59e0a7165c10ba7ad1", null ],
    [ "getBatchActions", "class_app_1_1_admin_1_1_page_admin.html#a5ac0a4273abd1eddca7fc5ee0127d705", null ],
    [ "setSecurity", "class_app_1_1_admin_1_1_page_admin.html#a5133ca6a97a6d3fb0c5d63c1bfc4077a", null ],
    [ "$datagridValues", "class_app_1_1_admin_1_1_page_admin.html#aad67c49ae68b5cf2e4b2d817fe9f4629", null ],
    [ "$security", "class_app_1_1_admin_1_1_page_admin.html#ad96ccc3f040d9a6ce2e2673109fdfe2b", null ]
];