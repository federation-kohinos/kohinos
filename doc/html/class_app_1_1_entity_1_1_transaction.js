var class_app_1_1_entity_1_1_transaction =
[
    [ "getAllOperations", "class_app_1_1_entity_1_1_transaction.html#a3d661e36bd3d64f2faf997e0490f16d1", null ],
    [ "getParenttype", "class_app_1_1_entity_1_1_transaction.html#a37665ff736ba831ea31585ee432ba613", null ],
    [ "getType", "class_app_1_1_entity_1_1_transaction.html#a5657b7597679c8f4640b2fc02dd46cf1", null ],
    [ "getUsersToNotify", "class_app_1_1_entity_1_1_transaction.html#a56aba49b5513329cbc41fbe41364ab41", null ],
    [ "operate", "class_app_1_1_entity_1_1_transaction.html#a839c2d95e7a49a1f771f6582d4cab99f", null ],
    [ "TYPE_TRANSACTION_ADHERENT_ADHERENT", "class_app_1_1_entity_1_1_transaction.html#a24231e7344301ef1224c80ba5085a96a", null ],
    [ "TYPE_TRANSACTION_ADHERENT_PRESTATAIRE", "class_app_1_1_entity_1_1_transaction.html#af88baf19a2c7f8a404fa117f77a5401f", null ],
    [ "TYPE_TRANSACTION_PRESTATAIRE_ADHERENT", "class_app_1_1_entity_1_1_transaction.html#ab4302cd5b191083dcbf21634adf22382", null ],
    [ "TYPE_TRANSACTION_PRESTATAIRE_PRESTATAIRE", "class_app_1_1_entity_1_1_transaction.html#aca09bea46150071e6fa3a91b50a2de0d", null ]
];