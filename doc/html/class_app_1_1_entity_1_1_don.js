var class_app_1_1_entity_1_1_don =
[
    [ "__construct", "class_app_1_1_entity_1_1_don.html#a66df49184d1e1be9e964af677985a93c", null ],
    [ "__toString", "class_app_1_1_entity_1_1_don.html#ad516badc2f78e920a707e57a7e6997c4", null ],
    [ "getAllOperations", "class_app_1_1_entity_1_1_don.html#a0c750fd69b0ed0d0e5df745738ba7ebe", null ],
    [ "getParenttype", "class_app_1_1_entity_1_1_don.html#a9b6f36c20e168dbef190746d2789f7f7", null ],
    [ "getType", "class_app_1_1_entity_1_1_don.html#a5c578e308e430b48805575875e4b6457", null ],
    [ "getUsersToNotify", "class_app_1_1_entity_1_1_don.html#abef091e531b258f629e659849d0a1b92", null ],
    [ "operate", "class_app_1_1_entity_1_1_don.html#ae8878d1cb3c7c37dceebd4c503b2d3de", null ],
    [ "$destinataire", "class_app_1_1_entity_1_1_don.html#ada4f5ef462bf5b1d9b5832654cf9ade0", null ],
    [ "TYPE_DON_ADHERENT", "class_app_1_1_entity_1_1_don.html#a1675a4e094c17c6933e21bd8c479b449", null ],
    [ "TYPE_DON_PRESTATAIRE", "class_app_1_1_entity_1_1_don.html#a737e0b1a3524d044b171f72f1b7c78e3", null ]
];