var class_app_1_1_admin_1_1_flux_admin =
[
    [ "configureDatagridFilters", "class_app_1_1_admin_1_1_flux_admin.html#a7575f9917bdacb0b98c4fbe6253fd24e", null ],
    [ "configureExportFields", "class_app_1_1_admin_1_1_flux_admin.html#afc37323575fa1afffa75c410d63ceaed", null ],
    [ "configureListFields", "class_app_1_1_admin_1_1_flux_admin.html#afb27d6551b578e5d2a35c1f2da2348f8", null ],
    [ "configureRoutes", "class_app_1_1_admin_1_1_flux_admin.html#a4531c45a38096b928be211dbfbe98fd0", null ],
    [ "createQuery", "class_app_1_1_admin_1_1_flux_admin.html#a4da316ef9061e408bcbc01091c156eb5", null ],
    [ "getDataSourceIterator", "class_app_1_1_admin_1_1_flux_admin.html#a7ce4912299893283c1c6b88e43cee00d", null ],
    [ "getExportFields", "class_app_1_1_admin_1_1_flux_admin.html#a26ef39e59a33a41c4ed458bebfbf1bc8", null ],
    [ "getTemplate", "class_app_1_1_admin_1_1_flux_admin.html#ad992cf2ecce535c4c282e0c5cae21702", null ],
    [ "getTotal", "class_app_1_1_admin_1_1_flux_admin.html#a9ee866baf55e29c14efd7fd903a40a66", null ],
    [ "getTotalLabel", "class_app_1_1_admin_1_1_flux_admin.html#ada61f938b60c14caf0dd1d249c0162a8", null ],
    [ "setSecurity", "class_app_1_1_admin_1_1_flux_admin.html#ad97e4cd4e9d0bcb7185c3d63d38eeee3", null ],
    [ "setTranslator", "class_app_1_1_admin_1_1_flux_admin.html#af8e95d78d66302d2770dc083de38555e", null ],
    [ "$datagridValues", "class_app_1_1_admin_1_1_flux_admin.html#a474a94cdbb9bb299964fa7d2e4ee12ce", null ],
    [ "$maxPerPage", "class_app_1_1_admin_1_1_flux_admin.html#ad0de1a8ae334502215c3a8b3ea5bbe83", null ],
    [ "$perPageOptions", "class_app_1_1_admin_1_1_flux_admin.html#a4cc77528d4e39ea1b2d2f3ec7bc9feda", null ],
    [ "$security", "class_app_1_1_admin_1_1_flux_admin.html#a0f0969720d92cf1099b457318aef1559", null ],
    [ "$translator", "class_app_1_1_admin_1_1_flux_admin.html#a4efcf338215e2d0864f805bf02bba57e", null ]
];