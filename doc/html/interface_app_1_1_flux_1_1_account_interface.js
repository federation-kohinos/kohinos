var interface_app_1_1_flux_1_1_account_interface =
[
    [ "getAccountableObject", "interface_app_1_1_flux_1_1_account_interface.html#ac3aeedc8363622b41d789876adca3ac3", null ],
    [ "getBalance", "interface_app_1_1_flux_1_1_account_interface.html#a9aaed15123cac3b2aeadd2864e4c3d73", null ],
    [ "getCurrency", "interface_app_1_1_flux_1_1_account_interface.html#ae13c2e49409242dec8931d297c7b17bd", null ],
    [ "getId", "interface_app_1_1_flux_1_1_account_interface.html#aa3250401b84dbf4bd51f2ef12895c13c", null ],
    [ "setAccountableObject", "interface_app_1_1_flux_1_1_account_interface.html#a9cbe0eff7a3b25ce02bb7099a333986d", null ],
    [ "setBalance", "interface_app_1_1_flux_1_1_account_interface.html#a498a2f4a25e264d73f5f9a17554d82c8", null ],
    [ "setCurrency", "interface_app_1_1_flux_1_1_account_interface.html#a6f6fe20c67e2704b02bd232d315f5cd8", null ]
];