var class_app_1_1_entity_1_1_vente =
[
    [ "getAllOperations", "class_app_1_1_entity_1_1_vente.html#a7748ed3647484e54f6eaff45b33ade3f", null ],
    [ "getParenttype", "class_app_1_1_entity_1_1_vente.html#a502b5373e74602f861910ab93fa10b77", null ],
    [ "getType", "class_app_1_1_entity_1_1_vente.html#a722eb81b19521629781d546e570ddfeb", null ],
    [ "getUsersToNotify", "class_app_1_1_entity_1_1_vente.html#a3006386e48cabc93462ac8fa9d9bca7b", null ],
    [ "operate", "class_app_1_1_entity_1_1_vente.html#ac2a49fdfd3afaf8ad412d81018b73a13", null ],
    [ "$expediteur", "class_app_1_1_entity_1_1_vente.html#ad561d18f510e55e47bdbc340bebd860b", null ],
    [ "TYPE_VENTE_ADHERENT", "class_app_1_1_entity_1_1_vente.html#ab4cdce896a4825b4615ad52f45995936", null ],
    [ "TYPE_VENTE_PRESTATAIRE", "class_app_1_1_entity_1_1_vente.html#a4e99df6dfaa7b7d66c607ca00e8ebdab", null ]
];