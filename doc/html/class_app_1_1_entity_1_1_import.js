var class_app_1_1_entity_1_1_import =
[
    [ "__construct", "class_app_1_1_entity_1_1_import.html#abdedf5526320e9200f9297c4e4c45cef", null ],
    [ "__toString", "class_app_1_1_entity_1_1_import.html#ac518a87d2e83395f53757cea2600e2d2", null ],
    [ "getErrors", "class_app_1_1_entity_1_1_import.html#af33b41ae1a5316a2f2ff0111a64f9440", null ],
    [ "getId", "class_app_1_1_entity_1_1_import.html#a8aa5ef7550fac5066cfcaf4e933d19f4", null ],
    [ "getMedia", "class_app_1_1_entity_1_1_import.html#adfb53d07c58923204e02d894953b6a52", null ],
    [ "getNbentityadded", "class_app_1_1_entity_1_1_import.html#aa138c939e79acd76b9b201ebef441c9a", null ],
    [ "getNbentityerror", "class_app_1_1_entity_1_1_import.html#a30746683d50598db7b888699bce173bf", null ],
    [ "getSendemail", "class_app_1_1_entity_1_1_import.html#a0fc82fd6851872b521f4267b80706496", null ],
    [ "getSuccess", "class_app_1_1_entity_1_1_import.html#a9a0405f8278ad8388cc6641cefa603ee", null ],
    [ "getTest", "class_app_1_1_entity_1_1_import.html#a61da6f93fcd1db796fb1845a9856f43f", null ],
    [ "getType", "class_app_1_1_entity_1_1_import.html#aec3187304649775af8998ad7121da4e1", null ],
    [ "getUser", "class_app_1_1_entity_1_1_import.html#aa5ec4c824ddd9ad32e31a5072356d14c", null ],
    [ "getWarnings", "class_app_1_1_entity_1_1_import.html#a15846beaaa28452149cc09b08ea3b202", null ],
    [ "setErrors", "class_app_1_1_entity_1_1_import.html#aaffd7f49454ad1ff01cd2734651b8ab0", null ],
    [ "setMedia", "class_app_1_1_entity_1_1_import.html#a64ac56da2dedb9f6660433d306aef446", null ],
    [ "setNbentityadded", "class_app_1_1_entity_1_1_import.html#a6ac5ecde1f2811b949128e7742abba9c", null ],
    [ "setNbentityerror", "class_app_1_1_entity_1_1_import.html#ab19a9ac619487e56e1d3b2ec999f4d19", null ],
    [ "setSendemail", "class_app_1_1_entity_1_1_import.html#a6f45ba7cae9b6e6b914890f74f23d0d8", null ],
    [ "setSuccess", "class_app_1_1_entity_1_1_import.html#a3343aeac0e4951695c5d80749d69040e", null ],
    [ "setTest", "class_app_1_1_entity_1_1_import.html#a183a5eed058c57b66857ab468135f74c", null ],
    [ "setType", "class_app_1_1_entity_1_1_import.html#a27064e2b516757c05d31b81f746356f6", null ],
    [ "setUser", "class_app_1_1_entity_1_1_import.html#a10de2beec8f890a8db7e074223edae7c", null ],
    [ "setWarnings", "class_app_1_1_entity_1_1_import.html#a159e0a83950704c3161535458bbd1640", null ],
    [ "$id", "class_app_1_1_entity_1_1_import.html#afaa5764c9b30d4eba60d285f38966c6e", null ],
    [ "$media", "class_app_1_1_entity_1_1_import.html#a780b2327cc388b874c53d60c3fed9a04", null ]
];