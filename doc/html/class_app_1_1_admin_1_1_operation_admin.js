var class_app_1_1_admin_1_1_operation_admin =
[
    [ "configureDatagridFilters", "class_app_1_1_admin_1_1_operation_admin.html#ac7c46f20313a9efea51d0f905a77995f", null ],
    [ "configureListFields", "class_app_1_1_admin_1_1_operation_admin.html#a043e1da74738396d9ca53969324d8413", null ],
    [ "configureRoutes", "class_app_1_1_admin_1_1_operation_admin.html#a5719676b113840e51e1cbb64df13f16d", null ],
    [ "getDataSourceIterator", "class_app_1_1_admin_1_1_operation_admin.html#a0090d7d83ff7bb468636718dd79f97d0", null ],
    [ "getExportFields", "class_app_1_1_admin_1_1_operation_admin.html#ab6ad49c739d0f98bec998ed8da54a5a1", null ],
    [ "setSecurity", "class_app_1_1_admin_1_1_operation_admin.html#a5e58377d66e05a705d8c8aaf5adbe235", null ],
    [ "setTranslator", "class_app_1_1_admin_1_1_operation_admin.html#adc5c94898aaf771e20fa066cb3dace71", null ],
    [ "$datagridValues", "class_app_1_1_admin_1_1_operation_admin.html#a5c2252d070bd01163134787c6aa87f5b", null ],
    [ "$maxPerPage", "class_app_1_1_admin_1_1_operation_admin.html#aa5fa2f1d2aab3b4ebc3198b772e1066f", null ],
    [ "$perPageOptions", "class_app_1_1_admin_1_1_operation_admin.html#aa84112350ca7d50e574fadb74c240348", null ],
    [ "$security", "class_app_1_1_admin_1_1_operation_admin.html#a58d364507d149493adb2969a5c62cebc", null ],
    [ "$translator", "class_app_1_1_admin_1_1_operation_admin.html#ab887583adbfa355df5213ba871b6049a", null ]
];