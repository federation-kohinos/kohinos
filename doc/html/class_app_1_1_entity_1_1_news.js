var class_app_1_1_entity_1_1_news =
[
    [ "__toString", "class_app_1_1_entity_1_1_news.html#a6f484a8cd7308519623cad9134350620", null ],
    [ "getId", "class_app_1_1_entity_1_1_news.html#a2ac25b049163919a04f0d3416f15c0ab", null ],
    [ "getMedia", "class_app_1_1_entity_1_1_news.html#ae5ca9438de6f5919e6a2aeae1c499aed", null ],
    [ "getPosition", "class_app_1_1_entity_1_1_news.html#a85d486c33601f48ada25f99ea018aaf1", null ],
    [ "getUser", "class_app_1_1_entity_1_1_news.html#aef525942bf423bb076197f44ba721954", null ],
    [ "isVisibleByAllGroups", "class_app_1_1_entity_1_1_news.html#a185e67e27eab294913c595bef539c1d6", null ],
    [ "setMedia", "class_app_1_1_entity_1_1_news.html#ac7fc044c1b2d498556ef3307af8d7544", null ],
    [ "setPosition", "class_app_1_1_entity_1_1_news.html#ad43c311f33cbbc1f0a727091cf5dd0aa", null ],
    [ "setUser", "class_app_1_1_entity_1_1_news.html#acad0e4347bf26b7f557d8844f72ac349", null ],
    [ "setVisibleByAllGroups", "class_app_1_1_entity_1_1_news.html#a8660d6f4da694482af0b14e4afa0a362", null ],
    [ "$id", "class_app_1_1_entity_1_1_news.html#adee4c9712dc30058bca8eb6abcf750a2", null ],
    [ "$media", "class_app_1_1_entity_1_1_news.html#af522d61daf4279c0d3d2aeb8368d3667", null ]
];