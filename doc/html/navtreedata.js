/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Kohinos", "index.html", [
    [ "DOCUMENTATION", "index.html", [
      [ "RESSOURCES", "index.html#autotoc_md32", [
        [ "SITE INTERNET", "index.html#autotoc_md33", null ],
        [ "GITLAB", "index.html#autotoc_md34", null ],
        [ "AUTRES DOCUMENTATIONS :", "index.html#autotoc_md35", null ]
      ] ],
      [ "DOCUMENTATION TECHNIQUE", "index.html#autotoc_md36", [
        [ "Schéma de BDD :", "index.html#autotoc_md37", null ],
        [ "Schéma de relations :", "index.html#autotoc_md38", [
          [ "Interface des flux :", "index.html#autotoc_md39", null ],
          [ "Interface des comptes :", "index.html#autotoc_md40", null ],
          [ "Interface des opérations :", "index.html#autotoc_md41", null ],
          [ "Controlleurs du front :", "index.html#autotoc_md42", null ]
        ] ]
      ] ]
    ] ],
    [ "Update v2.4.5 (2022-08-01)", "md__c_h_a_n_g_e_l_o_g.html", [
      [ "Update v2.4.4 (2022-07-30)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md1", null ],
      [ "Update v2.4.3 (2022-06-26)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md2", null ],
      [ "Update v2.4.2 (2022-06-02)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md3", null ],
      [ "Update v2.4.1 (2022-05-06)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md4", null ],
      [ "Update v2.4.0 (2022-04-20)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md5", null ],
      [ "Update v2.3.0 (2022-03-29)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md6", [
        [ "Sécurité sociale alimentaire (Solidoume pour la doume)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md7", null ]
      ] ],
      [ "Update v2.2.5 (2022-03-25)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md8", null ],
      [ "Update v2.2.4 (2022-03-22)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md9", null ],
      [ "Update v2.2.3 (2022-03-17)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md10", null ],
      [ "Update v2.2.2 (2022-03-17)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md11", null ],
      [ "Update v2.2.1 (2022-02-28)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md12", [
        [ "ADMIN", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md13", null ],
        [ "FRONT", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md14", null ],
        [ "[IMPORTANT]", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md15", null ]
      ] ],
      [ "Update v2.2.0 (2022-01-12)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md16", [
        [ "Security update", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md17", null ],
        [ "Translation", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md18", null ],
        [ "Prestataire connected", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md19", null ],
        [ "FRONT :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md20", null ],
        [ "ADMIN :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md21", null ],
        [ "DIVERS :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md22", null ]
      ] ],
      [ "Update v2.1.5 (2021-05-12)", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md23", [
        [ "Correction pour l'affichage des pages de confirmation de paiement", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md24", null ],
        [ "Suite aux remarques de Stephan :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md25", null ],
        [ "Différents bugfix :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md26", null ]
      ] ],
      [ "Update v2.1.4 (2021-02-22) :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md27", [
        [ "Refactorisation et amélioration du code", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md28", null ],
        [ "Ajout de la possibilité de faire un thème pour le front Kohinos", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md29", null ],
        [ "Sécurité et gestion de la monnaie :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md30", null ],
        [ "Autres améliorations / corrections :", "md__c_h_a_n_g_e_l_o_g.html#autotoc_md31", null ]
      ] ]
    ] ],
    [ "INSTALL", "md__i_n_s_t_a_l_l.html", null ],
    [ "README", "md__r_e_a_d_m_e.html", null ],
    [ "Liste des éléments obsolètes", "deprecated.html", null ],
    [ "Espaces de nommage", "namespaces.html", [
      [ "Liste des espaces de nommage", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Liste des classes", "annotated.html", "annotated_dup" ],
      [ "Index des classes", "classes.html", null ],
      [ "Hiérarchie des classes", "hierarchy.html", "hierarchy" ],
      [ "Membres de classe", "functions.html", [
        [ "Tout", "functions.html", "functions_dup" ],
        [ "Fonctions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"class_app_1_1_admin_1_1_import_admin.html#ab602ff2d0a72d7f5d59953f44b88d367",
"class_app_1_1_command_1_1_check_accounts_balance_command.html#a44960164a41b1423470a7ea11d3d8d86",
"class_app_1_1_controller_1_1_user_prestataire_controller.html#a11c702c42b3949ad1e2ed4b9bed6ddbc",
"class_app_1_1_entity_1_1_cotisation.html#ac4190d5d142c35cc327ef08367a49568",
"class_app_1_1_entity_1_1_groupe.html#a6f16adbdd546e1dd679992cad7bd5115",
"class_app_1_1_entity_1_1_prestataire.html#a3f16993d81219b3e46b53aecde0d573f",
"class_app_1_1_entity_1_1_user.html#a0d49f68fd4853b6d2a926b904b12c7d6",
"class_app_1_1_exporter_1_1_custom_doctrine_o_r_m_query_source_iterator.html#a7974fd956c20bef8e0e2e08e70321368",
"class_app_1_1_form_1_1_type_1_1_prestataire_infos_form_type.html#ab06b6989d31d18d8dbd1e59911577afa",
"class_app_1_1_repository_1_1_solidoume_parameter_repository.html#aaf907bb2c50fb891efb2f27d9ea903be",
"functions_func_a.html"
];

var SYNCONMSG = 'cliquez pour désactiver la synchronisation du panel';
var SYNCOFFMSG = 'cliquez pour activer la synchronisation du panel';