DOCUMENTATION                         {#mainpage}
============

Kohinos (en commun en grec), c’est un logiciel open-source et libre (sous licence AGPLv3), pour la gestion des Monnaies Locales Complémentaires & Citoyennes (MLCC) développé par des MLCC.
Kohinos, c’est aussi une fédération d’associations de MLCC, dotée d’une organisation collégiale autour des besoins réels et concrets des gestionnaires de monnaies locales, dont l’objet principal est le développement du logiciel Kohinos.

# RESSOURCES

## SITE INTERNET

https://kohinos.com/

## GITLAB 

https://gitlab.com/federation-kohinos/kohinos/

## AUTRES DOCUMENTATIONS : 

[DOCUMENTATION UTILISATEUR](http://wiki.kohinos.net/doku.php?id=doc:doc)

[INSTALLATION DU KOHINOS SUR UN SERVEUR O2SWITCH OU MUTUALISE](https://gitlab.com/federation-kohinos/kohinos/-/wikis/Installation-de-Kohinos)

[INSTALLATION DU KOHINOS SUR UN SERVEUR VPS](https://gitlab.com/federation-kohinos/kohinos/-/wikis/Installation-du-kohinos-dans-une-VM-debian-10-sur-OVH)

[AIDE AUX DEVELOPPEURS](https://gitlab.com/federation-kohinos/kohinos/-/wikis/Aide-aux-d%C3%A9veloppeurs)

# DOCUMENTATION TECHNIQUE

## Schéma de BDD :

Voici deux schémas de la Base de Données permettant de comprendre la complexité du Kohinos :

[Schéma global présentant les entités User / Adherent / Prestataire / Groupe / Comptoir etc... et leurs relations](../schemas/Kohinos-DB-User-Prestataire-Adherent.png)

[Schéma global présentant les entités Flux / Account / Operation etc... et leurs relations](../schemas/Kohinos-DB-Flux-account-operation.png)


## Schéma de relations :

Quelques raccourcis vers des schémas de relations pour bien comprendre :

### Interface des flux : 

[Schéma d'héritage de Flux Interface](../html/interface_app_1_1_flux_1_1_flux_interface.html)

[Schéma d'héritage des formulaires de Flux](../html/class_app_1_1_form_1_1_type_1_1_flux_form_type.html)

[Schéma d'héritage des Admin Flux](../html/class_app_1_1_admin_1_1_flux_admin.html)

### Interface des comptes : 

[Schéma d'héritage de Account Interface](../html/interface_app_1_1_flux_1_1_account_interface.html)

### Interface des opérations : 

[Schéma d'héritage de Operation Interface](../html/interface_app_1_1_flux_1_1_operation_interface.html)

### Controlleurs du front : 

[Schéma d'héritage des controlleurs du front](../html/class_app_1_1_controller_1_1_front_controller.html)