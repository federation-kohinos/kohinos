<?php

namespace App\EventListener;

use App\Entity\Usergroup;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\SwitchUserToken;
use Symfony\Component\Security\Core\Role\SwitchUserRole;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class SwitchUserSubscriber implements EventSubscriberInterface
{
    private $router;
    private $em;
    private $session;

    /**
     * SwitchUserSubscriber constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, EntityManagerInterface $em, SessionInterface $session)
    {
        $this->router = $router;
        $this->em = $em;
        $this->session = $session;
    }

    public function onSwitchUser(SwitchUserEvent $event)
    {
        $request = $event->getRequest();
        $token = $event->getToken();
        $user = $event->getTargetUser();

        $request->getSession()->remove('_prestagere');
        $request->getSession()->remove('_comptoirgere');
        $request->getSession()->remove('_groupegere');
        $request->getSession()->remove('_groupecontactgere');
        $request->getSession()->remove('_groupetresoriergere');

        if (1 == $user->getPossiblegroups()->count()) {
            if (1 != count($user->getGroups())) {
                $groupe = $user->getPossiblegroups()->first();
                $this->updateSessionGroup($user, $groupe);
                $user->setGroups([$groupe]);
                $this->em->persist($user);
                $this->em->flush();
                $this->updateToken($event, $user, $token);
            } else {
                $groupe = $user->getGroups()->first();
                $this->updateSessionGroup($user, $groupe);
            }
        } elseif ($user->getPossiblegroups()->count() > 1) {
            if (1 != $user->getGroups()->count()) {
                $hasSuperAdminRole = false;
                foreach ($user->getPossiblegroups() as $groupe) {
                    if (in_array('ROLE_SUPER_ADMIN', $groupe->getRoles())) {
                        $hasSuperAdminRole = true;
                        $user->setGroups([]);
                        $this->em->persist($user);
                        $this->em->flush();
                        $user->setGroups([$groupe]);
                        $this->em->persist($user);
                        $this->em->flush();
                        $this->updateToken($event, $user, $token);
                    }
                }
                if (!$hasSuperAdminRole) {
                    $user->setGroups([]);
                    $this->em->persist($user);
                    $this->em->flush();
                    $this->updateToken($event, $user, $token);
                }
            } else {
                $groupe = $user->getGroups()->first();
                $this->updateSessionGroup($user, $groupe);
            }
        }
    }

    private function updateToken($event, $user, $token)
    {
        $roles = $user->getRoles();
        $roles[] = new SwitchUserRole('ROLE_PREVIOUS_ADMIN', $token, false);
        $token = new SwitchUserToken($user, $user->getPassword(), 'main', $roles, $event->getToken()->getOriginalToken());

        $event->setToken($token);
    }

    public static function getSubscribedEvents()
    {
        return [
            // constant for security.switch_user
            SecurityEvents::SWITCH_USER => 'onSwitchUser',
        ];
    }

    private function updateSessionGroup(UserInterface $user, Usergroup $groupe)
    {
        if (in_array('ROLE_PRESTATAIRE', $groupe->getRoles()) && count($user->getPrestataires()) >= 1) {
            $this->session->set('_prestagere', $user->getPrestataires()[0]);
        } elseif (in_array('ROLE_CAISSIER', $groupe->getRoles()) && count($user->getCaissiers()) >= 1) {
            $this->session->set('_prestagere', $user->getCaissiers()[0]);
        } elseif (in_array('ROLE_COMPTOIR', $groupe->getRoles()) && count($user->getComptoirsGeres()) >= 1) {
            $this->session->set('_comptoirgere', $user->getComptoirsGeres()[0]);
        } elseif ((in_array('ROLE_TRESORIER', $groupe->getRoles()) || in_array('ROLE_CONTACT', $groupe->getRoles()) || in_array('ROLE_GESTION_GROUPE', $groupe->getRoles())) && count($user->getGroupesGeres()) >= 1) {
            $this->session->set('_groupegere', $user->getGroupesGeres()[0]);
        }
    }
}
