<?php

namespace App\EventListener;

use App\Entity\Geoloc;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Geocoder\Provider\Nominatim\Nominatim;
use Geocoder\Query\GeocodeQuery;

class GeolocListener
{
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        $this->updateGeoLoc($entity);
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        if (!($entity instanceof Geoloc) || ($entity instanceof Geoloc) && !($eventArgs->hasChangedField('adresse') || $eventArgs->hasChangedField('cpostal') || $eventArgs->hasChangedField('ville') || $eventArgs->hasChangedField('lat') || $eventArgs->hasChangedField('lon'))) {
            return;
        }
        $this->updateGeoLoc($entity);
    }

    private function updateGeoLoc($entity)
    {
        if (!$entity instanceof Geoloc) {
            return;
        }
        if (empty($entity->getLat()) && empty($entity->getLon())) {
            // GEOCODING ADDRESS :
            $httpClient = new \Http\Adapter\Guzzle6\Client();
            $provider = Nominatim::withOpenStreetMapServer($httpClient, 'Mozilla/5.0');
            $geocoder = new \Geocoder\StatefulGeocoder($provider, 'fr');
            $fullAddress = $entity->getAdresse() . ' ' . $entity->getCpostal() . ' ' . $entity->getVille();
            // Query geocoding from complete address
            $result = $geocoder->geocodeQuery(GeocodeQuery::create($fullAddress));
            if (count($result) > 0) {
                $coords = $result->first()->getCoordinates();
                $entity->setLat(floatval(str_replace(',', '.', $coords->getLatitude())));
                $entity->setLon(floatval(str_replace(',', '.', $coords->getLongitude())));
            }
        }
    }
}
