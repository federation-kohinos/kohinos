<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting.
 */
class UserListener implements EventSubscriberInterface
{
    protected $em;
    protected $router;
    protected $session;
    protected $tokenGenerator;

    public function __construct(EntityManagerInterface $em, RouterInterface $router, SessionInterface $session, TokenGeneratorInterface $tokenGenerator)
    {
        $this->em = $em;
        $this->router = $router;
        $this->session = $session;
        $this->tokenGenerator = $tokenGenerator;
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::CHANGE_PASSWORD_SUCCESS => 'onChangePasswordSuccess',
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_FAILURE => 'onRegistrationFailure',
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onResetSuccess',
            FOSUserEvents::RESETTING_SEND_EMAIL_INITIALIZE => 'onResetInitialize',
            FOSUserEvents::USER_CREATED => 'onUserCreated',
        ];
    }

    public function onResetInitialize(GetResponseNullableUserEvent $event)
    {
        $user = $event->getUser();
        if (null != $user) {
            $user->setPasswordRequestedAt(null);
            $user->setConfirmationToken(null);
            $this->em->persist($user);
            $this->em->flush();
        }
    }

    public function onChangePasswordSuccess(FormEvent $event)
    {
        $url = $this->router->generate('index');
        $event->setResponse(new RedirectResponse($url));
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $url = $this->router->generate('index');
        $event->setResponse(new RedirectResponse($url));
    }

    public function onRegistrationFailure(FormEvent $event)
    {
        $url = $this->router->generate('index');
        $event->setResponse(new RedirectResponse($url));
    }

    public function onResetSuccess(FormEvent $event)
    {
        $url = $this->router->generate('index');
        $event->setResponse(new RedirectResponse($url));
    }

    public function onUserCreated(UserEvent $event)
    {
        // @TODO : notification ?
    }
}
