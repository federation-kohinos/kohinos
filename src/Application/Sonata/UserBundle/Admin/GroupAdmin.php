<?php

declare(strict_types=1);

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Application\Sonata\UserBundle\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\UserBundle\Admin\Model\GroupAdmin as BaseGroupAdmin;

class GroupAdmin extends BaseGroupAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken();
        if (null != $securityContext && null != $securityContext->getUser() && $securityContext->getUser() instanceof User && !($securityContext->getUser()->isGranted('ROLE_SUPER_ADMIN') || $securityContext->getUser()->isGranted('ROLE_ADMIN_SIEGE'))) {
            $collection->clearExcept('list');
        }
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();
        unset($actions['list']);

        return $actions;
    }
}
