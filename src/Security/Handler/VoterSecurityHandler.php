<?php

namespace App\Security\Handler;

use App\Entity\GlobalParameter;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Security\Handler\RoleSecurityHandler;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

class VoterSecurityHandler extends RoleSecurityHandler
{
    protected $manager;

    /**
     * {@inheritdoc}
     */
    public function isGranted(AdminInterface $admin, $attributes, $object = null)
    {
        if (!\is_array($attributes)) {
            $attributes = [$attributes];
        }

        foreach ($attributes as $pos => $attribute) {
            $attributes[$pos] = sprintf($this->getBaseRole($admin), $attribute);
        }

        $allRole = sprintf($this->getBaseRole($admin), 'ALL');

        /* Si on utilise le front Wordpress, on ne fait pas apparaitre la gestion du front (faq, page, document, menu, news...) */
        $isWordpress = 'false' != $this->manager->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_WORDPRESS);
        if ($isWordpress) {
            foreach ($attributes as $attribute) {
                if (false !== strpos($attribute, 'ROLE_ADMIN_FAQ')
                    || false !== strpos($attribute, 'ROLE_ADMIN_PAGE')
                    || false !== strpos($attribute, 'ROLE_ADMIN_DOCUMENT')
                    || false !== strpos($attribute, 'ROLE_PRODIGIOUS_SONATA_MENU')
                    || false !== strpos($attribute, 'ROLE_ADMIN_NEWS')) {
                    return false;
                }
            }
        }
        try {
            return $this->isAnyGranted($this->superAdminRoles)
                || $this->isAnyGranted($attributes, $object)
                || $this->isAnyGranted([$allRole], $object);
        } catch (AuthenticationCredentialsNotFoundException $e) {
            return false;
        }
    }

    public function setManager(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    private function isAnyGranted(array $attributes, $subject = null): bool
    {
        foreach ($attributes as $attribute) {
            if ($this->authorizationChecker->isGranted($attribute, $subject)) {
                return true;
            }
        }

        return false;
    }
}
