<?php

namespace App\Flux;

use App\Entity\User;

interface FluxInterface
{
    /**
     * Obtenir la liste des utilisateurs à notifier.
     *
     * @return array Tableau d'utilisateurs
     */
    public function getUsersToNotify();

    public function getId();

    public function setOperateur(?User $operateur);

    public function getOperateur(): ?User;

    public function getRole(): ?string;

    public function setRole(?string $role);

    public function setParenttype($parenttype);

    public function getParenttype(): string;

    public function setType(string $type);

    public function getType(): string;

    public function setExpediteur($expediteur);

    public function getExpediteur();

    public function setDestinataire($destinataire);

    public function getDestinataire();

    public function getMontant(): ?float;

    public function setMontant(float $montant);

    public function getMoyen(): ?string;

    public function setMoyen($moyen);

    public function getReference(): ?string;

    public function setReference(string $reference);

    public function getHash();

    public function setHash($hash);

    public function isValidationAchat();

    public function getAllOperations($em);
}
