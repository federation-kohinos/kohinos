<?php

namespace App\Flux;

interface AccountableInterface
{
    public function getId();
}
