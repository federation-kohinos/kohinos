<?php

namespace App\Flux;

use App\Utils\AccountUtils;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class AccountableObject
{
    /**
     * @ORM\PostPersist
     *
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event): void
    {
        $object = $event->getEntity();
        $accountUtils = new AccountUtils($event->getEntityManager());
        // Create account related to entity if needed !
        $accountUtils->createAccountForEntity($object);
    }
}
