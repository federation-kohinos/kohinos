<?php

namespace App\Flux;

interface AccountInterface
{
    public function getId();

    public function getBalance(): ?float;

    public function setBalance(float $balance): self;

    // public function addAmount(float $montant): self;
    public function getCurrency(): string;

    public function setCurrency(string $currency): self;

    public function setAccountableObject(AccountableInterface $object): self;

    public function getAccountableObject(): AccountableInterface;
}
