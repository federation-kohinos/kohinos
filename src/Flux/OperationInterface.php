<?php

namespace App\Flux;

interface OperationInterface
{
    public function getId();

    public function getMontant(): ?float;

    public function setMontant(float $montant): self;

    public function getCurrency(): string;

    public function setCurrency(string $currency): self;
}
