<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210120161945 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE flux CHANGE hash hash LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'tmp'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flux CHANGE hash hash LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT null');
    }
}
