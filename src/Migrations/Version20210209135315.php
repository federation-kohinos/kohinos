<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210209135315 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation_adherent ADD hash LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE operation_comptoir ADD hash LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE operation_groupe ADD hash LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE operation_prestataire ADD hash LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE operation_siege ADD hash LONGTEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation_adherent DROP hash');
        $this->addSql('ALTER TABLE operation_comptoir DROP hash');
        $this->addSql('ALTER TABLE operation_groupe DROP hash');
        $this->addSql('ALTER TABLE operation_prestataire DROP hash');
        $this->addSql('ALTER TABLE operation_siege DROP hash');
    }
}
