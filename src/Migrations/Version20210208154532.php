<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210208154532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_adherent CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_comptoir CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_groupe CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_prestataire CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_siege CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_adherent CHANGE balance balance NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE account_comptoir CHANGE balance balance NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE account_groupe CHANGE balance balance NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE account_prestataire CHANGE balance balance NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE account_siege CHANGE balance balance NUMERIC(7, 2) NOT NULL');
    }
}
