<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210628101747 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent ADD idmlc VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE comptoir ADD idmlc VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE groupe ADD idmlc VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE groupeprestaire ADD idmlc VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE prestataire ADD idmlc VARCHAR(100) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP idmlc');
        $this->addSql('ALTER TABLE comptoir DROP idmlc');
        $this->addSql('ALTER TABLE groupe DROP idmlc');
        $this->addSql('ALTER TABLE groupeprestaire DROP idmlc');
        $this->addSql('ALTER TABLE prestataire DROP idmlc');
    }
}
