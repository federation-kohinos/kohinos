<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210219003446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_adherent CHANGE balance balance NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_comptoir CHANGE balance balance NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_groupe CHANGE balance balance NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_prestataire CHANGE balance balance NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE account_siege CHANGE balance balance NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE achatemlctoconfirm CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE adherent CHANGE ecompte ecompte NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE comptoir CHANGE compte compte NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE flux CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE groupe CHANGE compte compte NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_adherent CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_comptoir CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_groupe CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_prestataire CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_siege CHANGE montant montant NUMERIC(10, 2) NOT NULL');
        $this->addSql('ALTER TABLE prestataire CHANGE ecompte ecompte NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE siege CHANGE compte_nantie compte_nantie NUMERIC(10, 2) NOT NULL, CHANGE compte compte NUMERIC(10, 2) NOT NULL, CHANGE ecompte ecompte NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, CHANGE ecompte_nantie ecompte_nantie NUMERIC(10, 2) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_adherent CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE account_comptoir CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE account_groupe CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE account_prestataire CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE account_siege CHANGE balance balance NUMERIC(7, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE achatemlctoconfirm CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE adherent CHANGE ecompte ecompte NUMERIC(12, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE comptoir CHANGE compte compte NUMERIC(12, 2) NOT NULL');
        $this->addSql('ALTER TABLE flux CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE groupe CHANGE compte compte NUMERIC(12, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_adherent CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_comptoir CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_groupe CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_prestataire CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE operation_siege CHANGE montant montant NUMERIC(7, 2) NOT NULL');
        $this->addSql('ALTER TABLE prestataire CHANGE ecompte ecompte NUMERIC(12, 2) DEFAULT \'0.00\' NOT NULL');
        $this->addSql('ALTER TABLE siege CHANGE compte_nantie compte_nantie NUMERIC(12, 2) NOT NULL, CHANGE ecompte_nantie ecompte_nantie NUMERIC(12, 2) NOT NULL, CHANGE compte compte NUMERIC(12, 2) NOT NULL, CHANGE ecompte ecompte NUMERIC(12, 2) NOT NULL');
    }
}
