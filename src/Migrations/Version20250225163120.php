<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250225163120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACHAT_MONNAIE_SHOW_MANUEL_AMOUNT', 'Afficher le champ de montant libre', 'true', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACHAT_MONNAIE_SHOW_SLIDER_AMOUNT', 'Afficher le slider de choix de montant', 'false', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACHAT_MONNAIE_SHOW_RADIO_AMOUNT', 'Afficher le choix de montant', 'true', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACHAT_MONNAIE_VAL_RADIO_AMOUNT', 'Valeurs possibles de choix de montant séparées par une virgule (exemple : 10,20,30,50)', '10,20,30,50', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACHAT_MONNAIE_CAN_PAY_BY_CHECK_OR_TRANFERT', 'Autoriser le paiement par chèque ou virement', 'true', '1')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        
    }
}
