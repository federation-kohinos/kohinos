<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210211161633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX adherentcurrency ON account_adherent (adherent_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX comptoircurrency ON account_comptoir (comptoir_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX groupecurrency ON account_groupe (groupe_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX prestatairecurrency ON account_prestataire (prestataire_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX siegecurrency ON account_siege (siege_id, currency)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX adherentcurrency ON account_adherent');
        $this->addSql('DROP INDEX comptoircurrency ON account_comptoir');
        $this->addSql('DROP INDEX groupecurrency ON account_groupe');
        $this->addSql('DROP INDEX prestatairecurrency ON account_prestataire');
        $this->addSql('DROP INDEX siegecurrency ON account_siege');
    }
}
