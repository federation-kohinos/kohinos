<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225221731 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX adherentcurrency ON account_adherent (adherent_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX comptoircurrency ON account_comptoir (comptoir_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX groupecurrency ON account_groupe (groupe_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX prestatairecurrency ON account_prestataire (prestataire_id, currency)');
        $this->addSql('CREATE UNIQUE INDEX siegecurrency ON account_siege (siege_id, currency)');
        $this->addSql('ALTER TABLE adherent DROP INDEX IDX_90D3F060EF390162, ADD UNIQUE INDEX UNIQ_90D3F060EF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE comptoir DROP INDEX IDX_A6E2C35EEF390162, ADD UNIQUE INDEX UNIQ_A6E2C35EEF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE geoloc_prestataire DROP INDEX IDX_7A6F2D1FEF390162, ADD UNIQUE INDEX UNIQ_7A6F2D1FEF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE groupeprestaire DROP INDEX IDX_FB9ABBCEEF390162, ADD UNIQUE INDEX UNIQ_FB9ABBCEEF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE user DROP INDEX IDX_8D93D64925F06C53, ADD UNIQUE INDEX UNIQ_8D93D64925F06C53 (adherent_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX adherentcurrency ON account_adherent');
        $this->addSql('DROP INDEX comptoircurrency ON account_comptoir');
        $this->addSql('DROP INDEX groupecurrency ON account_groupe');
        $this->addSql('DROP INDEX prestatairecurrency ON account_prestataire');
        $this->addSql('DROP INDEX siegecurrency ON account_siege');
        $this->addSql('ALTER TABLE adherent DROP INDEX UNIQ_90D3F060EF390162, ADD INDEX IDX_90D3F060EF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE comptoir DROP INDEX UNIQ_A6E2C35EEF390162, ADD INDEX IDX_A6E2C35EEF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE geoloc_prestataire DROP INDEX UNIQ_7A6F2D1FEF390162, ADD INDEX IDX_7A6F2D1FEF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE groupeprestaire DROP INDEX UNIQ_FB9ABBCEEF390162, ADD INDEX IDX_FB9ABBCEEF390162 (geoloc_id)');
        $this->addSql('ALTER TABLE prestataire CHANGE iban iban LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci` COMMENT \'(DC2Type:personal_data)\'');
        $this->addSql('ALTER TABLE user DROP INDEX UNIQ_8D93D64925F06C53, ADD INDEX IDX_8D93D64925F06C53 (adherent_id)');
    }
}
