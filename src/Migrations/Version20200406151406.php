<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200406151406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE geoloc_prestataire (id INT AUTO_INCREMENT NOT NULL, prestataire_id INT NOT NULL, geoloc_id INT DEFAULT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_7A6F2D1F989D9B62 (slug), INDEX IDX_7A6F2D1FBE3DB2B7 (prestataire_id), UNIQUE INDEX UNIQ_7A6F2D1FEF390162 (geoloc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE geoloc_prestataire ADD CONSTRAINT FK_7A6F2D1FBE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id)');
        $this->addSql('ALTER TABLE geoloc_prestataire ADD CONSTRAINT FK_7A6F2D1FEF390162 FOREIGN KEY (geoloc_id) REFERENCES geoloc (id)');
        $this->addSql('ALTER TABLE prestataire DROP FOREIGN KEY FK_60A26480EF390162');
        $this->addSql('DROP INDEX UNIQ_60A26480EF390162 ON prestataire');
        $this->addSql('ALTER TABLE prestataire DROP geoloc_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE geoloc_prestataire');
        $this->addSql('ALTER TABLE prestataire ADD geoloc_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prestataire ADD CONSTRAINT FK_60A26480EF390162 FOREIGN KEY (geoloc_id) REFERENCES geoloc (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_60A26480EF390162 ON prestataire (geoloc_id)');
    }
}
