<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202094701 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE achatemlctoconfirm (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, siege_id INT DEFAULT NULL, adherent_dest_id INT DEFAULT NULL, prestataire_dest_id INT DEFAULT NULL, role VARCHAR(200) NOT NULL, type VARCHAR(200) NOT NULL, parenttype VARCHAR(20) NOT NULL, montant NUMERIC(7, 2) NOT NULL, moyen VARCHAR(100) NOT NULL, reference VARCHAR(255) DEFAULT NULL, hash LONGTEXT NOT NULL, reconverti TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_7EADEE38A76ED395 (user_id), INDEX IDX_7EADEE38BF006E8B (siege_id), INDEX IDX_7EADEE38FCF2AA19 (adherent_dest_id), INDEX IDX_7EADEE38ECEF536E (prestataire_dest_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE achatemlctoconfirm ADD CONSTRAINT FK_7EADEE38A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE achatemlctoconfirm ADD CONSTRAINT FK_7EADEE38BF006E8B FOREIGN KEY (siege_id) REFERENCES siege (id)');
        $this->addSql('ALTER TABLE achatemlctoconfirm ADD CONSTRAINT FK_7EADEE38FCF2AA19 FOREIGN KEY (adherent_dest_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE achatemlctoconfirm ADD CONSTRAINT FK_7EADEE38ECEF536E FOREIGN KEY (prestataire_dest_id) REFERENCES prestataire (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE achatemlctoconfirm');
    }
}
