<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\User;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221115155534 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ObjectManager
     */
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->em = $container->get('doctrine')->getManager();
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE usergroup SET roles = \'a:20:{i:0;s:19:"ROLE_GESTION_GROUPE";i:1;s:32:"ROLE_SONATA_USER_ADMIN_USER_VIEW";i:2;s:29:"ROLE_ADMIN_ADHERENT_GERER_ALL";i:3;s:35:"ROLE_ADMIN_ADHERENT_COTISATIONS_ALL";i:4;s:32:"ROLE_ADMIN_PRESTATAIRE_GERER_ALL";i:5;s:38:"ROLE_ADMIN_PRESTATAIRE_COTISATIONS_ALL";i:6;s:28:"ROLE_ADMIN_GROUPE_GERER_EDIT";i:7;s:28:"ROLE_ADMIN_GROUPE_GERER_VIEW";i:8;s:29:"ROLE_ADMIN_COMPTOIR_GERER_ALL";i:9;s:33:"ROLE_ADMIN_GROUPEPRESTA_GERER_ALL";i:10;s:29:"ROLE_ADMIN_DOCUMENT_GERER_ALL";i:11;s:31:"ROLE_ADMIN_TRANSFERT_GERER_EDIT";i:12;s:31:"ROLE_ADMIN_TRANSFERT_GERER_LIST";i:13;s:33:"ROLE_ADMIN_TRANSFERT_GERER_CREATE";i:14;s:33:"ROLE_ADMIN_TRANSFERT_GERER_EXPORT";i:15;s:43:"ROLE_ADMIN_OPERATION_PRESTATAIRE_GERER_LIST";i:16;s:40:"ROLE_ADMIN_OPERATION_ADHERENT_GERER_LIST";i:17;s:40:"ROLE_ADMIN_OPERATION_COMPTOIR_GERER_LIST";i:18;s:32:"ROLE_SONATA_USER_ADMIN_USER_LIST";i:19;s:34:"ROLE_SONATA_USER_ADMIN_USER_EXPORT";}\' WHERE name = "Gestionnaire de Groupe"');
    }

    public function postUp(Schema $schema): void
    {
        $contacts = $this->em->getRepository(User::class)->findByRole('ROLE_CONTACT');
        $tresoriers = $this->em->getRepository(User::class)->findByRole('ROLE_TRESORIER');
        // this up() migration is auto-generated, please modify it to your needs
        foreach ($contacts as $contact) {
            if (!$contact->hasRole('ROLE_GESTION_GROUPE')) {
                $contact->setGroupecontactsgeres($contact->getGroupesgeres());
                $this->em->persist($contact);
            }
        }
        foreach ($tresoriers as $tresorier) {
            if (!$tresorier->hasRole('ROLE_GESTION_GROUPE')) {
                $tresorier->setGroupetresoriersgeres($tresorier->getGroupesgeres());
                $this->em->persist($tresorier);
            }
        }
        $this->em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
