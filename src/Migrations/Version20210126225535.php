<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126225535 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT IGNORE INTO `usergroup` (`id`, `name`, `roles`) VALUES (NULL, 'Valide les demandes d\'achats d\'eMLC', 'a:2:{i:0;s:17:\\\"ROLE_VALIDE_ACHAT\\\";i:1;s:40:\\\"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL\\\";}');");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
