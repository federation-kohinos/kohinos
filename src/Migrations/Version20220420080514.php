<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220420080514 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT IGNORE INTO cron_job (id, name, command, schedule, description, enabled) VALUES
(1, 'Solidoume', 'kohinos:solidoume:execute', '0 1 * * *', 'Sécurité sociale alimentaire : envoyer les emails de rappel et executer le programme pour répartir les e-mlc entre les participants', 1)");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
