<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200309152747 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, media_id INT DEFAULT NULL, user_id INT NOT NULL, visible_by_all_groups TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_1DD39950989D9B62 (slug), INDEX IDX_1DD39950EA9FDD75 (media_id), INDEX IDX_1DD39950A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flux (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, siege_id INT DEFAULT NULL, cotisationinfos_id INT DEFAULT NULL, adherent_id INT DEFAULT NULL, prestataire_id INT DEFAULT NULL, adherent_dest_id INT DEFAULT NULL, prestataire_dest_id INT DEFAULT NULL, comptoir_id INT DEFAULT NULL, groupe_id INT DEFAULT NULL, type VARCHAR(200) NOT NULL, parenttype VARCHAR(20) NOT NULL, montant NUMERIC(7, 2) NOT NULL, moyen VARCHAR(100) NOT NULL, reference VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, discr VARCHAR(255) NOT NULL, reconverti TINYINT(1) DEFAULT NULL, INDEX IDX_7252313AA76ED395 (user_id), INDEX IDX_7252313ABF006E8B (siege_id), INDEX IDX_7252313A2CB102AD (cotisationinfos_id), INDEX IDX_7252313A25F06C53 (adherent_id), INDEX IDX_7252313ABE3DB2B7 (prestataire_id), INDEX IDX_7252313AFCF2AA19 (adherent_dest_id), INDEX IDX_7252313AECEF536E (prestataire_dest_id), INDEX IDX_7252313AAEB0C1F5 (comptoir_id), INDEX IDX_7252313A7A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rubrique (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8FA4097C989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rubrique_prestataire (rubrique_id INT NOT NULL, prestataire_id INT NOT NULL, INDEX IDX_7D4D41BB3BD38833 (rubrique_id), INDEX IDX_7D4D41BBBE3DB2B7 (prestataire_id), PRIMARY KEY(rubrique_id, prestataire_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, media_id INT DEFAULT NULL, user_id INT DEFAULT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_D8698A76989D9B62 (slug), INDEX IDX_D8698A76EA9FDD75 (media_id), INDEX IDX_D8698A76A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE typepresta (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_966F48A2989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE siege (id INT AUTO_INCREMENT NOT NULL, compte_nantie NUMERIC(12, 2) NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, compte NUMERIC(12, 2) NOT NULL, UNIQUE INDEX UNIQ_6706B4F7989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cotisationinfos (id INT AUTO_INCREMENT NOT NULL, annee INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, recu TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE import (id INT AUTO_INCREMENT NOT NULL, media_id INT DEFAULT NULL, user_id INT DEFAULT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9D4ECE1DEA9FDD75 (media_id), INDEX IDX_9D4ECE1DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adherent (id INT AUTO_INCREMENT NOT NULL, groupe_id INT DEFAULT NULL, geoloc_id INT DEFAULT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, ecompte NUMERIC(12, 2) NOT NULL, INDEX IDX_90D3F0607A45358C (groupe_id), UNIQUE INDEX UNIQ_90D3F060EF390162 (geoloc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comptoir (id INT AUTO_INCREMENT NOT NULL, media_id INT DEFAULT NULL, groupe_id INT DEFAULT NULL, geoloc_id INT DEFAULT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, tel VARCHAR(20) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, compte NUMERIC(12, 2) NOT NULL, UNIQUE INDEX UNIQ_A6E2C35E989D9B62 (slug), INDEX IDX_A6E2C35EEA9FDD75 (media_id), INDEX IDX_A6E2C35E7A45358C (groupe_id), UNIQUE INDEX UNIQ_A6E2C35EEF390162 (geoloc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comptoir_user (comptoir_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_615689B1AEB0C1F5 (comptoir_id), INDEX IDX_615689B1A76ED395 (user_id), PRIMARY KEY(comptoir_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, adherent_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(1000) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, etat VARCHAR(10) DEFAULT NULL, mobile VARCHAR(15) DEFAULT NULL, apiKey VARCHAR(255) DEFAULT NULL, alertemailflux TINYINT(1) DEFAULT \'1\' NOT NULL, UNIQUE INDEX UNIQ_8D93D64992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_8D93D649A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_8D93D649C05FB297 (confirmation_token), UNIQUE INDEX UNIQ_8D93D64925F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_usergroup (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_4A84F5F3A76ED395 (user_id), INDEX IDX_4A84F5F3FE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lien (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, url VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_A532B4B5989D9B62 (slug), INDEX IDX_A532B4B5A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prestataire (id INT AUTO_INCREMENT NOT NULL, typeprestataire_id INT DEFAULT NULL, media_id INT DEFAULT NULL, groupe_id INT DEFAULT NULL, geoloc_id INT DEFAULT NULL, raison VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, slug VARCHAR(100) NOT NULL, metier VARCHAR(100) DEFAULT NULL, statut VARCHAR(50) DEFAULT NULL, responsable VARCHAR(200) DEFAULT NULL, iban VARCHAR(100) NOT NULL, siret VARCHAR(50) NOT NULL, web VARCHAR(255) DEFAULT NULL, mlc TINYINT(1) DEFAULT \'0\' NOT NULL, horaires VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, compte NUMERIC(12, 2) NOT NULL, ecompte NUMERIC(12, 2) NOT NULL, UNIQUE INDEX UNIQ_60A26480989D9B62 (slug), INDEX IDX_60A26480618045AC (typeprestataire_id), INDEX IDX_60A26480EA9FDD75 (media_id), INDEX IDX_60A264807A45358C (groupe_id), UNIQUE INDEX UNIQ_60A26480EF390162 (geoloc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prestataire_user (prestataire_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_6A15943DBE3DB2B7 (prestataire_id), INDEX IDX_6A15943DA76ED395 (user_id), PRIMARY KEY(prestataire_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geoloc (id INT AUTO_INCREMENT NOT NULL, adresse VARCHAR(255) DEFAULT NULL, cpostal VARCHAR(10) DEFAULT NULL, ville VARCHAR(100) DEFAULT NULL, lat NUMERIC(10, 8) DEFAULT NULL, lon NUMERIC(10, 8) DEFAULT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE faq (id INT AUTO_INCREMENT NOT NULL, fichier_id INT DEFAULT NULL, media_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_E8FF75CC989D9B62 (slug), INDEX IDX_E8FF75CCF915CFE (fichier_id), INDEX IDX_E8FF75CCEA9FDD75 (media_id), INDEX IDX_E8FF75CCA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, js LONGTEXT DEFAULT NULL, css LONGTEXT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, keywords VARCHAR(255) DEFAULT NULL, template VARCHAR(255) DEFAULT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_140AB620989D9B62 (slug), INDEX IDX_140AB620A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE global_parameter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value LONGTEXT NOT NULL, mandatory TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe (id INT AUTO_INCREMENT NOT NULL, siege_id INT NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, compte NUMERIC(12, 2) NOT NULL, UNIQUE INDEX UNIQ_4B98C21989D9B62 (slug), INDEX IDX_4B98C21BF006E8B (siege_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupe_user (groupe_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_257BA9FE7A45358C (groupe_id), INDEX IDX_257BA9FEA76ED395 (user_id), PRIMARY KEY(groupe_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email_token (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expired_at DATE NOT NULL, INDEX IDX_C27AE0B4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, expediteur_id INT DEFAULT NULL, destinataire_id INT DEFAULT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_B6BD307F989D9B62 (slug), INDEX IDX_B6BD307F10335F61 (expediteur_id), INDEX IDX_B6BD307FA4F84F6E (destinataire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usergroup (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_4A6478175E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupeprestaire (id INT AUTO_INCREMENT NOT NULL, groupe_id INT DEFAULT NULL, media_id INT DEFAULT NULL, geoloc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, horaires VARCHAR(255) DEFAULT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, tel VARCHAR(20) DEFAULT NULL, email VARCHAR(100) DEFAULT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_FB9ABBCE989D9B62 (slug), INDEX IDX_FB9ABBCE7A45358C (groupe_id), INDEX IDX_FB9ABBCEEA9FDD75 (media_id), UNIQUE INDEX UNIQ_FB9ABBCEEF390162 (geoloc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groupeprestataire_prestataire (groupeprestataire_id INT NOT NULL, prestataire_id INT NOT NULL, INDEX IDX_66D8DEC98D18A702 (groupeprestataire_id), INDEX IDX_66D8DEC9BE3DB2B7 (prestataire_id), PRIMARY KEY(groupeprestataire_id, prestataire_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sonata_menu (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sonata_menu_item (id INT AUTO_INCREMENT NOT NULL, parent INT DEFAULT NULL, menu INT NOT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, class_attribute VARCHAR(255) DEFAULT NULL, position SMALLINT UNSIGNED DEFAULT NULL, target TINYINT(1) DEFAULT \'0\', enabled TINYINT(1) DEFAULT \'1\', INDEX IDX_57D4FCFA3D8E604F (parent), INDEX IDX_57D4FCFA7D053A93 (menu), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media__gallery_media (id INT AUTO_INCREMENT NOT NULL, gallery_id INT DEFAULT NULL, media_id INT DEFAULT NULL, position INT NOT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_80D4C5414E7AF8F (gallery_id), INDEX IDX_80D4C541EA9FDD75 (media_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media__gallery (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, context VARCHAR(64) NOT NULL, default_format VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media__media (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, provider_name VARCHAR(255) NOT NULL, provider_status INT NOT NULL, provider_reference VARCHAR(255) NOT NULL, provider_metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', width INT DEFAULT NULL, height INT DEFAULT NULL, length NUMERIC(10, 0) DEFAULT NULL, content_type VARCHAR(255) DEFAULT NULL, content_size INT DEFAULT NULL, copyright VARCHAR(255) DEFAULT NULL, author_name VARCHAR(255) DEFAULT NULL, context VARCHAR(64) DEFAULT NULL, cdn_is_flushable TINYINT(1) DEFAULT NULL, cdn_flush_identifier VARCHAR(64) DEFAULT NULL, cdn_flush_at DATETIME DEFAULT NULL, cdn_status INT DEFAULT NULL, updated_at DATETIME NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lexik_trans_unit (id INT AUTO_INCREMENT NOT NULL, key_name VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX key_domain_idx (key_name, domain), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lexik_translation_file (id INT AUTO_INCREMENT NOT NULL, domain VARCHAR(255) NOT NULL, locale VARCHAR(10) NOT NULL, extention VARCHAR(10) NOT NULL, path VARCHAR(255) NOT NULL, hash VARCHAR(255) NOT NULL, UNIQUE INDEX hash_idx (hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lexik_trans_unit_translations (id INT AUTO_INCREMENT NOT NULL, file_id INT DEFAULT NULL, trans_unit_id INT DEFAULT NULL, locale VARCHAR(10) NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, modified_manually TINYINT(1) NOT NULL, INDEX IDX_B0AA394493CB796C (file_id), INDEX IDX_B0AA3944C3C583C9 (trans_unit_id), UNIQUE INDEX trans_unit_locale_idx (trans_unit_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD39950A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313ABF006E8B FOREIGN KEY (siege_id) REFERENCES siege (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313A2CB102AD FOREIGN KEY (cotisationinfos_id) REFERENCES cotisationinfos (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313ABE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313AFCF2AA19 FOREIGN KEY (adherent_dest_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313AECEF536E FOREIGN KEY (prestataire_dest_id) REFERENCES prestataire (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313AAEB0C1F5 FOREIGN KEY (comptoir_id) REFERENCES comptoir (id)');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313A7A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE rubrique_prestataire ADD CONSTRAINT FK_7D4D41BB3BD38833 FOREIGN KEY (rubrique_id) REFERENCES rubrique (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rubrique_prestataire ADD CONSTRAINT FK_7D4D41BBBE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE import ADD CONSTRAINT FK_9D4ECE1DEA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE import ADD CONSTRAINT FK_9D4ECE1DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F0607A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060EF390162 FOREIGN KEY (geoloc_id) REFERENCES geoloc (id)');
        $this->addSql('ALTER TABLE comptoir ADD CONSTRAINT FK_A6E2C35EEA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE comptoir ADD CONSTRAINT FK_A6E2C35E7A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE comptoir ADD CONSTRAINT FK_A6E2C35EEF390162 FOREIGN KEY (geoloc_id) REFERENCES geoloc (id)');
        $this->addSql('ALTER TABLE comptoir_user ADD CONSTRAINT FK_615689B1AEB0C1F5 FOREIGN KEY (comptoir_id) REFERENCES comptoir (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comptoir_user ADD CONSTRAINT FK_615689B1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64925F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE user_usergroup ADD CONSTRAINT FK_4A84F5F3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_usergroup ADD CONSTRAINT FK_4A84F5F3FE54D947 FOREIGN KEY (group_id) REFERENCES usergroup (id)');
        $this->addSql('ALTER TABLE lien ADD CONSTRAINT FK_A532B4B5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE prestataire ADD CONSTRAINT FK_60A26480618045AC FOREIGN KEY (typeprestataire_id) REFERENCES typepresta (id)');
        $this->addSql('ALTER TABLE prestataire ADD CONSTRAINT FK_60A26480EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE prestataire ADD CONSTRAINT FK_60A264807A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE prestataire ADD CONSTRAINT FK_60A26480EF390162 FOREIGN KEY (geoloc_id) REFERENCES geoloc (id)');
        $this->addSql('ALTER TABLE prestataire_user ADD CONSTRAINT FK_6A15943DBE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prestataire_user ADD CONSTRAINT FK_6A15943DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE faq ADD CONSTRAINT FK_E8FF75CCF915CFE FOREIGN KEY (fichier_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE faq ADD CONSTRAINT FK_E8FF75CCEA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE faq ADD CONSTRAINT FK_E8FF75CCA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C21BF006E8B FOREIGN KEY (siege_id) REFERENCES siege (id)');
        $this->addSql('ALTER TABLE groupe_user ADD CONSTRAINT FK_257BA9FE7A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE groupe_user ADD CONSTRAINT FK_257BA9FEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE email_token ADD CONSTRAINT FK_C27AE0B4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F10335F61 FOREIGN KEY (expediteur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FA4F84F6E FOREIGN KEY (destinataire_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE groupeprestaire ADD CONSTRAINT FK_FB9ABBCE7A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE groupeprestaire ADD CONSTRAINT FK_FB9ABBCEEA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id)');
        $this->addSql('ALTER TABLE groupeprestaire ADD CONSTRAINT FK_FB9ABBCEEF390162 FOREIGN KEY (geoloc_id) REFERENCES geoloc (id)');
        $this->addSql('ALTER TABLE groupeprestataire_prestataire ADD CONSTRAINT FK_66D8DEC98D18A702 FOREIGN KEY (groupeprestataire_id) REFERENCES groupeprestaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE groupeprestataire_prestataire ADD CONSTRAINT FK_66D8DEC9BE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sonata_menu_item ADD CONSTRAINT FK_57D4FCFA3D8E604F FOREIGN KEY (parent) REFERENCES sonata_menu_item (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE sonata_menu_item ADD CONSTRAINT FK_57D4FCFA7D053A93 FOREIGN KEY (menu) REFERENCES sonata_menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media__gallery_media ADD CONSTRAINT FK_80D4C5414E7AF8F FOREIGN KEY (gallery_id) REFERENCES media__gallery (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media__gallery_media ADD CONSTRAINT FK_80D4C541EA9FDD75 FOREIGN KEY (media_id) REFERENCES media__media (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations ADD CONSTRAINT FK_B0AA394493CB796C FOREIGN KEY (file_id) REFERENCES lexik_translation_file (id)');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations ADD CONSTRAINT FK_B0AA3944C3C583C9 FOREIGN KEY (trans_unit_id) REFERENCES lexik_trans_unit (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rubrique_prestataire DROP FOREIGN KEY FK_7D4D41BB3BD38833');
        $this->addSql('ALTER TABLE prestataire DROP FOREIGN KEY FK_60A26480618045AC');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313ABF006E8B');
        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C21BF006E8B');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313A2CB102AD');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313A25F06C53');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313AFCF2AA19');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64925F06C53');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313AAEB0C1F5');
        $this->addSql('ALTER TABLE comptoir_user DROP FOREIGN KEY FK_615689B1AEB0C1F5');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950A76ED395');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313AA76ED395');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76A76ED395');
        $this->addSql('ALTER TABLE import DROP FOREIGN KEY FK_9D4ECE1DA76ED395');
        $this->addSql('ALTER TABLE comptoir_user DROP FOREIGN KEY FK_615689B1A76ED395');
        $this->addSql('ALTER TABLE user_usergroup DROP FOREIGN KEY FK_4A84F5F3A76ED395');
        $this->addSql('ALTER TABLE lien DROP FOREIGN KEY FK_A532B4B5A76ED395');
        $this->addSql('ALTER TABLE prestataire_user DROP FOREIGN KEY FK_6A15943DA76ED395');
        $this->addSql('ALTER TABLE faq DROP FOREIGN KEY FK_E8FF75CCA76ED395');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB620A76ED395');
        $this->addSql('ALTER TABLE groupe_user DROP FOREIGN KEY FK_257BA9FEA76ED395');
        $this->addSql('ALTER TABLE email_token DROP FOREIGN KEY FK_C27AE0B4A76ED395');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F10335F61');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FA4F84F6E');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313ABE3DB2B7');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313AECEF536E');
        $this->addSql('ALTER TABLE rubrique_prestataire DROP FOREIGN KEY FK_7D4D41BBBE3DB2B7');
        $this->addSql('ALTER TABLE prestataire_user DROP FOREIGN KEY FK_6A15943DBE3DB2B7');
        $this->addSql('ALTER TABLE groupeprestataire_prestataire DROP FOREIGN KEY FK_66D8DEC9BE3DB2B7');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060EF390162');
        $this->addSql('ALTER TABLE comptoir DROP FOREIGN KEY FK_A6E2C35EEF390162');
        $this->addSql('ALTER TABLE prestataire DROP FOREIGN KEY FK_60A26480EF390162');
        $this->addSql('ALTER TABLE groupeprestaire DROP FOREIGN KEY FK_FB9ABBCEEF390162');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313A7A45358C');
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F0607A45358C');
        $this->addSql('ALTER TABLE comptoir DROP FOREIGN KEY FK_A6E2C35E7A45358C');
        $this->addSql('ALTER TABLE prestataire DROP FOREIGN KEY FK_60A264807A45358C');
        $this->addSql('ALTER TABLE groupe_user DROP FOREIGN KEY FK_257BA9FE7A45358C');
        $this->addSql('ALTER TABLE groupeprestaire DROP FOREIGN KEY FK_FB9ABBCE7A45358C');
        $this->addSql('ALTER TABLE user_usergroup DROP FOREIGN KEY FK_4A84F5F3FE54D947');
        $this->addSql('ALTER TABLE groupeprestataire_prestataire DROP FOREIGN KEY FK_66D8DEC98D18A702');
        $this->addSql('ALTER TABLE sonata_menu_item DROP FOREIGN KEY FK_57D4FCFA7D053A93');
        $this->addSql('ALTER TABLE sonata_menu_item DROP FOREIGN KEY FK_57D4FCFA3D8E604F');
        $this->addSql('ALTER TABLE media__gallery_media DROP FOREIGN KEY FK_80D4C5414E7AF8F');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD39950EA9FDD75');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76EA9FDD75');
        $this->addSql('ALTER TABLE import DROP FOREIGN KEY FK_9D4ECE1DEA9FDD75');
        $this->addSql('ALTER TABLE comptoir DROP FOREIGN KEY FK_A6E2C35EEA9FDD75');
        $this->addSql('ALTER TABLE prestataire DROP FOREIGN KEY FK_60A26480EA9FDD75');
        $this->addSql('ALTER TABLE faq DROP FOREIGN KEY FK_E8FF75CCF915CFE');
        $this->addSql('ALTER TABLE faq DROP FOREIGN KEY FK_E8FF75CCEA9FDD75');
        $this->addSql('ALTER TABLE groupeprestaire DROP FOREIGN KEY FK_FB9ABBCEEA9FDD75');
        $this->addSql('ALTER TABLE media__gallery_media DROP FOREIGN KEY FK_80D4C541EA9FDD75');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations DROP FOREIGN KEY FK_B0AA3944C3C583C9');
        $this->addSql('ALTER TABLE lexik_trans_unit_translations DROP FOREIGN KEY FK_B0AA394493CB796C');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE flux');
        $this->addSql('DROP TABLE rubrique');
        $this->addSql('DROP TABLE rubrique_prestataire');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE typepresta');
        $this->addSql('DROP TABLE siege');
        $this->addSql('DROP TABLE cotisationinfos');
        $this->addSql('DROP TABLE import');
        $this->addSql('DROP TABLE adherent');
        $this->addSql('DROP TABLE comptoir');
        $this->addSql('DROP TABLE comptoir_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_usergroup');
        $this->addSql('DROP TABLE lien');
        $this->addSql('DROP TABLE prestataire');
        $this->addSql('DROP TABLE prestataire_user');
        $this->addSql('DROP TABLE geoloc');
        $this->addSql('DROP TABLE faq');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE global_parameter');
        $this->addSql('DROP TABLE groupe');
        $this->addSql('DROP TABLE groupe_user');
        $this->addSql('DROP TABLE email_token');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE usergroup');
        $this->addSql('DROP TABLE groupeprestaire');
        $this->addSql('DROP TABLE groupeprestataire_prestataire');
        $this->addSql('DROP TABLE sonata_menu');
        $this->addSql('DROP TABLE sonata_menu_item');
        $this->addSql('DROP TABLE media__gallery_media');
        $this->addSql('DROP TABLE media__gallery');
        $this->addSql('DROP TABLE media__media');
        $this->addSql('DROP TABLE lexik_trans_unit');
        $this->addSql('DROP TABLE lexik_translation_file');
        $this->addSql('DROP TABLE lexik_trans_unit_translations');
    }
}
