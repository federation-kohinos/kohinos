<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314204709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE usergroup SET roles = \'a:18:{i:0;s:16:"ROLE_ADMIN_SIEGE";i:1;s:31:"ROLE_SONATA_USER_ADMIN_USER_ALL";i:2;s:32:"ROLE_SONATA_USER_ADMIN_GROUP_ALL";i:3;s:29:"ROLE_ADMIN_ADHERENT_GERER_ALL";i:4;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:5;s:32:"ROLE_ADMIN_ALL_ACHATSMONNAIE_ALL";i:6;s:27:"ROLE_ADMIN_GROUPE_GERER_ALL";i:7;s:36:"ROLE_ADMIN_GLOBALPARAMETER_GERER_ALL";i:8;s:30:"ROLE_ADMIN_TRANSFERT_GERER_ALL";i:9;s:40:"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL";i:10;s:43:"ROLE_ADMIN_OPERATION_PRESTATAIRE_GERER_LIST";i:11;s:40:"ROLE_ADMIN_OPERATION_ADHERENT_GERER_LIST";i:12;s:40:"ROLE_ADMIN_OPERATION_COMPTOIR_GERER_LIST";i:13;s:38:"ROLE_ADMIN_OPERATION_GROUPE_GERER_LIST";i:14;s:37:"ROLE_ADMIN_OPERATION_SIEGE_GERER_LIST";i:15;s:35:"ROLE_ADMIN_ADHERENT_COTISATIONS_ALL";i:16;s:38:"ROLE_ADMIN_PRESTATAIRE_COTISATIONS_ALL";i:17;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";}\' WHERE name = "Administrateur du Siege"');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
