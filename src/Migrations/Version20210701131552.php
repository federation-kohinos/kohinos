<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210701131552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent CHANGE idmlc idmlc VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE comptoir CHANGE idmlc idmlc VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE groupe CHANGE idmlc idmlc VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE groupeprestaire CHANGE idmlc idmlc VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE prestataire CHANGE idmlc idmlc VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent CHANGE idmlc idmlc VARCHAR(100) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE comptoir CHANGE idmlc idmlc VARCHAR(100) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE groupe CHANGE idmlc idmlc VARCHAR(100) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE groupeprestaire CHANGE idmlc idmlc VARCHAR(100) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE prestataire CHANGE idmlc idmlc VARCHAR(100) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
    }
}
