<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Utils\IdToUuidMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225123627 extends IdToUuidMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `account_adherent` DROP INDEX `adherentcurrency`;');
        $this->addSql('ALTER TABLE `account_comptoir` DROP INDEX `comptoircurrency`;');
        $this->addSql('ALTER TABLE `account_groupe` DROP INDEX `groupecurrency`;');
        $this->addSql('ALTER TABLE `account_siege` DROP INDEX `siegecurrency`;');
        $this->addSql('ALTER TABLE `account_prestataire` DROP INDEX `prestatairecurrency`;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema): void
    {
        $this->migrate('achatemlctoconfirm');
        $this->migrate('adherent');
        $this->migrate('comptoir');
        $this->migrate('contact_comptoir');
        $this->migrate('contact_prestataire');
        $this->migrate('cotisationinfos');
        $this->migrate('document');
        $this->migrate('etat_prestataire');
        $this->migrate('faq');
        $this->migrate('flux');
        $this->migrate('geoloc');
        $this->migrate('geoloc_prestataire');
        $this->migrate('global_parameter');
        $this->migrate('groupe');
        $this->migrate('groupeprestaire');
        $this->migrate('import');
        $this->migrate('news');
        $this->migrate('payment');
        $this->migrate('prestataire');
        $this->migrate('rubrique');
        $this->migrate('siege');
        $this->migrate('typepresta');
        $this->migrate('user');
        $this->migrate('usergroup');
    }
}
