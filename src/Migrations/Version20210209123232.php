<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210209123232 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation_adherent ADD flux_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', ADD extra_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE operation_comptoir ADD flux_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', ADD extra_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE operation_groupe ADD flux_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', ADD extra_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE operation_prestataire ADD flux_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', ADD extra_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE operation_siege ADD flux_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', ADD extra_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation_adherent DROP flux_data, DROP extra_data');
        $this->addSql('ALTER TABLE operation_comptoir DROP flux_data, DROP extra_data');
        $this->addSql('ALTER TABLE operation_groupe DROP flux_data, DROP extra_data');
        $this->addSql('ALTER TABLE operation_prestataire DROP flux_data, DROP extra_data');
        $this->addSql('ALTER TABLE operation_siege DROP flux_data, DROP extra_data');
    }
}
