<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310120847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE usergroup SET roles = \'a:11:{i:0;s:14:"ROLE_TRESORIER";i:1;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:2;s:32:"ROLE_ADMIN_ALL_ACHATSMONNAIE_ALL";i:3;s:33:"ROLE_ADMIN_RECONVERSION_GERER_ALL";i:4;s:30:"ROLE_ADMIN_TRANSFERT_GERER_ALL";i:5;s:40:"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL";i:6;s:42:"ROLE_ADMIN_OPERATION_PRESTATAIRE_GERER_ALL";i:7;s:39:"ROLE_ADMIN_OPERATION_ADHERENT_GERER_ALL";i:8;s:39:"ROLE_ADMIN_OPERATION_COMPTOIR_GERER_ALL";i:9;s:37:"ROLE_ADMIN_OPERATION_GROUPE_GERER_ALL";i:10;s:36:"ROLE_ADMIN_OPERATION_SIEGE_GERER_ALL";}\' WHERE name = "Trésorier"');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
