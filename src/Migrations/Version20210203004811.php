<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210203004811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE usergroup SET roles = \'a:11:{i:0;s:16:"ROLE_ADMIN_SIEGE";i:1;s:31:"ROLE_SONATA_USER_ADMIN_USER_ALL";i:2;s:32:"ROLE_SONATA_USER_ADMIN_GROUP_ALL";i:3;s:29:"ROLE_ADMIN_ADHERENT_GERER_ALL";i:4;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:5;s:27:"ROLE_ADMIN_GROUPE_GERER_ALL";i:6;s:36:"ROLE_ADMIN_GLOBALPARAMETER_GERER_ALL";i:7;s:30:"ROLE_ADMIN_TRANSFERT_GERER_ALL";i:8;s:30:"ROLE_ADMIN_COMPTOIR_GERER_LIST";i:9;s:30:"ROLE_ADMIN_COMPTOIR_GERER_VIEW";i:10;s:40:"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL";}\' WHERE name = "Administrateur du Siege"');
        $this->addSql('UPDATE usergroup SET roles = \'a:7:{i:0;s:14:"ROLE_TRESORIER";i:1;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:2;s:33:"ROLE_ADMIN_RECONVERSION_GERER_ALL";i:3;s:30:"ROLE_ADMIN_TRANSFERT_GERER_ALL";i:4;s:30:"ROLE_ADMIN_COMPTOIR_GERER_LIST";i:5;s:30:"ROLE_ADMIN_COMPTOIR_GERER_VIEW";i:6;s:40:"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL";}\' WHERE name = "Trésorier"');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
