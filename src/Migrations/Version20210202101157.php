<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202101157 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE achatemlctoconfirm ADD achatmonnaie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE achatemlctoconfirm ADD CONSTRAINT FK_7EADEE38E0FA96F FOREIGN KEY (achatmonnaie_id) REFERENCES flux (id)');
        $this->addSql('CREATE INDEX IDX_7EADEE38E0FA96F ON achatemlctoconfirm (achatmonnaie_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE achatemlctoconfirm DROP FOREIGN KEY FK_7EADEE38E0FA96F');
        $this->addSql('DROP INDEX IDX_7EADEE38E0FA96F ON achatemlctoconfirm');
        $this->addSql('ALTER TABLE achatemlctoconfirm DROP achatmonnaie_id');
    }
}
