<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622095500 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE import ADD nbentityadded INT DEFAULT NULL, ADD nbentityerror INT DEFAULT NULL, CHANGE errors errors LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', CHANGE success success LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', CHANGE warnings warnings LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE import DROP nbentityadded, DROP nbentityerror, CHANGE errors errors LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE success success LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE warnings warnings LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`');
    }
}
