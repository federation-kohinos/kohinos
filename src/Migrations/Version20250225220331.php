<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250225220331 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE global_parameter SET description = 'Dons acceptés lors de la cotisation d\'un adhérent ?' WHERE name = 'ACCEPT_DON_ADHERENT_COTISATION'");
        $this->addSql("UPDATE global_parameter SET description = 'Dons acceptés lors de l\'achat de e-mlc d\'un adhérent ?' WHERE name = 'ACCEPT_DON_ADHERENT_ACHAT'");
        $this->addSql("UPDATE global_parameter SET description = 'Dons acceptés lors de la cotisation d\'un prestataire ?' WHERE name = 'ACCEPT_DON_PRESTATAIRE_COTISATION'");
        $this->addSql("UPDATE global_parameter SET description = 'Dons acceptés lors de l\'achat de e-mlc d\'un prestataire ?' WHERE name = 'ACCEPT_DON_PRESTATAIRE_ACHAT'");
        $this->addSql("UPDATE global_parameter SET description = 'Activer la sécurité sociale alimentaire' WHERE name = 'USE_SOLIDOUME'");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
