<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201119234106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE global_parameter ADD description LONGTEXT NOT NULL');
        $this->addSql('UPDATE global_parameter SET description = "\'true\' : utiliser le module Wordpress et désactiver les fonctions du site kohinos, \'false\' : utiliser le site Kohinos" WHERE name = "USE_WORDPRESS"');
        $this->addSql('UPDATE global_parameter SET description = "Nom (long) de la MLC " WHERE name = "MLC_NAME"');
        $this->addSql('UPDATE global_parameter SET description = "Nom raccourci de la MLC" WHERE name = "MLC_NAME_SMALL"');
        $this->addSql('UPDATE global_parameter SET description = "Email d\'envoi des notification" WHERE name = "MLC_NOTIF_EMAIL"');
        $this->addSql('UPDATE global_parameter SET description = "Email de reception du formulaire de contact" WHERE name = "MLC_CONTACT_EMAIL"');
        $this->addSql('UPDATE global_parameter SET description = "Montant minimum de la cotisation adhérents" WHERE name = "COTISATION_ADHERENT"');
        $this->addSql('UPDATE global_parameter SET description = "Montant minimum de la cotisation des prestataires" WHERE name = "COTISATION_PRESTATAIRE"');
        $this->addSql('UPDATE global_parameter SET description = "Taux de reconversion des prestataires" WHERE name = "RECONVERSION_PRESTATAIRE"');
        $this->addSql('UPDATE global_parameter SET description = "Centre de la carte (Coordonnées GPS inscrites comme ceci (sans les guillemets !) : \'[45.7,3.2]\')" WHERE name = "MAP_CENTER"');
        $this->addSql('UPDATE global_parameter SET description = "Zoom de la carte (entre 8 et 12 en général)" WHERE name = "MAP_ZOOM"');
        $this->addSql('UPDATE global_parameter SET description = "\'true\' : utiliser la module de paiement Payzen, \'false\' : ne pas utiliser ce module" WHERE name = "USE_PAYZEN"');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE global_parameter DROP description');
    }
}
