<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\AccountPrestataire;
use App\Entity\Prestataire;
use App\Enum\CurrencyEnum;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220329182822 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ObjectManager
     */
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->em = $container->get('doctrine')->getManager();
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
    }

    public function postUp(Schema $schema): void
    {
        $presta = $this->em->getRepository(Prestataire::class)->getPrestataireSolidoume();
        // this up() migration is auto-generated, please modify it to your needs
        $account = new AccountPrestataire();
        $account
            ->setBalance(0)
            ->setCurrency(CurrencyEnum::CURRENCY_EMLC)
        ;
        $presta->addAccount($account);
        $this->em->persist($account);
        $this->em->persist($presta);
        $this->em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
