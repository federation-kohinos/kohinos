<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220329182243 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE solidoume_item (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', adherent_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', amount DOUBLE PRECISION NOT NULL, is_recurrent TINYINT(1) NOT NULL, paiement_date INT NOT NULL, is_donation TINYINT(1) NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E1D50D5925F06C53 (adherent_id), INDEX IDX_E1D50D59A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE solidoume_parameter (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, reminder_email LONGTEXT DEFAULT NULL, minimum DOUBLE PRECISION NOT NULL, maximum DOUBLE PRECISION NOT NULL, commission DOUBLE PRECISION NOT NULL, reminder_days INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE solidoume_item ADD CONSTRAINT FK_E1D50D5925F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE solidoume_item ADD CONSTRAINT FK_E1D50D59A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('ALTER TABLE solidoume_parameter ADD confirm_email LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE solidoume_parameter ADD execution_date INT NOT NULL');
        $this->addSql('ALTER TABLE solidoume_parameter ADD user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', ADD enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE solidoume_parameter ADD CONSTRAINT FK_B4CCE5E1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B4CCE5E1A76ED395 ON solidoume_parameter (user_id)');

        $this->addSql('ALTER TABLE prestataire ADD solidoume TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'USE_SOLIDOUME', 'Activer la sécurité sociale alimentaire', 'false', '1')");

        $this->addSql('ALTER TABLE solidoume_item ADD comments LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE solidoume_item ADD last_month_payed INT DEFAULT NULL');

        $this->addSql("INSERT IGNORE INTO prestataire (id, media_id, raison, description, slug, metier, statut, responsable, iban, siret, web, mlc, horaires, enabled, created_at, updated_at, ecompte, tauxreconversion, groupe_id, typeprestataire_id, acceptemlc, idmlc, comments, solidoume) VALUES (UUID(), NULL, 'Sécurité Sociale Alimentaire', 'Sécurité Sociale Alimentaire', 'securite-sociale-alimentaire', NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, '0', CURRENT_DATE(), CURRENT_DATE(), '0.00', NULL, NULL, NULL, '0', NULL, 'Sécurité Sociale Alimentaire', '1')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql('DROP TABLE solidoume_parameter');
        $this->addSql('DROP TABLE solidoume_item');
        $this->addSql('ALTER TABLE prestataire DROP solidoume');
    }
}
