<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210208144223 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE account_adherent (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', adherent_id INT NOT NULL, balance NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, hash LONGTEXT NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E81D12D625F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_comptoir (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', comptoir_id INT NOT NULL, balance NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, hash LONGTEXT NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DE2C21E8AEB0C1F5 (comptoir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_groupe (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', groupe_id INT NOT NULL, balance NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, hash LONGTEXT NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_238D42527A45358C (groupe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_prestataire (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', prestataire_id INT NOT NULL, balance NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, hash LONGTEXT NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B1E59883BE3DB2B7 (prestataire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_siege (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', siege_id INT NOT NULL, balance NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, hash LONGTEXT NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_51896D86BF006E8B (siege_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_adherent (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', flux_id INT NOT NULL, account_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', montant NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_75A58FEDC85926E (flux_id), INDEX IDX_75A58FED9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_comptoir (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', flux_id INT NOT NULL, account_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', montant NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_4394BCD3C85926E (flux_id), INDEX IDX_4394BCD39B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_groupe (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', flux_id INT NOT NULL, account_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', montant NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_79393543C85926E (flux_id), INDEX IDX_793935439B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_prestataire (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', flux_id INT NOT NULL, account_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', montant NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_988E5D86C85926E (flux_id), INDEX IDX_988E5D869B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_siege (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', flux_id INT NOT NULL, account_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', montant NUMERIC(7, 2) NOT NULL, currency VARCHAR(25) DEFAULT \'\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E0216778C85926E (flux_id), INDEX IDX_E02167789B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account_adherent ADD CONSTRAINT FK_E81D12D625F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE account_comptoir ADD CONSTRAINT FK_DE2C21E8AEB0C1F5 FOREIGN KEY (comptoir_id) REFERENCES comptoir (id)');
        $this->addSql('ALTER TABLE account_groupe ADD CONSTRAINT FK_238D42527A45358C FOREIGN KEY (groupe_id) REFERENCES groupe (id)');
        $this->addSql('ALTER TABLE account_prestataire ADD CONSTRAINT FK_B1E59883BE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id)');
        $this->addSql('ALTER TABLE account_siege ADD CONSTRAINT FK_51896D86BF006E8B FOREIGN KEY (siege_id) REFERENCES siege (id)');
        $this->addSql('ALTER TABLE operation_adherent ADD CONSTRAINT FK_75A58FEDC85926E FOREIGN KEY (flux_id) REFERENCES flux (id)');
        $this->addSql('ALTER TABLE operation_adherent ADD CONSTRAINT FK_75A58FED9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_adherent (id)');
        $this->addSql('ALTER TABLE operation_comptoir ADD CONSTRAINT FK_4394BCD3C85926E FOREIGN KEY (flux_id) REFERENCES flux (id)');
        $this->addSql('ALTER TABLE operation_comptoir ADD CONSTRAINT FK_4394BCD39B6B5FBA FOREIGN KEY (account_id) REFERENCES account_comptoir (id)');
        $this->addSql('ALTER TABLE operation_groupe ADD CONSTRAINT FK_79393543C85926E FOREIGN KEY (flux_id) REFERENCES flux (id)');
        $this->addSql('ALTER TABLE operation_groupe ADD CONSTRAINT FK_793935439B6B5FBA FOREIGN KEY (account_id) REFERENCES account_groupe (id)');
        $this->addSql('ALTER TABLE operation_prestataire ADD CONSTRAINT FK_988E5D86C85926E FOREIGN KEY (flux_id) REFERENCES flux (id)');
        $this->addSql('ALTER TABLE operation_prestataire ADD CONSTRAINT FK_988E5D869B6B5FBA FOREIGN KEY (account_id) REFERENCES account_prestataire (id)');
        $this->addSql('ALTER TABLE operation_siege ADD CONSTRAINT FK_E0216778C85926E FOREIGN KEY (flux_id) REFERENCES flux (id)');
        $this->addSql('ALTER TABLE operation_siege ADD CONSTRAINT FK_E02167789B6B5FBA FOREIGN KEY (account_id) REFERENCES account_siege (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation_adherent DROP FOREIGN KEY FK_75A58FED9B6B5FBA');
        $this->addSql('ALTER TABLE operation_comptoir DROP FOREIGN KEY FK_4394BCD39B6B5FBA');
        $this->addSql('ALTER TABLE operation_groupe DROP FOREIGN KEY FK_793935439B6B5FBA');
        $this->addSql('ALTER TABLE operation_prestataire DROP FOREIGN KEY FK_988E5D869B6B5FBA');
        $this->addSql('ALTER TABLE operation_siege DROP FOREIGN KEY FK_E02167789B6B5FBA');
        $this->addSql('DROP TABLE account_adherent');
        $this->addSql('DROP TABLE account_comptoir');
        $this->addSql('DROP TABLE account_groupe');
        $this->addSql('DROP TABLE account_prestataire');
        $this->addSql('DROP TABLE account_siege');
        $this->addSql('DROP TABLE operation_adherent');
        $this->addSql('DROP TABLE operation_comptoir');
        $this->addSql('DROP TABLE operation_groupe');
        $this->addSql('DROP TABLE operation_prestataire');
        $this->addSql('DROP TABLE operation_siege');
    }
}
