<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630143300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flux ADD historical TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE operation_adherent ADD historical TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE operation_comptoir ADD historical TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE operation_groupe ADD historical TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE operation_prestataire ADD historical TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE operation_siege ADD historical TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flux DROP historical');
        $this->addSql('ALTER TABLE operation_adherent DROP historical');
        $this->addSql('ALTER TABLE operation_comptoir DROP historical');
        $this->addSql('ALTER TABLE operation_groupe DROP historical');
        $this->addSql('ALTER TABLE operation_prestataire DROP historical');
        $this->addSql('ALTER TABLE operation_siege DROP historical');
    }
}
