<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403104008 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE etat_prestataire (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, slug VARCHAR(150) NOT NULL, content LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_DD6CC57989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE etats_prestataires (etat_prestataire_id INT NOT NULL, prestataire_id INT NOT NULL, INDEX IDX_5D68706574D12AB (etat_prestataire_id), INDEX IDX_5D687065BE3DB2B7 (prestataire_id), PRIMARY KEY(etat_prestataire_id, prestataire_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE etats_prestataires ADD CONSTRAINT FK_5D68706574D12AB FOREIGN KEY (etat_prestataire_id) REFERENCES etat_prestataire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE etats_prestataires ADD CONSTRAINT FK_5D687065BE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE etats_prestataires DROP FOREIGN KEY FK_5D68706574D12AB');
        $this->addSql('DROP TABLE etat_prestataire');
        $this->addSql('DROP TABLE etats_prestataires');
    }
}
