<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220407133236 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'HelloAsso + don migration';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE helloasso (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', errors LONGTEXT DEFAULT NULL, data LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', amount DOUBLE PRECISION NOT NULL, type VARCHAR(255) NOT NULL, state VARCHAR(255) NOT NULL, helloassoid INT NOT NULL, payeremail VARCHAR(255) NOT NULL, payerfirstname VARCHAR(255) NOT NULL, payerlastname VARCHAR(255) NOT NULL, userfirstname VARCHAR(255) NOT NULL, userlastname VARCHAR(255) NOT NULL, useremail VARCHAR(255) NOT NULL, operation_state VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B3CF8E7AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE helloasso ADD CONSTRAINT FK_B3CF8E7AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('ALTER TABLE helloasso CHANGE amount amount DOUBLE PRECISION DEFAULT NULL');

        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'USE_HELLOASSO', 'Utiliser la module de paiement HelloAsso', 'false', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'HELLOASSO_CLIENTID', 'HELLOASSO CLIENT ID', '', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'HELLOASSO_CLIENTSECRET', 'HELLOASSO CLIENT SECRET', '', '1')");

        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'HELLOASSO_URL_EMLC_ADHERENT', 'HELLOASSO : Url de la campagne pour l\'achat de monnaie numérique pour les adhérents', '', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'HELLOASSO_URL_EMLC_PRESTATAIRE', 'HELLOASSO : Url de la campagne pour l\'achat de monnaie numérique pour les prestataires', '', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'HELLOASSO_URL_COTISATION_ADHERENT', 'HELLOASSO : Url de la campagne pour la cotisation d\'un adhérent', '', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'HELLOASSO_URL_COTISATION_PRESTATAIRE', 'HELLOASSO : Url de la campagne pour la cotisation d\'un prestataire', '', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACCEPT_DON_ADHERENT_COTISATION', 'Dons acceptés lors de la cotisation d\'un adhérent ?', 'true', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACCEPT_DON_ADHERENT_ACHAT', 'Dons acceptés lors de l\'achat de e-mlc d\'un adhérent ?', 'true', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACCEPT_DON_PRESTATAIRE_COTISATION', 'Dons acceptés lors de la cotisation d\'un prestataire ?', 'true', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'ACCEPT_DON_PRESTATAIRE_ACHAT', 'Dons acceptés lors de l\'achat de e-mlc d\'un prestataire ?', 'true', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'IBAN_ASSOCIATION', 'IBAN de gestion de l\'Association', 'FRXXXXXXXXXXXXXXXXXXXXXXXXX', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'IBAN_GUARANTY', 'IBAN du fond de garantie', 'FRXXXXXXXXXXXXXXXXXXXXXXXXX', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'CHECK_ADDRESS', 'Paiement par chèque : Envoyer à cette adresse', '', '1')");
        $this->addSql("INSERT IGNORE INTO global_parameter (id, name, description, value, mandatory) VALUES (UUID(), 'CHECK_ORDER', 'Paiement par chèque : Ordre à mettre sur ceux-ci', '', '1')");

        $this->addSql('UPDATE usergroup SET roles = \'a:20:{i:0;s:16:"ROLE_ADMIN_SIEGE";i:1;s:31:"ROLE_SONATA_USER_ADMIN_USER_ALL";i:2;s:32:"ROLE_SONATA_USER_ADMIN_GROUP_ALL";i:3;s:29:"ROLE_ADMIN_ADHERENT_GERER_ALL";i:4;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:5;s:32:"ROLE_ADMIN_ALL_ACHATSMONNAIE_ALL";i:6;s:27:"ROLE_ADMIN_GROUPE_GERER_ALL";i:7;s:36:"ROLE_ADMIN_GLOBALPARAMETER_GERER_ALL";i:8;s:30:"ROLE_ADMIN_TRANSFERT_GERER_ALL";i:9;s:40:"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL";i:10;s:43:"ROLE_ADMIN_OPERATION_PRESTATAIRE_GERER_LIST";i:11;s:40:"ROLE_ADMIN_OPERATION_ADHERENT_GERER_LIST";i:12;s:40:"ROLE_ADMIN_OPERATION_COMPTOIR_GERER_LIST";i:13;s:38:"ROLE_ADMIN_OPERATION_GROUPE_GERER_LIST";i:14;s:37:"ROLE_ADMIN_OPERATION_SIEGE_GERER_LIST";i:15;s:35:"ROLE_ADMIN_ADHERENT_COTISATIONS_ALL";i:16;s:38:"ROLE_ADMIN_PRESTATAIRE_COTISATIONS_ALL";i:17;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:18;s:24:"ROLE_ADMIN_HELLOASSO_ALL";i:19;s:19:"ROLE_ADMIN_DONS_ALL";}\' WHERE name = "Administrateur du Siege"');
        $this->addSql('UPDATE usergroup SET roles = \'a:13:{i:0;s:14:"ROLE_TRESORIER";i:1;s:30:"ROLE_ADMIN_ALL_COTISATIONS_ALL";i:2;s:32:"ROLE_ADMIN_ALL_ACHATSMONNAIE_ALL";i:3;s:33:"ROLE_ADMIN_RECONVERSION_GERER_ALL";i:4;s:30:"ROLE_ADMIN_TRANSFERT_GERER_ALL";i:5;s:40:"ROLE_ADMIN_ALL_DEMANDE_ACHATSMONNAIE_ALL";i:6;s:42:"ROLE_ADMIN_OPERATION_PRESTATAIRE_GERER_ALL";i:7;s:39:"ROLE_ADMIN_OPERATION_ADHERENT_GERER_ALL";i:8;s:39:"ROLE_ADMIN_OPERATION_COMPTOIR_GERER_ALL";i:9;s:37:"ROLE_ADMIN_OPERATION_GROUPE_GERER_ALL";i:10;s:36:"ROLE_ADMIN_OPERATION_SIEGE_GERER_ALL";i:11;s:24:"ROLE_ADMIN_HELLOASSO_ALL";i:12;s:19:"ROLE_ADMIN_DONS_ALL";}\' WHERE name = "Trésorier"');

        $this->addSql('ALTER TABLE helloasso DROP FOREIGN KEY FK_B3CF8E7AA76ED395');
        $this->addSql('ALTER TABLE helloasso ADD prestataire_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD adherent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE helloasso ADD CONSTRAINT FK_BDC98072BE3DB2B7 FOREIGN KEY (prestataire_id) REFERENCES prestataire (id)');
        $this->addSql('ALTER TABLE helloasso ADD CONSTRAINT FK_BDC9807225F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('CREATE INDEX IDX_BDC98072BE3DB2B7 ON helloasso (prestataire_id)');
        $this->addSql('CREATE INDEX IDX_BDC9807225F06C53 ON helloasso (adherent_id)');
        $this->addSql('DROP INDEX idx_b3cf8e7aa76ed395 ON helloasso');
        $this->addSql('CREATE INDEX IDX_BDC98072A76ED395 ON helloasso (user_id)');
        $this->addSql('ALTER TABLE helloasso ADD CONSTRAINT FK_B3CF8E7AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('ALTER TABLE helloasso ADD flux_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE helloasso ADD CONSTRAINT FK_BDC98072C85926E FOREIGN KEY (flux_id) REFERENCES flux (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BDC98072C85926E ON helloasso (flux_id)');

        $this->addSql('ALTER TABLE flux ADD don_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE flux ADD CONSTRAINT FK_7252313A7B3C9061 FOREIGN KEY (don_id) REFERENCES flux (id)');
        $this->addSql('CREATE INDEX IDX_7252313A7B3C9061 ON flux (don_id)');
        $this->addSql('ALTER TABLE helloasso CHANGE userfirstname userfirstname VARCHAR(255) DEFAULT NULL, CHANGE userlastname userlastname VARCHAR(255) DEFAULT NULL, CHANGE useremail useremail VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE helloasso ADD statepayment VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE helloasso ADD paymentReceiptUrl LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE helloasso ADD historical TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE helloasso CHANGE operation_state operation_state VARCHAR(255) DEFAULT NULL, CHANGE paymentReceiptUrl paymentReceiptUrl LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE helloasso');
        $this->addSql('ALTER TABLE flux DROP FOREIGN KEY FK_7252313A7B3C9061');
        $this->addSql('DROP INDEX IDX_7252313A7B3C9061 ON flux');
        $this->addSql('ALTER TABLE flux DROP don_id');
    }
}
