<?php

namespace App\Enum;

abstract class HelloassoStateEnum
{
    const HELLOASSO_STATE_START = 'START';
    const HELLOASSO_STATE_ERROR = 'ERROR';
    const HELLOASSO_STATE_ERROR_NOT_FOUND = 'NOT_FOUND';
    const HELLOASSO_STATE_ERROR_FLUX = 'ERROR FLUX';
    const HELLOASSO_STATE_OK = 'OK';
    const HELLOASSO_STATE_HISTORICAL = 'HISTORICAL';

    /** @var array user friendly named type */
    protected static $typeName = [
        self::HELLOASSO_STATE_START => 'START',
        self::HELLOASSO_STATE_ERROR => 'ERREUR',
        self::HELLOASSO_STATE_ERROR_NOT_FOUND => 'NOT FOUND',
        self::HELLOASSO_STATE_ERROR_FLUX => 'ERROR FLUX',
        self::HELLOASSO_STATE_OK => 'OK',
        self::HELLOASSO_STATE_HISTORICAL, 'HISTORICAL',
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::HELLOASSO_STATE_START,
            self::HELLOASSO_STATE_ERROR,
            self::HELLOASSO_STATE_ERROR_NOT_FOUND,
            self::HELLOASSO_STATE_ERROR_FLUX,
            self::HELLOASSO_STATE_OK,
            self::HELLOASSO_STATE_HISTORICAL,
        ];
    }
}
