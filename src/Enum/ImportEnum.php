<?php

namespace App\Enum;

abstract class ImportEnum
{
    const IMPORT_GROUPE = 'groupe';
    const IMPORT_COMPTOIR = 'comptoir';
    const IMPORT_PRESTATAIRE = 'prestataire';
    const IMPORT_ADHERENT = 'adherent';
    const IMPORT_OPERATION = 'operation';

    const IMPORT_OPERATION_SIEGE = 'siege';
    const IMPORT_OPERATION_GROUPE = 'groupe';
    const IMPORT_OPERATION_COMPTOIR = 'comptoir';
    const IMPORT_OPERATION_PRESTATAIRE = 'prestataire';
    const IMPORT_OPERATION_ADHERENT = 'adherent';

    /** @var array user friendly named type */
    protected static $typeName = [
        self::IMPORT_GROUPE => 'Groupe(s)',
        self::IMPORT_COMPTOIR => 'Comptoir(s)',
        self::IMPORT_PRESTATAIRE => 'Prestataire(s)',
        self::IMPORT_ADHERENT => 'Adherent(s)',
        self::IMPORT_OPERATION => 'Opération(s)',
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::IMPORT_GROUPE,
            self::IMPORT_COMPTOIR,
            self::IMPORT_PRESTATAIRE,
            self::IMPORT_ADHERENT,
            self::IMPORT_OPERATION,
        ];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableOperators()
    {
        return [
            self::IMPORT_OPERATION_SIEGE,
            self::IMPORT_OPERATION_GROUPE,
            self::IMPORT_OPERATION_COMPTOIR,
            self::IMPORT_OPERATION_PRESTATAIRE,
            self::IMPORT_OPERATION_ADHERENT,
        ];
    }
}
