<?php

namespace App\Enum;

use App\Entity\OperationAdherent;
use App\Entity\OperationComptoir;
use App\Entity\OperationGroupe;
use App\Entity\OperationPrestataire;
use App\Entity\OperationSiege;

abstract class OperationEnum
{
    const OPERATION_ADHERENT = 'OPERATION_ADHERENT';
    const OPERATION_PRESTATAIRE = 'OPERATION_PRESTATAIRE';
    const OPERATION_COMPTOIR = 'OPERATION_COMPTOIR';
    const OPERATION_GROUPE = 'OPERATION_GROUPE';
    const OPERATION_SIEGE = 'OPERATION_SIEGE';

    /** @var array user friendly named type */
    protected static $className = [
        self::OPERATION_ADHERENT => OperationAdherent::class,
        self::OPERATION_PRESTATAIRE => OperationPrestataire::class,
        self::OPERATION_COMPTOIR => OperationComptoir::class,
        self::OPERATION_GROUPE => OperationGroupe::class,
        self::OPERATION_SIEGE => OperationSiege::class,
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getClassName($typeShortName)
    {
        if (!isset(static::$className[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$className[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::OPERATION_ADHERENT,
            self::OPERATION_PRESTATAIRE,
            self::OPERATION_COMPTOIR,
            self::OPERATION_GROUPE,
            self::OPERATION_SIEGE,
        ];
    }
}
