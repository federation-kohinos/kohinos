<?php

namespace App\Enum;

abstract class MoyenEnum
{
    const MOYEN_CB = 'cb';
    const MOYEN_ESPECE = 'espece';
    const MOYEN_CHEQUE = 'cheque';
    const MOYEN_VIREMENT = 'virement';
    const MOYEN_HELLOASSO = 'helloasso';
    const MOYEN_EMLC = 'emlc';
    const MOYEN_MLC = 'mlc';
    const MOYEN_AUTRE = 'autre';

    /** @var array user friendly named type */
    protected static $typeName = [
        self::MOYEN_CB => 'Carte bancaire',
        self::MOYEN_ESPECE => 'Espèce',
        self::MOYEN_CHEQUE => 'Chèque',
        self::MOYEN_VIREMENT => 'Virement',
        self::MOYEN_HELLOASSO => 'HelloAsso',
        self::MOYEN_EMLC => 'EMLC',
        self::MOYEN_MLC => 'MLC',
        self::MOYEN_AUTRE => 'Autre',
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * Return available type for Comptoir (Vente MLC ou Vente eMLC).
     *
     * @return array<string>
     */
    public static function getAvailableTypesComptoir()
    {
        return [
            self::MOYEN_ESPECE,
            self::MOYEN_CHEQUE,
            self::MOYEN_AUTRE,
        ];
    }

    /**
     * Return all available type of moyen.
     *
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::MOYEN_CB,
            self::MOYEN_ESPECE,
            self::MOYEN_CHEQUE,
            self::MOYEN_VIREMENT,
            self::MOYEN_HELLOASSO,
            self::MOYEN_EMLC,
            self::MOYEN_MLC,
            self::MOYEN_AUTRE,
        ];
    }
}
