<?php

namespace App\Enum;

abstract class GroupePrestaEnum
{
    const MOYEN_AMAP = 'amap';
    const MOYEN_MARCHE = 'marche';

    /** @var array user friendly named type */
    protected static $typeName = [
        self::MOYEN_AMAP => 'AMAP',
        self::MOYEN_MARCHE => 'Marché',
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::MOYEN_AMAP,
            self::MOYEN_MARCHE,
        ];
    }
}
