<?php

namespace App\Enum;

abstract class CurrencyEnum
{
    const CURRENCY_EMLC = 'emlc';   // Monnaie numérique
    const CURRENCY_MLC = 'mlc';    // Billets
    const CURRENCY_EURO = 'euro';   // Euros
    const CURRENCY_MLC_NANTIE = 'mlc_nantie';   // Billets nantis

    /** @var array user friendly named type */
    protected static $typeName = [
        self::CURRENCY_EMLC => 'Emlc',
        self::CURRENCY_MLC => 'MLC',
        self::CURRENCY_EURO => 'Euro(s)',
        self::CURRENCY_MLC_NANTIE => 'MLC Nantie',
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::CURRENCY_EMLC,
            self::CURRENCY_MLC,
            self::CURRENCY_EURO,
            self::CURRENCY_MLC_NANTIE,
        ];
    }
}
