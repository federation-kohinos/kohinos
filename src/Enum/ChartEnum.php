<?php

namespace App\Enum;

abstract class ChartEnum
{
    const CHART_GROUPEMLC = 'groupeMlc';
    const CHART_COMPTOIRMLC = 'comptoirMlc';
    const CHART_NBUSER = 'nbuser';
    const CHART_NOMBRETRANSAC = 'nombretransac';
    const CHART_VOLUMETRANSAC = 'volumetransac';
    const CHART_FLUX_BY_TYPE = 'fluxbytype';
    const CHART_COUNT_USER = 'nbuser';
    const CHART_COUNT_ADHERENT = 'nbadherent';
    const CHART_COUNT_PRESTATAIRE = 'nbpresta';

    /** @var array user friendly named type */
    protected static $titleChart = [
        self::CHART_GROUPEMLC => 'Balance des groupes locaux',
        self::CHART_COMPTOIRMLC => 'Balance des comptoirs',
        self::CHART_NBUSER => "Nombre d'adhérents et de prestataires",
        self::CHART_NOMBRETRANSAC => 'Montant totaux des transactions',
        self::CHART_VOLUMETRANSAC => 'Volume des transactions',
        self::CHART_FLUX_BY_TYPE => 'Flux',
        self::CHART_COUNT_USER => "Nombre de prestataires et d'adherents",
        self::CHART_COUNT_ADHERENT => "Nombre d'adherents",
        self::CHART_COUNT_PRESTATAIRE => "Nombre de prestataires",
    ];

    /**
     * @param string $titleChartName
     *
     * @return string
     */
    public static function getChartTitle($titleChartName)
    {
        if (!isset(static::$titleChart[$titleChartName])) {
            return "Unknown type ($titleChartName)";
        }

        return static::$titleChart[$titleChartName];
    }

    /**
     * Return all available type of chart.
     *
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::CHART_GROUPEMLC,
            self::CHART_COMPTOIRMLC,
            self::CHART_NBUSER,
            self::CHART_NOMBRETRANSAC,
            self::CHART_VOLUMETRANSAC,
            self::CHART_FLUX_BY_TYPE,
            self::CHART_COUNT_USER,
            self::CHART_COUNT_ADHERENT,
            self::CHART_COUNT_PRESTATAIRE,
        ];
    }
}
