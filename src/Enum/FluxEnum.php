<?php

namespace App\Enum;

use App\Entity\AchatMonnaie;
use App\Entity\AchatMonnaieAdherent;
use App\Entity\AchatMonnaiePrestataire;
use App\Entity\Change;
use App\Entity\ChangeAdherentComptoir;
use App\Entity\ChangePrestataireComptoir;
use App\Entity\Cotisation;
use App\Entity\CotisationAdherent;
use App\Entity\CotisationPrestataire;
use App\Entity\Don;
use App\Entity\DonAdherent;
use App\Entity\DonPrestataire;
use App\Entity\Reconversion;
use App\Entity\Retrait;
use App\Entity\RetraitComptoirAdherent;
use App\Entity\RetraitComptoirPrestataire;
use App\Entity\Transaction;
use App\Entity\TransactionAdherentAdherent;
use App\Entity\TransactionAdherentPrestataire;
use App\Entity\TransactionPrestataireAdherent;
use App\Entity\TransactionPrestatairePrestataire;
use App\Entity\Transfert;
use App\Entity\TransfertComptoirGroupe;
use App\Entity\TransfertGroupeComptoir;
use App\Entity\TransfertGroupeSiege;
use App\Entity\TransfertSiegeGroupe;
use App\Entity\Vente;
use App\Entity\VenteComptoirAdherent;
use App\Entity\VenteComptoirPrestataire;
use App\Entity\VenteEmlc;
use App\Entity\VenteEmlcComptoirAdherent;
use App\Entity\VenteEmlcComptoirPrestataire;

abstract class FluxEnum
{
    const ACHAT_MONNAIE_ADHERENT = AchatMonnaie::TYPE_ACHAT_ADHERENT;
    const ACHAT_MONNAIE_PRESTATAIRE = AchatMonnaie::TYPE_ACHAT_PRESTATAIRE;
    const CHANGE_ADHERENT_COMPTOIR = Change::TYPE_CHANGE_ADHERENT_COMPTOIR;
    const CHANGE_PRESTATAIRE_COMPTOIR = Change::TYPE_CHANGE_PRESTATAIRE_COMPTOIR;
    const COTISATION_ADHERENT = Cotisation::TYPE_COTISATION_ADHERENT;
    const COTISATION_PRESTATAIRE = Cotisation::TYPE_COTISATION_PRESTATAIRE;
    const DON_ADHERENT = Don::TYPE_DON_ADHERENT;
    const DON_PRESTATAIRE = Don::TYPE_DON_PRESTATAIRE;
    const RECONVERSION_PRESTATAIRE = Reconversion::TYPE_RECONVERSION_PRESTATAIRE;
    const RETRAIT_ADHERENT = Retrait::TYPE_RETRAIT_ADHERENT;
    const RETRAIT_PRESTATAIRE = Retrait::TYPE_RETRAIT_PRESTATAIRE;
    const TRANSACTION_ADHERENT_ADHERENT = Transaction::TYPE_TRANSACTION_ADHERENT_ADHERENT;
    const TRANSACTION_ADHERENT_PRESTATAIRE = Transaction::TYPE_TRANSACTION_ADHERENT_PRESTATAIRE;
    const TRANSACTION_PRESTATAIRE_ADHERENT = Transaction::TYPE_TRANSACTION_PRESTATAIRE_ADHERENT;
    const TRANSACTION_PRESTATAIRE_PRESTATAIRE = Transaction::TYPE_TRANSACTION_PRESTATAIRE_PRESTATAIRE;
    const TRANSFERT_COMPTOIR_GROUPE = Transfert::TYPE_TRANSFERT_COMPTOIR_GROUPE;
    const TRANSFERT_GROUPE_COMPTOIR = Transfert::TYPE_TRANSFERT_GROUPE_COMPTOIR;
    const TRANSFERT_GROUPE_SIEGE = Transfert::TYPE_TRANSFERT_GROUPE_SIEGE;
    const TRANSFERT_SIEGE_GROUPE = Transfert::TYPE_TRANSFERT_SIEGE_GROUPE;
    const VENTE_COMPTOIR_ADHERENT = Vente::TYPE_VENTE_ADHERENT;
    const VENTE_COMPTOIR_PRESTATAIRE = Vente::TYPE_VENTE_PRESTATAIRE;
    const VENTE_EMLC_ADHERENT = VenteEmlc::TYPE_VENTE_EMLC_ADHERENT;
    const VENTE_EMLC_PRESTATAIRE = VenteEmlc::TYPE_VENTE_EMLC_PRESTATAIRE;

    /** @var array user friendly named type */
    protected static $typeName = [
        self::ACHAT_MONNAIE_ADHERENT => 'achat monnaie adherent',
        self::ACHAT_MONNAIE_PRESTATAIRE => 'achat monnaie prestataire',
        self::CHANGE_ADHERENT_COMPTOIR => 'adherent comptoir ',
        self::CHANGE_PRESTATAIRE_COMPTOIR => 'prestataire comptoir ',
        self::COTISATION_ADHERENT => 'cotisation adherent ',
        self::COTISATION_PRESTATAIRE => 'cotisation prestataire ',
        self::DON_ADHERENT => 'don adherent',
        self::DON_PRESTATAIRE => 'don prestataire',
        self::RECONVERSION_PRESTATAIRE => 'reconversion prestataire',
        self::RETRAIT_ADHERENT => 'retrait adherent ',
        self::RETRAIT_PRESTATAIRE => 'retrait prestataire ',
        self::TRANSACTION_ADHERENT_ADHERENT => 'adherent adherent ',
        self::TRANSACTION_ADHERENT_PRESTATAIRE => 'adherent prestataire ',
        self::TRANSACTION_PRESTATAIRE_ADHERENT => 'prestataire adherent ',
        self::TRANSACTION_PRESTATAIRE_PRESTATAIRE => 'prestataire prestataire ',
        self::TRANSFERT_COMPTOIR_GROUPE => 'comptoir groupe ',
        self::TRANSFERT_GROUPE_COMPTOIR => 'groupe comptoir ',
        self::TRANSFERT_GROUPE_SIEGE => 'groupe siege ',
        self::TRANSFERT_SIEGE_GROUPE => 'siege groupe ',
        self::VENTE_COMPTOIR_ADHERENT => 'comptoir adherent ',
        self::VENTE_COMPTOIR_PRESTATAIRE => 'comptoir prestataire ',
        self::VENTE_EMLC_ADHERENT => 'emlc adherent',
        self::VENTE_EMLC_PRESTATAIRE => 'emlc prestataire ',
    ];

    /** @var array of classes */
    protected static $className = [
        self::ACHAT_MONNAIE_ADHERENT => AchatMonnaieAdherent::class,
        self::ACHAT_MONNAIE_PRESTATAIRE => AchatMonnaiePrestataire::class,
        self::CHANGE_ADHERENT_COMPTOIR => ChangeAdherentComptoir::class,
        self::CHANGE_PRESTATAIRE_COMPTOIR => ChangePrestataireComptoir::class,
        self::COTISATION_ADHERENT => CotisationAdherent::class,
        self::COTISATION_PRESTATAIRE => CotisationPrestataire::class,
        self::DON_ADHERENT => DonAdherent::class,
        self::DON_PRESTATAIRE => DonPrestataire::class,
        self::RECONVERSION_PRESTATAIRE => Reconversion::class,
        self::RETRAIT_ADHERENT => RetraitComptoirAdherent::class,
        self::RETRAIT_PRESTATAIRE => RetraitComptoirPrestataire::class,
        self::TRANSACTION_ADHERENT_ADHERENT => TransactionAdherentAdherent::class,
        self::TRANSACTION_ADHERENT_PRESTATAIRE => TransactionAdherentPrestataire::class,
        self::TRANSACTION_PRESTATAIRE_ADHERENT => TransactionPrestataireAdherent::class,
        self::TRANSACTION_PRESTATAIRE_PRESTATAIRE => TransactionPrestatairePrestataire::class,
        self::TRANSFERT_COMPTOIR_GROUPE => TransfertComptoirGroupe::class,
        self::TRANSFERT_GROUPE_COMPTOIR => TransfertGroupeComptoir::class,
        self::TRANSFERT_GROUPE_SIEGE => TransfertGroupeSiege::class,
        self::TRANSFERT_SIEGE_GROUPE => TransfertSiegeGroupe::class,
        self::VENTE_COMPTOIR_ADHERENT => VenteComptoirAdherent::class,
        self::VENTE_COMPTOIR_PRESTATAIRE => VenteComptoirPrestataire::class,
        self::VENTE_EMLC_ADHERENT => VenteEmlcComptoirAdherent::class,
        self::VENTE_EMLC_PRESTATAIRE => VenteEmlcComptoirPrestataire::class,
    ];

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getTypeName($typeShortName)
    {
        if (!isset(static::$typeName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @param string $typeShortName
     *
     * @return string
     */
    public static function getClassName($typeShortName)
    {
        if (!isset(static::$className[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$className[$typeShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableTypes()
    {
        return [
            self::ACHAT_MONNAIE_ADHERENT,
            self::ACHAT_MONNAIE_PRESTATAIRE,
            self::CHANGE_ADHERENT_COMPTOIR,
            self::CHANGE_PRESTATAIRE_COMPTOIR,
            self::COTISATION_ADHERENT,
            self::COTISATION_PRESTATAIRE,
            self::DON_ADHERENT,
            self::DON_PRESTATAIRE,
            self::RECONVERSION_PRESTATAIRE,
            self::RETRAIT_ADHERENT,
            self::RETRAIT_PRESTATAIRE,
            self::TRANSACTION_ADHERENT_ADHERENT,
            self::TRANSACTION_ADHERENT_PRESTATAIRE,
            self::TRANSACTION_PRESTATAIRE_ADHERENT,
            self::TRANSACTION_PRESTATAIRE_PRESTATAIRE,
            self::TRANSFERT_COMPTOIR_GROUPE,
            self::TRANSFERT_GROUPE_COMPTOIR,
            self::TRANSFERT_GROUPE_SIEGE,
            self::TRANSFERT_SIEGE_GROUPE,
            self::VENTE_COMPTOIR_ADHERENT,
            self::VENTE_COMPTOIR_PRESTATAIRE,
            self::VENTE_EMLC_ADHERENT,
            self::VENTE_EMLC_PRESTATAIRE,
        ];
    }
}
