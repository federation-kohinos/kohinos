<?php

namespace App\Repository;

use App\Entity\AccountGroupe;
use App\Enum\CurrencyEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountGroupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountGroupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountGroupe[]    findAll()
 * @method AccountGroupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountGroupeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountGroupe::class);
    }

    // /**
    //  * @return AccountGroupe[] Returns an array of AccountGroupe objects
    //  */
    
    public function findAllBalancesByName(?string $currency)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('g.name as name, a.balance as balance')
            ->leftJoin('a.groupe', 'g')
            ->andWhere('a.enabled = :enabled')
            ->andWhere('g.enabled = :enabled')
            ->setParameter('enabled', true)
            ->orderBy('g.name', 'ASC')
            ->setMaxResults(10)
        ;
        if ($currency != null && in_array($currency, CurrencyEnum::getAvailableTypes())) {
            $qb
                ->andWhere('a.currency = :currency')
                ->setParameter('currency', $currency)
            ;
        }
        return $qb
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?Account
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
