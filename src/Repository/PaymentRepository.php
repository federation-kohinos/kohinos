<?php

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Siege|null find($id, $lockMode = null, $lockVersion = null)
 * @method Siege|null findOneBy(array $criteria, array $orderBy = null)
 * @method Siege[]    findAll()
 * @method Siege[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    /**
     * @param int $id User id
     *
     * @return Payment|null Returns user's last payment
     */
    public function getUserLastPayment($id)
    {
        $results = $this->findBy(
            ['clientId' => $id],
            ['id' => 'DESC'],
            1,
            0
        );

        if (empty($results)) {
            return null;
        } else {
            return $results[0];
        }
    }
}
