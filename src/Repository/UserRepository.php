<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findOrderByName()
    {
        $qb = $this->createQueryBuilder('u');

        return $qb
            ->where('u.enabled = :enabled')
            ->addSelect('CASE WHEN (u.lastname IS NULL OR u.lastname = \'\') THEN 1 ELSE 0 END AS HIDDEN nameIsNull')
            ->addSelect('CASE WHEN (u.firstname IS NULL OR u.firstname = \'\') THEN 1 ELSE 0 END AS HIDDEN fnameIsNull')
            ->setParameter('enabled', true)
            ->orderBy('nameIsNull', 'ASC')
            ->addOrderBy('fnameIsNull', 'ASC')
            ->addOrderBy('u.lastname', 'ASC')
            ->addOrderBy('u.firstname', 'ASC')
            ->addOrderBy('u.username', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string $role
     *
     * @return User[] Returns an array of User objects
     */
    public function findByRole($role)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->leftjoin('u.groups', 'g')
            ->leftjoin('u.possiblegroups', 'pg')
            ->where('u.roles LIKE :roles')
            ->orWhere('g.roles LIKE :roles')
            ->orWhere('pg.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $prestataire
     *
     * @return User[] Returns an array of User objects
     */
    public function findByPrestataire($prestataire)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->isMemberOf(':presta', 'u.prestataires'))
            ->setParameter('presta', $prestataire);

        return $qb->getQuery()->getResult();
    }
}
