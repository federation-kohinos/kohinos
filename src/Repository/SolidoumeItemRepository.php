<?php

namespace App\Repository;

use App\Entity\SolidoumeItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SolidoumeItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method SolidoumeItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method SolidoumeItem[]    findAll()
 * @method SolidoumeItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolidoumeItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SolidoumeItem::class);
    }
}
