<?php

namespace App\Repository;

use App\Entity\EtatPrestataire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EtatPrestataire|null find($id, $lockMode = null, $lockVersion = null)
 * @method EtatPrestataire|null findOneBy(array $criteria, array $orderBy = null)
 * @method EtatPrestataire[]    findAll()
 * @method EtatPrestataire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EtatPrestataireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EtatPrestataire::class);
    }

    // /**
    //  * @return EtatPrestataire[] Returns an array of EtatPrestataire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EtatPrestataire
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
