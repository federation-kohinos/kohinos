<?php

namespace App\Repository;

use App\Entity\AccountComptoir;
use App\Entity\Groupe;
use App\Enum\CurrencyEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountComptoir|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountComptoir|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountComptoir[]    findAll()
 * @method AccountComptoir[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountComptoirRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountComptoir::class);
    }

    // /**
    //  * @return AccountComptoir[] Returns an array of AccountComptoir objects
    //  */
    
    public function findAllBalancesByName(?string $currency, ?Groupe $groupe = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('c.name as name, a.balance as balance')
            ->leftJoin('a.comptoir', 'c')
            ->andWhere('a.enabled = :enabled')
            ->andWhere('c.enabled = :enabled')
            ->setParameter('enabled', true)
            ->orderBy('c.name', 'ASC')
            ->setMaxResults(10)
        ;
        if ($currency != null && in_array($currency, CurrencyEnum::getAvailableTypes())) {
            $qb
                ->andWhere('a.currency = :currency')
                ->setParameter('currency', $currency)
            ;
        }
        if ($groupe != null) {
            $qb
                ->andWhere('c.groupe = :groupe')
                ->setParameter('groupe', $groupe)
            ;
        }
        return $qb
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?Account
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
