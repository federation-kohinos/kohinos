<?php

namespace App\Repository;

use App\Entity\Adherent;
use App\Entity\EntityTrait\PersonRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Adherent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adherent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Adherent[]    findAll()
 * @method Adherent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdherentRepository extends ServiceEntityRepository
{
    use PersonRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Adherent::class);
    }

    private function addDefaultFilter(QueryBuilder $qb)
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();
        if ($this->getEntityManager()->getFilters()->isEnabled('enabled_filter')) {
            $this->getEntityManager()->getFilters()->disable('enabled_filter');
        }

        $qb
            ->andWhere('p.enabled = :enabled')
            ->setParameter('enabled', true)
        ;

        return $qb;
    }

    public function findOneByEmail($email)
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->leftjoin('p.user', 'u')
            ->where('p.enabled = :enabled')
            ->andWhere('u.email = :email')
            ->setParameter('enabled', true)
            ->setParameter('email', $email)
            ->getQuery()
            ->getSingleResult()
        ;
    }

    /**
     * Retourne un adhérent unique si un seul est trouvé, null sinon
     *
     * @param array $data Les critères de recherche
     * @return Adherent|null Un adhérent si un seul est trouvé, null si aucun ou plusieurs sont trouvés
     */
    public function findByData(array $data): ?Adherent
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.users', 'u');

        if (isset($data['email'])) {
            $qb =
                $qb
                    ->where('u.email = :email')
                    ->setParameter('email', $data['email'])
                ;
        }
        if (isset($data['name'])) {
            $qb =
                $qb
                    ->where('CONCAT(u.firstname, \' \', u.lastname) LIKE :name')
                    ->orWhere('CONCAT(u.lastname, \' \', u.firstname) LIKE :name')
                    ->setParameter('name', $data['name'] . '%')
                ;
        }

        $results = $qb
            ->getQuery()
            ->getResult()
        ;
        
        // Retourne un résultat unique ou null si plusieurs/aucun résultats
        if (count($results) === 1) {
            return $results[0];
        }
        
        return null;
    }

    public function findsByData(array $data)
    {
        $qb = $this->createQueryBuilder('p');

        $qb = $qb
            ->leftjoin('p.user', 'u')
        ;

        if (isset($data['email'])) {
            $qb =
                $qb
                    ->orWhere('u.email = :email')
                    ->setParameter('email', $data['email'])
                ;
        }
        if (isset($data['name'])) {
            $qb =
                $qb
                    ->orWhere('CONCAT(u.firstname, \' \', u.lastname) LIKE :name')
                    ->orWhere('CONCAT(u.lastname, \' \', u.firstname) LIKE :name')
                    ->setParameter('name', $data['name'] . '%')
                ;
        }

        return $qb
            ->andWhere('p.enabled = :enabled')
            ->setParameter('enabled', true)
            ->orderBy('u.lastname', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOrderByName()
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->leftjoin('p.user', 'u')
            ->addSelect('u')
            ->where('p.enabled = :enabled')
            ->setParameter('enabled', true)
            ->orderBy('u.lastname', 'ASC')
            ->getQuery()
            ->useResultCache(true, 60)
            ->getResult()
        ;
    }

    /**
     * @return Adherent[] Returns an array of Adherent objects
     */
    public function findbyExclude(Adherent $adherent)
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->leftjoin('p.user', 'u')
            ->where('p.id != :presta')
            ->andWhere('p.enabled = :enabled')
            ->setParameter('presta', $adherent->getId())
            ->setParameter('enabled', true)
            ->orderBy('u.lastname', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
