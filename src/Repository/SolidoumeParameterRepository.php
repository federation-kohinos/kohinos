<?php

namespace App\Repository;

use App\Entity\SolidoumeParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SolidoumeParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method SolidoumeParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method SolidoumeParameter[]    findAll()
 * @method SolidoumeParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolidoumeParameterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SolidoumeParameter::class);
    }

    /**
     * @return SolidoumeParameter[] Returns an array of SolidoumeParameter objects
     */
    public function findTheOne()
    {
        return $this->findOneBy(['enabled' => true]);
    }

    public function getValueOf($param)
    {
        $solidoumeParam = $this->findTheOne();
        if (!empty($solidoumeParam)) {
            $action = 'get' . ucwords($param);
            if (is_callable([$solidoumeParam, $action])) {
                return $solidoumeParam->$action();
            }
        }

        return null;
    }
}
