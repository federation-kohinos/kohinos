<?php

namespace App\Repository;

use App\Entity\GlobalParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GlobalParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method GlobalParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method GlobalParameter[]    findAll()
 * @method GlobalParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GlobalParameterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GlobalParameter::class);
    }

    public function findAllByName()
    {
        $results = $this->createQueryBuilder('g')
            ->getQuery()
            ->getArrayResult()
        ;
        $assocArray = array_reduce($results, function ($result, $item) {
            $item = (array) $item;
            $key = $item['name'];
            $value = $item['value'];
            // @TODO : mettre le prefixe 'KOH_' en parametre (conf yaml)
            $result['KOH_' . $key] = $value;

            return $result;
        }, []);

        return $assocArray;
    }

    public function findAllParameters()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function val($name)
    {
        $global = $this->createQueryBuilder('g')
            ->andWhere('g.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $global ? $global->getValue() : '';
    }
}
