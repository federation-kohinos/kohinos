<?php

namespace App\Repository;

use App\Entity\Siege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Siege|null find($id, $lockMode = null, $lockVersion = null)
 * @method Siege|null findOneBy(array $criteria, array $orderBy = null)
 * @method Siege[]    findAll()
 * @method Siege[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiegeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Siege::class);
    }

    public function getTheOne()
    {
        $siege = $this->findBy([])[0];
        if (null === $siege) {
            $sieges = $this->findAll();
            if (count($siege) > 0) {
                return $siege[0]; // Get the first and
            }
        }

        return $siege;
    }
}
