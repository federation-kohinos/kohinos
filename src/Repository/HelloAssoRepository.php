<?php

namespace App\Repository;

use App\Entity\HelloAsso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HelloAsso|null find($id, $lockMode = null, $lockVersion = null)
 * @method HelloAsso|null findOneBy(array $criteria, array $orderBy = null)
 * @method HelloAsso[]    findAll()
 * @method HelloAsso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HelloAssoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HelloAsso::class);
    }
}
