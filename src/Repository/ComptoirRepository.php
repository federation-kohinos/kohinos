<?php

namespace App\Repository;

use App\Entity\Comptoir;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comptoir|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comptoir|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comptoir[]    findAll()
 * @method Comptoir[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComptoirRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comptoir::class);
    }

    /**
     * @return Comptoir[] Returns an array of Comptoir objects
     */
    public function findByGeoloc($lat, $lon, $distance)
    {
        $sqlDistance = '(6373000 * acos(cos(radians(' . $lat . ')) * cos(radians(g.lat)) * cos(radians(g.lon) - radians(' . $lon . ')) + sin(radians(' . $lat . ')) * sin(radians(g.lat))))';
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->leftJoin('p.geoloc', 'g')
            ->andWhere('p.enabled = :enabled')
            ->setParameter('enabled', true)
            ->andWhere('' . $sqlDistance . ' < :distance')
            ->setParameter('distance', $distance)
            ->orderBy('p.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
