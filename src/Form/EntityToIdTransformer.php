<?php

namespace App\Form;

use App\Entity\Siege;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Lazy\LazyUuidFromString;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class EntityToIdTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $class;

    public function __construct(EntityManagerInterface $objectManager, $class)
    {
        $this->objectManager = $objectManager;
        $this->class = $class;
    }

    public function transform($entity)
    {
        if (1 === $entity && Siege::class == $this->class) {
            return $this->objectManager
                ->getRepository($this->class)
                ->getTheOne();
        }
        if (null === $entity) {
            return;
        }
        if (!is_object($entity)) {
            $entityO = $this->objectManager
                            ->getRepository($this->class)
                            ->find($entity);
            if (null === $entityO) {
                throw new TransformationFailedException();
            }

            return $entityO;
        }
        if ($entity instanceof LazyUuidFromString) {
            return $entity->toString();
        }

        return $entity->getId();
    }

    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
        $entity = $this->objectManager
                       ->getRepository($this->class)
                       ->find($id);
        if (null === $entity) {
            throw new TransformationFailedException();
        }

        return $entity;
    }
}
