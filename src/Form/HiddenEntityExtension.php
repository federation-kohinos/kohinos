<?php

namespace App\Form;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HiddenEntityExtension extends AbstractTypeExtension
{
    public static function getExtendedTypes()
    {
        return [HiddenType::class];
    }

    /**
     * Add the entity_class option.
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['em']);
        $resolver->setDefined(['entity_class']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (isset($options['em']) && isset($options['entity_class'])) {
            $builder->addModelTransformer(new EntityToIdTransformer(
                $options['em'],
                $options['entity_class']
            ));
        }
    }
}
