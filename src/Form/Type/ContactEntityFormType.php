<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class ContactEntityFormType extends AbstractType
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du contact / description :',
                'required' => true,
            ])
            ->add('tel', TextType::class, [
                'label' => 'Téléphone :',
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email :',
                'required' => false,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Public ?',
                'required' => false,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formContact';
    }
}
