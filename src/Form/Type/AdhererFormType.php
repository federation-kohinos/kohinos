<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\GlobalParameter;
use App\Entity\Groupe;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class AdhererFormType extends AbstractType
{
    protected $container;
    protected $em;
    protected $security;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em, Security $security)
    {
        $this->container = $container;
        $this->em = $em;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $adherent = $options['data'];
        $builder
            ->add('user', RegistrationFormType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('groupe', ChoiceType::class, [
                'required' => true,
                'label' => 'Groupe local',
                'choices' => $this->container->get('doctrine')->getRepository(Groupe::class)->findBy(['enabled' => true], ['name' => 'ASC']),
                'choice_label' => 'name',
                'choice_value' => 'id',
                'placeholder' => 'Choisir un groupe',
            ])
            ->add('geoloc', GeolocFormType::class, [
                'label' => false,
                'required' => true,
                'with_geoloc' => false,
                'with_latlon' => false,
            ])
            ->add('cotisation', AddCotisationFormType::class, [
                'label' => false,
                'required' => true,
                'mapped' => false,
                'data_class' => null,
            ])
        ;
        if ('true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_PAYZEN)) {
            $builder
                ->add('save', SubmitType::class, ['label' => 'Payer en CB', 'translation_domain' => 'messages'])
            ;
        }
        if ('true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_HELLOASSO) &&
            !empty($this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::HELLOASSO_URL_COTISATION_ADHERENT))) {
            $builder
                ->add('saveHelloAsso', SubmitType::class, [
                    'label' => 'Payer via HelloAsso',
                    'translation_domain' => 'messages',
                    'attr' => [
                        'class' => 'btn-primary btn ml-2',
                    ],
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adherent::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formAdherer';
    }
}
