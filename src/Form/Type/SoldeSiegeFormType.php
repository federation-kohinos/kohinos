<?php

namespace App\Form\Type;

use App\Entity\Siege;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class SoldeSiegeFormType extends AbstractType
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security->getUser())) {
            throw new \Exception('[FORM SOLDE SIEGE EDIT] Opération impossible !');
        }
        $builder
            ->add('compte', MoneyType::class, [
                'label' => 'Solde :',
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Siege::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formSoldeSiege';
    }
}
