<?php

namespace App\Form\Type;

use App\Entity\AchatMonnaiePrestataire;
use App\Entity\DonPrestataire;
use App\Entity\GlobalParameter;
use App\Entity\Prestataire;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AchatMonnaiePrestataireFormType extends AchatMonnaieFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[FORM ACHAT MONNAIE PRESTATAIRE] Opération impossible !');
        }
        $don = null;
        if ('true' == $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::ACCEPT_DON_PRESTATAIRE_ACHAT)) {
            $don = new DonPrestataire();
            $don->setExpediteur($this->session->get('_prestagere'));
            $don->setDestinataire($this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]));
        }
        $builder
            ->add('destinataire', HiddenType::class, [
                'data' => $this->session->get('_prestagere'),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
            ->add('reference', HiddenType::class, [
                'data' => 'Achat monnaie en CB Prestataire',
            ])
        ;
        if (null != $don) {
            $builder
                ->add('don', DonPrestataireFormType::class, [
                    'label' => false,
                    'required' => false,
                    'data' => $don,
                ])
            ;
        }
        if ('true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_HELLOASSO) &&
            !empty($this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::HELLOASSO_URL_EMLC_PRESTATAIRE))) {
            $builder
                ->add('saveHelloAsso', SubmitType::class, [
                    'label' => 'Payer via HelloAsso',
                    'translation_domain' => 'messages',
                    'attr' => [
                        'class' => 'btn-primary btn achatHelloAssoSubmit',
                    ],
                ])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => AchatMonnaiePrestataire::class,
        ]);
    }

    public function getParent()
    {
        return AchatMonnaieFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formAchatMonnaiePrestataire';
    }
}
