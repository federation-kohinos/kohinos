<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\TransactionAdherentAdherent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionAdherentAdherentFormType extends TransactionFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->security->getUser()->getAdherent(),
                'data_class' => null,
                'entity_class' => Adherent::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Adherent::class,
                'choices' => $this->em->getRepository(Adherent::class)->findbyExclude($this->security->getUser()->getAdherent()),
                'attr' => [
                    'class' => 'form-control select2adh',
                ],
                'placeholder' => 'Choisissez un adhérent',
                'required' => true,
                'label' => 'Adherent :',
            ])
            ->add('destinataireType', HiddenType::class, [
                'data' => 'adherent',
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransactionAdherentAdherent::class,
        ]);
    }

    public function getParent()
    {
        return TransactionFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransactionAdherentAdherent';
    }
}
