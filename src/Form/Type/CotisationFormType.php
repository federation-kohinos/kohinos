<?php

namespace App\Form\Type;

use App\Entity\Cotisation;
use App\Entity\GlobalParameter;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class CotisationFormType extends FluxFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('montant', MoneyType::class, [
                'label' => 'Montant',
                'data' => floatval($this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_ADHERENT)),
                'scale' => 2,
                'constraints' => [
                    new Regex(['pattern' => '/[0-9]{1,}(\.[0-9]{1,2})?/']),
                ],
            ])
            ->add('moyen', ChoiceType::class, [
                'required' => true,
                'choices' => MoyenEnum::getAvailableTypes(),
                'choice_label' => function ($choice) {
                    return MoyenEnum::getTypeName($choice);
                },
            ])
            ->add('recu', CheckboxType::class, [
                'label' => 'Paiement bien reçu',
            ])
            ->add('cotisationInfos', CotisationInfosFormType::class, [
                'label' => 'Infos',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cotisation::class,
            'cascade_validation' => true,
        ]);
    }

    // public function getParent()
    // {
    //     return FluxFormType::class;
    // }

    public function getBlockPrefix()
    {
        return 'formCotisation';
    }
}
