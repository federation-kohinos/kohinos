<?php

namespace App\Form\Type;

use App\Entity\Prestataire;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class DonFormType extends FluxFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_CB, //or MOYEN_EMLC si paiement en EMLC
            ])
            ->add('destinataire', HiddenType::class, [
                'data' => $this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
            ->add('montant', MoneyType::class, [
                'data' => 0,
                'label' => "Don à l'association :",
                'required' => false,
                'constraints' => [
                    new GreaterThanOrEqual(['value' => 0]),
                ],
            ])
            ->remove('save')
        ;
    }

    public function getParent()
    {
        return FluxFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formDon';
    }
}
