<?php

namespace App\Form\Type;

use App\Entity\GlobalParameter;
use App\Entity\Prestataire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VenteEmlcPrestataireFormType extends VenteEmlcFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[FORM VENTE EMLC PRESTATAIRE] Opération impossible !');
        }
        $builder
            ->add('destinataire', EntityType::class, [
                'class' => Prestataire::class,
                'choices' => $this->em->getRepository(Prestataire::class)->findForEPaiement(true),
                'placeholder' => 'Prestataire',
                'required' => true,
                'label' => 'Prestataire :',
            ])
            ->add('reference', HiddenType::class, [
                'data' => 'Achat ' . $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::MLC_SYMBOL) . ' Prestataire',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => VenteEmlcComptoirPrestataire::class,
        ]);
    }

    public function getParent()
    {
        return VenteEmlcFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formVenteEmlcPrestataire';
    }
}
