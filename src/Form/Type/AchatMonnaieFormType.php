<?php

namespace App\Form\Type;

use App\Entity\GlobalParameter;
use App\Entity\Siege;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class AchatMonnaieFormType extends FluxFormType
{
    private $defaultMontantSlider = 30;
    private $maxMontantSlider = 1000;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $showManuelAmount = 'true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::ACHAT_MONNAIE_SHOW_MANUEL_AMOUNT);
        $showSliderAmount = 'true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::ACHAT_MONNAIE_SHOW_SLIDER_AMOUNT);
        $showRadioAmount = 'true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::ACHAT_MONNAIE_SHOW_RADIO_AMOUNT);
        $valsRadioAmount = $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::ACHAT_MONNAIE_VAL_RADIO_AMOUNT);
        $canPayByCheckOrTranfert = 'true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::ACHAT_MONNAIE_CAN_PAY_BY_CHECK_OR_TRANFERT);
        $usePayzen = 'true' === $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_PAYZEN);
        $builder
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_CB,
            ])
            ->add('reconverti', HiddenType::class, [
                'data' => false,
                'empty_data' => false,
            ])
            ->add('expediteur', HiddenType::class, [
                'data' => $this->em->getRepository(Siege::class)->getTheOne(),
                'data_class' => null,
                'entity_class' => Siege::class,
                'em' => $this->em,
            ])
        ;

        if ($showManuelAmount) {
            $builder
                ->add('montant', MoneyType::class, [
                    'label' => 'Montant : ',
                    'required' => true,
                    'data' => $this->defaultMontantSlider,
                    'attr' => [
                        'class' => 'achatmonnaie-montant',
                        'style' => 'width: 70%'
                    ]
                    ]);
        }
        if ($showRadioAmount) {
            // Convertir la chaîne de valeurs en tableau de choix
            $values = array_map('trim', explode(',', $valsRadioAmount));
            $choices = [];
            foreach ($values as $value) {
                $choices[$value . ' €'] = (int)$value;
            }

            $builder
                ->add('montantradio', ChoiceType::class, [
                    'choices' => $choices,
                    'required' => false,
                    'placeholder' => false,
                    'data' => $this->defaultMontantSlider,
                    'expanded' => true,
                    'multiple' => false,
                    'label' => 'Sélectionnez un montant :',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'achatmonnaie-montant-radio'
                    ]
                    ]);
        }
        if ($showSliderAmount) {
            $builder
                ->add('montantslider', RangeType::class, [
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'min' => 1,
                        'max' => $this->maxMontantSlider,
                        'data-provide' => 'slider',
                        'data-slider-ticks' => '[1, 50, 100, 150, 200, 250, 500, 750, 1000]',
                        'data-slider-ticks-labels' => '[1, 50, 100, 150, 200, 250, 500, 750, 1000]',
                        'data-slider-ticks-positions' => '[0, 12, 24, 36, 48, 60, 72, 86, 100]',
                        'data-slider-min' => '1',
                        'data-slider-max' => $this->maxMontantSlider,
                        'data-slider-step' => '5',
                        'data-slider-value' => $this->defaultMontantSlider,
                        'style' => 'width:100%;',
                        'class' => 'achatmonnaie-montant-slider',
                    ],
                    'label' => 'Sélectionnez un montant :'
                ]);
        }
        if ($canPayByCheckOrTranfert) {
            $builder
                ->add('payOther', SubmitType::class, [
                    'label' => 'Payer autrement',
                    'translation_domain' => 'messages',
                    'attr' => [
                        'class' => 'btn-secondary',
                    ],
                ])
            ;
        }
        if ($usePayzen) {
            $builder
                ->add('save', SubmitType::class, [
                    'label' => 'Payer en CB',
                    'translation_domain' => 'messages',
                    'attr' => [
                        'class' => 'btn-primary btn achatCBSubmit',
                    ],
                ])
            ;
        } else {
            // delete from form save submittype herited from fluxformtype
            $builder->remove('save');
        }
    }

    public function getParent()
    {
        return FluxFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formAchatMonnaie';
    }
}
