<?php

namespace App\Form\Type;

use App\Entity\DonPrestataire;
use App\Entity\Prestataire;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DonPrestataireFormType extends DonFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[FORM DON PRESTATAIRE] Opération impossible !');
        }
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_prestagere'),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
            ->add('reference', HiddenType::class, [
                'data' => 'Don Prestataire',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => DonPrestataire::class,
        ]);
    }

    public function getParent()
    {
        return DonFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formDonPrestataire';
    }
}
