<?php

namespace App\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstallFormType extends AbstractType
{
    protected $container;
    protected $em;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('config', GlobalConfigurationFormType::class, [
                'label' => 'CONFIGURATIONS GLOBALES :',
                'label_attr' => ['class' => 'font-weight-bold'],
            ])
            ->add('iban', TextType::class, [
                'label' => "IBAN de l'association gérant la MLC :",
            ])
            ->add('website', UrlType::class, [
                'label' => 'Page web Helloasso :',
            ])
            ->add('groupe', FirstGroupeFormType::class, [
                'label' => 'PREMIER GROUPE LOCAL',
                'label_attr' => ['class' => 'font-weight-bold'],
                'help' => 'Au moins un groupe local est requis pour installer le Kohinos !',
            ])
            ->add('comptoir', FirstComptoirFormType::class, [
                'label' => 'PREMIER COMPTOIR',
                'label_attr' => ['class' => 'font-weight-bold'],
                'help' => 'Au moins un comptoir est requis pour installer le Kohinos !',
                'required' => false,
            ])
            ->add('siege', SiegeFormType::class, [
                'label' => 'CONFIGURATION DU SIÈGE :',
                'label_attr' => ['class' => 'font-weight-bold'],
            ])
            ->add('user', RegistrationFormType::class, [
                'label' => "CRÉATION DE L'UTILISATEUR SUPER ADMIN :",
                'label_attr' => ['class' => 'font-weight-bold'],
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Installer le système'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formInstallation';
    }
}
