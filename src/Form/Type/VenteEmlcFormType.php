<?php

namespace App\Form\Type;

use App\Entity\Comptoir;
use App\Entity\VenteEmlc;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class VenteEmlcFormType extends FluxFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[FORM VENTE EMLC] Opération impossible !');
        }
        $builder
            ->add('moyen', ChoiceType::class, [
                'required' => true,
                'choices' => MoyenEnum::getAvailableTypesComptoir(),
                'choice_label' => function ($choice) {
                    return MoyenEnum::getTypeName($choice);
                },
                'expanded' => true,
                'multiple' => false,
                'label' => 'Sélectionnez un moyen de paiement :',
            ])
            ->add('montant', MoneyType::class, [
                'label' => 'Montant',
                'required' => true,
                'scale' => 2,
                'constraints' => [
                    new Regex(['pattern' => '/[0-9]{1,}(\.[0-9]{1,2})?/']),
                ],
            ])
            // ->add('reconverti', HiddenType::class, [
            //     'data' => true,
            //     'empty_data' => true
            // ])
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_comptoirgere'),
                'data_class' => null,
                'entity_class' => Comptoir::class,
                'em' => $this->em,
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider la vente de e-MLC'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => VenteEmlc::class,
        ]);
    }

    public function getParent()
    {
        return FluxFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formVenteEmlc';
    }
}
