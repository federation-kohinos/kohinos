<?php

namespace App\Form\Type;

use App\Entity\Cotisation;
use App\Entity\GlobalParameter;
use App\Enum\MoyenEnum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddCotisationFormType extends AbstractType
{
    protected $em;
    protected $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime();
        $cotisationMinimum = intval($this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_ADHERENT));
        $cotisationDefault = intval($this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_ADHERENT_DEFAULT));
        $freeAmount = 'true' == $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_FREE_AMOUNT);
        $builder
            ->add('reference', HiddenType::class, [
                'required' => true,
                'data' => 'Adhésion ' . $now->format('d/m/Y'),
            ])
            ->add('montant', IntegerType::class, [
                'label' => 'Montant de la cotisation :',
                'data' => $cotisationDefault,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(['value' => $cotisationMinimum]),
                ],
            ])
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_CB,
            ])
        ;
        if ($freeAmount) {
            $builder
                ->add('montant', IntegerType::class, [
                    'label' => 'Montant en prix libre :',
                    'data' => 0,
                    'constraints' => [
                        new GreaterThanOrEqual(['value' => 0]),
                    ],
                ])
            ;
        } else {
            $builder
                ->add('montant', IntegerType::class, [
                    'label' => 'Montant (minimum ' . $cotisationMinimum . '€) :',
                    'data' => $cotisationMinimum,
                    'constraints' => [
                        new NotBlank(),
                        new GreaterThanOrEqual(['value' => $cotisationMinimum]),
                    ],
                ])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cotisation::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formAddCotisation';
    }
}
