<?php

namespace App\Form\Type;

use App\Entity\Groupe;
use App\Entity\Siege;
use App\Entity\TransfertGroupeSiege;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransfertGroupeSiegeFormType extends TransfertFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', EntityType::class, [
                'class' => Groupe::class,
                'choices' => $this->em->getRepository(Groupe::class)->findBy(['enabled' => true], ['name' => 'ASC']),
                'choice_label' => function ($choice) {
                    return (!empty($choice->getName()) ? $choice->getName() : 'Groupe') . ' (' . (!empty($choice->getMlcAccount()) ? $choice->getMlcAccount()->getBalance() : 0) . ')';
                },
                'placeholder' => 'Groupe',
                'required' => true,
                'label' => 'Groupe :',
            ])
            ->add('destinataire', HiddenType::class, [
                'data' => $this->em->getRepository(Siege::class)->getTheOne(),
                'data_class' => null,
                'entity_class' => Siege::class,
                'em' => $this->em,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransfertGroupeSiege::class,
        ]);
    }

    public function getParent()
    {
        return TransfertFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransfertGroupeSiege';
    }
}
