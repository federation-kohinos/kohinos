<?php

namespace App\Form\Type;

use App\Entity\CotisationAdherent;
use App\Entity\User;
use App\Enum\MoyenEnum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class AdherentCotiserFormType extends CotiserFormType
{
    protected $em;
    protected $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime();

        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('Opération impossible ! Utilisateur non connecté !');
        }
        $builder
            ->add('operateur', HiddenType::class, [
                'entity_class' => User::class,
                'em' => $this->em,
                'data_class' => null,
                'data' => $this->security->getUser(),
            ])
            ->add('reference', HiddenType::class, [
                'label' => 'Reference :',
                'required' => true,
                'data' => 'Cotisation ' . $now->format('Y'),
            ])
            ->add('montant', HiddenType::class, [
            ])
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_AUTRE,
            ])
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CotisationAdherent::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formCotiser';
    }
}
