<?php

namespace App\Form\Type;

use App\Entity\Siege;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiegeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', HiddenType::class, [
                'label' => false,
                'data' => 'Siège',
                'required' => true,
            ])
            ->add('compte', NumberType::class, [
                'label' => 'Solde de monnaie papier actuellement au siège :',
                'required' => true,
            ])
            ->add('compteNantie', NumberType::class, [
                'label' => 'Compte de monnaie nantie :',
                'required' => true,
                'help' => '(montant du fond de garantie)',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Siege::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formSiege';
    }
}
