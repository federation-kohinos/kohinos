<?php

namespace App\Form\Type;

use App\Entity\GeolocPrestataire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class GeolocPrestataireFormType extends AbstractType
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $geoloc = null;
        if (!empty($options['data'])) {
            $geoloc = $options['data'];
        }
        $builder
            ->add('name', TextType::class, [
                'label' => "Nom de l'adresse :",
                'required' => true,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Visible sur la carte ?',
                'required' => false,
                'data' => true,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
            ->add('geoloc', GeolocFormType::class, [
                'label' => false,
                'with_latlon' => $options['with_latlon'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // $resolver->setRequired(['with_latlon']);
        $resolver->setDefaults([
            'data_class' => GeolocPrestataire::class,
            'with_latlon' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formGeolocPrestataire';
    }
}
