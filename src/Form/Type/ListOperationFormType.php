<?php

namespace App\Form\Type;

use App\Enum\MoyenEnum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ListOperationFormType extends AbstractType
{
    protected $em;
    protected $security;
    protected $session;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        SessionInterface $session
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyen', ChoiceType::class, [
                'label' => 'Moyen',
                'required' => false,
                'choices' => MoyenEnum::getAvailableTypes(),
                'choice_label' => function ($choice) {
                    return MoyenEnum::getTypeName($choice);
                },
            ])
            ->add('datemin', DateTimeType::class, [
                'label' => 'Date min',
                'required' => false,
                'widget' => 'single_text',
                // 'data' => new \DateTime('first day of this month')
            ])
            ->add('datemax', DateTimeType::class, [
                'label' => 'Date max',
                'required' => false,
                'widget' => 'single_text',
                // 'data' => new \DateTime('now')
            ])
            ->add('submit', SubmitType::class, ['label' => 'Rechercher'])
            ->add('json', SubmitType::class)
            ->add('csv', SubmitType::class)
            ->add('xls', SubmitType::class)
            ->add('xml', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formListOperations';
    }
}
