<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\Prestataire;
use App\Entity\TransactionAdherentPrestataire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionAdherentPrestataireFormType extends TransactionFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->security->getUser()->getAdherent(),
                'data_class' => null,
                'entity_class' => Adherent::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Prestataire::class,
                'choices' => $this->em->getRepository(Prestataire::class)->findForEPaiement(true),
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'placeholder' => 'Choisissez un prestataire',
                'required' => true,
                'label' => 'Prestataire :',
            ])
            ->add('destinataireType', HiddenType::class, [
                'data' => 'prestataire',
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransactionAdherentPrestataire::class,
        ]);
    }

    public function getParent()
    {
        return TransactionFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransactionAdherentPrestataire';
    }
}
