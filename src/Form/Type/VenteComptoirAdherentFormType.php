<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\Comptoir;
use App\Entity\VenteComptoirAdherent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VenteComptoirAdherentFormType extends VenteFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_comptoirgere'),
                'data_class' => null,
                'entity_class' => Comptoir::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Adherent::class,
                'choices' => $this->em->getRepository(Adherent::class)->findOrderByName(),
                'placeholder' => 'Adherent',
                'required' => true,
                'label' => 'Adherent :',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => VenteComptoirAdherent::class,
        ]);
    }

    public function getParent()
    {
        return VenteFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formVenteComptoirAdherent';
    }
}
