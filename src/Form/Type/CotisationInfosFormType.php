<?php

namespace App\Form\Type;

use App\Entity\CotisationInfos;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class CotisationInfosFormType extends FluxFormType
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime();
        $builder
            ->add('annee', TextType::class, [
                'label' => 'Année',
                'data' => $now->format('Y'),
            ])
            ->add('debut', DateType::class, [
                'label' => 'Date de début',
                'data' => new \DateTime(),
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('fin', DateType::class, [
                'label' => 'Date de fin',
                'data' => new \DateTime('+ 1 year'),
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
            ]);
        if (null != $this->security->getUser() && $this->security->isGranted('ROLE_TRESORIER')) {
            $builder
                    ->add('recu', CheckboxType::class, ['label' => 'Paiement bien reçu']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CotisationInfos::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formCotisationInfos';
    }
}
