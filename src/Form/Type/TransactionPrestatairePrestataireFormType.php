<?php

namespace App\Form\Type;

use App\Entity\Prestataire;
use App\Entity\TransactionPrestatairePrestataire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionPrestatairePrestataireFormType extends TransactionFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_prestagere'),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Prestataire::class,
                'choices' => $this->em->getRepository(Prestataire::class)->findbyExclude($this->session->get('_prestagere')),
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'placeholder' => 'Choisissez un prestataire',
                'required' => true,
                'label' => 'Prestataire :',
            ])
            ->add('destinataireType', HiddenType::class, [
                'data' => 'prestataire',
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransactionPrestatairePrestataire::class,
        ]);
    }

    public function getParent()
    {
        return TransactionFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransactionPrestatairePrestataire';
    }
}
