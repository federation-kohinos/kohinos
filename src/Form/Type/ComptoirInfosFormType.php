<?php

namespace App\Form\Type;

use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\Comptoir;
use Doctrine\ORM\EntityManagerInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComptoirInfosFormType extends AbstractType
{
    protected $container;
    protected $em;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $comptoir = $options['data'];
        if (null != $comptoir->getMedia()) {
            $comptoirMedia = $this->em->getRepository(Media::class)->findOneById($comptoir->getMedia()->getId());
        }
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom :',
                'required' => true,
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Description :',
                'required' => false,
            ])
            ->add('horaires', CKEditorType::class, [
                'label' => 'Horaires :',
                'required' => false,
            ])
            ->add('media', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context' => 'comptoir',
                'label' => 'Image',
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider les modifications'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comptoir::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formComptoirInfos';
    }
}
