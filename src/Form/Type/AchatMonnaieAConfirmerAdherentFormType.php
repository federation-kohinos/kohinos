<?php

namespace App\Form\Type;

use App\Entity\AchatMonnaieAConfirmerAdherent;
use App\Entity\Adherent;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AchatMonnaieAConfirmerAdherentFormType extends AchatMonnaieAConfirmerFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[DEMANDE ACHAT MONNAIE ADHERENT] Opération impossible !');
        }

        $builder
            ->add('destinataire', HiddenType::class, [
                'data' => $this->security->getUser()->getAdherent(),
                'data_class' => null,
                'entity_class' => Adherent::class,
                'em' => $this->em,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => AchatMonnaieAConfirmerAdherent::class,
        ]);
    }

    public function getParent()
    {
        return AchatMonnaieAConfirmerFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formAchatMonnaieAConfirmerAdherent';
    }
}
