<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\GlobalParameter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VenteEmlcAdherentFormType extends VenteEmlcFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[FORM VENTE EMLC ADHERENT] Opération impossible !');
        }
        $builder
            ->add('destinataire', EntityType::class, [
                'class' => Adherent::class,
                'choices' => $this->em->getRepository(Adherent::class)->findOrderByName(),
                'placeholder' => 'Adherent',
                'required' => true,
                'label' => 'Adherent :',
            ])
            ->add('reference', HiddenType::class, [
                'data' => 'Achat e' . $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::MLC_SYMBOL) . ' Adhérent',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => VenteEmlcComptoirAdherent::class,
        ]);
    }

    public function getParent()
    {
        return VenteEmlcFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formVenteEmlcAdherent';
    }
}
