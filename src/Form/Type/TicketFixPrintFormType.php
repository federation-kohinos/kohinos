<?php

namespace App\Form\Type;

use App\Entity\Siege;
use App\Entity\TicketFixPrint;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketFixPrintFormType extends TicketFixFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->em->getRepository(Siege::class)->getTheOne(),
                'data_class' => null,
                'entity_class' => Siege::class,
                'em' => $this->em,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TicketFixPrint::class,
        ]);
    }

    public function getParent()
    {
        return TicketFixFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTicketFixPrint';
    }
}
