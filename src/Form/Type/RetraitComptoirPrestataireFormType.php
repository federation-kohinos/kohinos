<?php

namespace App\Form\Type;

use App\Entity\Comptoir;
use App\Entity\Prestataire;
use App\Entity\RetraitComptoirPrestataire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RetraitComptoirPrestataireFormType extends RetraitFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_comptoirgere'),
                'data_class' => null,
                'entity_class' => Comptoir::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Prestataire::class,
                'choices' => $this->em->getRepository(Prestataire::class)->findForEPaiement(false),
                'placeholder' => 'Prestataire',
                'required' => true,
                'label' => 'Prestataire :',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => RetraitComptoirPrestataire::class,
        ]);
    }

    public function getParent()
    {
        return RetraitFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formRetraitComptoirPrestataire';
    }
}
