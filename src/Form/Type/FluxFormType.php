<?php

namespace App\Form\Type;

use App\Entity\Flux;
use App\Entity\User;
use App\Enum\MoyenEnum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\Regex;

class FluxFormType extends AbstractType
{
    protected $em;
    protected $security;
    protected $container;
    protected $session;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        ContainerInterface $container,
        SessionInterface $session
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->container = $container;
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $flux = $options['data'];
        if (empty($this->security->getUser()) || empty($this->security->getUser()->getId())) {
            throw new \Exception("[FLUX] Opération impossible ! Pas d'utilisateur connecté !");
        }
        if (empty($flux)) {
            throw new \Exception('[FLUX] Opération impossible ! Pas de flux paramétré !');
        }
        // @TODO : voir pourquoi ce code est commenté !
        // if (empty($flux) || empty($flux->getExpediteur())) {
        //     throw new \Exception("[FLUX] Opération impossible ! Pas d'expediteur paramétré !");
        // }
        $builder
            ->add('operateur', HiddenType::class, [
                'data' => $this->security->getUser(),
                'data_class' => null,
                'entity_class' => User::class,
                'em' => $this->em,
            ])
            ->add('role', HiddenType::class, [
                'data' => $this->security->getUser()->getGroups()[0]->__toString(),
            ])
        ;
        if (empty($flux->getExpediteur())) {
            $builder
                ->add('montant', MoneyType::class, [
                    'label' => 'Montant :',
                    'required' => true,
                    'scale' => 2,
                    'constraints' => [
                        new Regex(['pattern' => '/[0-9]{1,}(\.[0-9]{1,2})?/']),
                    ],
                ])
            ;
        } else {
            $builder
                ->add('montant', MoneyType::class, [
                    'label' => 'Montant :',
                    'required' => true,
                    'currency' => 'EUR',
                ])
            ;
        }
        $builder
            ->add('reference', TextType::class, [
                'label' => 'Reference :',
                'required' => true,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Valider',
                'attr' => [
                    'class' => 'btn-primary btn fluxSubmit',
                ],
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                $flux = $event->getData();
                if ($event->getForm()->isValid() && (!$flux || null === $flux->getId()) && Flux::TYPE_COTISATION == $flux->getParenttype() && MoyenEnum::MOYEN_MLC == $flux->getMoyen()) {
                    if ($flux->getExpediteur()->getEcompte() < $flux->getMontant()) {
                        $event->getForm()->get('montant')->addError(new FormError('Montant supérieur à votre solde !'));
                    }
                }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => ['class' => 'flux_form'],
            'data_class' => Flux::class,
            'cascade_validation' => true,
            'csrf_protection' => true,
            'csrf_field_name' => '_token_kohinos',
            'csrf_token_id' => 'flux_form',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formFlux';
    }
}
