<?php

namespace App\Form\Type;

use App\Entity\ChangePrestataireComptoir;
use App\Entity\Comptoir;
use App\Entity\Prestataire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePrestataireComptoirFormType extends ChangeFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('destinataire', HiddenType::class, [
                'data' => $this->session->get('_comptoirgere'),
                'data_class' => null,
                'entity_class' => Comptoir::class,
                'em' => $this->em,
            ])
            ->add('expediteur', EntityType::class, [
                'class' => Prestataire::class,
                'choices' => $this->em->getRepository(Prestataire::class)->findForEPaiement(false),
                'placeholder' => 'Prestataire',
                'required' => true,
                'label' => 'Prestataire :',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => ChangePrestataireComptoir::class,
        ]);
    }

    public function getParent()
    {
        return ChangeFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formChangePrestataireComptoir';
    }
}
