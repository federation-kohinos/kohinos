<?php

namespace App\Form\Type;

use App\Entity\Comptoir;
use App\Entity\Groupe;
use App\Entity\TransfertComptoirGroupe;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransfertComptoirGroupeFormType extends TransfertFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', EntityType::class, [
                'choices' => $this->em->getRepository(Comptoir::class)->findBy(['enabled' => true, 'groupe' => $this->session->get('_groupegere')], ['name' => 'ASC']),
                'class' => Comptoir::class,
                'placeholder' => 'Comptoir',
                'required' => true,
                'label' => 'Comptoir :',
            ])
            ->add('destinataire', HiddenType::class, [
                'entity_class' => Groupe::class,
                'data' => $this->session->get('_groupegere'),
                'data_class' => null,
                'em' => $this->em,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransfertComptoirGroupe::class,
        ]);
    }

    public function getParent()
    {
        return TransfertFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransfertComptoirGroupe';
    }
}
