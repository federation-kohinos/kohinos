<?php

namespace App\Form\Type;

use App\Entity\AccountAdherent;
use App\Entity\AccountPrestataire;
use App\Entity\Adherent;
use App\Entity\GlobalParameter;
use App\Entity\Prestataire;
use App\Entity\Reconversion;
use App\Entity\Siege;
use App\Enum\CurrencyEnum;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class ReconversionFormType extends FluxFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $flux = $options['data'];
        $currency = $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::MLC_SYMBOL);
        if ($flux->getExpediteur() instanceof Prestataire) {
            $account = $this->em->getRepository(AccountPrestataire::class)->findOneBy(['prestataire' => $flux->getExpediteur(), 'currency' => CurrencyEnum::CURRENCY_EMLC]);
        } elseif ($flux->getExpediteur() instanceof Adherent) {
            $account = $this->em->getRepository(AccountAdherent::class)->findOneBy(['adherent' => $flux->getExpediteur(), 'currency' => CurrencyEnum::CURRENCY_EMLC]);
        }
        $builder
            ->add('montant', MoneyType::class, [
                'label' => 'Montant :',
                'required' => true,
                'currency' => $currency ? $currency : 'MLC',
                'constraints' => [
                    new GreaterThanOrEqual([
                        'message' => 'Montant inférieur à zéro !',
                        'value' => 0,
                    ]),
                    new LessThanOrEqual([
                        'message' => 'Montant supérieur à votre solde !',
                        'value' => $account->getBalance(),
                    ]),
                ],
            ])
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_AUTRE,
            ])
            ->add('tauxreconversion', HiddenType::class, [
                'data' => (!empty($this->session->get('_prestagere')->getTauxreconversion()) ? $this->session->get('_prestagere')->getTauxreconversion() : $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::RECONVERSION_PRESTATAIRE)),
            ])
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_prestagere'),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
            ->add('destinataire', HiddenType::class, [
                'data' => $this->em->getRepository(Siege::class)->getTheOne(),
                'data_class' => null,
                'entity_class' => Siege::class,
                'em' => $this->em,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Reconversion::class,
        ]);
    }

    public function getParent()
    {
        return FluxFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formReconversion';
    }
}
