<?php

namespace App\Form\Type;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Votre nom :',
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'Votre email :',
                'required' => true,
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Votre message :',
                'required' => true,
            ])
            ->add('captcha', CaptchaType::class)
            ->add('save', SubmitType::class, ['label' => 'Envoyer'])
        ;
    }

    public function getBlockPrefix()
    {
        return 'formContact';
    }
}
