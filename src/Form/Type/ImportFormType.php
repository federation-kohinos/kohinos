<?php

namespace App\Form\Type;

use App\Entity\Import;
use App\Entity\User;
use App\Enum\ImportEnum;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ImportFormType extends AbstractType
{
    protected $em;
    protected $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('Opération impossible ! Utilisateur non connecté !');
        }
        $builder
            ->add('media', MediaType::class, [
                'provider' => 'sonata.media.provider.file',
                'context' => 'import',
                'label' => 'Fichier .csv',
                'show_unlink' => false,
                'required' => true,
            ])
            ->add('user', HiddenType::class, [
                'data_class' => null,
                'data' => $this->security->getUser(),
                'entity_class' => User::class,
                'em' => $this->em,
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Choisir le type de données à importer : ',
                'placeholder' => '',
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'choices' => ImportEnum::getAvailableTypes(),
                'choice_label' => function ($choice) {
                    return ImportEnum::getTypeName($choice);
                },
            ])
            ->add('sendemail', CheckboxType::class, [
                'label' => 'Envoyer un email aux nouveaux comptes créés ?',
                'required' => false,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
            ->add('test', CheckboxType::class, [
                'label' => "Lancer la procédure d'import pour tester (aucun enregistrement en base de données ou mail envoyé) ?",
                'required' => false,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
            ->add('save', SubmitType::class, ['label' => 'Importer les données'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Import::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formImport';
    }
}
