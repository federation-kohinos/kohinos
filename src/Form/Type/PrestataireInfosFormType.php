<?php

namespace App\Form\Type;

use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Entity\Prestataire;
use Doctrine\ORM\EntityManagerInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrestataireInfosFormType extends AbstractType
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $presta = $options['data'];
        if (null != $presta->getMedia()) {
            $prestaMedia = $this->em->getRepository(Media::class)->findOneById($presta->getMedia()->getId());
        }
        $caissiers = '';
        if (count($presta->getCaissiers()) > 0) {
            foreach ($presta->getCaissiers() as $caissier) {
                if ('' !== $caissiers) {
                    $caissiers .= ';';
                }
                $caissiers .= $caissier->getEmail();
            }
        }
        $builder
            ->add('media', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context' => 'prestataire',
                'label' => 'Image',
            ])
            ->add('raison', TextType::class, [
                'label' => 'Raison :',
                'required' => true,
            ])
            ->add('statut', TextType::class, [
                'label' => 'Activité :',
                'required' => false,
            ])
            ->add('siret', TextType::class, [
                'label' => 'SIRET :',
                'required' => false,
            ])
            ->add('iban', TextType::class, [
                'label' => 'IBAN :',
                'required' => false,
            ])
            ->add('responsable', TextType::class, [
                'label' => 'Responsable :',
                'required' => false,
            ])
            ->add('metier', TextType::class, [
                'label' => 'Métier responsable :',
                'required' => false,
            ])
            ->add('horaires', TextType::class, [
                'label' => 'Horaires :',
                'required' => false,
            ])
            ->add('web', UrlType::class, [
                'label' => 'Site web :',
                'required' => false,
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Description :',
                'required' => false,
            ])
            ->add('acceptemlc', CheckboxType::class, [
                'label' => 'Accepter la monnaie locale électronique',
                'required' => false,
            ])
            ->add('newcaissiers', TextType::class, [
                'mapped' => false,
                'required' => false,
                'data' => $caissiers,
                'label' => 'Caissier(s) :',
                'help' => 'Ajouter par email séparé par un point virgule (email1;email2;...)',
            ])
            ->add('geolocs', CollectionType::class, [
                'entry_type' => GeolocPrestataireFormType::class,
                'entry_options' => ['label' => false, 'with_latlon' => false],
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider les modifications'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prestataire::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formPrestataireInfos';
    }
}
