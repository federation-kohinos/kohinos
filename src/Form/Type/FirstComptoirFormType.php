<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FirstComptoirFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enabled', HiddenType::class, ['data' => true])
            ->add('name', TextType::class, [
                'label' => 'Nom :',
                'required' => false,
                'help' => "Vous pourrez éditer la description dans l'interface d'administration",
            ])
            ->add('geoloc', GeolocFormType::class, [
                'label' => 'Géolocalisation :',
                'attr' => ['class' => 'ml-4'],
                'required' => false,
            ])
            ->add('compte', NumberType::class, [
                'label' => 'Solde de monnaie papier actuellement au comptoir :',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'validation_groups' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formFirstComptoir';
    }
}
