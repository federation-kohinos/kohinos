<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\Groupe;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class AdherentInfosFormType extends AbstractType
{
    protected $container;
    protected $em;
    protected $security;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em, Security $security)
    {
        $this->container = $container;
        $this->em = $em;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', UserInfosFormType::class, [
                'label' => false,
            ])
            ->add('geoloc', GeolocFormType::class, [
                'label' => false,
                'required' => true,
                'with_geoloc' => false,
                'with_latlon' => false,
            ])
            // ->add('groupe', ChoiceType::class, array(
            //     'required' => true,
            //     'disabled' => true,
            //     'label' => 'GROUPE LOCAL',
            //     'choices' => $this->container->get('doctrine')->getRepository(Groupe::class)->findAll(),
            //     'choice_label' => 'name',
            //     'placeholder' => 'Choisir un groupe',
            // ))
            ->add('save', SubmitType::class, ['label' => 'Modifier'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adherent::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formAdherentInfos';
    }
}
