<?php

namespace App\Form\Type;

use App\Entity\Prestataire;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class TicketFixFormType extends TicketFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_AUTRE,
            ])
            ->add('destinataire', HiddenType::class, [
                'data' => $this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
        ;
    }

    public function getParent()
    {
        return TicketFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTicketFix';
    }
}
