<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\ChangeAdherentComptoir;
use App\Entity\Comptoir;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeAdherentComptoirFormType extends ChangeFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('destinataire', HiddenType::class, [
                'data' => $this->session->get('_comptoirgere'),
                'data_class' => null,
                'entity_class' => Comptoir::class,
                'em' => $this->em,
            ])
            ->add('expediteur', EntityType::class, [
                'class' => Adherent::class,
                'choices' => $this->em->getRepository(Adherent::class)->findOrderByName(),
                'placeholder' => 'Adherent',
                'required' => true,
                'label' => 'Adherent :',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => ChangeAdherentComptoir::class,
        ]);
    }

    public function getParent()
    {
        return ChangeFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formChangeAdherentComptoir';
    }
}
