<?php

namespace App\Form\Type;

use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class TransfertFormType extends TicketFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyen', HiddenType::class, [
                'data' => MoyenEnum::MOYEN_MLC,
            ])
        ;
    }

    public function getParent()
    {
        return TicketFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransfert';
    }
}
