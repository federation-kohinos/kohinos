<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\Prestataire;
use App\Entity\TransactionPrestataireAdherent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionPrestataireAdherentFormType extends TransactionFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_prestagere'),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Adherent::class,
                'choices' => $this->em->getRepository(Adherent::class)->findOrderByName(),
                'attr' => [
                    'class' => 'form-control select2',
                ],
                'placeholder' => 'Choisissez un adhérent',
                'required' => true,
                'label' => 'Adherent :',
            ])
            ->add('destinataireType', HiddenType::class, [
                'data' => 'adherent',
                'mapped' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransactionPrestataireAdherent::class,
        ]);
    }

    public function getParent()
    {
        return TransactionFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransactionPrestataireAdherent';
    }
}
