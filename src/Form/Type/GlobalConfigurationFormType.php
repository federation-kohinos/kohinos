<?php

namespace App\Form\Type;

use App\Entity\GlobalParameter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\Regex as RegexConstraint;
use Symfony\Component\Validator\Constraints\Type as TypeConstraint;

class GlobalConfigurationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mlcurl', GlobalParameterType::class, [
                'label' => "URL complète d'accès au Kohinos :",
                '_description' => "URL complète d'accès au Kohinos",
                'name_param' => GlobalParameter::MLC_URL,
                '_placeholder' => 'https://kohinos.xxx.fr',
                'required' => true,
            ])
            ->add('mlcname', GlobalParameterType::class, [
                'label' => 'Nom de la Monnaie (long) :',
                '_description' => 'Nom (long) de la MLC',
                'name_param' => GlobalParameter::MLC_NAME,
                '_placeholder' => 'Monnaie Locale Complémentaire MoLoCo',
                'required' => true,
            ])
            ->add('mlcsmallname', GlobalParameterType::class, [
                'label' => 'Nom court de la Monnaie ou abréviation :',
                '_description' => 'Nom raccourci de la MLC',
                'name_param' => GlobalParameter::MLC_NAME_SMALL,
                '_placeholder' => 'MoLoCo',
                'required' => true,
            ])
            ->add('mlcsymbol', GlobalParameterType::class, [
                'label' => 'Symbole / devise de la monnaie locale (3 caractères maximum) :',
                '_description' => 'Symbole / Devise de la MLC',
                'name_param' => GlobalParameter::MLC_SYMBOL,
                '_placeholder' => 'MLC',
                'required' => true,
            ])
            ->add('mlcnotifemail', GlobalParameterType::class, [
                'label' => "Email utilisé pour l'envoi des notifications :",
                '_description' => "Email d'envoi des notifications",
                'name_param' => GlobalParameter::MLC_NOTIF_EMAIL,
                'required' => true,
                '_placeholder' => 'notification@votredomaine.fr',
                'constraints_param' => [
                    new EmailConstraint(['message' => 'Email invalide !']),
                ],
            ])
            ->add('mlccontactemail', GlobalParameterType::class, [
                'label' => 'Email de reception des demandes de contact :',
                '_description' => 'Email de réception du formulaire de contact',
                'name_param' => GlobalParameter::MLC_CONTACT_EMAIL,
                'required' => true,
                '_placeholder' => 'contact@votredomaine.fr',
                'constraints_param' => [
                    new EmailConstraint(['message' => 'Email invalide !']),
                ],
            ])
            ->add('mlccotisationadh', GlobalParameterType::class, [
                'label' => 'Montant minimum des cotisations des adhérents :',
                '_description' => 'Montant minimum des cotisations des adhérents',
                'name_param' => GlobalParameter::COTISATION_ADHERENT,
                'help' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
                'required' => true,
                '_placeholder' => '5',
                'constraints_param' => [
                    new TypeConstraint(['type' => 'numeric', 'message' => "Le montant n'est pas valide !"]),
                ],
            ])
            ->add('mlccotisationadhdefault', GlobalParameterType::class, [
                'label' => 'Montant affiché par défaut de la cotisation des adhérents :',
                '_description' => 'Montant affiché par défaut de la cotisation des adhérents',
                'name_param' => GlobalParameter::COTISATION_ADHERENT_DEFAULT,
                'help' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
                'required' => true,
                '_placeholder' => '10',
                'constraints_param' => [
                    new TypeConstraint(['type' => 'numeric', 'message' => "Le montant n'est pas valide !"]),
                ],
            ])
            ->add('mlccotisationfreeamount', GlobalParameterType::class, [
                'label' => 'Montant libre ? :',
                '_description' => 'Est-ce que la cotisation est un montant libre ?',
                'name_param' => GlobalParameter::COTISATION_FREE_AMOUNT,
                'help' => 'Est-ce que la cotisation est un montant libre ?',
                'required' => true,
                '_placeholder' => 'false',
                '_data' => 'false',
            ])
            ->add('mlccotisationpresta', GlobalParameterType::class, [
                'label' => 'Montant minimum des cotisations des prestataires :',
                '_description' => 'Montant minimum des cotisations des prestataires :',
                'name_param' => GlobalParameter::COTISATION_PRESTATAIRE,
                'help' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
                'required' => true,
                '_placeholder' => '10',
                'constraints_param' => [
                    new TypeConstraint(['type' => 'numeric', 'message' => "Le montant n'est pas valide !"]),
                ],
            ])
            ->add('mlcreconversionpresta', GlobalParameterType::class, [
                'label' => 'Taux de reconversion des prestataires (Pour 2%, il faut taper "2" et pas 0.02 ou 2%) :',
                '_description' => 'Taux de reconversion des prestataires',
                'name_param' => GlobalParameter::RECONVERSION_PRESTATAIRE,
                'help' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
                'required' => true,
                '_placeholder' => '0',
                'constraints_param' => [
                    new TypeConstraint(['type' => 'numeric', 'message' => "Le montant n'est pas valide !"]),
                ],
            ])
            ->add('mapcenter', GlobalParameterType::class, [
                'label' => 'Coordonnées du centre de la carte (Format [lat,long], exemple : [45.7,3.2]):',
                '_description' => 'Centre de la carte (Coordonnées GPS inscrites comme ceci (sans les guillemets !) : \'[45.7,3.2]\'',
                'name_param' => GlobalParameter::MAP_CENTER,
                'required' => true,
                '_placeholder' => '[45.7,3.2]',
                'constraints_param' => [
                    new RegexConstraint(['pattern' => '/^\[-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+\]/', 'message' => 'Coordonnées invalide, format attendu : [45.7,3.2]']),
                ],
                'help_html' => true,
                'help' => "Pour déterminer le centre et zoom de la carte <a target='_blank' rel='noopener noreferrer' href='https://www.openstreetmap.org/'>Ouvrir OpenStreetMap</a><br/>
                            Exemple : <em>https://www.openstreetmap.org/search?query=clermont#map=12/45.7811/3.0927</em><br/>
                            Zoom de la carte   : 12<br/>
                            Centre de la carte : [45.7811,3.0927]",
            ])
            ->add('mapzoom', GlobalParameterType::class, [
                'label' => 'Zoom de la carte (entre 8 et 12 en général) :',
                '_description' => 'Zoom de la carte (entre 8 et 12 en général)',
                'name_param' => GlobalParameter::MAP_ZOOM,
                'required' => true,
                '_placeholder' => '9',
                'constraints_param' => [
                    new RegexConstraint(['pattern' => '/^[1-9]$|^1[0-9]$/', 'message' => 'Le zoom doit être un nombre entre 1 et 19, généralement entre 9 et 15 pour les MLC !']),
                ],
            ])
            // @TODO : checkbox au lieu de texttype pour certains paramètres globaux
            ->add('usewordpress', GlobalParameterType::class, [
                '_description' => 'Utiliser le module Wordpress et désactiver les fonctions du site kohinos (ne pas cocher, pas encore développé)',
                'label' => 'Utiliser Wordpress pour le site web (pas encore développé)',
                'name_param' => GlobalParameter::USE_WORDPRESS,
                '_placeholder' => 'false',
                '_data' => 'false',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser Wordpress pour le site web (pas encore développé)',
                //     'required' => false,
                //     // 'disabled' => true,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", le site web public du Kohinos sera désactivé, seul la page de connexion et l\'administration seront accesssible !',
            ])
            ->add('usepayzen', GlobalParameterType::class, [
                '_description' => 'Utiliser la module de paiement Payzen',
                'name_param' => GlobalParameter::USE_PAYZEN,
                'label' => 'Utiliser le paiement par CB avec Payzen',
                '_placeholder' => 'false',
                '_data' => 'false',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les utilisateurs pourront cotiser et acheter de la monnaie locale par CB sur l\'application  (compte Payzen activé obligatoire).',
            ])
            ->add('usesolidoume', GlobalParameterType::class, [
                'required' => true,
                '_description' => 'Activer la sécurité sociale alimentaire',
                'name_param' => GlobalParameter::USE_SOLIDOUME,
                'label' => 'Utiliser la sécurité sociale alimentaire',
                '_placeholder' => 'false',
                '_data' => 'false',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les utilisateurs pourront accéder et participer au programme de Sécurité Sociale alimentaire',
            ])
            ->add('usehelloasso', GlobalParameterType::class, [
                '_description' => 'Utiliser la module de paiement HelloAsso',
                'name_param' => GlobalParameter::USE_HELLOASSO,
                'label' => 'Utiliser le paiement par CB avec HelloAsso',
                '_placeholder' => 'false',
                '_data' => 'false',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les utilisateurs pourront cotiser et acheter de la monnaie locale par CB sur l\'application (compte Helloasso activé obligatoire).',
            ])
            ->add('adhesiontext', GlobalParameterType::class, [
                'required' => false,
                '_description' => "Description présente sur la page d'adhésion",
                'name_param' => GlobalParameter::ADHESION_TEXT,
                'label' => "Description présente sur la page d'adhésion",
            ])
            ->add('allTickets', GlobalParameterType::class, [
                'label' => 'Valeur des différents montants des billets de MLC :',
                '_description' => 'Valeur des différents montants des billets de MLC séparé par une virgule (exemple : 1,2,5,10,20,50)',
                'help' => 'Valeurs séparés par une virgule, le séparateur décimal est le point',
                'name_param' => GlobalParameter::ALL_TICKETS,
                'required' => true,
                '_placeholder' => '0.5,1,2,5,10,20,50',
                'constraints_param' => [
                    new RegexConstraint(['pattern' => '/^(([1-9]?[0-9]*\.?[0-9]*),?)*$/', 'message' => 'La valeur doit être des nombres supérieur à zéro séparés par une virgule']),
                ],
            ])
            ->add('helloAssoClientId', GlobalParameterType::class, [
                'label' => 'HELLOASSO Client Id :',
                '_description' => 'HELLOASSO CLIENT ID',
                'help' => 'Vous pouvez le trouver en étant connecté sur helloasso, dans Mon compte > Intégration et API',
                'name_param' => GlobalParameter::HELLOASSO_CLIENTID,
                'required' => false,
            ])
            ->add('helloAssoClientSecret', GlobalParameterType::class, [
                'label' => 'HELLOASSO Client Secret :',
                '_description' => 'HELLOASSO CLIENT SECRET',
                'help' => 'Vous pouvez le trouver en étant connecté sur helloasso, dans Mon compte > Intégration et API',
                'name_param' => GlobalParameter::HELLOASSO_CLIENTSECRET,
                'required' => false,
            ])
            ->add('helloAssoUrlCotisationAdherent', GlobalParameterType::class, [
                'label' => 'HELLOASSO URL COTISATION ADHERENT :',
                '_description' => 'HELLOASSO : Url de la campagne pour la cotisation d\'un adhérent',
                'name_param' => GlobalParameter::HELLOASSO_URL_COTISATION_ADHERENT,
                'required' => false,
            ])
            ->add('helloAssoUrlCotisationPrestataire', GlobalParameterType::class, [
                'label' => 'HELLOASSO URL COTISATION PRESTATAIRE :',
                '_description' => 'HELLOASSO : Url de la campagne pour la cotisation d\'un prestataire',
                'name_param' => GlobalParameter::HELLOASSO_URL_COTISATION_PRESTATAIRE,
                'required' => false,
            ])
            ->add('helloAssoUrlAchatEmlcAdherent', GlobalParameterType::class, [
                'label' => 'HELLOASSO URL ACAHT EMLC ADHERENT :',
                '_description' => 'HELLOASSO : Url de la campagne pour l\'achat de monnaie numérique pour les adhérents',
                'name_param' => GlobalParameter::HELLOASSO_URL_EMLC_ADHERENT,
                'required' => false,
            ])
            ->add('helloAssoUrlAchatEmlcPrestataire', GlobalParameterType::class, [
                'label' => 'HELLOASSO URL ACAHT EMLC PRESTATAIRE :',
                '_description' => 'HELLOASSO : Url de la campagne pour l\'achat de monnaie numérique pour les prestataires',
                'name_param' => GlobalParameter::HELLOASSO_URL_EMLC_PRESTATAIRE,
                'required' => false,
            ])
            ->add('mlcIbanAssociation', GlobalParameterType::class, [
                'label' => 'IBAN de gestion de l\'Association :',
                '_description' => 'IBAN de gestion de l\'Association',
                'name_param' => GlobalParameter::IBAN_ASSOCIATION,
                'required' => false,
                '_placeholder' => 'FRXXXXXXXXXXXXXXXXXXXXXXXXX',
            ])
            ->add('mlcIbanGuaranty', GlobalParameterType::class, [
                'label' => 'IBAN du fond de garantie :',
                '_description' => 'IBAN du fond de garantie',
                'name_param' => GlobalParameter::IBAN_GUARANTY,
                'required' => false,
                '_placeholder' => 'FRXXXXXXXXXXXXXXXXXXXXXXXXX',
            ])
            ->add('checkaddress', GlobalParameterType::class, [
                'label' => 'Paiement par chèque : Envoyer à cette adresse :',
                '_description' => 'Paiement par chèque : Envoyer à cette adresse',
                'name_param' => GlobalParameter::CHECK_ADDRESS,
                'required' => false,
                '_placeholder' => '',
            ])
            ->add('checkorder', GlobalParameterType::class, [
                'label' => 'Paiement par chèque : Ordre à mettre sur ceux-ci :',
                '_description' => 'Paiement par chèque : Ordre à mettre sur ceux-ci',
                'name_param' => GlobalParameter::CHECK_ORDER,
                'required' => false,
                '_placeholder' => '',
            ])
            ->add('acceptdonadhcotisation', GlobalParameterType::class, [
                '_description' => 'Dons acceptés lors de la cotisation d\'un adhérent ?',
                'name_param' => GlobalParameter::ACCEPT_DON_ADHERENT_COTISATION,
                'label' => 'Dons acceptés lors de la cotisation d\'un adhérent ?',
                '_placeholder' => 'true',
                '_data' => 'true',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les adhérents pourront ajouter un don lors de leur cotisation.',
            ])
            ->add('acceptdonprestacotisation', GlobalParameterType::class, [
                '_description' => 'Dons acceptés lors de la cotisation d\'un prestataire ?',
                'name_param' => GlobalParameter::ACCEPT_DON_PRESTATAIRE_COTISATION,
                'label' => 'Dons acceptés lors de la cotisation d\'un prestataire ?',
                '_placeholder' => 'true',
                '_data' => 'true',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les prestataires pourront ajouter un don lors de leur cotisation.',
            ])
            ->add('acceptdonadhachat', GlobalParameterType::class, [
                '_description' => 'Dons acceptés lors de l\'achat de e-mlc d\'un adhérent ?',
                'name_param' => GlobalParameter::ACCEPT_DON_ADHERENT_ACHAT,
                'label' => 'Dons acceptés lors de l\'achat de e-mlc d\'un adhérent ?',
                '_placeholder' => 'true',
                '_data' => 'true',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les adhérents pourront ajouter un don lors de leur acaht d\'e-mlc.',
            ])
            ->add('acceptdonprestaachat', GlobalParameterType::class, [
                '_description' => 'Dons acceptés lors de l\'achat de e-mlc d\'un prestataire ?',
                'name_param' => GlobalParameter::ACCEPT_DON_PRESTATAIRE_ACHAT,
                'label' => 'Dons acceptés lors de l\'achat de e-mlc d\'un prestataire ?',
                '_placeholder' => 'true',
                '_data' => 'true',
                // '_type' => CheckboxType::class,
                // '_type_options' => ['label' => 'Utiliser le paiement par CB avec Payzen',
                //     'required' => false,
                //     'label_attr' => ['class' => 'checkbox-inline'], ],
                'help' => 'Si cette option est à "true", les prestataires pourront ajouter un don lors de leur acaht d\'e-mlc.',
            ])
        ;
    }

    public function getBlockPrefix()
    {
        return 'formGlobalConfiguration';
    }
}
