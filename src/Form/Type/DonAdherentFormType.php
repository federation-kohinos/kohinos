<?php

namespace App\Form\Type;

use App\Entity\Adherent;
use App\Entity\DonAdherent;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DonAdherentFormType extends DonFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[FORM DON ADHERENT] Opération impossible !');
        }
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->security->getUser()->getAdherent(),
                'data_class' => null,
                'entity_class' => Adherent::class,
                'em' => $this->em,
            ])
            ->add('reference', HiddenType::class, [
                'data' => 'Don Adhérent',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => DonAdherent::class,
        ]);
    }

    public function getParent()
    {
        return DonFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formDonAdherent';
    }
}
