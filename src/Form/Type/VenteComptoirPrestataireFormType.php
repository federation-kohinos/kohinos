<?php

namespace App\Form\Type;

use App\Entity\Comptoir;
use App\Entity\Prestataire;
use App\Entity\VenteComptoirPrestataire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VenteComptoirPrestataireFormType extends VenteFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_comptoirgere'),
                'data_class' => null,
                'entity_class' => Comptoir::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Prestataire::class,
                'choices' => $this->em->getRepository(Prestataire::class)->findForEPaiement(false),
                'placeholder' => 'Prestataire',
                'required' => true,
                'label' => 'Prestataire :',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => VenteComptoirPrestataire::class,
        ]);
    }

    public function getParent()
    {
        return VenteFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formVenteComptoirPrestataire';
    }
}
