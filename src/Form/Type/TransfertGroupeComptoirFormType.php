<?php

namespace App\Form\Type;

use App\Entity\Comptoir;
use App\Entity\Groupe;
use App\Entity\TransfertGroupeComptoir;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransfertGroupeComptoirFormType extends TransfertFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security->getUser()) || empty($this->session->get('_groupegere'))) {
            throw new \Exception('[FORM TRANSFERT GROUPE COMPTOIR] Opération impossible !');
        }
        $builder
            ->add('expediteur', HiddenType::class, [
                'data' => $this->session->get('_groupegere'),
                'data_class' => null,
                'entity_class' => Groupe::class,
                'em' => $this->em,
            ])
            ->add('destinataire', EntityType::class, [
                'class' => Comptoir::class,
                'choices' => $this->em->getRepository(Comptoir::class)->findBy(['groupe' => $this->session->get('_groupegere')->getId(), 'enabled' => true], ['name' => 'ASC']),
                'placeholder' => 'Comptoir',
                'required' => true,
                'label' => 'Comptoir :',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => TransfertGroupeComptoir::class,
        ]);
    }

    public function getParent()
    {
        return TransfertFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formTransfertGroupeComptoir';
    }
}
