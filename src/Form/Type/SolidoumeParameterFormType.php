<?php

namespace App\Form\Type;

use App\Entity\SolidoumeParameter;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class SolidoumeParameterFormType extends AbstractType
{
    protected $em;
    protected $security;

    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', HiddenType::class, [
                'data' => $this->security->getUser(),
                'data_class' => null,
                'entity_class' => User::class,
                'em' => $this->em,
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom du programme',
                'required' => true,
            ])
            ->add('description', CKEditorType::class, [
                'label' => "Description du programme (sur la page d'accueil)",
                'required' => false,
            ])
            ->add('descriptionCotisation', CKEditorType::class, [
                'label' => "Description sur la page de participation au programme",
                'required' => false,
            ])
            ->add('minimum', NumberType::class, [
                'label' => 'Montant minimum',
                'required' => true,
            ])
            ->add('maximum', NumberType::class, [
                'label' => 'Montant maximum',
                'required' => true,
            ])
            ->add('commission', NumberType::class, [
                'label' => 'Commission (en %)',
                'required' => true,
                'attr' => ['placeholder' => '0.0'],
            ])
            ->add('executionDate', ChoiceType::class, [
                'label' => 'Date de redistribution',
                'required' => false,
                'choices' => range(1, 28),
                'choice_label' => function ($choice) {
                    return $choice;
                },
                'attr' => ['class' => 'w-50 m-auto'],
            ])
            ->add('reminderDays', IntegerType::class, [
                'label' => 'Nombre de jour(s) avant prélèvement pour envoyer la relance si solde insuffisant',
                'required' => true,
                'attr' => ['placeholder' => '3'],
            ])
            ->add('reminderEmail', CKEditorType::class, [
                'label' => 'Email de relance',
                'required' => false,
                'help' => 'Email reçu par le participant x jours avant l\'execution du programme seulement s\'il n\'a pas le solde suffisant sur son compte<br/>
                    Vous pouvez utilisez ces variables dans le text de l\'email :<br/>
                    <p class="ml-5">%name% : nom du programme</p>
                    <p class="ml-5">%adherent_name% : Nom de l\'adhérent</p>
                    <p class="ml-5">%amount% : Montant de sa participation</p>
                    <p class="ml-5">%paiementDate% : Jour (numéro) de son paiement</p>
                    <p class="ml-5">%executionDate% : Jour d\execution du programme</p>',
                'help_html' => true,
            ])
            ->add('confirmEmail', CKEditorType::class, [
                'label' => 'Email de confirmation de participation au programme',
                'help' => 'Email reçu par le participant après avoir validé le formulaire<br/>
                    Vous pouvez utilisez ces variables dans le text de l\'email :<br/>
                    <p class="ml-5">%name% : nom du programme</p>
                    <p class="ml-5">%adherent_name% : Nom de l\'adhérent</p>
                    <p class="ml-5">%amount% : Montant de sa participation</p>
                    <p class="ml-5">%paiementDate% : Jour (numéro) de son paiement</p>
                    <p class="ml-5">%executionDate% : Jour d\execution du programme</p>',
                'help_html' => true,
                'required' => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SolidoumeParameter::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'formSolidoumeParameter';
    }
}
