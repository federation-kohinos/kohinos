<?php

namespace App\Form\Type;

use App\Entity\AchatMonnaieAConfirmerPrestataire;
use App\Entity\Prestataire;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AchatMonnaieAConfirmerPrestataireFormType extends AchatMonnaieAConfirmerFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (empty($this->security) || empty($this->security->getUser())) {
            throw new \Exception('[DEMANDE ACHAT MONNAIE PRESTATAIRE] Opération impossible !');
        }
        $builder
            ->add('destinataire', HiddenType::class, [
                'data' => $this->session->get('_prestagere'),
                'data_class' => null,
                'entity_class' => Prestataire::class,
                'em' => $this->em,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => AchatMonnaieAConfirmerPrestataire::class,
        ]);
    }

    public function getParent()
    {
        return AchatMonnaieAConfirmerFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formAchatMonnaieAConfirmerPrestataire';
    }
}
