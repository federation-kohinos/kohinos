<?php

namespace App\Form\Type;

use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class VenteFormType extends TicketFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyen', ChoiceType::class, [
                'required' => true,
                'choices' => MoyenEnum::getAvailableTypesComptoir(),
                'choice_label' => function ($choice) {
                    return MoyenEnum::getTypeName($choice);
                },
            ])
        ;
    }

    public function getParent()
    {
        return TicketFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formVente';
    }
}
