<?php

namespace App\Form\Type;

use App\Entity\AchatMonnaieAConfirmer;
use App\Entity\Siege;
use App\Enum\MoyenEnum;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class AchatMonnaieAConfirmerFormType extends FluxFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('moyen', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    MoyenEnum::MOYEN_CHEQUE,
                    MoyenEnum::MOYEN_VIREMENT,
                ],
                'choice_label' => function ($choice) {
                    return MoyenEnum::getTypeName($choice);
                },
                'expanded' => true,
                'multiple' => false,
                'label' => 'Sélectionnez un moyen de paiement :',
            ])
            ->add('expediteur', HiddenType::class, [
                'data' => $this->em->getRepository(Siege::class)->getTheOne(),
                'data_class' => null,
                'entity_class' => Siege::class,
                'em' => $this->em,
            ])
            ->add('montant', MoneyType::class, [
                'label' => 'Montant : ',
                'required' => true,
                'scale' => 2,
                'constraints' => [
                    new Regex(['pattern' => '/[0-9]{1,}(\.[0-9]{1,2})?/']),
                ],
            ])
            ->add('reference', TextType::class, [
                'label' => "Référence du virement, date d'envoi du chèque, référence du paiement Helloasso",
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Valider'])
        ;
    }

    public function getParent()
    {
        return FluxFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'formAchatMonnaieAConfirmer';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AchatMonnaieAConfirmer::class,
            'cascade_validation' => true,
        ]);
    }
}
