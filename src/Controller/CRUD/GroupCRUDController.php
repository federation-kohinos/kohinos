<?php

namespace App\Controller\CRUD;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GroupCRUDController extends Controller
{
    public function deleteAction($id)
    {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id: %s', $id));
        }

        if ($object->hasRole('ROLE_ADMIN_SIEGE') || $object->hasRole('ROLE_SUPER_ADMIN') || $object->hasRole('ROLE_ADMIN')) {
            $this->addFlash(
                'sonata_flash_error',
                'Vous ne pouvez pas supprimer les groupe ayant les rôles ROLE_SUPER_ADMIN, ROLE_ADMIN et ROLE_ADMIN_SIEGE !'
            );

            return $this->redirectTo($object);
        }

        return parent::deleteAction($id);
    }

    public function batchActionDelete(ProxyQueryInterface $query)
    {
        $objects = $query->execute();

        foreach ($objects as $object) {
            if ($object->hasRole('ROLE_ADMIN_SIEGE') || $object->hasRole('ROLE_SUPER_ADMIN') || $object->hasRole('ROLE_ADMIN')) {
                $this->addFlash(
                    'sonata_flash_error',
                    'Vous ne pouvez pas supprimer les groupe ayant les rôles ROLE_SUPER_ADMIN, ROLE_ADMIN et ROLE_ADMIN_SIEGE !'
                );

                return new RedirectResponse(
                    $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
                );
            }
        }

        return parent::batchActionDelete($query);
    }
}
