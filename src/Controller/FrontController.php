<?php

namespace App\Controller;

use App\Entity\GlobalParameter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /**
     * If the global parameter USE_WORDPRESS is set to false, then the front is activated.
     *
     * @return The value of the parameter USE_WORDPRESS
     */
    protected function isFrontActivated()
    {
        return 'false' == $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_WORDPRESS);
    }
}
