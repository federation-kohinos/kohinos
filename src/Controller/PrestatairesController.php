<?php

namespace App\Controller;

use App\Entity\Groupe;
use App\Entity\Prestataire;
use App\Entity\Rubrique;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class PrestatairesController extends FrontController
{
    protected $em;
    private $router;

    public function __construct(EntityManagerInterface $em, RouterInterface $router)
    {
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * @Route("/prestataire/{slug}", name="show_prestataire")
     */
    public function showPrestaAction(Prestataire $prestataire)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }
        if ($prestataire->isMlc()) {
            // Ne pas montrer la page du prestataire recevant les cotisations !
            return new RedirectResponse($this->router->generate('index'));
        }

        return $this->render('@kohinos/presta/show.html.twig', [
            'presta' => $prestataire,
        ]);
    }

    /**
     * @Route("/prestataires/liste/{order}", name="liste_prestataire", defaults={"order": "raison"})
     */
    public function listePrestaAction($order = 'raison')
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        if ('groupelocal' == $order) {
            $prestas = $this->em->getRepository(Prestataire::class)->findDefault('prestataire', 'groupelocal');

            return $this->render('@kohinos/presta/liste_prestataires_bygroupelocal.html.twig', [
                'prestas' => $prestas,
                'type' => 'Prestataires',
            ]);
        }
        $prestas = $this->em->getRepository(Prestataire::class)->findDefault('prestataire');

        return $this->render('@kohinos/presta/liste_prestataires.html.twig', [
            'prestas' => $prestas,
            'type' => 'Prestataires',
        ]);
    }

    /**
     * @Route("/prestataires/groupe/{slug}/liste", name="liste_presta_by_groupe")
     */
    public function listePrestaByGroupeAction(Groupe $groupe)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        $prestas = $this->em->getRepository(Prestataire::class)->findByGroupe($groupe);

        return $this->render('@kohinos/presta/liste_prestataires.html.twig', [
            'prestas' => $prestas,
            'type' => 'Prestataires du groupe local ' . $groupe->getName(),
        ]);
    }

    /**
     * @Route("/partenaires/liste", name="liste_partenaire")
     */
    public function listePartnerAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }
        $partners = $this->em->getRepository(Prestataire::class)->findDefault('partenaire');

        return $this->render('@kohinos/presta/liste_prestataires.html.twig', [
            'prestas' => $partners,
            'type' => 'Partenaires',
        ]);
    }

    /**
     * @Route("/prestataires/carte", name="carte_prestataire")
     */
    public function cartePrestaAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }
        $prestas = $this->em->getRepository(Prestataire::class)->findDefault();

        return $this->render('@kohinos/presta/carte.html.twig', [
            'prestas' => $prestas,
        ]);
    }

    /**
     * @Route("/prestataires/rubriques", name="rubriques_prestataire")
     */
    public function rubriquesAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }
        $rubriques = $this->em->getRepository(Rubrique::class)->findBy(['enabled' => true], ['name' => 'ASC']);

        return $this->render('@kohinos/presta/rubriques.html.twig', [
            'rubriques' => $rubriques,
        ]);
    }

    /**
     * @Route("/prestataires/rubrique/{slug}", name="show_rubrique")
     */
    public function showRubriqueAction(Rubrique $rubrique)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/presta/show_rubrique.html.twig', [
            'rubrique' => $rubrique,
            'prestataires' => $this->em->getRepository(Prestataire::class)->findByRubrique($rubrique),
        ]);
    }
}
