<?php

namespace App\Controller\Admin;

use App\Entity\GlobalParameter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * @Route("/admin/global-parameter")
 */
class GlobalParameterController extends AbstractController
{
    /**
     * @Route("/{id}/update-value", name="admin_global_parameter_update_value", methods={"POST"})
     */
    public function updateValue(
        Request $request, 
        GlobalParameter $parameter, 
        EntityManagerInterface $entityManager,
        CsrfTokenManagerInterface $csrfTokenManager
    ): JsonResponse
    {
        try {
            // Vérifier le token CSRF
            $token = $request->request->get('_token');
            if (!$token) {
                // Si le token n'est pas dans les paramètres POST, essayer de le récupérer des en-têtes
                $token = $request->headers->get('X-CSRF-Token');
            }
            
            if (!$token || !$csrfTokenManager->isTokenValid(new CsrfToken('sonata_admin', $token))) {
                return new JsonResponse([
                    'success' => false, 
                    'message' => 'Token CSRF invalide'
                ], 403);
            }
            
            $value = $request->request->get('value');
            $parameter->setValue($value);
            $entityManager->flush();

            return new JsonResponse(['success' => true]);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
