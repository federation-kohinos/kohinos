<?php

namespace App\Controller;

use App\Entity\GlobalParameter;
use App\Entity\Prestataire;
use App\Entity\SolidoumeItem;
use App\Entity\SolidoumeParameter;
use App\Entity\TransactionAdherentPrestataire;
use App\Enum\CurrencyEnum;
use App\Enum\MoyenEnum;
use App\Form\Type\SolidoumeFormType;
use App\Utils\OperationUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

class SolidoumeController extends AbstractController
{
    private $em;
    private $security;
    private $mailer;
    private $templating;
    private $operationUtils;
    private $session;

    public function __construct(EntityManagerInterface $em, Security $security, \Swift_Mailer $mailer, Environment $templating, OperationUtils $operationUtils, SessionInterface $session)
    {
        $this->em = $em;
        $this->security = $security;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->operationUtils = $operationUtils;
        $this->session = $session;
    }

    /**
     * @Route("/ssa/desinscription", name="solidoume_unsubscribe")
     */
    public function unsubscribeAction(Request $request)
    {
        if (!(null != $this->security->getUser()->getAdherent() && $this->security->getUser()->isGranted('ROLE_ADHERENT'))) {
            $this->addFlash(
                'error',
                'Page inaccessible, vous devez être connecté en adhérent !'
            );

            return $this->redirectToRoute('index');
        }
        $solidoumeItem = $this->em->getRepository(SolidoumeItem::class)->findOneBy(['adherent' => $this->security->getUser()->getAdherent(), 'enabled' => true]);
        $solidoumeItem->setEnabled(false);
        $this->em->persist($solidoumeItem);
        $this->em->flush();

        $solidoumeParam = $this->em->getRepository(SolidoumeParameter::class)->findTheOne();
        $name = 'la sécurite sociale alimentaire';
        if (null != $solidoumeParam) {
            $name = $solidoumeParam->getName();
        }
        $this->addFlash(
            'success',
            'Désinscription à ' . $name . ' bien prise en compte !'
        );

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/ssa/adherer", name="solidoume_adherer")
     */
    public function adhererAction(Request $request)
    {
        if (null == $this->security->getUser() || !(null != $this->security->getUser()->getAdherent() && $this->security->getUser()->isGranted('ROLE_ADHERENT'))) {
            $this->addFlash(
                'error',
                'Page inaccessible, vous devez être connecté en adhérent !'
            );

            return $this->redirectToRoute('index');
        }

        $solidoumeParam = $this->em->getRepository(SolidoumeParameter::class)->findTheOne();
        if (empty($solidoumeParam)) {
            $this->addFlash(
                'error',
                'Le programme de Sécurité Sociale Alimentaire est non configuré !'
            );

            return $this->redirectToRoute('index');
        }

        $data = $this->em->getRepository(SolidoumeItem::class)->findOneBy(['adherent' => $this->security->getUser()->getAdherent(), 'enabled' => true]);
        $alreadyExist = false;
        if (!empty($data)) {
            $alreadyExist = true;
        }
        $form = $this->createForm(SolidoumeFormType::class, $data);
        $form->handleRequest($request);
        $solidoumeItem = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $solidoumeItem = $form->getData();
            if (!$solidoumeItem->getIsRecurrent()) {
                if (!$this->makeSolidoumeTransaction($solidoumeItem)) {
                    $referer = $request->headers->get('referer');

                    return $this->redirect($referer);
                }
            }
            $this->em->persist($solidoumeItem);
            $this->em->flush();
            $this->sendMail($solidoumeItem);
            $this->session->set('_solidoumeSubmit', true);
        }

        return $this->render('@kohinos/solidoume/index.html.twig', [
            'form' => $form->createView(),
            'param' => $solidoumeParam,
            'item' => $solidoumeItem,
        ]);
    }

    private function makeSolidoumeTransaction(SolidoumeItem $item)
    {
        $solidoumeParam = $this->em->getRepository(SolidoumeParameter::class)->findTheOne();
        $nowDay = (new \DateTime('now', new \DateTimeZone('UTC')))->format('d');
        $nowMonth = intval((new \DateTime('now', new \DateTimeZone('UTC')))->format('Ym'));
        $item->setPaiementDate($nowDay);
        $amount = $item->getAmount();
        $accountEmlc = $item->getAdherent()->getAccountWithCurrency(CurrencyEnum::CURRENCY_EMLC);
        if (!$item->getIsRecurrent()) {
            if (null != $accountEmlc && $accountEmlc->getBalance() < $amount) {
                $this->addFlash(
                    'error',
                    $solidoumeParam->getName() . ' : Solde inférieur au montant à prélever ! (Solde : ' . $accountEmlc->getBalance() . ' eMLC) !'
                );

                return false;
            } else {
                $flux = new TransactionAdherentPrestataire();
                $flux->setExpediteur($item->getAdherent());
                $flux->setDestinataire($this->em->getRepository(Prestataire::class)->getPrestataireSolidoume());
                $flux->setMontant($amount);
                $flux->setMoyen(MoyenEnum::MOYEN_EMLC);
                $now = (new \Datetime('now'))->format('d/m/Y H:i:s');
                $flux->setReference($this->em->getRepository(SolidoumeParameter::class)->getValueOf('name') . ' prélèvement en date du ' . $now);
                $item->setLastMonthPayed($nowMonth);
                $item->setEnabled(false);
                $this->em->persist($flux);
                $this->em->persist($item);
                // Write operations for this flux !
                $this->operationUtils->executeOperations($flux);
                $this->em->flush();
            }
        }

        return true;
    }

    private function sendMail(SolidoumeItem $solidoumeItem)
    {
        $subject = $this->em->getRepository(SolidoumeParameter::class)->getValueOf('name');
        $from = $this->em->getRepository(GlobalParameter::class)->val(GlobalParameter::MLC_NOTIF_EMAIL);
        $mail = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($solidoumeItem->getUser()->getEmail())
            ->setBody(
                $this->templating->render(
                    '@kohinos/email/solidoume/confirm.html.twig',
                    [
                        'subject' => $subject,
                        'item' => $solidoumeItem,
                    ]
                ),
                'text/html'
            );
        $this->mailer->send($mail);
    }
}
