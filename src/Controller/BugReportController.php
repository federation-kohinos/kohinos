<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Shivas\VersioningBundle\Service\VersionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BugReportController extends AbstractController
{
    /**
     * @Route("/bugreport", name="bugreport")
     * @IsGranted("ROLE_USER")
     */
    public function bugreport(Request $request, VersionManager $manager)
    {
        $urltype = 'https://gitlab.com/Kohinos/kohinos/-/issues/new?issue[title]=%%title%%&issue[description]=%%description%%';
        $newline = '%0A';
        $referer = $request->headers->get('referer');
        $version = $manager->getVersion();
        $title = '[V' . $version . '] ';
        $description =
            '[URL] ' . $referer . $newline . $newline .
            '[Kohinos v ' . $version . ']' . $newline .
            '[USER] ' . $this->getUser()->getName() . ' [' . $this->getUser()->getId() . ']' . $newline .
            '[ROLES] ' . implode(array_map(function ($o) {
                return $o->getName();
            }, $this->getUser()->getGroups()->toArray())) . $newline . $newline .
            '* Résumé' . $newline .
            '(Résumé concis du bug)' . $newline . $newline .
            '* Étapes pour reproduire' . $newline .
            '(Comment reproduire le bug)' . $newline . $newline .
            '* Comportement courant' . $newline .
            '(ce qu\'il se passe et que vous trouver anormal)' . $newline . $newline .
            '* Comportement attendu' . $newline .
            '(ce qui devrait se passer selon vous ou qui se passait avant la régression)' . $newline . $newline .
            '* Environnement' . $newline .
            '( Version / environnement / instance, ...)  ' . $newline . $newline .
            '* Préciser le Compte / Rôle pour se connecter' . $newline .
            '(fournir les comptes si besoin)' . $newline . $newline .
            '* Logs et/ou captures d\'écran pertinent' . $newline
        ;

        $url = str_replace(['%%title%%', '%%description%%'], [$title, $description], $urltype);

        return $this->redirect($url);
    }
}
