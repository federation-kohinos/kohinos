<?php

namespace App\Controller;

use App\Entity\Comptoir;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class ComptoirController extends FrontController
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/comptoirs/liste", name="comptoirs_liste")
     */
    public function listeComptoirAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/comptoir/liste.html.twig', [
            'comptoirs' => $this->em->getRepository(Comptoir::class)->findBy(['enabled' => true], ['createdAt' => 'DESC']),
        ]);
    }

    /**
     * @Route("/comptoirs/carte", name="comptoirs_carte")
     */
    public function carteComptoirAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/comptoir/carte.html.twig', [
            'comptoirs' => $this->em->getRepository(Comptoir::class)->findBy(['enabled' => true], ['createdAt' => 'DESC']),
        ]);
    }

    /**
     * @Route("/comptoir/{slug}", name="show_comptoir")
     */
    public function showGroupeAction(Comptoir $comptoir)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/comptoir/show.html.twig', [
            'comptoir' => $comptoir,
        ]);
    }
}
