<?php

namespace App\Controller;

use App\Entity\Groupeprestataire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire.
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class GroupePrestaController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/prestataires/groupe/{slug}", name="show_groupeprestataire")
     */
    public function showGroupeAction(Groupeprestataire $groupe)
    {
        return $this->render('@kohinos/groupepresta/show.html.twig', [
            'groupepresta' => $groupe,
        ]);
    }

    /**
     * @Route("/groupe/prestataires/{type}/liste", name="groupepresta_liste")
     */
    public function listeGroupePrestaAction($type, Request $request)
    {
        $groupes = $this->em->getRepository(Groupeprestataire::class)->findBy(['type' => $type, 'enabled' => true], ['name' => 'ASC']);

        return $this->render('@kohinos/groupepresta/liste.html.twig', [
            'groupes' => $groupes,
            'groupetype' => $type,
        ]);
    }

    /**
     * @Route("/groupe/prestataires/{type}/carte", name="groupepresta_carte")
     */
    public function carteGroupePrestaAction($type, Request $request)
    {
        $groupes = $this->em->getRepository(Groupeprestataire::class)->findBy(['type' => $type, 'enabled' => true]);

        return $this->render('@kohinos/groupepresta/carte.html.twig', [
            'groupes' => $groupes,
            'groupetype' => $type,
        ]);
    }
}
