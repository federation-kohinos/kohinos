<?php

namespace App\Controller;

use App\Entity\CotisationAdherent;
use App\Entity\CotisationPrestataire;
use App\Entity\Prestataire;
use App\Enum\MoyenEnum;
use App\Utils\CustomEntityManager;
use DateTime;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;

class PrestataireAdminController extends CRUDController
{
    protected $em;
    protected $security;

    public function __construct(CustomEntityManager $em, Security $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    /**
     * Ajouter une cotisation gratuite à l'adhérent.
     *
     * @param Uuid $id Id du prestataire
     *
     * @return Response
     */
    public function addfreecotisationadhAction($id): Response
    {
        $prestataire = $this->admin->getSubject();

        if (!$prestataire) {
            throw new NotFoundHttpException(sprintf('Impossible de trouver le prestataire avec l\'id: %s', $id));
        }

        $managers = $prestataire->getUsers();
        if (1 == count($managers)) {
            $manager = $managers[0];
            if (null != $manager->getAdherent()) {
                $cotisation = new CotisationAdherent();
                $cotisation->setExpediteur($manager->getAdherent());
                $now = new DateTime();
                $cotisation->setRecu(true);
                $cotisation->setReference('Cotisation gratuite');
                // $cotisation->setOperateur(null);
                $cotisation->setRole($this->getUser()->getGroups()[0]->__toString());
                $mlcPrestataire = $this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]);
                $cotisation->setDestinataire($mlcPrestataire);
                $cotisation->setMoyen(MoyenEnum::MOYEN_AUTRE);

                $cotisation->setMontant(0);
                $cotisation->getCotisationInfos()->setAnnee(date('Y'));
                $cotisation->getCotisationInfos()->setDebut($now);
                $cotisation->getCotisationInfos()->setFin(new DateTime('+ 1 year'));

                $this->em->persist($cotisation);
                $this->em->flush();
                $this->addFlash('sonata_flash_success', sprintf('Cotisation gratuite ajouté au gestionnaire %s du prestataire %s', $manager->__toString(), $prestataire->__toString()));
            } else {
                $this->addFlash('sonata_flash_error', sprintf('Impossible d\ajouter une cotisation gratuite au gestionnaire %s du prestataire %s car il n\'a pas de compte adhérent !', $manager->getEmail(), $prestataire->__toString()));
            }
        } else {
            $this->addFlash('sonata_flash_error', sprintf('Impossible d\ajouter une cotisation gratuite à tous les gestionnaires du prestataire %s !', $prestataire->__toString()));
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * Ajouter une cotisation gratuite au prestataire.
     *
     * @param Uuid $id Id du prestataire
     *
     * @return Response
     */
    public function addfreecotisationprestaAction($id): Response
    {
        $prestataire = $this->admin->getSubject();

        if (!$prestataire) {
            throw new NotFoundHttpException(sprintf('Impossible de trouver le prestataire avec l\'id: %s', $id));
        }

        $cotisation = new CotisationPrestataire();
        $cotisation->setExpediteur($prestataire);
        $now = new DateTime();
        $cotisation->setRecu(true);
        $cotisation->setReference('Cotisation gratuite');
        // $cotisation->setOperateur(null);
        $cotisation->setRole($this->getUser()->getGroups()[0]->__toString());
        $mlcPrestataire = $this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]);
        $cotisation->setDestinataire($mlcPrestataire);
        $cotisation->setMoyen(MoyenEnum::MOYEN_AUTRE);

        $cotisation->setMontant(0);
        $cotisation->getCotisationInfos()->setAnnee(date('Y'));
        $cotisation->getCotisationInfos()->setDebut($now);
        $cotisation->getCotisationInfos()->setFin(new DateTime('+ 1 year'));

        $this->em->persist($cotisation);
        $this->em->flush();

        $this->addFlash('sonata_flash_success', sprintf('Cotisation gratuite ajouté au prestataire %s', $prestataire->__toString()));

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }
}
