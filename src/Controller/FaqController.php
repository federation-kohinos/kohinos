<?php

namespace App\Controller;

use App\Entity\Faq;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class FaqController extends FrontController
{
    protected $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/faq", name="faq")
     */
    public function faqAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/faq/liste.html.twig', [
            'faqs' => $this->em->getRepository(Faq::class)->findBy(['enabled' => true], ['createdAt' => 'DESC']),
        ]);
    }

    /**
     * @Route("/faq/{slug}", name="show_faq")
     */
    public function showFaqAction(Faq $faq)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/faq/show.html.twig', [
            'faq' => $faq,
        ]);
    }
}
