<?php

namespace App\Controller;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends FrontController
{
    protected $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/page/{slug}", name="show_page")
     */
    public function pageAction(Page $page)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }
        $template = 'page.html.twig';
        if (!empty($page->getTemplate()) && $this->get('templating')->exists($page->getTemplate())) {
            $template = $page->getTemplate();
        }

        return $this->render('@kohinos/' . $template, [
            'page' => $page,
        ]);
    }
}
