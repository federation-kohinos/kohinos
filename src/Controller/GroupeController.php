<?php

namespace App\Controller;

use App\Entity\Groupe;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class GroupeController extends FrontController
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/groupes", name="liste_groupe")
     */
    public function listeGroupeAction()
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/groupe/liste.html.twig', [
            'groupes' => $this->em->getRepository(Groupe::class)->findBy(['enabled' => true], ['createdAt' => 'DESC']),
        ]);
    }

    /**
     * @Route("/groupe/{slug}", name="show_groupe")
     */
    public function showGroupeAction(Groupe $groupe)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/groupe/show.html.twig', [
            'groupe' => $groupe,
        ]);
    }
}
