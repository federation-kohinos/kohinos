<?php

namespace App\Controller;

use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends FrontController
{
    protected $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/news", name="news")
     */
    public function listeNewsAction(Request $request)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }
        $pagination = $this->paginator->paginate(
            $this->em->getRepository(News::class)->findLatest(),
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('@kohinos/news/liste.html.twig', [
            'news' => $pagination,
        ]);
    }

    /**
     * @Route("/news/{slug}", name="show_news")
     */
    public function showNewsAction(News $news)
    {
        if (!$this->isFrontActivated()) {
            return $this->redirectToRoute('index');
        }

        return $this->render('@kohinos/news/show.html.twig', [
            'news' => $news,
        ]);
    }
}
