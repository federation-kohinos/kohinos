<?php

namespace App\Controller;

use App\Entity\Groupe;
use App\Entity\TransfertComptoirGroupe;
use App\Entity\TransfertGroupeComptoir;
use App\Form\Type\GroupeInfosFormType;
use App\Form\Type\TransfertComptoirGroupeFormType;
use App\Form\Type\TransfertGroupeComptoirFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserGestionnaireGroupeController extends FluxController
{
    /**
     * @Route("/user/groupe/infos", name="groupe_infos")
     * @IsGranted({"ROLE_GESTION_GROUPE", "ROLE_CONTACT", "ROLE_TRESORIER"})
     */
    public function groupeInfosAction(Request $request)
    {
        // @TODO : récupérer groupe gere en session
        $form = $this->createForm(GroupeInfosFormType::class, $this->session->get('_groupegere'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->em->persist($data);
            $this->em->flush();
            $this->addFlash(
                'success',
                $this->translator->trans('Groupe local bien modifié !')
            );
            $referer = $request->headers->get('referer');
            if ($referer && !$request->isXmlHttpRequest()) {
                return $this->redirect($referer);
            } elseif (!$request->isXmlHttpRequest()) {
                return new Response('', Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/user/groupe/transfert/comptoir/", name="transfertGroupeComptoir")
     * @IsGranted("ROLE_GESTION_GROUPE")
     */
    public function transfertGroupeComptoirAction(Request $request)
    {
        $entity = new TransfertGroupeComptoir();
        $entity->setOperateur($this->getUser());
        $form = $this->createForm(TransfertGroupeComptoirFormType::class, $entity);

        return $this->manageFluxForm(
            $request,
            $form
        );
    }

    /**
     * @Route("/user/groupe/retour/comptoir/", name="transfertComptoirGroupe")
     * @IsGranted("ROLE_GESTION_GROUPE")
     */
    public function transfertComptoirGroupeAction(Request $request)
    {
        $entity = new TransfertComptoirGroupe();
        $entity->setOperateur($this->getUser());
        $form = $this->createForm(TransfertComptoirGroupeFormType::class, $entity);

        return $this->manageFluxForm(
            $request,
            $form
        );
    }
}
