<?php

namespace App\Controller;

use App\Entity\Groupe;
use App\Entity\Siege;
use App\Entity\TicketFixDestroy;
use App\Entity\TicketFixPrint;
use App\Entity\TransfertGroupeSiege;
use App\Entity\TransfertSiegeGroupe;
use App\Form\Type\SoldeSiegeFormType;
use App\Form\Type\TicketFixDestroyFormType;
use App\Form\Type\TicketFixPrintFormType;
use App\Form\Type\TransfertGroupeSiegeFormType;
use App\Form\Type\TransfertSiegeGroupeFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserSiegeController extends FluxController
{
    // /**
    //  * @Route("/user/siege/infos", name="groupe_infos")
    //  * @IsGranted("ROLE_AMIN_SIEGE")
    //  */
    // public function siegeInfosAction(Request $request)
    // {
    //     $form = $this->createForm(SiegeInfosFormType::class, $this->getUser()->getGroupesgere());
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $data = $form->getData();
    //         $this->em->persist($data);
    //         $this->em->flush();
    //         $this->addFlash(
    //             'success',
    //             'Siege bien modifié !'
    //         );
    //         $referer = $request->headers->get('referer');
    //         if ($referer && !$request->isXmlHttpRequest()) {
    //             return $this->redirect($referer);
    //         } elseif (!$request->isXmlHttpRequest()) {
    //             return new Response('', Response::HTTP_BAD_REQUEST);
    //         }
    //     }

    //     return $this->redirectToRoute('index');
    // }

    /**
     * @Route("/siege/transfert/groupe/", name="transfertSiegeGroupe")
     * @IsGranted("ROLE_ADMIN_SIEGE")
     */
    public function transfertSiegeGroupeAction(Request $request)
    {
        $entity = new TransfertSiegeGroupe();
        $entity->setOperateur($this->getUser());
        $form = $this->createForm(TransfertSiegeGroupeFormType::class, $entity);

        return $this->manageFluxForm(
            $request,
            $form
        );
    }

    /**
     * @Route("/groupe/transfert/siege/", name="transfertGroupeSiege")
     * @IsGranted("ROLE_ADMIN_SIEGE")
     */
    public function transfertGroupeSiegeAction(Request $request)
    {
        $entity = new TransfertGroupeSiege();
        $entity->setOperateur($this->getUser());
        $form = $this->createForm(TransfertGroupeSiegeFormType::class, $entity);

        return $this->manageFluxForm(
            $request,
            $form
        );
    }

    /**
     * @Route("/groupe/ticket/fix/print", name="ticketFixPrint")
     * @IsGranted("ROLE_ADMIN_SIEGE")
     */
    public function ticketFixPrintAction(Request $request)
    {
        $entity = new TicketFixPrint();
        $entity->setOperateur($this->getUser());
        $form = $this->createForm(TicketFixPrintFormType::class, $entity);

        return $this->manageFluxForm(
            $request,
            $form
        );
    }

    /**
     * @Route("/groupe/ticket/fix/destroy", name="ticketFixDestroy")
     * @IsGranted("ROLE_ADMIN_SIEGE")
     */
    public function ticketFixDestroyAction(Request $request)
    {
        $entity = new TicketFixDestroy();
        $entity->setOperateur($this->getUser());
        $form = $this->createForm(TicketFixDestroyFormType::class, $entity);

        return $this->manageFluxForm(
            $request,
            $form
        );
    }

    /**
     * @Route("/solde/siege/", name="soldeSiege")
     * @IsGranted("ROLE_ADMIN_SIEGE")
     */
    public function soldeSiegeAction(Request $request)
    {
        $siege = $this->em->getRepository(Siege::class)->getTheOne();
        $form = $this->createForm(SoldeSiegeFormType::class, $siege);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->em->persist($data);
            $this->em->flush();
            $this->addFlash(
                'success',
                $this->translator->trans('Solde bien modifié !')
            );
            $referer = $request->headers->get('referer');
            if ($referer && !$request->isXmlHttpRequest()) {
                return $this->redirect($referer);
            } elseif (!$request->isXmlHttpRequest()) {
                return new Response('', Response::HTTP_BAD_REQUEST);
            }
        }

        return $this->render('@kohinos/siege/block/solde_edit_page.html.twig', [
            'form' => $form->createView(),
            'title' => $this->translator->trans('Solde de billet au siège'),
        ]);
    }
}
