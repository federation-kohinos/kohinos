<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSACTION
 *  - Achat de monnaie en CB par un Adhérent.
 *
 * @ORM\Entity
 */
class AchatMonnaieAdherent extends AchatMonnaie
{
    /**
     * @ORM\ManyToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_dest_id", referencedColumnName="id", nullable=true)
     */
    protected $destinataire;

    /**
     * @var DonAdherent
     * @ORM\OneToOne(targetEntity="DonAdherent", cascade={"persist"})
     * @ORM\JoinColumn(name="don_id", referencedColumnName="id", nullable=true)
     */
    protected $don;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_ACHAT_ADHERENT;
    }

    /**
     * Get don.
     *
     * @return DonAdherent
     */
    public function getDon(): ?DonAdherent
    {
        return $this->don;
    }

    /**
     * Set don.
     *
     * @return $this
     */
    public function setDon(?DonAdherent $don)
    {
        $this->don = $don;

        return $this;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => ['siege'],
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
