<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="operation_groupe")
 */
class OperationGroupe extends Operation
{
    /**
     * @ORM\ManyToOne(targetEntity=AccountGroupe::class, inversedBy="operations", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     */
    protected $account;

    /**
     * @ORM\ManyToOne(targetEntity=Flux::class, inversedBy="operationsGroupe", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="flux_id", referencedColumnName="id", nullable=false)
     */
    protected $flux;
}
