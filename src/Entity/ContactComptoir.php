<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\ContactEmailTelTrait;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_VIEW') or is_granted('ROLE_API')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_LIST') or is_granted('ROLE_API')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_VIEW') or is_granted('ROLE_API')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"cread"}},
 *     denormalizationContext={"groups"={"cwrite"}}
 * ).
 *
 * @ORM\Entity
 * @ORM\Table(name="contact_comptoir")
 */
class ContactComptoir
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;
    use ContactEmailTelTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"cread"})
     */
    protected $id;

    /**
     * @var Comptoir
     *
     * @ORM\ManyToOne(targetEntity="Comptoir", cascade={"persist"}, inversedBy="contacts")
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id", nullable=false)
     * @Groups({"cread", "cwrite"})
     * @MaxDepth(1)
     */
    private $comptoir;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get comptoir.
     *
     * @return
     */
    public function getComptoir(): Comptoir
    {
        return $this->comptoir;
    }

    /**
     * Set comptoir.
     *
     * @return $this
     */
    public function setComptoir($comptoir): self
    {
        $this->comptoir = $comptoir;

        return $this;
    }
}
