<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CHANGE
 *  - ADHERENTS      =>     COMPTOIRS       (dépôt de billet et chargement du compte numérique).
 *
 * @ORM\Entity()
 */
class ChangeAdherentComptoir extends Change
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_CHANGE_ADHERENT_COMPTOIR;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => [$this->getExpediteur()->getUser()],
            'destinataires' => $this->getDestinataire()->getGestionnaires()->toArray(),
        ];
    }
}
