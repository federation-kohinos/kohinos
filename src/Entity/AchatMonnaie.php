<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Achat de monnaie en CB par un Adhérent ou Prestataire.
 *
 * @ORM\Entity
 */
class AchatMonnaie extends Flux
{
    const TYPE_ACHAT_ADHERENT = 'achat_monnaie_adherent';
    const TYPE_ACHAT_PRESTATAIRE = 'achat_monnaie_prestataire';

    /**
     * @ORM\ManyToOne(targetEntity="Siege")
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id", nullable=true)
     */
    protected $expediteur;

    /**
     * @var bool RECONVERTI => Validation du dépôt d'espèce/chèque/virement
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     * @Groups({"read", "write"})
     */
    protected $reconverti = false;

    protected $don;

    public function __construct()
    {
        parent::__construct();
        $this->reconverti = false;
    }

    public function getReconverti(): bool
    {
        return $this->reconverti;
    }

    public function setReconverti(bool $reconverti)
    {
        $this->reconverti = $reconverti;

        return $this;
    }

    public function getAllOperations($em)
    {
        $achatOperations = [
            OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
            OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
        ];
        if (null != $this->getDon()) {
            return array_merge($achatOperations, $this->getDon()->getAllOperations($em));
        }

        return $achatOperations;
    }

    public function operate($em)
    {
        if ($this->getReconverti()) {
            // Increment Ecompte of Siege
            $this->getExpediteur()->addEcompteNantie($this->getMontant());
            $this->getDestinataire()->addEcompte($this->getMontant());

            return [$this->getExpediteur(), $this->getDestinataire()];
        } else {
            //DO NOTHING IF NOT VALIDATE !
            return [];
        }
    }

    public function getUsersToNotify()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_ACHAT;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }
}
