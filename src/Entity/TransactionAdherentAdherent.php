<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TRANSACTION
 *   - ADHERENTS        =>    ADHERENTS      (transfert de monnaie numérique).
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TransactionAdherentAdherent extends Transaction
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_dest_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSACTION_ADHERENT_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => [$this->getExpediteur()->getUser()],
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
