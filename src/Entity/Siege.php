<?php

namespace App\Entity;

use App\Entity\EntityTrait\HasAccountsTrait;
use App\Entity\EntityTrait\HasCompteEntity;
use App\Entity\EntityTrait\HasEcompteEntity;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use App\Flux\AccountableInterface;
use App\Flux\AccountableObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SiegeRepository")
 * @ORM\Table(name="siege")
 * @ORM\HasLifecycleCallbacks()
 */
class Siege extends AccountableObject implements AccountableInterface
{
    use NameSlugContentEntityTrait;
    use HasCompteEntity;
    use HasEcompteEntity;
    use HasAccountsTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var ArrayCollection|Groupe[]
     * @ORM\OneToMany(targetEntity="Groupe", mappedBy="siege")
     */
    private $groupes;

    /**
     * @var float
     *
     * @ORM\Column(name="compte_nantie", type="decimal", scale=2)
     */
    private $compteNantie = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="ecompte_nantie", type="decimal", scale=2)
     */
    private $ecompteNantie = 0;

    /**
     * @var ArrayCollection|AccountSiege[]
     * @ORM\OneToMany(targetEntity="AccountSiege", mappedBy="siege")
     */
    private $accounts;

    public function __construct()
    {
        $this->groupes = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getCompteNantie(): float
    {
        return $this->compteNantie;
    }

    /**
     * @param float $compteNantie
     *
     * @return $this
     */
    public function setCompteNantie(float $compteNantie)
    {
        $this->compteNantie = $compteNantie;

        return $this;
    }

    /**
     * @param float $compteNantie
     *
     * @return $this
     */
    public function addCompteNantie(float $compteNantie)
    {
        $this->compteNantie += $compteNantie;

        return $this;
    }

    /**
     * @param float $compteNantie
     *
     * @return $this
     */
    public function removeCompteNantie(float $compteNantie)
    {
        $this->compteNantie -= $compteNantie;

        return $this;
    }

    /**
     * @return float
     */
    public function getEcompteNantie(): float
    {
        return $this->ecompteNantie;
    }

    /**
     * @param float $ecompteNantie
     *
     * @return $this
     */
    public function setEcompteNantie(float $ecompteNantie)
    {
        $this->ecompteNantie = $ecompteNantie;

        return $this;
    }

    /**
     * @param float $ecompteNantie
     *
     * @return $this
     */
    public function addEcompteNantie(float $ecompteNantie)
    {
        $this->ecompteNantie += $ecompteNantie;

        return $this;
    }

    /**
     * @param float $ecompteNantie
     *
     * @return $this
     */
    public function removeEcompteNantie(float $ecompteNantie)
    {
        $this->ecompteNantie -= $ecompteNantie;

        return $this;
    }

    /**
     * @return Groupe[]|ArrayCollection
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * @param Groupe $groupe
     *
     * @return $this
     */
    public function addGroupe(Groupe $groupe)
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
            $groupe->setSiege($this);
        }

        return $this;
    }

    /**
     * @param Groupe $groupe
     *
     * @return $this
     */
    public function removeGroupe(Groupe $groupe)
    {
        if ($this->groupes->contains($groupe)) {
            $this->groupes->removeElement($groupe);
            $groupe->setSiege(null);
        }

        return $this;
    }

    public function __toString(): string
    {
        return 'SIEGE';
    }
}
