<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * TicketFix => (Impression / destruction de monnaie papier => modifie le compte de MLC disponible au siège).
 *
 * @ORM\Entity
 */
class TicketFix extends Flux
{
    const TYPE_TICKET_FIX_PRINT = 'ticket_print';
    const TYPE_TICKET_FIX_DESTROY = 'ticket_destroy';

    /**
     * @ORM\OneToOne(targetEntity="Siege")
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     */
    protected $destinataire;

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_TICKET_FIX_PRINT => self::TYPE_TICKET_FIX_PRINT,
            self::TYPE_TICKET_FIX_DESTROY => self::TYPE_TICKET_FIX_DESTROY,
        ];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_TICKET_FIX;
    }

    public function getMontantForAction(): float
    {
        return $this->getMontant();
    }

    public function getAllOperations($em)
    {
        return [
            OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_MLC, $this->getMontantForAction()),
        ];
    }

    public function operate($em)
    {
        // $compteExp = $this->getExpediteur()->getCompte() - $this->getMontant();
        // if ($compteExp < 0) {
        //     throw new \Exception("[FLUX] Vente impossible ! Montant supérieur au solde du comptoir !");
        // } else {
        //     return [$this->getExpediteur()];
        // }
        return [];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur(),
        ];
    }
}
