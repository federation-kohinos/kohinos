<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW') or is_granted('ROLE_API')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_LIST') or is_granted('ROLE_API')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW') or is_granted('ROLE_API')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity
 * @ORM\Table(name="typepresta")
 */
class TypePrestataire
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups("read")
     */
    protected $id;

    /**
     * @var ArrayCollection|Prestataire[]
     * @ORM\OneToMany(targetEntity="Prestataire", mappedBy="typeprestataire")
     * @Groups("read")
     */
    private $prestataires;

    public function getId()
    {
        return $this->id;
    }
}
