<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketFixPrint => (Impression de monnaie papier => modifie le compte de MLC disponible au siège).
 *
 * @ORM\Entity()
 */
class TicketFixPrint extends TicketFix
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TICKET_FIX_PRINT;
    }

    public function getParenttype(): string
    {
        return parent::TYPE_TICKET_FIX_PRINT;
    }

    public function getMontantForAction(): float
    {
        return $this->getMontant();
    }
}
