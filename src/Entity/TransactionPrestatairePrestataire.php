<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TRANSACTION
 *   - PRESTATAIRES     =>    PRESTATAIRES      (Virement entre prestataires).
 *
 * @ORM\Entity
 */
class TransactionPrestatairePrestataire extends Transaction
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_dest_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSACTION_PRESTATAIRE_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => array_merge($this->getExpediteur()->getUsers()->toArray(), $this->getExpediteur()->getCaissiers()->toArray()),
            'destinataires' => array_merge($this->getDestinataire()->getUsers()->toArray(), $this->getDestinataire()->getCaissiers()->toArray()),
        ];
    }
}
