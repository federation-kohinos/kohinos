<?php

namespace App\Entity;

use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Enum\HelloassoStateEnum;
use App\Flux\FluxInterface;
use App\Repository\HelloAssoRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=HelloAssoRepository::class)
 * @ORM\Table(name="helloasso")
 */
class HelloAsso
{
    use EnablableEntityTrait;
    use TimestampableEntity;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var Prestataire
     *
     * @ORM\ManyToOne(targetEntity="Prestataire", cascade={"persist"})
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=true)
     */
    private $prestataire;

    /**
     * @ORM\ManyToOne(targetEntity="Adherent", cascade={"persist"})
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id", nullable=true)
     */
    protected $adherent;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $errors;

    /**
     * HelloAsso data.
     *
     * @ORM\Column(type="json")
     */
    private $data = [];

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * HelloAsso State of data return.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * HelloAsso State of data return.
     *
     * @ORM\Column(name="statepayment", type="string", length=255)
     */
    private $statePayment;

    /**
     * HelloAsso PaymentReceiptUrl.
     *
     * @ORM\Column(name="paymentReceiptUrl", type="text", nullable=true)
     */
    private $paymentReceiptUrl;

    /**
     * @ORM\Column(type="integer")
     */
    private $helloassoid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $payeremail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $payerfirstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $payerlastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $userfirstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $userlastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $useremail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $operationState;

    /**
     * @ORM\OneToOne(targetEntity=Flux::class)
     * @ORM\JoinColumn(name="flux_id", referencedColumnName="id", nullable=true)
     */
    protected $flux;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $historical;

    public function __construct()
    {
        $this->setEnabled(false);
        $this->setHistorical(false);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     *
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get prestataire.
     *
     * @return
     */
    public function getPrestataire(): ?Prestataire
    {
        return $this->prestataire;
    }

    /**
     * Set prestataire.
     *
     * @param Prestataire $prestataire
     *
     * @return $this
     */
    public function setPrestataire($prestataire): self
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * Get adherent.
     *
     * @return Adherent adherent
     */
    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    /**
     * Set Adherent.
     *
     * @param Adherent $adherent
     *
     * @return $this
     */
    public function setAdherent(Adherent $adherent): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    public function __toString(): string
    {
        return 'HelloAsso  id#' . $this->getId() . ' at:' . ($this->getCreatedAt() ? $this->getCreatedAt()->format('d/m/Y H:i') : '?');
    }

    public function getErrors(): ?string
    {
        return $this->errors;
    }

    public function setErrors(?string $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getStatePayment(): ?string
    {
        return $this->statePayment;
    }

    public function setStatePayment(?string $statePayment): self
    {
        $this->statePayment = $statePayment;

        return $this;
    }

    public function getPaymentReceiptUrl(): ?string
    {
        return $this->paymentReceiptUrl;
    }

    public function setPaymentReceiptUrl(string $paymentReceiptUrl): self
    {
        $this->paymentReceiptUrl = $paymentReceiptUrl;

        return $this;
    }

    public function getHelloassoid(): ?int
    {
        return $this->helloassoid;
    }

    public function setHelloassoid(int $helloassoid): self
    {
        $this->helloassoid = $helloassoid;

        return $this;
    }

    public function getPayeremail(): ?string
    {
        return $this->payeremail;
    }

    public function setPayeremail(string $payeremail): self
    {
        $this->payeremail = $payeremail;

        return $this;
    }

    public function getPayerfirstname(): ?string
    {
        return $this->payerfirstname;
    }

    public function setPayerfirstname(string $payerfirstname): self
    {
        $this->payerfirstname = $payerfirstname;

        return $this;
    }

    public function getPayerlastname(): ?string
    {
        return $this->payerlastname;
    }

    public function setPayerlastname(string $payerlastname): self
    {
        $this->payerlastname = $payerlastname;

        return $this;
    }

    public function getUserfirstname(): ?string
    {
        return $this->userfirstname;
    }

    public function setUserfirstname(string $userfirstname): self
    {
        $this->userfirstname = $userfirstname;

        return $this;
    }

    public function getUserlastname(): ?string
    {
        return $this->userlastname;
    }

    public function setUserlastname(string $userlastname): self
    {
        $this->userlastname = $userlastname;

        return $this;
    }

    public function getUseremail(): ?string
    {
        return $this->useremail;
    }

    public function setUseremail(string $useremail): self
    {
        $this->useremail = $useremail;

        return $this;
    }

    public function getOperationState(): ?string
    {
        return $this->operationState;
    }

    public function setOperationState(string $operationState): self
    {
        if (!in_array($operationState, HelloassoStateEnum::getAvailableTypes())) {
            throw new \InvalidArgumentException('Status HelloAsso invalide !');
        }
        $this->operationState = $operationState;

        return $this;
    }

    /**
     * Get flux.
     *
     * @return
     */
    public function getFlux(): ?FluxInterface
    {
        return $this->flux;
    }

    /**
     * Set flux.
     *
     * @return $this
     */
    public function setFlux(FluxInterface $flux)
    {
        $this->flux = $flux;

        return $this;
    }

    public function getFullpayer(): ?string
    {
        return $this->payerlastname . ' ' . $this->payerfirstname . ' (' . $this->payeremail . ')';
    }

    public function getFulluser(): ?string
    {
        return $this->userlastname . ' ' . $this->userfirstname . ' (' . $this->useremail . ')';
    }

    public function getHistorical(): ?bool
    {
        return $this->historical;
    }

    public function setHistorical(?bool $historical): self
    {
        $this->historical = $historical;

        return $this;
    }
}
