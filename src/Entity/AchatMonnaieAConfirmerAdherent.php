<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSACTION
 *  - Demande d'achat de monnaie par un Adherent (chèque, virement, ...).
 *
 * @ORM\Entity
 */
class AchatMonnaieAConfirmerAdherent extends AchatMonnaieAConfirmer
{
    /**
     * @ORM\ManyToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_dest_id", referencedColumnName="id", nullable=true)
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_DEMANDE_ACHAT_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => ['siege'],
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
