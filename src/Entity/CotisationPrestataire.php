<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_VIEW')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * ).
 *
 * TRANSACTION
 *   - Cotisation(s) d'un prestataire
 *
 * @ORM\Entity
 */
class CotisationPrestataire extends Cotisation
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=true)
     */
    protected $expediteur;

    /**
     * @var DonPrestataire
     * @ORM\OneToOne(targetEntity="DonPrestataire", cascade={"persist"})
     * @ORM\JoinColumn(name="don_id", referencedColumnName="id", nullable=true)
     */
    protected $don;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_COTISATION_PRESTATAIRE;
    }

    /**
     * Get don.
     *
     * @return DonPrestataire
     */
    public function getDon(): ?DonPrestataire
    {
        return $this->don;
    }

    /**
     * Set don.
     *
     * @return $this
     */
    public function setDon(?DonPrestataire $don)
    {
        $this->don = $don;

        return $this;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getUsers()->toArray(),
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
