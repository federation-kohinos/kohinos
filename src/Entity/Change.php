<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * Change
 * Les changes correspondent à des opérations de changement de monnaie papier en monnaie numérique.
 *
 * Types de change :
 *
 *  - COMPTOIRS         =>     ADHERENTS                (Change de monnaie papier en monnaie numérique auprès des adhérents)
 *  - COMPTOIRS         =>     PRESTATAIRES             (Change de monnaie papier en monnaie numérique auprès des prestataires)
 *
 * @ORM\Entity
 */
class Change extends Flux
{
    const TYPE_CHANGE_ADHERENT_COMPTOIR = 'adherent_comptoir';
    const TYPE_CHANGE_PRESTATAIRE_COMPTOIR = 'prestataire_comptoir';

    /**
     * @ORM\OneToOne(targetEntity="Comptoir")
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id")
     */
    protected $destinataire;

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_CHANGE_ADHERENT_COMPTOIR => self::TYPE_CHANGE_ADHERENT_COMPTOIR,
            self::TYPE_CHANGE_PRESTATAIRE_COMPTOIR => self::TYPE_CHANGE_PRESTATAIRE_COMPTOIR,
        ];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_CHANGE;
    }

    public function getAllOperations($em)
    {
        return [
            OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
            OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_MLC, $this->getMontant()),
            OperationFactory::getOperation($this, $this->getDestinataire()->getGroupe()->getSiege(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
            OperationFactory::getOperation($this, $this->getDestinataire()->getGroupe()->getSiege(), CurrencyEnum::CURRENCY_MLC_NANTIE, -$this->getMontant()),
        ];
    }

    public function operate($em)
    {
        $this->getExpediteur()->addEcompte($this->getMontant());
        $this->getDestinataire()->addCompte($this->getMontant());
        $this->getDestinataire()->getGroupe()->getSiege()->addEcompteNantie($this->getMontant());
        $this->getDestinataire()->getGroupe()->getSiege()->removeCompteNantie($this->getMontant());

        return [$this->getExpediteur(), $this->getDestinataire(), $this->getDestinataire()->getGroupe()->getSiege()];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function getUsersToNotify()
    {
        return [];
    }
}
