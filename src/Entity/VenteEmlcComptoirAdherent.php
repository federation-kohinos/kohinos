<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VENTE MONNAIE NUMERIQUE
 *  - COMPTOIRS         =>     ADHERENTS                (Vente de monnaie numérique auprès des adhérents).
 *
 * @ORM\Entity()
 */
class VenteEmlcComptoirAdherent extends VenteEmlc
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_VENTE_EMLC_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
