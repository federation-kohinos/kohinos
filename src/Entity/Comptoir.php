<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\GeolocEntityTrait;
use App\Entity\EntityTrait\HasAccountsTrait;
use App\Entity\EntityTrait\HasCompteEntity;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use App\Flux\AccountableInterface;
use App\Flux\AccountableObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_VIEW') or is_granted('ROLE_API')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_LIST') or is_granted('ROLE_API')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_VIEW') or is_granted('ROLE_API')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_COMPTOIR_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ComptoirRepository")
 * @UniqueEntity(fields="name", message="Le comptoir avec ce nom existe déjà.")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="comptoir")
 */
class Comptoir extends AccountableObject implements AccountableInterface
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;
    use GeolocEntityTrait;
    use HasCompteEntity;
    use HasAccountsTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idmlc", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $idmlc;

    /**
     * @var string|null (champ libre)
     *
     * @ORM\Column(name="horaires", type="text", nullable=true)
     */
    private $horaires;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     * @Groups({"read", "write"})
     */
    protected $media;

    /**
     * @var Groupe
     *
     * @ORM\ManyToOne(targetEntity="Groupe", inversedBy="comptoirs")
     * @ORM\OrderBy({"name": "ASC"})
     */
    private $groupe;

    /**
     * @var ArrayCollection|User[]
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="comptoirsgeres", fetch="EXTRA_LAZY")
     */
    private $gestionnaires;

    /**
     * @var ArrayCollection|ContactComptoir[]
     * @ORM\OneToMany(targetEntity="ContactComptoir", cascade={"persist"}, mappedBy="comptoir")
     * @Groups({"read", "write"})
     * @MaxDepth(1)
     */
    private $contacts;

    /**
     * @var ArrayCollection|AccountComptoir[]
     * @ORM\OneToMany(targetEntity="AccountComptoir", mappedBy="comptoir")
     */
    private $accounts;

    public function __construct()
    {
        $this->gestionnaires = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get idmlc.
     *
     * @return
     */
    public function getIdmlc(): ?string
    {
        return $this->idmlc;
    }

    /**
     * Set idmlc.
     *
     * @return $this
     */
    public function setIdmlc(string $idmlc): self
    {
        $this->idmlc = $idmlc;

        return $this;
    }
    
    /**
     * Get horaires.
     *
     * @return string Horaires
     */
    public function getHoraires(): ?string
    {
        return $this->horaires;
    }

    /**
     * Set horaires.
     *
     * @return $this
     */
    public function setHoraires(?string $horaires): self
    {
        $this->horaires = $horaires;

        return $this;
    }

    /**
     * Get media.
     *
     * @return
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set media.
     *
     * @return $this
     */
    public function setMedia($media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @param Groupe|null $groupes
     *
     * @return $this
     */
    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return Groupe|null
     */
    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getGestionnaires()
    {
        return $this->gestionnaires;
    }

    /**
     * @param User[]|ArrayCollection
     *
     * @return $this
     */
    public function setGestionnaires($gestionnaires): self
    {
        $this->gestionnaires = $gestionnaires;

        return $this;
    }

    /**
     * @param User $gestionnaire
     *
     * @return $this
     */
    public function addGestionnaire(User $gestionnaire): self
    {
        if (!$this->gestionnaires->contains($gestionnaire)) {
            $this->gestionnaires[] = $gestionnaire;
        }

        return $this;
    }

    /**
     * @param User $gestionnaire
     *
     * @return $this
     */
    public function removeGestionnaire(User $gestionnaire): self
    {
        if ($this->gestionnaires->contains($gestionnaire)) {
            $this->gestionnaires->removeElement($gestionnaire);
        }

        return $this;
    }

    /**
     * getcontacts.
     *
     * @return ArrayCollection contact
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * setContacts.
     *
     * @param [type] $contacts [description]
     */
    public function setContacts($contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @param ContactComptoir $contact
     *
     * @return $this
     */
    public function addContact(ContactComptoir $contact): self
    {
        if (is_null($this->contacts)) {
            $this->contacts = new ArrayCollection();
        }
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setComptoir($this);
        }

        return $this;
    }

    /**
     * @param ContactComptoir $contact
     *
     * @return $this
     */
    public function removeContact(ContactComptoir $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            $contact->setComptoir(null);
        }

        return $this;
    }

    public function getContactsString()
    {
        return join(' - ', array_map(function ($contact) {
            $return = ucwords($contact->getName()) . ($contact->getEmail() == $contact->getName() ? '' : (':' . $contact->getEmail()));
            $return .= $contact->getTel() ? ',' . $contact->getTel() : '';

            return $return;
        }, $this->contacts->getValues()));
    }

    public function __toString(): string
    {
        return !empty($this->name) ? $this->name : 'Comptoir';
    }
}
