<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VENTE
 *  - COMPTOIRS         =>     ADHERENTS                (Diffusion de monnaie papier auprès des adhérents).
 *
 * @ORM\Entity()
 */
class VenteComptoirAdherent extends Vente
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_VENTE_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
