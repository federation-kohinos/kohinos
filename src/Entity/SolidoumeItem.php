<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Repository\SolidoumeItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @Gedmo\Loggable()
 * @ORM\Entity(repositoryClass=SolidoumeItemRepository::class)
 */
class SolidoumeItem
{
    use TimestampableEntity;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @ORM\Column(type="float")
     * @Gedmo\Versioned
     */
    private $amount;

    /**
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned
     */
    private $isRecurrent;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned
     */
    private $paiementDate;

    /**
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned
     */
    private $isDonation;

    /**
     * @ORM\ManyToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id", nullable=false)
     */
    private $adherent;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Gedmo\Versioned
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $comments;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastMonthPayed;

    /**
     * @var bool
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     * @Gedmo\Versioned
     */
    protected $enabled = true;

    /**
     * @var bool
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     */
    protected $quizzSended = false;

    public function getId()
    {
        return $this->id;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function isQuizzSended(): bool
    {
        return $this->quizzSended;
    }

    public function setQuizzSended(bool $quizzSended)
    {
        $this->quizzSended = $quizzSended;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getIsRecurrent(): ?bool
    {
        return $this->isRecurrent;
    }

    public function setIsRecurrent(bool $isRecurrent): self
    {
        $this->isRecurrent = $isRecurrent;

        return $this;
    }

    public function getPaiementDate(): ?int
    {
        return $this->paiementDate;
    }

    public function setPaiementDate(int $paiementDate): self
    {
        $this->paiementDate = $paiementDate;

        return $this;
    }

    public function getIsDonation(): ?bool
    {
        return $this->isDonation;
    }

    public function setIsDonation(bool $isDonation): self
    {
        $this->isDonation = $isDonation;

        return $this;
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function setAdherent(Adherent $adherent): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getLastMonthPayed(): ?int
    {
        return $this->lastMonthPayed;
    }

    public function setLastMonthPayed(?int $lastMonthPayed): self
    {
        $this->lastMonthPayed = $lastMonthPayed;

        return $this;
    }

    public function toArray()
    {
        return ['item' => [
            'id' => $this->getId(),
            'amount' => $this->getAmount(),
            'adherent' => $this->getAdherent()->__toString(),
            'paiementDate' => $this->getPaiementDate(),
            'lastMonthPayed' => $this->getLastMonthPayed(),
            'don' => $this->getIsDonation(),
            'recurrent' => $this->getIsRecurrent(),
            'enabled' => $this->isEnabled()
            ]
        ];

    }

    public function __toString()
    {
        return 'SolidoumeItem Montant : ' . $this->getAmount() . ', adherent ' . $this->getAdherent()->__toString() . ', paiementDate : ' . $this->getPaiementDate() . ', lastMonthPayed : ' . $this->getLastMonthPayed() . ', don : ' . $this->getIsDonation() . ', recurrent : ' . $this->getIsRecurrent() . ', enabled : ' . $this->isEnabled();
    }
}
