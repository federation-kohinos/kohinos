<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vente => (Diffusion de monnaie papier auprès des adhérents).
 *
 * Types de vente :
 *
 *   - COMPTOIR     =>    ADHERENTS         (Vente à un adherent)
 *   - COMPTOIR     =>    PRESTATAIRES      (Vente à un prestataire)
 *
 * @ORM\Entity
 */
class Vente extends Flux
{
    const TYPE_VENTE_ADHERENT = 'vente_adherent';
    const TYPE_VENTE_PRESTATAIRE = 'vente_prestataire';

    /**
     * @ORM\OneToOne(targetEntity="Comptoir")
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id")
     */
    protected $expediteur;

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_VENTE_ADHERENT => self::TYPE_VENTE_ADHERENT,
            self::TYPE_VENTE_PRESTATAIRE => self::TYPE_VENTE_PRESTATAIRE,
        ];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_VENTE;
    }

    public function getAllOperations($em)
    {
        return [
            OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_MLC, -$this->getMontant()),
            OperationFactory::getOperation($this, $this->getExpediteur()->getGroupe()->getSiege(), CurrencyEnum::CURRENCY_MLC_NANTIE, $this->getMontant()),
        ];
    }

    public function operate($em)
    {
        $compteExp = $this->getExpediteur()->getCompte() - $this->getMontant();
        if ($compteExp < 0) {
            throw new \Exception('[FLUX] Vente impossible ! Montant supérieur au solde du comptoir !');
        } else {
            $this->getExpediteur()->removeCompte($this->getMontant());
            $this->getExpediteur()->setUpdatedAt(new \DateTime());
            $this->getExpediteur()->getGroupe()->getSiege()->addCompteNantie($this->getMontant());

            return [$this->getExpediteur(), $this->getExpediteur()->getGroupe()->getSiege()];
        }

        return [];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function getUsersToNotify()
    {
        return [];
    }
}
