<?php

namespace App\Entity;

use App\Entity\EntityTrait\EnablableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="import")
 */
class Import
{
    use EnablableEntityTrait;
    use TimestampableEntity;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    protected $media;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="imports", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $errors;

    /**
     * @var string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $success;

    /**
     * @var string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $warnings;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="nbentityadded", type="integer", nullable=true)
     */
    private $nbentityadded;

    /**
     * @var string
     *
     * @ORM\Column(name="nbentityerror", type="integer", nullable=true)
     */
    private $nbentityerror;

    /**
     * @var string
     *
     * @ORM\Column(name="sendemail", type="string", length=100)
     */
    private $sendemail;

    /**
     * @var string
     *
     * @ORM\Column(name="test", type="boolean", nullable=false, options={"default": false})
     */
    private $test;

    public function __construct()
    {
        $this->setEnabled(false);
        $this->sendemail = false;
        $this->test = false;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     *
     * @return $this
     */
    public function setUser(?User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get media.
     *
     * @return
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set media.
     *
     * @return $this
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get errors.
     *
     * @return
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set errors.
     *
     * @return $this
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get success.
     *
     * @return
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set success.
     *
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get warnings.
     *
     * @return
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * Set warnings.
     *
     * @return $this
     */
    public function setWarnings($warnings)
    {
        $this->warnings = $warnings;

        return $this;
    }

    /**
     * Get type.
     *
     * @return
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Get sendemail.
     *
     * @return
     */
    public function getSendemail(): bool
    {
        return $this->sendemail;
    }

    /**
     * Set sendemail.
     *
     * @return $this
     */
    public function setSendemail(bool $sendemail): self
    {
        $this->sendemail = $sendemail;

        return $this;
    }

    /**
     * Get test.
     *
     * @return
     */
    public function getTest(): bool
    {
        return $this->test;
    }

    /**
     * Set test.
     *
     * @return $this
     */
    public function setTest(bool $test): self
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get nbentityadded.
     *
     * @return
     */
    public function getNbentityadded()
    {
        return $this->nbentityadded;
    }

    /**
     * Set nbentityadded.
     *
     * @return $this
     */
    public function setNbentityadded($nbentityadded)
    {
        $this->nbentityadded = $nbentityadded;

        return $this;
    }

    /**
     * Get nbentityerror.
     *
     * @return
     */
    public function getNbentityerror()
    {
        return $this->nbentityerror;
    }

    /**
     * Set nbentityerror.
     *
     * @return $this
     */
    public function setNbentityerror($nbentityerror)
    {
        $this->nbentityerror = $nbentityerror;

        return $this;
    }

    /**
     * Set type.
     *
     * @return $this
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString(): string
    {
        return 'Import de ' . $this->getType() . ' du ' . ($this->getCreatedAt() ? $this->getCreatedAt()->format('d/m/Y H:i') : '?');
    }
}
