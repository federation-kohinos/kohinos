<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vente => (Vente de monnaie numérique auprès des adhérents/prestataires).
 *
 * Types de vente d'emlc :
 *
 *   - COMPTOIR     =>    ADHERENTS         (Vente d'emlc à un adherent)
 *   - COMPTOIR     =>    PRESTATAIRES      (Vente d'emlc à un prestataire)
 *
 * @ORM\Entity
 */
class VenteEmlc extends Flux
{
    const TYPE_VENTE_EMLC_ADHERENT = 'vente_emlc_adherent';
    const TYPE_VENTE_EMLC_PRESTATAIRE = 'vente_emlc_prestataire';

    /**
     * @ORM\OneToOne(targetEntity="Comptoir")
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id")
     */
    protected $expediteur;

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_VENTE_EMLC_ADHERENT => self::TYPE_VENTE_EMLC_ADHERENT,
            self::TYPE_VENTE_EMLC_PRESTATAIRE => self::TYPE_VENTE_EMLC_PRESTATAIRE,
        ];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_VENTE_EMLC;
    }

    public function getAllOperations($em)
    {
        return [
            OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
            OperationFactory::getOperation($this, $this->getExpediteur()->getGroupe()->getSiege(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
        ];
    }

    public function operate($em)
    {
        if (empty($this->getExpediteur()->getGroupe())) {
            throw new \Exception("[FLUX] Vente e-mlc impossible ! Le comptoir n'a pas de groupe local !");
        }
        $this->getExpediteur()->getGroupe()->getSiege()->addEcompteNantie($this->getMontant());
        $this->getDestinataire()->addEcompte($this->getMontant());

        return [$this->getDestinataire(), $this->getDestinataire()->getGroupe()->getSiege()];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function getUsersToNotify()
    {
        return [];
    }
}
