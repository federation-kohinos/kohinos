<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Enum\MoyenEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * Don d'un utilisateur.
 *
 * @ORM\Entity
 */
class Don extends Flux
{
    const TYPE_DON_ADHERENT = 'don_adherent';
    const TYPE_DON_PRESTATAIRE = 'don_prestataire';

    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_dest_id", referencedColumnName="id", nullable=true)
     */
    protected $destinataire;

    /**
     * Constructeur.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_DON_ADHERENT => self::TYPE_DON_ADHERENT,
            self::TYPE_DON_PRESTATAIRE => self::TYPE_DON_PRESTATAIRE,
        ];
    }

    public function getAllOperations($em)
    {
        if (MoyenEnum::MOYEN_EMLC == $this->getMoyen()) {
            return [
                OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_EMLC, -$this->getMontant()),
                OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_EMLC, $this->getMontant()),
            ];
        } elseif (MoyenEnum::MOYEN_CB == $this->getMoyen()) {
            return [
                OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_EURO, $this->getMontant()),
            ];
        }

        return [];
    }

    public function operate($em)
    {
        if (MoyenEnum::MOYEN_MLC == $this->getMoyen()) {
            $compteExp = $this->getExpediteur()->getEcompte() - $this->getMontant();
            if ($compteExp < 0) {
                throw new \Exception('[FLUX] Opération impossible ! Montant supérieur au solde de monnaie éléctronique !');
            } else {
                $this->getExpediteur()->removeEcompte($this->getMontant());
                $this->getDestinataire()->addEcompte($this->getMontant());

                return [$this->getExpediteur(), $this->getDestinataire()];
            }
        }
        // Pas d'opération si la cotisation se fait par CB
        return [];
    }

    public function getUsersToNotify()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_DON;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function __toString(): string
    {
        if (empty($this->getDestinataire()) || empty($this->getExpediteur()) || null === $this->getMontant()) {
            return '[FLUX] Visualisation impossible ! Destinataire / Expéditeur et/ou montant manquant(s) !';
        }

        return ucwords(str_replace('_', ' ', $this->getType())) . ' : ' . $this->getExpediteur() . ' : ' . $this->getMontant() . '€' . ($this->getCreatedAt() ? ' (le ' . $this->getCreatedAt()->format('d/m/Y H:i') . ')' : '');
    }
}
