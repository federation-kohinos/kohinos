<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Payment as BasePayment;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 * @ORM\Table(name="payment")
 */
class Payment extends BasePayment
{
    const TYPE_ACHAT_MONNAIE_ADHERENT = 'achat_monnaie_adherent';
    const TYPE_ACHAT_MONNAIE_PRESTA = 'achat_monnaie_presta';
    const TYPE_COTISATION_ADHERENT = 'cotisation_adherent';
    const TYPE_COTISATION_PRESTA = 'cotisation_presta';
    const TYPE_ADHESION = 'adhesion';

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $status;

    /**
     * @var string|null
     *                  JSON array of 'Flux' to persist if payment valid
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $flux_data;

    /**
     * @var string|null
     *                  Extra data to persist if payment valid
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $extra_data;

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Payment
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getFluxData(): ?string
    {
        return $this->flux_data;
    }

    /**
     * @param string $flux_data
     *
     * @return Payment
     */
    public function setFluxData(string $flux_data): self
    {
        $this->flux_data = $flux_data;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtraData(): ?string
    {
        return $this->extra_data;
    }

    /**
     * @param string $extra_data
     *
     * @return Payment
     */
    public function setExtraData(string $extra_data): self
    {
        $this->extra_data = $extra_data;

        return $this;
    }
}
