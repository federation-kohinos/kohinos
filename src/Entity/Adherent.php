<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\GeolocEntityTrait;
use App\Entity\EntityTrait\HasAccountsTrait;
use App\Entity\EntityTrait\HasEcompteEntity;
use App\Flux\AccountableInterface;
use App\Flux\AccountableObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_ADHERENT_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_ADHERENT_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_ADHERENT_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_ADHERENT_GERER_LIST')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_ADHERENT_GERER_EDIT') or object.user == user"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * ).
 *
 * @ORM\Entity(repositoryClass="App\Repository\AdherentRepository")
 * @ORM\Table(name="adherent")
 * @ORM\HasLifecycleCallbacks()
 */
class Adherent extends AccountableObject implements AccountableInterface
{
    use EnablableEntityTrait;
    use TimestampableEntity;
    use GeolocEntityTrait;
    use HasEcompteEntity;
    use HasAccountsTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idmlc", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $idmlc;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", cascade={"all"}, mappedBy="adherent", fetch="LAZY")
     */
    protected $user;

    /**
     * @var Groupe
     *
     * @ORM\ManyToOne(targetEntity="Groupe", inversedBy="adherents", fetch="LAZY")
     */
    private $groupe;

    /**
     * @var ArrayCollection|AccountAdherent[]
     * @ORM\OneToMany(targetEntity="AccountAdherent", mappedBy="adherent")
     */
    private $accounts;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get idmlc.
     *
     * @return
     */
    public function getIdmlc(): ?string
    {
        return $this->idmlc;
    }

    /**
     * Set idmlc.
     *
     * @return $this
     */
    public function setIdmlc(string $idmlc): self
    {
        $this->idmlc = $idmlc;

        return $this;
    }

    public function getCompte(): float
    {
        return $this->getEcompte();
    }

    public function setCompte(float $ecompte): self
    {
        $this->setEcompte($ecompte);

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Prestataire
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param Groupe|null $groupe
     *
     * @return $this
     */
    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return Groupe|null
     */
    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function isEnabled(): bool
    {
        return $this->getUser()->isEnabled();
    }

    public function setEnabled($enabled): self
    {
        $this->getUser()->setEnabled($enabled);

        return $this;
    }

    public function getName(): string
    {
        return $this->__toString();
    }

    public function __toString(): string
    {
        if (!empty($this->getUser())) {
            if (!empty($this->getUser()->getLastname() . $this->getUser()->getFirstname())) {
                return $this->getUser()->getLastname() . ' ' . $this->getUser()->getFirstname();
            }
            if (!empty($this->getUser()->getUsername())) {
                return $this->getUser()->getUsername();
            }
            if (!empty($this->getUser()->getEmail())) {
                return $this->getUser()->getEmail();
            }
        }

        return 'Adhérent xxx';
    }

    public function getEmail(): ?string
    {
        if (!empty($this->getUser())) {
            return $this->getUser()->getEmail();
        }
        return '';
    }

    public function getFullname(): string
    {
        $return = '';
        if (!empty($this->getUser())) {
            if (!empty($this->getUser()->getLastname() . $this->getUser()->getFirstname())) {
                $return = $this->getUser()->getLastname() . ' ' . $this->getUser()->getFirstname();
                if (!empty($this->getUser()->getEmail())) {
                    $return .= '(' . $this->getUser()->getEmail() . ')';
                }
            } elseif (!empty($this->getUser()->getEmail())) {
                $return = $this->getUser()->getEmail();
            }

            return $return;
        }

        return 'Adhérent xxx';
    }
}
