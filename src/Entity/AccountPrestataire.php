<?php

namespace App\Entity;

use App\Flux\AccountableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="account_prestataire", uniqueConstraints={@ORM\UniqueConstraint(name="prestatairecurrency", columns={"prestataire_id", "currency"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"prestataire", "currency"},
 *     errorPath="prestataire",
 *     message="Le compte du prestataire existe déjà."
 * )
 */
class AccountPrestataire extends Account
{
    /**
     * @ORM\ManyToOne(targetEntity=Prestataire::class, inversedBy="accounts", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=false)
     */
    private $prestataire;

    /**
     * @var ArrayCollection|OperationPrestataire[]
     * @ORM\OneToMany(targetEntity=OperationPrestataire::class, mappedBy="account")
     */
    protected $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getPrestataire(): ?Prestataire
    {
        return $this->prestataire;
    }

    public function setPrestataire(Prestataire $prestataire): self
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    public function setAccountableObject(AccountableInterface $object): self
    {
        return $this->setPrestataire($object);
    }

    public function getAccountableObject(): AccountableInterface
    {
        return $this->getPrestataire();
    }
}
