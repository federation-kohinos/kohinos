<?php

namespace App\Entity;

use App\Flux\AccountableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountComptoirRepository")
 * @ORM\Table(name="account_comptoir", uniqueConstraints={@ORM\UniqueConstraint(name="comptoircurrency", columns={"comptoir_id", "currency"})})
 * @UniqueEntity(
 *     fields={"comptoir", "currency"},
 *     errorPath="comptoir",
 *     message="Le compte du comptoir existe déjà."
 * )
 */
class AccountComptoir extends Account
{
    /**
     * @ORM\ManyToOne(targetEntity=Comptoir::class, inversedBy="accounts", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id", nullable=false)
     */
    private $comptoir;

    /**
     * @var ArrayCollection|OperationComptoir[]
     * @ORM\OneToMany(targetEntity=OperationComptoir::class, mappedBy="account")
     */
    protected $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getComptoir(): ?Comptoir
    {
        return $this->comptoir;
    }

    public function setComptoir(Comptoir $comptoir): self
    {
        $this->comptoir = $comptoir;

        return $this;
    }

    public function setAccountableObject(AccountableInterface $object): self
    {
        return $this->setComptoir($object);
    }

    public function getAccountableObject(): AccountableInterface
    {
        return $this->getComptoir();
    }
}
