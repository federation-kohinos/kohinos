<?php

namespace App\Entity;

use App\Flux\AccountableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountGroupeRepository")
 * @ORM\Table(name="account_groupe", uniqueConstraints={@ORM\UniqueConstraint(name="groupecurrency", columns={"groupe_id", "currency"})}) )
 * @UniqueEntity(
 *     fields={"groupe", "currency"},
 *     errorPath="groupe",
 *     message="Le compte du groupe existe déjà."
 * )
 */
class AccountGroupe extends Account
{
    /**
     * @ORM\ManyToOne(targetEntity=Groupe::class, inversedBy="accounts", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="groupe_id", referencedColumnName="id", nullable=false)
     */
    private $groupe;

    /**
     * @var ArrayCollection|OperationGroupe[]
     * @ORM\OneToMany(targetEntity=OperationGroupe::class, mappedBy="account")
     */
    protected $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function setAccountableObject(AccountableInterface $object): self
    {
        return $this->setGroupe($object);
    }

    public function getAccountableObject(): AccountableInterface
    {
        return $this->getGroupe();
    }
}
