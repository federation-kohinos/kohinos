<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\ContactEmailTelTrait;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * ).
 *
 * @ORM\Entity
 * @ORM\Table(name="contact_prestataire")
 */
class ContactPrestataire
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;
    use ContactEmailTelTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var Prestataire
     *
     * @ORM\ManyToOne(targetEntity="Prestataire", cascade={"persist"}, inversedBy="contacts")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=false)
     */
    private $prestataire;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get prestataire.
     *
     * @return
     */
    public function getPrestataire(): Prestataire
    {
        return $this->prestataire;
    }

    /**
     * Set prestataire.
     *
     * @return $this
     */
    public function setPrestataire($prestataire): self
    {
        $this->prestataire = $prestataire;

        return $this;
    }
}
