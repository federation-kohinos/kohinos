<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TRANSACTION
 *   - ADHERENTS        =>    PRESTATAIRES      (Paiement numérique).
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TransactionAdherentPrestataire extends Transaction
{
    /**
     * @ORM\ManyToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $expediteur;

    /**
     * @ORM\ManyToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSACTION_ADHERENT_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => [$this->getExpediteur()->getUser()],
            'destinataires' => array_merge($this->getDestinataire()->getUsers()->toArray(), $this->getDestinataire()->getCaissiers()->toArray()),
        ];
    }
}
