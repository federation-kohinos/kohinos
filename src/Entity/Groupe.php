<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\HasAccountsTrait;
use App\Entity\EntityTrait\HasCompteEntity;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use App\Flux\AccountableInterface;
use App\Flux\AccountableObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_GROUPE_GERER_VIEW') or is_granted('ROLE_API')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_GROUPE_GERER_LIST') or is_granted('ROLE_API')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_GROUPE_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_GROUPE_GERER_VIEW') or is_granted('ROLE_API')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_GROUPE_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity
 * @UniqueEntity(fields="name", message="Le groupe avec ce nom existe déjà.")
 * @ORM\Table(name="groupe")
 * @ORM\HasLifecycleCallbacks()
 */
class Groupe extends AccountableObject implements AccountableInterface
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;
    use HasCompteEntity;
    use HasAccountsTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idmlc", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $idmlc;

    /**
     * @var Siege
     *
     * @ORM\ManyToOne(targetEntity="Siege", inversedBy="groupes")
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id", nullable=false)
     */
    private $siege;

    /**
     * @var ArrayCollection|Comptoir[]
     * @ORM\OneToMany(targetEntity="Comptoir", mappedBy="groupe", fetch="LAZY")
     * @ORM\OrderBy({"name": "ASC"})
     */
    private $comptoirs;

    /**
     * @var ArrayCollection|Prestataire[]
     * @ORM\OneToMany(targetEntity="Prestataire", mappedBy="groupe", fetch="LAZY")
     * @ORM\OrderBy({"raison": "ASC"})
     */
    private $prestataires;

    /**
     * @var ArrayCollection|Groupeprestataire[]
     * @ORM\OneToMany(targetEntity="Groupeprestataire", mappedBy="groupe", fetch="LAZY")
     * @ORM\OrderBy({"name": "ASC"})
     */
    private $groupeprestataires;

    /**
     * @var ArrayCollection|Adherent[]
     * @ORM\OneToMany(targetEntity="Adherent", mappedBy="groupe", fetch="LAZY")
     * @ORM\OrderBy({"updatedAt": "ASC"})
     */
    private $adherents;

    /**
     * @var ArrayCollection|User[]
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groupesgeres", fetch="LAZY")
     */
    private $gestionnaires;

    /**
     * @var ArrayCollection|User[]
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groupecontactsgeres", fetch="LAZY")
     */
    private $contacts;

    /**
     * @var ArrayCollection|User[]
     * @ORM\ManyToMany(targetEntity="User", mappedBy="groupetresoriersgeres", fetch="LAZY")
     */
    private $tresoriers;

    /**
     * @var ArrayCollection|AccountGroupe[]
     * @ORM\OneToMany(targetEntity="AccountGroupe", mappedBy="groupe", fetch="LAZY")
     */
    private $accounts;

    public function __construct()
    {
        $this->comptoirs = new ArrayCollection();
        $this->prestataires = new ArrayCollection();
        $this->adherents = new ArrayCollection();
        $this->groupeprestataires = new ArrayCollection();
        $this->gestionnaires = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->tresoriers = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    /**
     * Get siege.
     *
     * @return Siege
     */
    public function getSiege()
    {
        return $this->siege;
    }

    /**
     * Set siege.
     *
     * @return $this
     */
    public function setSiege($siege)
    {
        $this->siege = $siege;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get idmlc.
     *
     * @return
     */
    public function getIdmlc(): ?string
    {
        return $this->idmlc;
    }

    /**
     * Set idmlc.
     *
     * @return $this
     */
    public function setIdmlc(string $idmlc): self
    {
        $this->idmlc = $idmlc;

        return $this;
    }

    /**
     * Set id.
     *
     * @param string $id [description]
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Comptoir[]|ArrayCollection
     */
    public function getComptoirs()
    {
        return $this->comptoirs;
    }

    /**
     * @param Comptoir $comptoir
     *
     * @return $this
     */
    public function addComptoir(Comptoir $comptoir): self
    {
        if (!$this->comptoirs->contains($comptoir)) {
            $this->comptoirs[] = $comptoir;
            $comptoir->setGroupe($this);
        }

        return $this;
    }

    /**
     * @param Comptoir $comptoir
     *
     * @return $this
     */
    public function removeComptoir(Comptoir $comptoir): self
    {
        if ($this->comptoirs->contains($comptoir)) {
            $this->comptoirs->removeElement($comptoir);
            $comptoir->setGroupe(null);
        }

        return $this;
    }

    /**
     * @return Prestataire[]|ArrayCollection
     */
    public function getPrestataires()
    {
        return $this->prestataires;
    }

    /**
     * @param Prestataire $prestataire
     *
     * @return $this
     */
    public function addPrestataire(Prestataire $prestataire): self
    {
        if (!$this->prestataires->contains($prestataire)) {
            $this->prestataires[] = $prestataire;
        }

        return $this;
    }

    /**
     * @param Prestataire $prestataire
     *
     * @return $this
     */
    public function removePrestataire(Prestataire $prestataire): self
    {
        if ($this->prestataires->contains($prestataire)) {
            $this->prestataires->removeElement($prestataire);
        }

        return $this;
    }

    /**
     * @return Adherent[]|ArrayCollection
     */
    public function getAdherents()
    {
        return $this->adherents;
    }

    /**
     * @param Adherent $adherent
     *
     * @return $this
     */
    public function addAdherent(Adherent $adherent): self
    {
        if (!$this->adherents->contains($adherent)) {
            $this->adherents[] = $adherent;
            $adherent->setGroupe($this);
        }

        return $this;
    }

    /**
     * @param Adherent $adherent
     *
     * @return $this
     */
    public function removeAdherent(Adherent $adherent): self
    {
        if ($this->adherents->contains($adherent)) {
            $this->adherents->removeElement($adherent);
            $adherent->setGroupe(null);
        }

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getGestionnaires()
    {
        return $this->gestionnaires;
    }

    /**
     * @param User[]|ArrayCollection
     *
     * @return $this
     */
    public function setGestionnaires($gestionnaires): self
    {
        $this->gestionnaires = $gestionnaires;

        return $this;
    }

    /**
     * @param User $gestionnaire
     *
     * @return $this
     */
    public function addGestionnaire(User $gestionnaire): self
    {
        if (!$this->gestionnaires->contains($gestionnaire)) {
            $this->gestionnaires[] = $gestionnaire;
        }

        return $this;
    }

    /**
     * @param User $gestionnaire
     *
     * @return $this
     */
    public function removeGestionnaire(User $gestionnaire): self
    {
        if ($this->gestionnaires->contains($gestionnaire)) {
            $this->gestionnaires->removeElement($gestionnaire);
        }

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param User[]|ArrayCollection
     *
     * @return $this
     */
    public function setContacts($contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @param User $contact
     *
     * @return $this
     */
    public function addContact(User $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
        }

        return $this;
    }

    /**
     * @param User $contact
     *
     * @return $this
     */
    public function removeContact(User $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
        }

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getTresoriers()
    {
        return $this->tresoriers;
    }

    /**
     * @param User[]|ArrayCollection
     *
     * @return $this
     */
    public function setTresoriers($tresoriers): self
    {
        $this->tresoriers = $tresoriers;

        return $this;
    }

    /**
     * @param User $tresorier
     *
     * @return $this
     */
    public function addTresorier(User $tresorier): self
    {
        if (!$this->tresoriers->contains($tresorier)) {
            $this->tresoriers[] = $tresorier;
        }

        return $this;
    }

    /**
     * @param User $tresorier
     *
     * @return $this
     */
    public function removeTresorier(User $tresorier): self
    {
        if ($this->tresoriers->contains($tresorier)) {
            $this->tresoriers->removeElement($tresorier);
        }

        return $this;
    }

    /**
     * @return Amap[]|ArrayCollection
     */
    public function getGroupeprestataires()
    {
        return $this->groupeprestataires;
    }

    /**
     * @param Amap $amap
     *
     * @return $this
     */
    public function addGroupeprestataire(Groupeprestataire $groupeprestataire): self
    {
        if (!$this->groupeprestataires->contains($groupeprestataire)) {
            $this->groupeprestataires[] = $groupeprestataire;
            $groupeprestataire->addGroupe($this);
        }

        return $this;
    }

    /**
     * @param Amap $amap
     *
     * @return $this
     */
    public function removeGroupeprestataire(Groupeprestataire $groupeprestataires): self
    {
        if ($this->groupeprestataires->contains($groupeprestataire)) {
            $this->groupeprestataires->removeElement($groupeprestataire);
            $groupeprestataire->removeGroupe($this);
        }

        return $this;
    }

    public function getComptoirsCount()
    {
        return $this->getComptoirs()->count();
    }

    public function getPrestatairesCount()
    {
        return $this->getPrestataires()->count();
    }

    public function getAdherentsCount()
    {
        return $this->getAdherents()->count();
    }

    public function __toString(): string
    {
        return !empty($this->getName()) ? $this->getName() : 'Groupe';
    }
}
