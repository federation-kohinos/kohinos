<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Enum\MoyenEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reconversion
 *  - PRESTATAIRES      =>     SIEGE.
 *
 * @ORM\Entity()
 */
class Reconversion extends Flux
{
    const TYPE_RECONVERSION_PRESTATAIRE = 'reconversion_prestataire';

    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Siege")
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @var bool
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     * @Groups({"read", "write"})
     */
    protected $reconverti = false;

    public function __construct()
    {
        parent::__construct();
        $this->reconverti = false;
    }

    public function getReconverti(): bool
    {
        return $this->reconverti;
    }

    public function setReconverti(bool $reconverti)
    {
        $this->reconverti = $reconverti;

        return $this;
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_RECONVERSION;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPE_RECONVERSION_PRESTATAIRE;
    }

    public function getAllOperations($em)
    {
        $operations = [
            OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_EMLC, -$this->getMontant()),
        ];
        $taux = $this->getExpediteur()->getTauxreconversion();
        if ($taux < 0 || null === $taux) {
            $taux = $em->getRepository(GlobalParameter::class)->val(GlobalParameter::RECONVERSION_PRESTATAIRE);
        }
        if ($taux > 0) {
            // On calcul les montants à virer au prestataire et la commission
            $montantAVirer = round(($this->getMontant() - ($this->getMontant() * ($taux / 100))), 2);
            $montantCommission = round(($this->getMontant() - $montantAVirer), 2);
            $this->setTauxreconversion($taux);

            // On récupère le presta qui reçoit les commissions
            $mlcPrestataire = $em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]);

            $operations[] = OperationFactory::getOperation($this, $mlcPrestataire, CurrencyEnum::CURRENCY_EURO, $montantCommission);
        }
        $operations[] = OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_EMLC, -$this->getMontant());

        return $operations;
    }

    public function operate($em)
    {
        $this->getExpediteur()->removeEcompte($this->getMontant());

        // On récupère le taux de reconversion (du prestataire s'il est précisé sinon par défaut dans la configuration globale)
        $taux = $this->getExpediteur()->getTauxreconversion();
        if ($taux < 0 || null === $taux) {
            $taux = $em->getRepository(GlobalParameter::class)->val(GlobalParameter::RECONVERSION_PRESTATAIRE);
        } else {
            // On calcul les montants à virer au prestataire et la commission
            $montantAVirer = round(($this->getMontant() - ($this->getMontant() * ($taux / 100))), 2);
            $montantCommission = round(($this->getMontant() - $montantAVirer), 2);
            $this->setMontant($montantAVirer);
            $this->setTauxreconversion($taux);

            // On récupère le presta qui reçoit les commissions
            $mlcPrestataire = $em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]);

            //Création d'un flux supplémentaire pour représenter la commission prise au prestataire et transféré à l'association qui gère la MLC
            // $fluxCommission = new CommissionPrestataireMLC();
            // $fluxCommission->setExpediteur($this->getExpediteur());
            // $fluxCommission->setOperateur($this->getOperateur());
            // $fluxCommission->setRole($this->getRole());
            // $fluxCommission->setDestinataire($mlcPrestataire);
            // $fluxCommission->setMontant($montantCommission);
            // $fluxCommission->setReference('Commission de reconversion');
            // $fluxCommission->setMoyen(MoyenEnum::MOYEN_MLC);
            // $fluxCommission->setTauxreconversion($taux);

            $mlcPrestataire->addEcompte($montantCommission);

            return [$this->getExpediteur(), $this->getDestinataire(), $fluxCommission, $mlcPrestataire];
        }

        return [$this->getExpediteur(), $this->getDestinataire()];
    }

    // public function operate($em)
    // {
    //     $this->getExpediteur()->removeEcompte($this->getMontant());

    //     // On récupère le taux de reconversion (du prestataire s'il est précisé sinon par défaut dans la configuration globale)
    //     $taux = $this->getExpediteur()->getTauxreconversion();
    //     if ($taux < 0 || null === $taux) {
    //         $taux = $em->getRepository(GlobalParameter::class)->val(GlobalParameter::RECONVERSION_PRESTATAIRE);
    //     } else {
    //         // On calcul les montants à virer au prestataire et la commission
    //         $montantAVirer = round(($this->getMontant() - ($this->getMontant() * ($taux / 100))), 2);
    //         $montantCommission = round(($this->getMontant() - $montantAVirer), 2);
    //         $this->setMontant($montantAVirer);
    //         $this->setTauxreconversion($taux);

    //         // On récupère le presta qui reçoit les commissions
    //         $mlcPrestataire = $em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]);

    //         //Création d'un flux supplémentaire pour représenter la commission prise au prestataire et transféré à l'association qui gère la MLC
    //         $fluxCommission = new CommissionPrestataireMLC();
    //         $fluxCommission->setExpediteur($this->getExpediteur());
    //         $fluxCommission->setOperateur($this->getOperateur());
    //         $fluxCommission->setRole($this->getRole());
    //         $fluxCommission->setDestinataire($mlcPrestataire);
    //         $fluxCommission->setMontant($montantCommission);
    //         $fluxCommission->setReference('Commission de reconversion');
    //         $fluxCommission->setMoyen(MoyenEnum::MOYEN_MLC);
    //         $fluxCommission->setTauxreconversion($taux);

    //         $mlcPrestataire->addEcompte($montantCommission);

    //         return [$this->getExpediteur(), $this->getDestinataire(), $fluxCommission, $mlcPrestataire];
    //     }

    //     return [$this->getExpediteur(), $this->getDestinataire()];
    // }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getUsers()->toArray(),
            'destinataires' => ['siege', 'tresorier'],
        ];
    }
}
