<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSACTION
 *  - Demande d'achat de monnaie par un Prestataire (chèque, virement, ...).
 *
 * @ORM\Entity
 */
class AchatMonnaieAConfirmerPrestataire extends AchatMonnaieAConfirmer
{
    /**
     * @ORM\ManyToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_dest_id", referencedColumnName="id", nullable=true)
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_DEMANDE_ACHAT_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => ['siege'],
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
