<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\HasAccountsTrait;
use App\Entity\EntityTrait\HasEcompteEntity;
use App\Flux\AccountableInterface;
use App\Flux\AccountableObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use SpecShaper\GdprBundle\Validator\Constraints as GdprAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW') or is_granted('ROLE_API')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_LIST') or is_granted('ROLE_API')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW') or is_granted('ROLE_API')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PrestataireRepository")
 * @UniqueEntity(fields="raison", message="Le prestataire avec cette raison existe déjà.")
 * @ORM\Table(name="prestataire")
 * @ORM\HasLifecycleCallbacks()
 */
class Prestataire extends AccountableObject implements AccountableInterface
{
    use EnablableEntityTrait;
    use TimestampableEntity;
    use HasEcompteEntity;
    use HasAccountsTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idmlc", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $idmlc;

    /**
     * @var string
     *
     * @ORM\Column(name="raison", type="string", length=100)
     * @Groups({"read", "write"})
     */
    private $raison;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read", "write"})
     */
    private $description;

    /**
     * @var string|null
     *
     * @Gedmo\Slug(fields={"raison"})
     * @ORM\Column(length=100, unique=true)
     * @Groups({"read"})
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="metier", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    private $metier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statut", type="string", length=255, nullable=true)
     * @Groups({"read", "write"})
     */
    private $statut;

    /**
     * @var string|null
     *
     * @ORM\Column(name="responsable", type="string", length=200, nullable=true)
     * @Groups({"read", "write"})
     */
    private $responsable;

    /**
     * Iban bank account number.
     *
     * @var string
     *
     * @GdprAssert\PersonalData({
     *     @Assert\NotBlank,
     *     @Assert\Iban
     * })
     *
     * @ORM\Column(name="iban", type="personal_data", nullable=true, options={
     *     "format" = "STRING",
     *     "isSensitive"=false,
     *     "isEncrypted"=true,
     *     "idMethod"="INDIRECT",
     *     "basisOfCollection"="LEGITIMATE_INTEREST",
     *     "identifiableBy"="Can be used to identify an individual if compared with third party database",
     *     "providedBy"="The employee, the employer",
     *     "purposeFor"="Used to pay employees by SEPA",
     *     "retainFor"="P6Y",
     *     "disposeBy"="SET_NULL",
     *     "methodOfReceipt"={"HTTPS"},
     *     "receiptProtection"={"TSS"},
     *     "methodOfReturn"={"HTTPS", "PDF"},
     *     "returnProtection"={"TSS","ENCRYPTED_PDF"}
     * })
     */
    private $iban;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=50, nullable=true)
     * @Groups({"read", "write"})
     */
    private $siret;

    /**
     * @var string|null
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=true)
     * @Assert\Url(message = "L'adresse : '{{ value }}' est invalide.")
     * @Groups({"read", "write"})
     */
    private $web;

    /**
     * Le prestataire peut recevoir des cotisations (En règle générale, l'association gérant la monnaie locale)
     * Ce prestataire est normalement crée à l'installation du kohinos.
     *
     * @var bool
     *
     * @ORM\Column(name="mlc", type="boolean", nullable=false, options={"default" : false})
     * @Groups({"read"})
     */
    private $mlc = false;

    /**
     * Le prestataire un peu spécial reçoit les participations au programme solidoume.
     *
     * @var bool
     *
     * @ORM\Column(name="solidoume", type="boolean", nullable=false, options={"default" : false})
     * @Groups({"read"})
     */
    private $solidoume = false;

    /**
     * Le prestataire accepte la monnaie électronique !
     *
     * @var bool
     *
     * @ORM\Column(name="acceptemlc", type="boolean", nullable=false, options={"default" : false})
     * @Groups({"read"})
     */
    private $acceptemlc = false;

    /**
     * @var string|null (champ libre)
     *
     * @ORM\Column(name="horaires", type="text", nullable=true)
     * @Groups({"read", "write"})
     */
    private $horaires;

    /**
     * @var TypePrestataire
     *
     * @ORM\ManyToOne(targetEntity="TypePrestataire", cascade={"persist"}, inversedBy="prestataires")
     * @Groups({"read", "write"})
     */
    private $typeprestataire;

    /**
     * @var ArrayCollection|Rubrique[]
     * @ORM\ManyToMany(targetEntity="Rubrique", mappedBy="prestataires", cascade={"persist"}, fetch="EAGER")
     * @Groups({"read", "write"})
     */
    private $rubriques;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     * @Groups({"read", "write"})
     */
    protected $media;

    /**
     * @var ArrayCollection|GeolocPrestataire[]
     * @ORM\OneToMany(targetEntity="GeolocPrestataire", cascade={"persist"}, mappedBy="prestataire", fetch="LAZY")
     * @Groups({"read", "write"})
     */
    private $geolocs;

    /**
     * @var ArrayCollection|ContactPrestataire[]
     * @ORM\OneToMany(targetEntity="ContactPrestataire", cascade={"persist"}, mappedBy="prestataire")
     * @Groups({"read", "write"})
     */
    private $contacts;

    /**
     * @var ArrayCollection|User[]
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="prestataires", cascade={"persist"}, fetch="EAGER")
     * @Groups({"read", "write"})
     */
    protected $users;

    /**
     * @var ArrayCollection|User[]
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="caissiers", cascade={"persist"})
     * @ORM\JoinTable(name="user_caissier",
     *      joinColumns={@ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     * @Groups({"read", "write"})
     */
    protected $caissiers;

    /**
     * @var Groupe
     *
     * @ORM\ManyToOne(targetEntity="Groupe", inversedBy="prestataires", cascade={"persist"})
     * @Groups({"read", "write"})
     */
    private $groupe;

    /**
     * AMAP / Marché.
     *
     * @var ArrayCollection|Groupeprestataire[]
     * @ORM\ManyToMany(targetEntity="Groupeprestataire", cascade={"persist"}, mappedBy="prestataires")
     * @ORM\JoinTable(name="groupesprestataires")
     * @Groups({"read", "write"})
     */
    private $groupeprestataires;

    /**
     * EtatPrestataire $etats.
     *
     * @var ArrayCollection|EtatPrestataire[]
     * @ORM\ManyToMany(targetEntity="EtatPrestataire", cascade={"persist"}, mappedBy="prestataires", fetch="EXTRA_LAZY")
     * @Groups({"read", "write"})
     */
    private $etats;

    /**
     * Taux de reconversion propre au prestataire.
     *
     * @var float
     * @ORM\Column(name="tauxreconversion", type="decimal", precision=7, scale=2, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $tauxreconversion;

    /**
     * @var ArrayCollection|AccountPrestataire[]
     * @ORM\OneToMany(targetEntity="AccountPrestataire", mappedBy="prestataire")
     */
    private $accounts;

    /**
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->caissiers = new ArrayCollection();
        $this->groupeprestataires = new ArrayCollection();
        $this->geolocs = new ArrayCollection();
        $this->etats = new ArrayCollection();
        $this->rubriques = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRaison(): ?string
    {
        return $this->raison;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->getRaison();
    }

    /**
     * @param string $raison
     *
     * @return Prestataire
     */
    public function setRaison(string $raison): self
    {
        $this->raison = $raison;

        return $this;
    }

    /**
     * Get idmlc.
     *
     * @return
     */
    public function getIdmlc(): ?string
    {
        return $this->idmlc;
    }

    /**
     * Set idmlc.
     *
     * @return $this
     */
    public function setIdmlc(string $idmlc): self
    {
        $this->idmlc = $idmlc;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function setDescription(?string $description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getMetier(): ?string
    {
        return $this->metier;
    }

    /**
     * @param string $metier
     *
     * @return Prestataire
     */
    public function setMetier(string $metier): self
    {
        $this->metier = $metier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatut(): ?string
    {
        return $this->statut;
    }

    /**
     * @param string|null $statut
     *
     * @return Prestataire
     */
    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResponsable(): ?string
    {
        return $this->responsable;
    }

    /**
     * @param string|null $responsable
     *
     * @return Prestataire
     */
    public function setResponsable(?string $responsable): self
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * @return string
     */
    public function getIban(): ?string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     *
     * @return Prestataire
     */
    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * @return string
     */
    public function getSiret(): ?string
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     *
     * @return Prestataire
     */
    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWeb(): ?string
    {
        return $this->web;
    }

    /**
     * @param string|null $web
     *
     * @return Prestataire
     */
    public function setWeb(?string $web): self
    {
        $this->web = $web;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMlc(): bool
    {
        return $this->mlc;
    }

    /**
     * @param bool $mlc
     *
     * @return Prestataire
     */
    public function setMlc(bool $mlc): self
    {
        $this->mlc = $mlc;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSolidoume(): bool
    {
        return $this->solidoume;
    }

    /**
     * @param bool $solidoume
     *
     * @return Prestataire
     */
    public function setSolidoume(bool $solidoume): self
    {
        $this->solidoume = $solidoume;

        return $this;
    }

    /**
     * Accepte la monnaie électronique.
     *
     * @return bool
     */
    public function isAcceptemlc(): bool
    {
        return $this->acceptemlc;
    }

    /**
     * @param bool $acceptemlc
     *
     * @return Prestataire
     */
    public function setAcceptemlc(bool $acceptemlc): self
    {
        $this->acceptemlc = $acceptemlc;

        return $this;
    }

    /**
     * getgeolocs.
     *
     * @return ArrayCollection geoloc
     */
    public function getGeolocs()
    {
        return $this->geolocs;
    }

    /**
     * setGeolocs.
     *
     * @param [type] $geolocs [description]
     */
    public function setGeolocs($geolocs): self
    {
        $this->geolocs = $geolocs;

        return $this;
    }

    /**
     * @param GeolocPrestataire $geoloc
     *
     * @return $this
     */
    public function addGeoloc(GeolocPrestataire $geoloc): self
    {
        if (!$this->geolocs->contains($geoloc)) {
            $geoloc->setPrestataire($this);
            $this->geolocs->add($geoloc);
        }

        return $this;
    }

    /**
     * @param GeolocPrestataire $geoloc
     *
     * @return $this
     */
    public function removeGeoloc(GeolocPrestataire $geoloc): self
    {
        if ($this->geolocs->contains($geoloc)) {
            $this->geolocs->removeElement($geoloc);
            $geoloc->setPrestataire(null);
        }

        return $this;
    }

    /**
     * getcontacts.
     *
     * @return ArrayCollection contact
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * setContacts.
     *
     * @param [type] $contacts [description]
     */
    public function setContacts($contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @param ContactPrestataire $contact
     *
     * @return $this
     */
    public function addContact(ContactPrestataire $contact): self
    {
        if (is_null($this->contacts)) {
            $this->contacts = new ArrayCollection();
        }
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPrestataire($this);
        }

        return $this;
    }

    /**
     * @param ContactPrestataire $contact
     *
     * @return $this
     */
    public function removeContact(ContactPrestataire $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            $contact->setPrestataire(null);
        }

        return $this;
    }

    /**
     * @return ArrayCollection[]|User
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return ArrayCollection[]|User
     */
    public function getCaissiers()
    {
        return $this->caissiers;
    }

    public function getUsersString()
    {
        return join(' - ', array_map(function ($user) {
            $return = $user->getName() . ($user->getEmail() == $user->getName() ? '' : (':' . $user->getEmail()));
            $return .= '[' . implode(', ', $user->getPossiblegroups()->toArray()) . ']';

            return $return;
        }, $this->users->getValues()));
    }    

    public function getGestionnairesEmailsString()
    {
        return join(' - ', array_map(function ($user) {
            return $user->getEmail();
        }, $this->users->getValues()));
    }

    public function getCaissiersString()
    {
        return join(' - ', array_map(function ($user) {
            return $user->getName() . ':' . $user->getEmail();
        }, $this->caissiers->getValues()));
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addPrestataire($this);
        }

        return $this;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function removeUser(User $users)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removePrestataire($this);
        }

        return $this;
    }

    /**
     * @param ArrayCollection[]|User $users
     *
     * @return Prestataire
     */
    public function setUsers($users): self
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @param User $caissier
     *
     * @return $this
     */
    public function addCaissier(User $caissier): self
    {
        if (!$this->caissiers->contains($caissier)) {
            $this->caissiers[] = $caissier;
            $caissier->addCaissier($this);
        }

        return $this;
    }

    /**
     * @param User $caissier
     *
     * @return $this
     */
    public function removeCaissier(User $caissiers)
    {
        if ($this->caissiers->contains($caissier)) {
            $this->caissiers->removeElement($caissier);
            $caissier->removeCaissier($this);
        }

        return $this;
    }

    /**
     * @param ArrayCollection[]|User $caissiers
     *
     * @return Prestataire
     */
    public function setCaissiers($caissiers): self
    {
        $this->caissiers = $caissiers;

        return $this;
    }

    /**
     * @param Groupe|null $groupe
     *
     * @return $this
     */
    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return Groupe|null
     */
    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    /**
     * Get typeprestataire.
     *
     * @return
     */
    public function getTypeprestataire()
    {
        return $this->typeprestataire;
    }

    /**
     * Set typeprestataire.
     *
     * @return $this
     */
    public function setTypeprestataire($typeprestataire): self
    {
        $this->typeprestataire = $typeprestataire;

        return $this;
    }

    /**
     * @return Rubrique[]|ArrayCollection
     */
    public function getRubriques()
    {
        return $this->rubriques;
    }

    public function getRubriquesString(): ?string
    {
        return join(' - ', array_map(function ($rubrique) {
            return $rubrique->getName();
        }, $this->rubriques->getValues()));
    }

    /**
     * @param Rubrique $rubrique
     *
     * @return $this
     */
    public function addRubrique(Rubrique $rubrique): self
    {
        if (!$this->rubriques->contains($rubrique)) {
            $this->rubriques[] = $rubrique;
            $rubrique->addPrestataire($this);
        }

        return $this;
    }

    /**
     * @param Rubrique $rubrique
     *
     * @return $this
     */
    public function removeRubrique(Rubrique $rubrique): self
    {
        if ($this->rubriques->contains($rubrique)) {
            $this->rubriques->removeElement($rubrique);
            $rubrique->removePrestataire($this);
        }

        return $this;
    }

    /**
     * @return Groupeprestataire[]|ArrayCollection
     */
    public function getGroupeprestataires()
    {
        return $this->groupeprestataires;
    }

    /**
     * @param Groupeprestataire[]|ArrayCollection
     *
     * @return $this
     */
    public function setGroupeprestataires($groupeprestataires): self
    {
        $this->groupeprestataires = $groupeprestataires;

        return $this;
    }

    /**
     * @param Groupeprestataire $groupeprestataire
     *
     * @return $this
     */
    public function addGroupeprestataire(Groupeprestataire $groupeprestataire): self
    {
        if (!$this->groupeprestataires->contains($groupeprestataire)) {
            $this->groupeprestataires[] = $groupeprestataire;
            $groupeprestataire->addPrestataire($this);
        }

        return $this;
    }

    /**
     * @param Groupeprestataire $groupeprestataire
     *
     * @return $this
     */
    public function removeGroupeprestataire(Groupeprestataire $groupeprestataire): self
    {
        if ($this->groupeprestataires->contains($groupeprestataire)) {
            $this->groupeprestataires->removeElement($groupeprestataire);
            $groupeprestataire->removePrestataire($this);
        }

        return $this;
    }

    /**
     * @return EtatPrestataire[]|ArrayCollection
     */
    public function getEtats()
    {
        return $this->etats;
    }

    public function getEtatsString(): ?string
    {
        return join(' - ', array_map(function ($etat) {
            return $etat->getName();
        }, $this->etats->getValues()));
    }

    /**
     * @param EtatPrestataire[]|ArrayCollection
     *
     * @return $this
     */
    public function setEtats($etats): self
    {
        $this->etats = $etats;

        return $this;
    }

    /**
     * @param EtatPrestataire $etat
     *
     * @return $this
     */
    public function addEtat(EtatPrestataire $etat): self
    {
        if (!$this->etats->contains($etat)) {
            $this->etats[] = $etat;
            $etat->addPrestataire($this);
        }

        return $this;
    }

    /**
     * @param Etat Prestataire $etat
     *
     * @return $this
     */
    public function removeEtat(EtatPrestataire $etat): self
    {
        if ($this->etats->contains($etat)) {
            $this->etats->removeElement($etat);
            $etat->removePrestataire($this);
        }

        return $this;
    }

    /**
     * Get horaires.
     *
     * @return string Horaires
     */
    public function getHoraires(): ?string
    {
        return $this->horaires;
    }

    /**
     * Set horaires.
     *
     * @return $this
     */
    public function setHoraires(?string $horaires): self
    {
        $this->horaires = $horaires;

        return $this;
    }

    /**
     * Get media.
     *
     * @return
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set media.
     *
     * @return $this
     */
    public function setMedia($media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get tauxreconversion.
     *
     * @return
     */
    public function getTauxreconversion(): ?float
    {
        return $this->tauxreconversion;
    }

    /**
     * Set tauxreconversion.
     *
     * @return $this
     */
    public function setTauxreconversion(?float $tauxreconversion): self
    {
        $this->tauxreconversion = $tauxreconversion;

        return $this;
    }

    /**
     * Get comments.
     *
     * @return string comments
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * Set comments.
     *
     * @return $this
     */
    public function setComments($comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getRaisonAndIdmlc(): ?string
    {
        return $this->getRaison() . ($this->getIdmlc() ? '<br/>(' . $this->getIdmlc() . ')' : '');
    }

    public function __toString(): string
    {
        return $this->raison ? $this->raison : 'Prestataire xxx';
    }

    public function getMapContent(): ?string
    {
        return 'mapcontentpresta';
    }
}
