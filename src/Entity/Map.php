<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\MapController;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *     },
 *     itemOperations={
 *     },
 *     collectionOperations={
 *         "get_all_poi"={
 *             "controller"=MapController::class,
 *             "method"="GET",
 *             "input"=false,
 *             "output"=Map::class,
 *             "pagination_enabled"=false,
 *             "path"="/public/poi/all",
 *             "read"=false,
 *             "swagger_context" = {
 *                 "parameters" = {
 *                      {
 *                          "name" = "geoloc",
 *                          "in" = "query",
 *                          "description" = "[Latitude, Longitude], exemple : [45.123,4.023]",
 *                          "required" = false,
 *                          "type" : "string",
 *                      },
 *                      {
 *                          "name" = "distance",
 *                          "in" = "query",
 *                          "description" = "Distance in kilometers",
 *                          "required" = false,
 *                          "type" : "string"
 *                      }
 *                  }
 *             },
 *             "security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_LIST') or is_granted('ROLE_API')"},
 *     },
 * )
 */
class Map
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue
     * @ApiProperty(identifier=true)
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(length=150)
     * @Groups({"read", "write"})
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read", "write"})
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=true)
     * @Groups({"read", "write"})
     */
    private $web;

    /**
     * @var string|null
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     * @Groups({"read", "write"})
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hours", type="string", length=255, nullable=true)
     * @Groups({"read", "write"})
     */
    private $hours;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     * @Groups({"read", "write"})
     */
    private $icon;

    /**
     * @var Rubrique[]
     * @ORM\OneToMany(targetEntity="Rubrique")
     * @Groups({"read", "write"})
     */
    private $rubriques;

    /**
     * @var Geoloc[]
     * @ORM\OneToMany(targetEntity="Geoloc")
     * @Groups({"read", "write"})
     */
    private $geolocs;

    /**
     * Get id.
     *
     * @return
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get name.
     *
     * @return
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get content.
     *
     * @return
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get web.
     *
     * @return
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Get link.
     *
     * @return
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Get hours.
     *
     * @return
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Get icon.
     *
     * @return
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Get rubriques.
     *
     * @return
     */
    public function getRubriques(): ?Rubrique
    {
        return $this->rubriques;
    }

    /**
     * Get geolocs.
     *
     * @return
     */
    public function getGeolocs(): ?Geoloc
    {
        return $this->geolocs;
    }
}
