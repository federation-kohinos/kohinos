<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Repository\SolidoumeParameterRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * ApiResource().
 *
 * @ORM\Entity(repositoryClass=SolidoumeParameterRepository::class)
 */
class SolidoumeParameter
{
    use EnablableEntityTrait;
    use TimestampableEntity;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * Description of the first page to present the program (in the homepage)
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;
    
    /**
     * Description of the cotisation page
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionCotisation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reminderEmail;

    /**
     * @ORM\Column(type="float")
     */
    private $minimum;

    /**
     * @ORM\Column(type="float")
     */
    private $maximum;

    /**
     * @ORM\Column(type="float")
     */
    private $commission;

    /**
     * @ORM\Column(type="integer")
     */
    private $reminderDays;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $confirmEmail;

    /**
     * @ORM\Column(type="integer")
     */
    private $executionDate;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="news")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getDescriptionCotisation(): ?string
    {
        return $this->descriptionCotisation;
    }

    public function setDescriptionCotisation(?string $descriptionCotisation): self
    {
        $this->descriptionCotisation = $descriptionCotisation;

        return $this;
    }

    public function getReminderEmail(): ?string
    {
        return $this->reminderEmail;
    }

    public function setReminderEmail(?string $reminderEmail): self
    {
        $this->reminderEmail = $reminderEmail;

        return $this;
    }

    public function getMinimum(): ?float
    {
        return $this->minimum;
    }

    public function setMinimum(float $minimum): self
    {
        $this->minimum = $minimum;

        return $this;
    }

    public function getMaximum(): ?float
    {
        return $this->maximum;
    }

    public function setMaximum(float $maximum): self
    {
        $this->maximum = $maximum;

        return $this;
    }

    public function getCommission(): ?float
    {
        return $this->commission;
    }

    public function setCommission(float $commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    public function getReminderDays(): ?int
    {
        return $this->reminderDays;
    }

    public function setReminderDays(int $reminderDays): self
    {
        $this->reminderDays = $reminderDays;

        return $this;
    }

    public function getConfirmEmail(): ?string
    {
        return $this->confirmEmail;
    }

    public function setConfirmEmail(?string $confirmEmail): self
    {
        $this->confirmEmail = $confirmEmail;

        return $this;
    }

    public function getExecutionDate(): ?int
    {
        return $this->executionDate;
    }

    public function setExecutionDate(int $executionDate): self
    {
        $this->executionDate = $executionDate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
