<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_VIEW')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * TRANSACTION
 *   - Cotisation d'un adhérent.
 *
 * @ORM\Entity(repositoryClass="App\Repository\CotisationRepository")
 */
class CotisationAdherent extends Cotisation
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id", nullable=true)
     */
    protected $expediteur;

    /**
     * @var DonAdherent
     * @ORM\OneToOne(targetEntity="DonAdherent", cascade={"persist"})
     * @ORM\JoinColumn(name="don_id", referencedColumnName="id", nullable=true)
     */
    protected $don;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_COTISATION_ADHERENT;
    }

    /**
     * Get don.
     *
     * @return DonAdherent
     */
    public function getDon(): ?DonAdherent
    {
        return $this->don;
    }

    /**
     * Set don.
     *
     * @return $this
     */
    public function setDon(?DonAdherent $don)
    {
        $this->don = $don;

        return $this;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => [$this->getExpediteur()->getUser()],
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
