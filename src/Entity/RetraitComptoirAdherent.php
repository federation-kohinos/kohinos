<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RETRAIT
 *  - ADHERENTS        =>    COMPTOIR         (Retrait d'un adherent).
 *
 * @ORM\Entity()
 */
class RetraitComptoirAdherent extends Retrait
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_RETRAIT_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
