<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VENTE MONNAIE NUMERIQUE
 *  - COMPTOIRS         =>     PRESTATAIRES             (Vente de monnaie numérique auprès des prestataires).
 *
 * @ORM\Entity()
 */
class VenteEmlcComptoirPrestataire extends VenteEmlc
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_VENTE_EMLC_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
