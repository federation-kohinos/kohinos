<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSACTION
 *  - Achat de monnaie en CB par un Prestataire.
 *
 * @ORM\Entity
 */
class AchatMonnaiePrestataire extends AchatMonnaie
{
    /**
     * @ORM\ManyToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_dest_id", referencedColumnName="id", nullable=true)
     */
    protected $destinataire;

    /**
     * @var DonPrestataire
     * @ORM\OneToOne(targetEntity="DonPrestataire", cascade={"persist"})
     * @ORM\JoinColumn(name="don_id", referencedColumnName="id", nullable=true)
     */
    protected $don;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_ACHAT_PRESTATAIRE;
    }

    /**
     * Get don.
     *
     * @return DonPrestataire
     */
    public function getDon(): ?DonPrestataire
    {
        return $this->don;
    }

    /**
     * Set don.
     *
     * @return $this
     */
    public function setDon(?DonPrestataire $don)
    {
        $this->don = $don;

        return $this;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => ['siege'],
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
