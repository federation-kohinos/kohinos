<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RETRAIT
 *  - PRESTATAIRES     =>    COMPTOIR      (Retrait d'un prestataire).
 *
 * @ORM\Entity()
 */
class RetraitComptoirPrestataire extends Retrait
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_RETRAIT_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
