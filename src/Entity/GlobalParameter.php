<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GlobalParameterRepository")
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="Ce nom de paramètre est déjà utilisé !"
 * )
 */
class GlobalParameter
{
    // Default global parameters for MLC
    const USE_WORDPRESS = 'USE_WORDPRESS';
    const MLC_URL = 'MLC_URL';
    const MLC_NAME = 'MLC_NAME';
    const MLC_NAME_SMALL = 'MLC_NAME_SMALL';
    const MLC_SYMBOL = 'MLC_SYMBOL';
    const MLC_NOTIF_EMAIL = 'MLC_NOTIF_EMAIL';
    const MLC_CONTACT_EMAIL = 'MLC_CONTACT_EMAIL';
    const COTISATION_ADHERENT = 'COTISATION_ADHERENT';
    const COTISATION_ADHERENT_DEFAULT = 'COTISATION_ADHERENT_DEFAULT';
    const COTISATION_PRESTATAIRE = 'COTISATION_PRESTATAIRE';
    const COTISATION_FREE_AMOUNT = 'COTISATION_FREE_AMOUNT';
    const ACCEPT_DON_ADHERENT_COTISATION = 'ACCEPT_DON_ADHERENT_COTISATION';
    const ACCEPT_DON_ADHERENT_ACHAT = 'ACCEPT_DON_ADHERENT_ACHAT';
    const ACCEPT_DON_PRESTATAIRE_COTISATION = 'ACCEPT_DON_PRESTATAIRE_COTISATION';
    const ACCEPT_DON_PRESTATAIRE_ACHAT = 'ACCEPT_DON_PRESTATAIRE_ACHAT';
    const RECONVERSION_PRESTATAIRE = 'RECONVERSION_PRESTATAIRE';
    const MAP_CENTER = 'MAP_CENTER';
    const MAP_ZOOM = 'MAP_ZOOM';
    const USE_PAYZEN = 'USE_PAYZEN';
    const ALL_TICKETS = 'ALL_TICKETS';
    const USE_SOLIDOUME = 'USE_SOLIDOUME';
    const ADHESION_TEXT = 'ADHESION_TEXT';
    const IBAN_ASSOCIATION = 'IBAN_ASSOCIATION';
    const IBAN_GUARANTY = 'IBAN_GUARANTY';
    const CHECK_ORDER = 'CHECK_ORDER';
    const CHECK_ADDRESS = 'CHECK_ADDRESS';
    const USE_HELLOASSO = 'USE_HELLOASSO';
    const HELLOASSO_CLIENTID = 'HELLOASSO_CLIENTID';
    const HELLOASSO_CLIENTSECRET = 'HELLOASSO_CLIENTSECRET';
    const HELLOASSO_URL_EMLC_ADHERENT = 'HELLOASSO_URL_EMLC_ADHERENT';
    const HELLOASSO_URL_EMLC_PRESTATAIRE = 'HELLOASSO_URL_EMLC_PRESTATAIRE';
    const HELLOASSO_URL_COTISATION_ADHERENT = 'HELLOASSO_URL_COTISATION_ADHERENT';
    const HELLOASSO_URL_COTISATION_PRESTATAIRE = 'HELLOASSO_URL_COTISATION_PRESTATAIRE';
    const ACHAT_MONNAIE_SHOW_MANUEL_AMOUNT = 'ACHAT_MONNAIE_SHOW_MANUEL_AMOUNT';
    const ACHAT_MONNAIE_SHOW_SLIDER_AMOUNT = 'ACHAT_MONNAIE_SHOW_SLIDER_AMOUNT';
    const ACHAT_MONNAIE_SHOW_RADIO_AMOUNT = 'ACHAT_MONNAIE_SHOW_RADIO_AMOUNT';
    const ACHAT_MONNAIE_VAL_RADIO_AMOUNT = 'ACHAT_MONNAIE_VAL_RADIO_AMOUNT';
    const ACHAT_MONNAIE_CAN_PAY_BY_CHECK_OR_TRANFERT = 'ACHAT_MONNAIE_CAN_PAY_BY_CHECK_OR_TRANFERT';

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="boolean")
     */
    private $mandatory;

    public function __toString()
    {
        return $this->getName() . ' => ' . $this->getValue();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get mandatory.
     *
     * @return
     */
    public function getMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Set mandatory.
     *
     * @return $this
     */
    public function setMandatory($mandatory)
    {
        $this->mandatory = $mandatory;

        return $this;
    }
}
