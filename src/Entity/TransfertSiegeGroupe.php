<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSFERT
 *  - SIEGE             =>     GROUPES LOCAUX           (Transfert du siège au groupe).
 *
 * @ORM\Entity()
 */
class TransfertSiegeGroupe extends Transfert
{
    /**
     * @ORM\OneToOne(targetEntity="Siege")
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Groupe")
     * @ORM\JoinColumn(name="groupe_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSFERT_SIEGE_GROUPE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => ['siege'],
            'destinataires' => $this->getDestinataire()->getGestionnaires()->toArray(),
        ];
    }
}
