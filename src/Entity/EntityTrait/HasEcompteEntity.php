<?php

namespace App\Entity\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait HasEcompteEntity
{
    /**
     * Compte de MLC numérique.
     *
     * @var int
     *
     * @ORM\Column(name="ecompte", type="decimal", scale=2, options={"default" : 0})
     * @Groups({"read"})
     */
    private $ecompte = 0;

    /**
     * @return int
     */
    public function getEcompte(): float
    {
        return $this->ecompte;
    }

    /**
     * @param int $ecompte
     *
     * @return $this
     */
    public function setEcompte(float $ecompte)
    {
        $this->ecompte = $ecompte;

        return $this;
    }

    /**
     * Incremente le ecompte.
     *
     * @param int $ecompte
     *
     * @return $this
     */
    public function addEcompte(float $ecompte)
    {
        $this->ecompte += $ecompte;

        return $this;
    }

    /**
     * Décremente le ecompte.
     *
     * @param int $ecompte
     *
     * @return $this
     */
    public function removeEcompte(float $ecompte)
    {
        $this->ecompte -= $ecompte;

        return $this;
    }
}
