<?php

namespace App\Entity\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait ContactEmailTelTrait
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="tel", type="string", length=20, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $tel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    protected $email;

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel)
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email)
    {
        $this->email = $email;

        return $this;
    }
}
