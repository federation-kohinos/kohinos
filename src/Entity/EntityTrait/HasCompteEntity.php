<?php

namespace App\Entity\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait HasCompteEntity
{
    /**
     * Compte de billets.
     *
     * @var int
     *
     * @ORM\Column(name="compte", type="decimal", scale=2)
     * @Groups({"read"})
     */
    private $compte = 0;

    /**
     * @return int
     */
    public function getCompte(): ?float
    {
        return $this->compte;
    }

    /**
     * @param int $compte
     *
     * @return $this
     */
    public function setCompte(?float $compte)
    {
        $this->compte = $compte;

        return $this;
    }

    /**
     * Incremente le compte.
     *
     * @param int $compte
     *
     * @return $this
     */
    public function addCompte(float $compte)
    {
        $this->compte += $compte;

        return $this;
    }

    /**
     * Décremente le compte.
     *
     * @param int $compte
     *
     * @return $this
     */
    public function removeCompte(float $compte)
    {
        $this->compte -= $compte;

        return $this;
    }
}
