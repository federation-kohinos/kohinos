<?php

namespace App\Entity\EntityTrait;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

trait EnablableEntityTrait
{
    /**
     * @var bool
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     * @Groups({"read", "write"})
     */
    protected $enabled = true;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }
}
