<?php

namespace App\Entity\EntityTrait;

use App\Entity\Geoloc;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait GeolocEntityTrait
{
    /**
     * @var Geoloc
     *
     * @ORM\OneToOne(targetEntity="Geoloc", cascade={"persist"}, orphanRemoval=true)
     * @Groups({"read", "write"})
     */
    private $geoloc;

    public function getGeoloc(): ?Geoloc
    {
        return $this->geoloc;
    }

    public function setGeoloc(?Geoloc $geoloc)
    {
        $this->geoloc = $geoloc;

        return $this;
    }

    public function getFullAddresse()
    {
        if (null != $this->geoloc) {
            return $this->geoloc->getAdresse() . ' ' . $this->geoloc->getCpostal() . ' ' . $this->geoloc->getVille();
        }

        return '';
    }
}
