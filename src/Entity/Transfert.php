<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSFERT
 * Les transferts dans la structure sont les flux de billets détenus par les opérateurs.
 * Pour ajouter il faut un rôle spécifique :
 *   – Administrateur (Siège)
 *   – Gestionnaire (Groupe local)
 *   – Comptoir + Vente de monnaie locale
 *   – Prestataire (Reconversion).
 *
 * Types de transfert : (Les transferts dans la structure sont les flux de billets détenus par les opérateurs.)
 *
 *  - SIEGE             =>     GROUPES LOCAUX           (Transfert du siège au groupe)
 *  - GROUPE            =>     SIEGE                    (Transfert du groupe au siège)
 *  - GROUPES LOCAUX    =>     COMPTOIRS                (Transfert du groupe au comptoir)
 *  - COMPTOIRS         =>     GROUPES LOCAUX           (Transfert du comptoir au groupe)
 *  - COMPTOIRS         =>     ADHERENTS                (Diffusion de monnaie papier auprès des adhérents)
 *  - COMPTOIRS         =>     PRESTATAIRES             (Diffusion de monnaie papier auprès des prestataires)
 *
 * @ORM\Entity
 */
class Transfert extends Flux
{
    const TYPE_TRANSFERT_COMPTOIR_GROUPE = 'comptoir_groupe';
    const TYPE_TRANSFERT_GROUPE_COMPTOIR = 'groupe_comptoir';
    const TYPE_TRANSFERT_GROUPE_SIEGE = 'groupe_siege';
    const TYPE_TRANSFERT_SIEGE_GROUPE = 'siege_groupe';

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_TRANSFERT_COMPTOIR_GROUPE => self::TYPE_TRANSFERT_COMPTOIR_GROUPE,
            self::TYPE_TRANSFERT_GROUPE_COMPTOIR => self::TYPE_TRANSFERT_GROUPE_COMPTOIR,
            self::TYPE_TRANSFERT_GROUPE_SIEGE => self::TYPE_TRANSFERT_GROUPE_SIEGE,
            self::TYPE_TRANSFERT_SIEGE_GROUPE => self::TYPE_TRANSFERT_SIEGE_GROUPE,
        ];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_TRANSFERT;
    }

    public function getAllOperations($em)
    {
        return [
            OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_MLC, -$this->getMontant()),
            OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_MLC, $this->getMontant()),
        ];
    }

    public function operate($em)
    {
        $compteExp = $this->getExpediteur()->getCompte() - $this->getMontant();
        if ($compteExp < 0) {
            throw new \Exception("[FLUX] Transfert impossible ! Montant supérieur au solde de l'expéditeur !");
        } else {
            $this->getExpediteur()->removeCompte($this->getMontant());
            $this->getDestinataire()->addCompte($this->getMontant());

            return [$this->getExpediteur(), $this->getDestinataire()];
        }

        return [];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function getUsersToNotify()
    {
        return [];
    }
}
