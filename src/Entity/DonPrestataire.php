<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Don d'un prestataire.
 *
 * @ORM\Entity
 */
class DonPrestataire extends Don
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id", nullable=true)
     */
    protected $expediteur;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_DON_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getUsers()->toArray(),
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
