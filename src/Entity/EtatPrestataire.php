<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_VIEW')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_PRESTATAIRE_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * ).
 *
 * @ORM\Entity(repositoryClass="App\Repository\EtatPrestataireRepository")
 */
class EtatPrestataire
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * @var ArrayCollection|EtatPrestataire[]
     *
     * @ORM\ManyToMany(targetEntity="Prestataire", inversedBy="etats", cascade={"persist"})
     * @ORM\JoinTable(name="etats_prestataires")
     */
    protected $prestataires;

    public function __construct()
    {
        $this->prestataires = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Prestataire[]|ArrayCollection
     */
    public function getPrestataires()
    {
        return $this->prestataires;
    }

    /**
     * @param Prestataire $prestataire
     *
     * @return $this
     */
    public function addPrestataire(Prestataire $prestataire): self
    {
        if (!$this->prestataires->contains($prestataire)) {
            $this->prestataires[] = $prestataire;
        }

        return $this;
    }

    /**
     * @param Prestataire $prestataire
     *
     * @return $this
     */
    public function removePrestataire(Prestataire $prestataire): self
    {
        if ($this->prestataires->contains($prestataire)) {
            $this->prestataires->removeElement($prestataire);
        }

        return $this;
    }

    public function getPrestatairesCount()
    {
        return $this->getPrestataires()->count();
    }

    public function __toString(): string
    {
        return $this->getName() ? $this->getName() : '[Tag]';
    }
}
