<?php

namespace App\Entity;

use App\Enum\CurrencyEnum;
use App\Enum\MoyenEnum;
use App\Utils\OperationFactory;
use Doctrine\ORM\Mapping as ORM;

/**
 * Retrait => (Retrait de monnaie papier en échange de monnaie numérique).
 *
 * Types de retraits :
 *
 *   - ADHERENTS        =>    COMPTOIR          (Retrait d'un adherent)
 *   - PRESTATAIRES     =>    COMPTOIR          (Retrait d'un prestataire)
 *
 * @ORM\Entity
 */
class Retrait extends Flux
{
    const TYPE_RETRAIT_ADHERENT = 'retrait_adherent';
    const TYPE_RETRAIT_PRESTATAIRE = 'retrait_prestataire';

    /**
     * @ORM\OneToOne(targetEntity="Comptoir")
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id")
     */
    protected $expediteur;

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_RETRAIT_ADHERENT => self::TYPE_RETRAIT_ADHERENT,
            self::TYPE_RETRAIT_PRESTATAIRE => self::TYPE_RETRAIT_PRESTATAIRE,
        ];
    }

    /**
     * @return string
     */
    public function getParenttype(): string
    {
        return parent::TYPE_RETRAIT;
    }

    public function getAllOperations($em)
    {
        if (MoyenEnum::MOYEN_MLC == $this->getMoyen()) {
            return [
                OperationFactory::getOperation($this, $this->getExpediteur(), CurrencyEnum::CURRENCY_MLC, -$this->getMontant()),
                OperationFactory::getOperation($this, $this->getDestinataire(), CurrencyEnum::CURRENCY_EMLC, -$this->getMontant()),
                OperationFactory::getOperation($this, $this->getExpediteur()->getGroupe()->getSiege(), CurrencyEnum::CURRENCY_EMLC, -$this->getMontant()),
                OperationFactory::getOperation($this, $this->getExpediteur()->getGroupe()->getSiege(), CurrencyEnum::CURRENCY_MLC_NANTIE, $this->getMontant()),
            ];
        }

        return [];
    }

    public function operate($em)
    {
        $compteExp = $this->getExpediteur()->getCompte() - $this->getMontant();
        $compteDest = $this->getDestinataire()->getEcompte() - $this->getMontant();
        if ($compteExp < 0) {
            throw new \Exception('[FLUX] Retrait impossible ! Montant supérieur au solde du comptoir !');
        } elseif ($compteDest < 0) {
            throw new \Exception('[FLUX] Retrait impossible ! Montant supérieur au solde du demandeur !');
        } else {
            $this->getExpediteur()->removeCompte($this->getMontant());
            $this->getExpediteur()->getGroupe()->getSiege()->addCompteNantie($this->getMontant());
            $this->getExpediteur()->getGroupe()->getSiege()->removeEcompteNantie($this->getMontant());
            $this->getDestinataire()->removeEcompte($this->getMontant());

            return [$this->getExpediteur(), $this->getDestinataire(), $this->getExpediteur()->getGroupe()->getSiege()];
        }

        return [];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return '';
    }

    public function getUsersToNotify()
    {
        return [];
    }
}
