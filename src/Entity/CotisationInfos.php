<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_VIEW') or is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_LIST') or is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_EDIT') or is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_LIST') or is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_VIEW')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_ADHERENT_COTISATIONS_GERER_EDIT') or is_granted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * Cotisation d'un utilisateur.
 *
 * @ORM\Entity
 * @ORM\Table(name="cotisationinfos")
 */
class CotisationInfos
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Cotisation", cascade={"all"}, mappedBy="cotisationInfos")
     */
    protected $cotisation;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="string", length=20)
     */
    private $annee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="debut", type="date")
     */
    private $debut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="date")
     */
    private $fin;

    /**
     * RECU.
     *
     * @var bool
     *
     * @ORM\Column(name="recu", type="boolean", nullable=false)
     */
    private $recu;

    /**
     * Constructeur.
     */
    public function __construct()
    {
        $now = new \DateTime();
        $this->annee = (string) $now->format('Y');
        $this->debut = $now;
        $this->fin = new \DateTime('+1 year');
        $this->recu = false;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Cotisation
     */
    public function getCotisation(): ?Cotisation
    {
        return $this->cotisation;
    }

    /**
     * @param Cotisation $cotisation
     *
     * @return Cotisation
     */
    public function setCotisation(Cotisation $cotisation)
    {
        $this->cotisation = $cotisation;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnnee(): ?string
    {
        return $this->annee;
    }

    /**
     * @param string $annee
     *
     * @return Cotisation
     */
    public function setAnnee(string $annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDebut(): ?\DateTime
    {
        return $this->debut;
    }

    /**
     * @param \DateTime $debut
     *
     * @return Cotisation
     */
    public function setDebut(\DateTime $debut)
    {
        $this->debut = $debut;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFin(): ?\DateTime
    {
        return $this->fin;
    }

    /**
     * @param \DateTime $fin
     *
     * @return Cotisation
     */
    public function setFin(\DateTime $fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRecu(): ?bool
    {
        return $this->recu;
    }

    /**
     * @param bool $recu
     *
     * @return Cotisation
     */
    public function setRecu(bool $recu)
    {
        $this->recu = $recu;

        return $this;
    }
}
