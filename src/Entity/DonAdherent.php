<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Don d'un adhérent.
 *
 * @ORM\Entity
 */
class DonAdherent extends Don
{
    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id", nullable=true)
     */
    protected $expediteur;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_DON_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => [$this->getExpediteur()->getUser()],
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
