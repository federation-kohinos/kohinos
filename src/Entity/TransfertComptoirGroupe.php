<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSFERT
 *  - COMPTOIRS         =>     GROUPES LOCAUX           (Transfert du comptoir au groupe).
 *
 * @ORM\Entity()
 */
class TransfertComptoirGroupe extends Transfert
{
    /**
     * @ORM\OneToOne(targetEntity="Comptoir")
     * @ORM\JoinColumn(name="comptoir_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Groupe")
     * @ORM\JoinColumn(name="groupe_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSFERT_COMPTOIR_GROUPE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => $this->getDestinataire()->getGestionnaires()->toArray(),
        ];
    }
}
