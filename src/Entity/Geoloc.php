<?php

namespace App\Entity;

use App\Entity\EntityTrait\EnablableEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="geoloc")
 */
class Geoloc
{
    use EnablableEntityTrait;

    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"read"})
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     * @Groups({"read", "write"})
     */
    private $adresse;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cpostal", type="string", length=10, nullable=true)
     * @Groups({"read", "write"})
     */
    private $cpostal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="string", length=100, nullable=true)
     * @Groups({"read", "write"})
     */
    private $ville;

    /**
     * @var float|null
     *
     * @ORM\Column(name="lat", type="decimal", precision=10, scale=8, nullable=true)
     * @Groups({"read", "write"})
     */
    private $lat;

    /**
     * @var float|null
     *
     * @ORM\Column(name="lon", type="decimal", precision=10, scale=8, nullable=true)
     * @Groups({"read", "write"})
     */
    private $lon;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    /**
     * @param string|null $adresse
     *
     * @return Geoloc
     */
    public function setAdresse(?string $adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCpostal(): ?string
    {
        return $this->cpostal;
    }

    /**
     * @param string|null $cpostal
     *
     * @return Geoloc
     */
    public function setCpostal(?string $cpostal)
    {
        $this->cpostal = $cpostal;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVille(): ?string
    {
        return $this->ville;
    }

    /**
     * @param string|null $ville
     *
     * @return Geoloc
     */
    public function setVille(?string $ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     *
     * @return Geoloc
     */
    public function setLat(?float $lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLon(): ?float
    {
        return $this->lon;
    }

    /**
     * @param float|null $lon
     *
     * @return Geoloc
     */
    public function setLon(?float $lon)
    {
        $this->lon = $lon;

        return $this;
    }

    public function __toString(): string
    {
        return (!empty($this->adresse) ? $this->adresse : '') . ' ' . (!empty($this->cpostal) ? $this->cpostal : '') . ' ' . (!empty($this->ville) ? $this->ville : '');
    }
}
