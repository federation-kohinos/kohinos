<?php

namespace App\Entity;

use App\Flux\AccountableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="account_siege", uniqueConstraints={@ORM\UniqueConstraint(name="siegecurrency", columns={"siege_id", "currency"})}) )
 * @UniqueEntity(
 *     fields={"siege", "currency"},
 *     errorPath="siege",
 *     message="Le compte du siège existe déjà."
 * )
 */
class AccountSiege extends Account
{
    /**
     * @ORM\ManyToOne(targetEntity=Siege::class, inversedBy="accounts", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id", nullable=false)
     */
    private $siege;

    /**
     * @var ArrayCollection|OperationSiege[]
     * @ORM\OneToMany(targetEntity=OperationSiege::class, mappedBy="account")
     */
    protected $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getSiege(): ?Siege
    {
        return $this->siege;
    }

    public function setSiege(Siege $siege): self
    {
        $this->siege = $siege;

        return $this;
    }

    public function setAccountableObject(AccountableInterface $object): self
    {
        return $this->setSiege($object);
    }

    public function getAccountableObject(): AccountableInterface
    {
        return $this->getSiege();
    }
}
