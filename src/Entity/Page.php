<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\EntityTrait\EnablableEntityTrait;
use App\Entity\EntityTrait\NameSlugContentEntityTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ApiResource(
 *     attributes={"security"="is_granted('ROLE_ADMIN_PAGE_GERER_VIEW')"},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PAGE_GERER_LIST')"},
 *         "post"={"security"="is_granted('ROLE_ADMIN_PAGE_GERER_EDIT')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN_PAGE_GERER_VIEW')"},
 *         "put"={"security"="is_granted('ROLE_ADMIN_PAGE_GERER_EDIT')"},
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * ).
 *
 * @ORM\Entity
 * @ORM\Table(name="page")
 */
class Page
{
    use NameSlugContentEntityTrait;
    use TimestampableEntity;
    use EnablableEntityTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="js", type="text", nullable=true)
     */
    private $js;

    /**
     * @var string|null
     *
     * @ORM\Column(name="css", type="text", nullable=true)
     */
    private $css;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=true)
     */
    private $template;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"}, inversedBy="pages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    public function getId()
    {
        return $this->getId();
    }

    /**
     * @return string|null
     */
    public function getJs(): ?string
    {
        return $this->js;
    }

    /**
     * @param string|null $js
     *
     * @return Page
     */
    public function setJs(?string $js)
    {
        $this->js = $js;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCss(): ?string
    {
        return $this->css;
    }

    /**
     * @param string|null $css
     *
     * @return Page
     */
    public function setCss(?string $css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     *
     * @return Page
     */
    public function setMetaDescription(?string $metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string|null $metaKeywords
     *
     * @return Page
     */
    public function setMetaKeywords(?string $metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemplate(): ?string
    {
        return $this->template;
    }

    /**
     * @param string|null $template
     *
     * @return Page
     */
    public function setTemplate(?string $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Page
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUrl(): string
    {
        return '/page/' . $this->getSlug();
    }

    public function __toString(): string
    {
        return $this->getName() ? $this->getName() : 'Page';
    }
}
