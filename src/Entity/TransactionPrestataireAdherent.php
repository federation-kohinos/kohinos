<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TRANSACTION
 *   - PRESTATAIRES     =>    ADHERENTS         (Virement vers un adherent).
 *
 * @ORM\Entity
 */
class TransactionPrestataireAdherent extends Transaction
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Adherent")
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSACTION_PRESTATAIRE_ADHERENT;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => array_merge($this->getExpediteur()->getUsers()->toArray(), $this->getExpediteur()->getCaissiers()->toArray()),
            'destinataires' => [$this->getDestinataire()->getUser()],
        ];
    }
}
