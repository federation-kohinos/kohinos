<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VENTE
 *  - COMPTOIRS         =>     PRESTATAIRES             (Diffusion de monnaie papier auprès des prestataires).
 *
 * @ORM\Entity()
 */
class VenteComptoirPrestataire extends Vente
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_VENTE_PRESTATAIRE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => $this->getDestinataire()->getUsers()->toArray(),
        ];
    }
}
