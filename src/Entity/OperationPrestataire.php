<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="operation_prestataire")
 * @ORM\HasLifecycleCallbacks()
 */
class OperationPrestataire extends Operation
{
    /**
     * @ORM\ManyToOne(targetEntity=AccountPrestataire::class, inversedBy="operations", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     */
    protected $account;

    /**
     * @ORM\ManyToOne(targetEntity=Flux::class, inversedBy="operationsPrestataire", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="flux_id", referencedColumnName="id", nullable=false)
     */
    protected $flux;
}
