<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRANSFERT
 *  - GROUPES LOCAUX      =>     SIEGE           (Transfert du groupe au siège).
 *
 * @ORM\Entity()
 */
class TransfertGroupeSiege extends Transfert
{
    /**
     * @ORM\OneToOne(targetEntity="Groupe")
     * @ORM\JoinColumn(name="groupe_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @ORM\OneToOne(targetEntity="Siege")
     * @ORM\JoinColumn(name="siege_id", referencedColumnName="id")
     */
    protected $destinataire;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TRANSFERT_GROUPE_SIEGE;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getGestionnaires()->toArray(),
            'destinataires' => ['siege'],
        ];
    }
}
