<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CHANGE
 *  - PRESTATAIRES      =>     COMPTOIRS       (dépôt de billet et chargement du compte numérique).
 *
 * @ORM\Entity()
 */
class ChangePrestataireComptoir extends Change
{
    /**
     * @ORM\OneToOne(targetEntity="Prestataire")
     * @ORM\JoinColumn(name="prestataire_id", referencedColumnName="id")
     */
    protected $expediteur;

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_CHANGE_PRESTATAIRE_COMPTOIR;
    }

    public function getUsersToNotify()
    {
        return [
            'expediteurs' => $this->getExpediteur()->getUsers()->toArray(),
            'destinataires' => $this->getDestinataire()->getGestionnaires()->toArray(),
        ];
    }
}
