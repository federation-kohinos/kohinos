<?php

namespace App\Entity;

use App\Flux\AccountableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="account_adherent", uniqueConstraints={@ORM\UniqueConstraint(name="adherentcurrency", columns={"adherent_id", "currency"})})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"adherent", "currency"},
 *     errorPath="adherent",
 *     message="Le compte de l'adherent existe déjà."
 * )
 */
class AccountAdherent extends Account
{
    /**
     * @ORM\ManyToOne(targetEntity=Adherent::class, inversedBy="accounts", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="adherent_id", referencedColumnName="id", nullable=false)
     */
    private $adherent;

    /**
     * @var ArrayCollection|OperationAdherent[]
     * @ORM\OneToMany(targetEntity=OperationAdherent::class, mappedBy="account")
     */
    protected $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function setAdherent(Adherent $adherent): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    public function setAccountableObject(AccountableInterface $object): self
    {
        return $this->setAdherent($object);
    }

    public function getAccountableObject(): AccountableInterface
    {
        return $this->getAdherent();
    }

    public function __toString(): string
    {
        return 'AccountAdherent ' . $this->getAdherent();
    }
}
