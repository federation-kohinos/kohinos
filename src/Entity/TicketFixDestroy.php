<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketFixDestroy => (Destruction de monnaie papier => modifie le compte de MLC disponible au siège).
 *
 * @ORM\Entity()
 */
class TicketFixDestroy extends TicketFix
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::TYPE_TICKET_FIX_DESTROY;
    }

    public function getParenttype(): string
    {
        return parent::TYPE_TICKET_FIX_DESTROY;
    }

    public function getMontantForAction(): float
    {
        return -$this->getMontant();
    }
}
