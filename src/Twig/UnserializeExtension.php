<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class UnserializeExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('unserialize', [$this, 'unserialize']),
        ];
    }

    public function unserialize($string)
    {
        if (empty($string) || is_array($string)) {
            return '';
        }

        return unserialize($string);
    }
}
