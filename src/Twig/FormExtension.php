<?php

namespace App\Twig;

use App\Entity\User;
use Twig\Extension\AbstractExtension;

/**
 * Fonctions twig permettant d'accéder aux formulaires du Kohinos.
 *
 * @TODO : a refactoriser !
 */
class FormExtension extends AbstractExtension
{
    public $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getUserInfosForm', [$this, 'getUserInfosForm']),
            new \Twig_SimpleFunction('getUserPasswordForm', [$this, 'getUserPasswordForm']),
            new \Twig_SimpleFunction('getGroupeInfosForm', [$this, 'getGroupeInfosForm']),
            new \Twig_SimpleFunction('getComptoirInfosForm', [$this, 'getComptoirInfosForm']),
            new \Twig_SimpleFunction('getPrestataireInfosForm', [$this, 'getPrestataireInfosForm']),
            new \Twig_SimpleFunction('getAdherentInfosForm', [$this, 'getAdherentInfosForm']),
            new \Twig_SimpleFunction('getGroupePrestataireInscriptionForm', [$this, 'getGroupePrestataireInscriptionForm']),
            new \Twig_SimpleFunction('getTransactionAdherentAdherentForm', [$this, 'getTransactionAdherentAdherentForm']),
            new \Twig_SimpleFunction('getTransactionAdherentPrestataireForm', [$this, 'getTransactionAdherentPrestataireForm']),
            new \Twig_SimpleFunction('getTransactionPrestataireAdherentForm', [$this, 'getTransactionPrestataireAdherentForm']),
            new \Twig_SimpleFunction('getTransactionPrestatairePrestataireForm', [$this, 'getTransactionPrestatairePrestataireForm']),
            new \Twig_SimpleFunction('getReconversionForm', [$this, 'getReconversionForm']),
            new \Twig_SimpleFunction('getChangeAdherentComptoirForm', [$this, 'getChangeAdherentComptoirForm']),
            new \Twig_SimpleFunction('getChangePrestataireComptoirForm', [$this, 'getChangePrestataireComptoirForm']),
            new \Twig_SimpleFunction('getRetraitComptoirToXForm', [$this, 'getRetraitComptoirToXForm']),
            new \Twig_SimpleFunction('getVenteComptoirToXForm', [$this, 'getVenteComptoirToXForm']),
            new \Twig_SimpleFunction('getTransfertComptoirGroupeForm', [$this, 'getTransfertComptoirGroupeForm']),
            new \Twig_SimpleFunction('getTransfertGroupeComptoirForm', [$this, 'getTransfertGroupeComptoirForm']),
            new \Twig_SimpleFunction('getTransfertSiegeGroupeForm', [$this, 'getTransfertSiegeGroupeForm']),
            new \Twig_SimpleFunction('getTransfertGroupeSiegeForm', [$this, 'getTransfertGroupeSiegeForm']),
            new \Twig_SimpleFunction('getSoldeSiegeForm', [$this, 'getSoldeSiegeForm']),
            new \Twig_SimpleFunction('getAchatMonnaieAdherentForm', [$this, 'getAchatMonnaieAdherentForm']),
            new \Twig_SimpleFunction('getAchatMonnaiePrestataireForm', [$this, 'getAchatMonnaiePrestataireForm']),
            new \Twig_SimpleFunction('getVenteEmlcAdherentForm', [$this, 'getVenteEmlcAdherentForm']),
            new \Twig_SimpleFunction('getVenteEmlcPrestataireForm', [$this, 'getVenteEmlcPrestataireForm']),
            new \Twig_SimpleFunction('getAchatMonnaieAConfirmerPrestataireForm', [$this, 'getAchatMonnaieAConfirmerPrestataireForm']),
            new \Twig_SimpleFunction('getAchatMonnaieAConfirmerAdherentForm', [$this, 'getAchatMonnaieAConfirmerAdherentForm']),
            new \Twig_SimpleFunction('getAchatMonnaieAConfirmerPrestataireForm', [$this, 'getAchatMonnaieAConfirmerPrestataireForm']),
            new \Twig_SimpleFunction('getTicketFixPrintForm', [$this, 'getTicketFixPrintForm']),
            new \Twig_SimpleFunction('getTicketFixDestroyForm', [$this, 'getTicketFixDestroyForm']),
        ];
    }

    public function getPrestataireInfosForm(User $user)
    {
        return $this->container->get('app.formfactory')->getPrestataireInfosForm($user);
    }

    public function getAdherentInfosForm(User $user)
    {
        return $this->container->get('app.formfactory')->getAdherentInfosForm($user);
    }

    public function getUserInfosForm(User $user)
    {
        return $this->container->get('app.formfactory')->getUserInfosForm($user);
    }

    public function getUserPasswordForm(User $user)
    {
        return $this->container->get('app.formfactory')->getUserPasswordForm($user);
    }

    public function getGroupeInfosForm(User $user)
    {
        return $this->container->get('app.formfactory')->getGroupeInfosForm($user);
    }

    public function getComptoirInfosForm(User $user)
    {
        return $this->container->get('app.formfactory')->getComptoirInfosForm($user);
    }

    public function getGroupePrestataireInscriptionForm(User $user)
    {
        return $this->container->get('app.formfactory')->getGroupePrestataireInscriptionForm($user);
    }

    public function getTransactionAdherentAdherentForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransactionAdherentAdherentForm($user);
    }

    public function getTransactionAdherentPrestataireForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransactionAdherentPrestataireForm($user);
    }

    public function getTransactionPrestataireAdherentForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransactionPrestataireAdherentForm($user);
    }

    public function getTransactionPrestatairePrestataireForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransactionPrestatairePrestataireForm($user);
    }

    public function getReconversionForm(User $user)
    {
        return $this->container->get('app.formfactory')->getReconversionForm($user);
    }

    public function getChangeAdherentComptoirForm(User $user)
    {
        return $this->container->get('app.formfactory')->getChangeAdherentComptoirForm($user);
    }

    public function getChangePrestataireComptoirForm(User $user)
    {
        return $this->container->get('app.formfactory')->getChangePrestataireComptoirForm($user);
    }

    public function getRetraitComptoirToXForm(User $user, $destinataire)
    {
        return $this->container->get('app.formfactory')->getRetraitComptoirToXForm($user, $destinataire);
    }

    public function getVenteComptoirToXForm(User $user, $destinataire)
    {
        return $this->container->get('app.formfactory')->getVenteComptoirToXForm($user, $destinataire);
    }

    public function getTransfertComptoirGroupeForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransfertComptoirGroupeForm($user);
    }

    public function getTransfertGroupeComptoirForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransfertGroupeComptoirForm($user);
    }

    public function getTransfertSiegeGroupeForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransfertSiegeGroupeForm($user);
    }

    public function getTransfertGroupeSiegeForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTransfertGroupeSiegeForm($user);
    }

    public function getSoldeSiegeForm(User $user)
    {
        return $this->container->get('app.formfactory')->getSoldeSiegeForm($user);
    }

    public function getAchatMonnaieAdherentForm(User $user)
    {
        return $this->container->get('app.formfactory')->getAchatMonnaieAdherentForm($user);
    }

    public function getAchatMonnaiePrestataireForm(User $user)
    {
        return $this->container->get('app.formfactory')->getAchatMonnaiePrestataireForm($user);
    }

    public function getVenteEmlcAdherentForm(User $user)
    {
        return $this->container->get('app.formfactory')->getVenteEmlcAdherentForm($user);
    }

    public function getVenteEmlcPrestataireForm(User $user)
    {
        return $this->container->get('app.formfactory')->getVenteEmlcPrestataireForm($user);
    }

    public function getAchatMonnaieAConfirmerAdherentForm(User $user)
    {
        return $this->container->get('app.formfactory')->getAchatMonnaieAConfirmerAdherentForm($user);
    }

    public function getAchatMonnaieAConfirmerPrestataireForm(User $user)
    {
        return $this->container->get('app.formfactory')->getAchatMonnaieAConfirmerPrestataireForm($user);
    }

    public function getTicketFixPrintForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTicketFixPrintForm($user);
    }

    public function getTicketFixDestroyForm(User $user)
    {
        return $this->container->get('app.formfactory')->getTicketFixDestroyForm($user);
    }
}
