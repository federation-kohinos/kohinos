<?php

namespace App\Twig;

use App\Entity\Siege;
use App\Entity\TypePrestataire;
use App\Enum\CurrencyEnum;
use App\Utils\ChartUtils;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;

class StatsExtension extends AbstractExtension
{
    protected $em;
    protected $chartUtils;
    protected $templating;

    public function __construct(EntityManagerInterface $em, ChartUtils $chartUtils, Environment $templating)
    {
        $this->em = $em;
        $this->chartUtils = $chartUtils;
        $this->templating = $templating;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('showMlcChart', [$this, 'showMlcChart']),
            new \Twig_SimpleFunction('showMlcStats', [$this, 'showMlcStats']),
            new \Twig_SimpleFunction('getStatsMonnaie', [$this, 'getStatsMonnaie']),
        ];
    }

    public function showMlcChart(string $type, array $params = [])
    {
        return $this->templating->render(
            '@kohinos/stats/block_chart.html.twig',
            [
                'chart' => $this->chartUtils->getMlcChart($type, $params)
            ]
        );
    }

    public function showMlcStats($stats)
    {
        $class = '';
        switch ($stats) {
            case 'user':
                $class = 'App\Entity\User';
                break;
            case 'adherent':
                $class = 'App\Entity\Adherent';
                break;
            case 'comptoir':
                $class = 'App\Entity\Comptoir';
                break;
            case 'groupe':
                $class = 'App\Entity\Groupe';
                break;
            case 'prestataire':
                $class = null;
                $typepresta = $this->em->getRepository(TypePrestataire::class)->findOneBySlug('prestataire');
                $query = $this->em->createQuery(
                    'SELECT count(distinct p.id)
                    FROM App\\Entity\\Prestataire p
                    WHERE p.enabled = true AND p.typeprestataire = :typepresta'
                )->setParameter('typepresta', $typepresta);

                $val = $query->getSingleScalarResult();
                break;
            case 'prestataireall':
                $class = null;
                $query = $this->em->createQuery(
                    'SELECT count(distinct p.id)
                    FROM App\\Entity\\Prestataire p
                    WHERE p.enabled = true'
                );

                $val = $query->getSingleScalarResult();
                break;
            case 'partenaire':
                $class = null;
                $typepresta = $this->em->getRepository(TypePrestataire::class)->findOneBySlug('partenaire');
                $query = $this->em->createQuery(
                    'SELECT count(distinct p.id)
                    FROM App\\Entity\\Prestataire p
                    WHERE p.enabled = true AND p.typeprestataire = :typepresta'
                )->setParameter('typepresta', $typepresta);

                $val = $query->getSingleScalarResult();
                break;
        }
        if (!empty($class)) {
            $query2 = $this->em->createQuery(
                'SELECT count(distinct u.id)
                FROM ' . $class . ' u
                WHERE u.enabled = true'
            );

            $val = $query2->getSingleScalarResult();
        }

        return $val;
    }

    public function getStatsMonnaie(string $currency = CurrencyEnum::CURRENCY_EMLC): float
    {
        $siege = $this->em->getRepository(Siege::class)->getTheOne();

        if (!empty($currency)) {
            if (!in_array($currency, [CurrencyEnum::CURRENCY_EMLC, CurrencyEnum::CURRENCY_MLC_NANTIE])) {
                return (float) 0;
            }

            if ($currency == CurrencyEnum::CURRENCY_MLC_NANTIE) {
                return round($siege->getAccountWithCurrency($currency)->getBalance(), 0);
            }
            return $siege->getAccountWithCurrency($currency)->getBalance();
        }

        return $siege->getEmlcAccount()->getBalance();
    }
}
