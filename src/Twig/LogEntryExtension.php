<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class LogEntryExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getLogUser', [$this, 'getLogUser']),
        ];
    }

    public function getLogUser(?string $username): ?User
    {
        if (null !== $username) {
            return $this->em->getRepository(User::class)->findOneByUsername($username);
        }

        return null;
    }
}
