<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class GetClassExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_object', [$this, 'isObject']),
            new TwigFunction('get_class', [$this, 'getClass']),
        ];
    }

    public function getClass($object)
    {
        return get_class($object);
    }

    public function isObject($object)
    {
        return is_object($object);
    }
}
