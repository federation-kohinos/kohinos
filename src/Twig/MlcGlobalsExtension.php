<?php

namespace App\Twig;

use App\Entity\GlobalParameter;
use App\Entity\SolidoumeParameter;
use Doctrine\ORM\EntityManager;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class MlcGlobalsExtension extends AbstractExtension implements GlobalsInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getGlobals()
    {
        $arrayGlobals = $this->em->getRepository(GlobalParameter::class)->findAllByName();

        // DEFAULT MAP CENTER & ZOOM : to have a working map on center of France
        if (!isset($arrayGlobals['KOH_MAP_CENTER'])) {
            $arrayGlobals['KOH_MAP_CENTER'] = '[46.898,3.230]';
        }
        if (!isset($arrayGlobals['KOH_MAP_ZOOM'])) {
            $arrayGlobals['KOH_MAP_ZOOM'] = '6';
        }

        // $solidoumeParam = $this->em->getRepository(SolidoumeParameter::class)->findTheOne();
        // if (!empty($solidoumeParam)) {
        //     foreach ($solidoumeParam as $key => $param) {
        //         $arrayGlobals['SD_'.$key] = $param;
        //     }
        // }

        return $arrayGlobals;
    }
}
