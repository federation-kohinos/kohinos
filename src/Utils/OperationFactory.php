<?php

namespace App\Utils;

use App\Enum\CurrencyEnum;
use App\Enum\OperationEnum;
use App\Flux\AccountableInterface;
use App\Flux\FluxInterface;
use App\Flux\OperationInterface;

/**
 * Operation factory.
 */
class OperationFactory
{
    public static function getOperation(FluxInterface $flux, AccountableInterface $object, string $currency, float $montant): OperationInterface
    {
        $ref = new \ReflectionClass($object);
        $type = 'OPERATION_' . strtoupper($ref->getShortName());
        if (!in_array($type, OperationEnum::getAvailableTypes())) {
            throw new \Exception("Opération impossible ! Type d'operation " . $type . ' inexistant');
            // @TODO
            // throw new IllegalTypeException($type);
        }
        if (!in_array($currency, CurrencyEnum::getAvailableTypes())) {
            throw new \Exception('Opération impossible ! Type de currency  ' . $currency . ' inexistant');
            // @TODO
            // throw new IllegalCurrencyException($currency);
        }
        $class = OperationEnum::getClassName($type);
        $operation = new $class();
        $account = $object->getAccountWithCurrency($currency);
        if (null == $account) {
            throw new \Exception("No account for object $object #{$object->getId()}! and currency : $currency");
        }

        $operation
            ->setMontant($montant)
            ->setCurrency($currency)
            ->setFlux($flux)
            ->setAccount($account);

        return $operation;
    }
}
