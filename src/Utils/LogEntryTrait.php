<?php

namespace App\Utils;

use App\Entity\SolidoumeItem;
use Gedmo\Loggable\Entity\LogEntry;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Trait use for Sonata Admin to print historic of changed to show view.
 */
trait LogEntryTrait
{
    protected function addLogEntries(ShowMapper &$showMapper, $class = 'col-12'): void
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $logEntryRepository = $em->getRepository(LogEntry::class);
        $object = $showMapper->getAdmin()->getSubject();
        if (null !== $showMapper && null !== $object && null !== $logEntryRepository && count($this->getLogEntries($object)) > 0) {
            $showMapper
                ->with('Versions', ['class' => $class])
                    ->add('logentries', null, [
                        'label' => false,
                        'template' => '@kohinos/history/logentries.html.twig',
                    ])
                ->end()
            ;
        }
    }

    public function getLogEntries($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $logEntryRepository = $em->getRepository(LogEntry::class);
        return $logEntryRepository->getLogEntries($object);
    }

    public function getLogEntryMappingClassName(string $className)
    {
        switch ($className) {
            case SolidoumeItem::class:
                return "Soli'Item";
            default:
                return $className;
        }
    }
}
