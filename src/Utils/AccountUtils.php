<?php

namespace App\Utils;

use App\Entity\AccountAdherent;
use App\Entity\AccountComptoir;
use App\Entity\AccountGroupe;
use App\Entity\AccountPrestataire;
use App\Entity\AccountSiege;
use App\Entity\Adherent;
use App\Entity\Comptoir;
use App\Entity\Groupe;
use App\Entity\Prestataire;
use App\Entity\Siege;
use App\Enum\CurrencyEnum;
use App\Flux\AccountableObject;
use Doctrine\ORM\EntityManagerInterface;

class AccountUtils
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createAccountForEntity(AccountableObject $object)
    {
        if ($object instanceof Adherent) {
            return $this->createAccountForAdherent($object);
        } elseif ($object instanceof Prestataire) {
            return $this->createAccountForPrestataire($object);
        } elseif ($object instanceof Siege) {
            return $this->createAccountForSiege($object);
        } elseif ($object instanceof Groupe) {
            return $this->createAccountForGroupe($object);
        } elseif ($object instanceof Comptoir) {
            return $this->createAccountForComptoir($object);
        }
    }

    public function createAccountForAdherent(Adherent $adherent)
    {
        if (null == $adherent->getAccountWithCurrency(CurrencyEnum::CURRENCY_EMLC)) {
            $account = new AccountAdherent();
            $account
                ->setCurrency(CurrencyEnum::CURRENCY_EMLC)
            ;
            $adherent->addAccount($account);
            $this->em->persist($account);
            $this->em->flush();
        }

        return false;
    }

    public function createAccountForPrestataire(Prestataire $presta)
    {
        $return = false;
        if (null == $presta->getAccountWithCurrency(CurrencyEnum::CURRENCY_EMLC)) {
            $account = new AccountPrestataire();
            $account
                ->setCurrency(CurrencyEnum::CURRENCY_EMLC)
            ;
            $presta->addAccount($account);
            $this->em->persist($account);
            $this->em->flush();
            $return = true;
        }
        if ($presta->isMlc() && null == $presta->getAccountWithCurrency(CurrencyEnum::CURRENCY_EURO)) {
            // Ajout du compte de fonctionnement euro pour le prestataire MLC
            $accountEuro = new AccountPrestataire();
            $accountEuro
                    ->setCurrency(CurrencyEnum::CURRENCY_EURO)
                ;
            $presta->addAccount($accountEuro);
            $this->em->persist($accountEuro);
            $this->em->flush();
            $return = true;
        }

        return $return;
    }

    public function createAccountForSiege(Siege $siege)
    {
        $return = false;
        if (null == $siege->getAccountWithCurrency(CurrencyEnum::CURRENCY_EMLC)) {
            $account = new AccountSiege();
            $account
                ->setBalance($siege->getEcompteNantie())
                ->setCurrency(CurrencyEnum::CURRENCY_EMLC) // compte de monnaie numérique nantie
            ;
            $siege->addAccount($account);
            $this->em->persist($account);
            $this->em->flush();
            $return = true;
        }
        if (null == $siege->getAccountWithCurrency(CurrencyEnum::CURRENCY_MLC)) {
            $accountMLC = new AccountSiege();
            $accountMLC
                ->setBalance($siege->getCompte())
                ->setCurrency(CurrencyEnum::CURRENCY_MLC) // compte de monnaie papier au siege non nantie
            ;
            $siege->addAccount($accountMLC);
            $this->em->persist($accountMLC);
            $this->em->flush();
            $return = true;
        }
        // if ($siege->getAccountWithCurrency(CurrencyEnum::CURRENCY_EURO) == null) {
        //     $accountEuro = new AccountSiege();
        //     $accountEuro
        //         ->setCurrency(CurrencyEnum::CURRENCY_EURO) // compte réel de nantissement // @TODO : offrir la possibilité de voir les opérations du compte euro du fond de garanti ? Ou plutôt de suivre les euros à récupérés aux comptoirs
        //     ;
        //     $siege->addAccount($accountEuro);
        //     $this->em->persist($accountEuro);
        //
        // }
        if (null == $siege->getAccountWithCurrency(CurrencyEnum::CURRENCY_MLC_NANTIE)) {
            $accountMlcNantie = new AccountSiege();
            $accountMlcNantie
                ->setBalance($siege->getCompteNantie())
                ->setCurrency(CurrencyEnum::CURRENCY_MLC_NANTIE) // compte de monnaie papier nantie
            ;
            $siege->addAccount($accountMlcNantie);
            $this->em->persist($accountMlcNantie);
            $this->em->flush();
            $return = true;
        }

        return $return;
    }

    public function createAccountForGroupe(Groupe $groupe)
    {
        if (null == $groupe->getAccountWithCurrency(CurrencyEnum::CURRENCY_MLC)) {
            $account = new AccountGroupe();
            $account
                ->setBalance($groupe->getCompte())
                ->setCurrency(CurrencyEnum::CURRENCY_MLC)
            ;
            $groupe->addAccount($account);
            $this->em->persist($account);
            $this->em->flush();

            return true;
        }

        return false;
    }

    public function createAccountForComptoir(Comptoir $comptoir)
    {
        if (null == $comptoir->getAccountWithCurrency(CurrencyEnum::CURRENCY_MLC)) {
            $account = new AccountComptoir();
            $account
                ->setBalance($comptoir->getCompte())
                ->setCurrency(CurrencyEnum::CURRENCY_MLC)
            ;
            $comptoir->addAccount($account);
            $this->em->persist($account);
            $this->em->flush();

            return true;
        }

        return false;
    }
}
