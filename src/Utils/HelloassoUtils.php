<?php

namespace App\Utils;

use App\Entity\AchatMonnaie;
use App\Entity\AchatMonnaieAdherent;
use App\Entity\AchatMonnaiePrestataire;
use App\Entity\Cotisation;
use App\Entity\CotisationAdherent;
use App\Entity\CotisationPrestataire;
use App\Entity\Don;
use App\Entity\DonAdherent;
use App\Entity\DonPrestataire;
use App\Entity\Flux;
use App\Entity\HelloAsso;
use App\Entity\Prestataire;
use App\Entity\Siege;
use App\Enum\MoyenEnum;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

class HelloassoUtils
{
    protected $em;
    protected $security;
    protected $eventDispatcher;
    protected $operationUtils;
    protected $client;

    public function __construct(
        Security $security,
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger,
        OperationUtils $operationUtils
    ) {
        $this->security = $security;
        $this->logger = $logger;
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->operationUtils = $operationUtils;
    }

    public function addDonation(HelloAsso $helloasso): ?Don
    {
        if (!empty($helloasso->getAdherent())) {
            $flux = new DonAdherent();
            $flux->setExpediteur($helloasso->getAdherent());
            $flux->setDestinataire($this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]));
        } elseif (!empty($helloasso->getPrestataire())) {
            $flux = new DonPrestataire();
            $flux->setExpediteur($helloasso->getPrestataire());
            $flux->setDestinataire($this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]));
        }
        $flux->setRole('ROLE_HELLOASSO');
        $flux->setDestinataire($this->em->getRepository(Prestataire::class)->getPrestataireMLC());
        $flux->setMontant($helloasso->getAmount());
        $flux->setMoyen(MoyenEnum::MOYEN_HELLOASSO);
        $flux->setReference('Helloasso #' . $helloasso->getHelloassoid());
        $flux->setData(['Helloasso ' => '#' . $helloasso->getHelloassoid()]);

        $this->em->persist($flux);
        $this->em->flush();

        // Write operations for this flux !
        $this->operationUtils->executeOperations($flux);

        return $flux;
    }

    public function addAchatEmlc(HelloAsso $helloasso): ?AchatMonnaie
    {
        if (!empty($helloasso->getAdherent())) {
            $flux = new AchatMonnaieAdherent();
            $flux->setDestinataire($helloasso->getAdherent());
        } elseif (!empty($helloasso->getPrestataire())) {
            $flux = new AchatMonnaiePrestataire();
            $flux->setDestinataire($helloasso->getPrestataire());
        } else {
            return null;
        }
        // $flux->setOperateur(null);
        $flux->setRole('ROLE_HELLOASSO');
        $flux->setExpediteur($this->em->getRepository(Siege::class)->getTheOne());
        $flux->setMontant($helloasso->getAmount());
        $flux->setMoyen(MoyenEnum::MOYEN_HELLOASSO);
        $flux->setReference('Helloasso #' . $helloasso->getHelloassoid());
        $flux->setReconverti(true);
        $flux->setData(['Helloasso' => '#' . $helloasso->getHelloassoid()]);

        $this->em->persist($flux);
        $this->em->flush();

        // Write operations for this flux !
        $this->operationUtils->executeOperations($flux);

        return $flux;
    }

    public function addCotisation(HelloAsso $helloasso): ?Cotisation
    {
        if (!empty($helloasso->getAdherent())) {
            $cotisation = new CotisationAdherent();
            $cotisation->setExpediteur($helloasso->getAdherent());
        } elseif (!empty($helloasso->getPrestataire())) {
            $cotisation = new CotisationPrestataire();
            $cotisation->setExpediteur($helloasso->getPrestataire());
        } else {
            throw new \Exception('Impossible de créer la cotisation, ni adherent ni prestataire trouvé !');
        }
        $now = new DateTime();
        $cotisation->setRecu(true);
        $cotisation->setReference('Helloasso #' . $helloasso->getHelloassoid());
        // $cotisation->setOperateur(null);
        $cotisation->setRole('ROLE_HELLOASSO');
        $mlcPrestataire = $this->em->getRepository(Prestataire::class)->findOneBy(['mlc' => true]);
        $cotisation->setDestinataire($mlcPrestataire);
        $cotisation->setMoyen(MoyenEnum::MOYEN_HELLOASSO);

        $cotisation->setMontant($helloasso->getAmount());
        $cotisation->getCotisationInfos()->setAnnee(date('Y'));
        $cotisation->getCotisationInfos()->setDebut($now);
        $cotisation->getCotisationInfos()->setFin(new DateTime('+ 1 year'));

        $this->em->persist($cotisation);
        $this->em->flush();

        $this->operationUtils->executeOperations($cotisation);

        return $cotisation;
    }
}
