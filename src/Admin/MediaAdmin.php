<?php

namespace App\Admin;

use Sonata\AdminBundle\Object\Metadata;
use Sonata\MediaBundle\Admin\ORM\MediaAdmin as BaseMediaAdmin;
use Sonata\MediaBundle\Provider\MediaProviderInterface;

/**
 * Administration des medias (image, document...).
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class MediaAdmin extends BaseMediaAdmin
{
    public function getObjectMetadata($object)
    {
        $provider = $this->pool->getProvider($object->getProviderName());

        $url = $provider->generatePublicUrl(
            $object,
            $provider->getFormatName($object, MediaProviderInterface::FORMAT_ADMIN)
        );

        return new Metadata($object->getName(), $object->getDescription(), $url);
    }

    // public function getDashboardActions()
    // {
    //     // $actions = parent::getDashboardActions();
    //     // unset($actions['list']);
    //     return [];
    // }
}
