<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Administration des traductions.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class TraductionAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'lexik_translation_overview';
    protected $baseRoutePattern = 'translations';

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->add('list', 'list', [
            '_controller' => 'LexikTranslationBundle:Translation:overview',
        ]);
        $collection->add('grid', 'grid', [
            '_controller' => 'LexikTranslationBundle:Translation:grid',
        ]);
        $collection->add('new', 'new', [
            '_controller' => 'LexikTranslationBundle:Translation:new',
        ]);
    }
}
