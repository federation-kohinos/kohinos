<?php

namespace App\Admin;

use App\Entity\User;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Security;

/**
 * Administration des pages.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class PageAdmin extends AbstractAdmin
{
    protected $security;
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'updatedAt',
    ];

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    public function configure()
    {
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('user')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $page = $this->getSubject();
        if ($this->isCurrentRoute('create')) {
            $page->setUser($this->security->getUser());
        }
        $formMapper
            ->add('user', HiddenType::class, [
                'data' => $this->security->getUser(),
                'data_class' => null,
                'entity_class' => User::class,
                'em' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager(),
            ])
            ->add('name', TextType::class, [
                'label' => 'Titre :',
                'required' => true,
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Contenu :',
                'required' => true,
            ])
            ->add('metaDescription', TextType::class, [
                'label' => 'Meta Description :',
                'required' => true,
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => 'Meta Keywords :',
                'required' => true,
            ])
            ->add('template', TextType::class, [
                'label' => 'Template :',
                'required' => false,
            ])
            ->add('css', TextareaType::class, [
                'label' => 'CSS :',
                'required' => false,
            ])
            ->add('js', TextareaType::class, [
                'label' => 'JS :',
                'required' => false,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Activé ?',
                'label_attr' => ['class' => 'checkbox-inline'],
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('user', null, ['label' => 'Auteur'])
            ->addIdentifier('name', null, ['label' => 'Titre'])
            ->addIdentifier('url', null, ['label' => 'Url'])
            ->add('_action', null, [
                'actions' => ['edit' => []],
            ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
}
