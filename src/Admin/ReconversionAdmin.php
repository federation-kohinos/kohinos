<?php

namespace App\Admin;

use App\Entity\Flux;
use App\Entity\User;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Security\Core\Security;

/**
 * Administration des reconversions (transfert d'un prestataire au siège).
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class ReconversionAdmin extends FluxAdmin
{
    protected $security;
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    ];

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $user = $this->security->getUser();
        $query = parent::createQuery($context);
        $query
            ->andWhere($query->getRootAliases()[0] . '.parenttype = :parenttype')
            ->setParameter('parenttype', Flux::TYPE_RECONVERSION);

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('createdAt', null, ['label' => 'Date'])
            ->addIdentifier('operateur', User::class, ['label' => 'Operateur'])
            ->addIdentifier('expediteur', null, ['label' => 'Expediteur'])
            ->add('montant', 'decimal', ['label' => 'Montant initial', 'attributes' => ['fraction_digits' => 2]])
            ->add('tauxreconversionpercent', 'percent', ['label' => 'Taux reconversion'])
            ->add('montantareconvertir', 'decimal', ['label' => 'Montant à transférer', 'attributes' => ['fraction_digits' => 2]])
            ->add('montantcommission', 'decimal', ['label' => 'Commission', 'attributes' => ['fraction_digits' => 2]])
            ->add('reference', null, ['label' => 'Reference'])
            ->add('reconverti', null, ['label' => 'Reconverti ?', 'datatype' => 'App.Flux', 'template' => '@kohinos/block/reconverti_field.html.twig'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->remove('transfert_or_transaction')
            ->remove('type')
            ->remove('operateur')
        ;
    }

    public function getDataSourceIterator()
    {
        $iterator = parent::getDataSourceIterator();
        $iterator->setDateTimeFormat('d/m/Y H:i:s'); //change this to suit your needs

        return $iterator;
    }

    public function getExportFields()
    {
        return [
            'Date' => 'created_at',
            'Operateur' => 'operateurAndRole',
            'Expediteur' => 'expediteur',
            'Montant' => 'montant',
            'tauxreconversionpercent' => 'tauxreconversionpercent',
            'montantareconvertir' => 'montantareconvertir',
            'montantcommission' => 'montantcommission',
            'montantcommission' => 'montantcommission',
            'Reference' => 'reference',
            'Reconverti ?' => 'reconverti',
        ];
    }
}
