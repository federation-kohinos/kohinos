<?php

namespace App\Admin;

use App\Entity\Adherent;
use App\Entity\Flux;
use App\Entity\GlobalParameter;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Administration des cotisations des adhérents.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class CotisationAdherentAdmin extends CotisationAdmin
{
    protected $baseRouteName = 'cotisation_adherent';
    protected $baseRoutePattern = 'cotisation_adherent';

    public function configure()
    {
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $user = $this->security->getUser();
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAliases()[0] . '.operateur', 'u')
              ->andWhere($query->getRootAliases()[0] . ".type='cotisation_adherent'")
              // ->andWhere('u.adherent IS NOT NULL')
        ;
        // if ($this->hasRequest()) {
        //     if (empty($this->getRequest()->getSession()->get('_groupegere'))) {
        //         if ($this->security->isGranted('ROLE_GESTION_GROUPE') || $this->security->isGranted('ROLE_CONTACT') || $this->security->isGranted('ROLE_TRESORIER')) {
        //             if (!$this->security->isGranted('ROLE_TRESORIER')) {
        //                 $query->andWhere('false = true');
        //             }
        //         }
        //     } else {
        //         $groupe = $this->getRequest()->getSession()->get('_groupegere');
        //         $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        //         $adhTable = $em->getMetadataFactory()->getMetadataFor(Adherent::class)->getTableName();
        //         $fluxTable = $em->getMetadataFactory()->getMetadataFor(Flux::class)->getTableName();
        //         $connection = $em->getConnection();
        //         $statement = $connection->prepare("SELECT f.id FROM $fluxTable f INNER JOIN $adhTable a ON (a.id = f.adherent_id OR a.id = f.adherent_dest_id) WHERE a.groupe_id = :groupe_id");
        //         $statement->bindValue(':groupe_id', $groupe->getId());
        //         $statement->execute();
        //         $ids = $statement->fetchAll();
        //         $query
        //             ->andWhere($query->expr()->in($query->getRootAliases()[0] . '.id', ':ids'))
        //             ->setParameter('ids', $ids)
        //         ;
        //     }
        // }

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        parent::configureShowFields($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('expediteur', null, ['label' => 'Adhérent'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $cotisation = $this->getSubject();
        $expediteurInfos = [
            'label' => 'Adhérent',
            'class' => Adherent::class,
            'choices' => $em->getRepository(Adherent::class)->findOrderByName(),
            'placeholder' => 'Choisir un adhérent',
            'required' => true,
        ];
        if ($this->hasRequest()) {
            $exp = $this->getRequest()->get('expediteur');
            if (!empty($exp)) {
                $expediteurInfos['data'] = $em->getRepository(Adherent::class)->findOneById($exp);
            }
        }
        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Cotisation', ['class' => 'col-md-8'])
                ->add('montant', MoneyType::class, [
                    'label' => 'Montant en euro(s)',
                    'required' => true,
                    'scale' => 2,
                    'data' => (float) (null !== $cotisation->getMontant()) ? $cotisation->getMontant() : floatval($em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_ADHERENT)),
                    'help' => 'Montant minimum des cotisations adhérent : ' . floatval($em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_ADHERENT)) . '€',
                    'constraints' => [
                        new Regex(['pattern' => '/[0-9]{1,}(\.[0-9]{1,2})?/']),
                    ],
                ])
                ->add('reference', TextType::class, [
                    'required' => false,
                ])
                ->add('expediteur', EntityType::class, $expediteurInfos)
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('expediteur', null, [
                'label' => 'Adherent',
            ])
        ;
        parent::configureListFields($listMapper);
    }
}
