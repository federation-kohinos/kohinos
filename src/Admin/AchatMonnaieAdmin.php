<?php

namespace App\Admin;

use App\Entity\Flux;
use App\Entity\GlobalParameter;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Administration des flux 'Achat de monnaie'.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Damien Moulard <dam.moulard@gmail.com>
 */
class AchatMonnaieAdmin extends FluxAdmin
{
    protected $baseRouteName = 'achat_monnaie';
    protected $baseRoutePattern = 'achat_monnaie';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere($query->getRootAliases()[0] . '.parenttype = :parenttype')
                ->setParameter('parenttype', Flux::TYPE_ACHAT);

        return $query;
    }

    // protected function configureRoutes(RouteCollection $collection)
    // {
    //     $collection->clearExcept(['create', 'list', 'export']);
    // }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper->add('type', null, [
            'advanced_filter' => false,
            'show_filter' => true,
            'field_type' => ChoiceType::class,
            'field_options' => [
                'choices' => ['Adhérent' => 'achat_monnaie_adherent', 'Prestataire' => 'achat_monnaie_prestataire'],
                'placeholder' => 'Indifférent',
                'expanded' => true,
                'multiple' => false,
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->remove('type')
            ->remove('expediteur')
        ;
    }

    public function getTotalLabel()
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        return $this->translator->trans('Total des achats de e' . $em->getRepository(GlobalParameter::class)->val(GlobalParameter::MLC_SYMBOL));
    }

    public function getTotal()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $query = clone $datagrid->getQuery();
        $query
            ->select('SUM( ' . $query->getRootAlias() . '.montant) as total')
            ->andWhere($query->getRootAlias() . '.parenttype = :parenttype')
            ->setParameter('parenttype', Flux::TYPE_ACHAT)
            ->setFirstResult(null)
            ->setMaxResults(null);

        $result = $query->execute([], \Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);

        return $result;
    }
}
