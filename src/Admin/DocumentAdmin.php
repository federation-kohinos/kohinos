<?php

namespace App\Admin;

use App\Entity\User;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Administration des documents.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class DocumentAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'document';
    protected $baseRoutePattern = 'document';

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $document = $this->getSubject();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken() ? $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser() : null;
        $formMapper
            ->add('user', HiddenType::class, [
                'data' => $user,
                'data_class' => null,
                'entity_class' => User::class,
                'em' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager(),
            ])
            ->add('media', MediaType::class, [
                'provider' => 'sonata.media.provider.file',
                'context' => 'document',
                'label' => 'Document',
            ])
            ->add('name', TextType::class, [
                'label' => 'Titre :',
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Texte',
                'required' => false,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Activé ?',
                'required' => false,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('name', null, ['label' => 'Titre'])
            ->addIdentifier('content', 'html', ['truncate' => ['length' => 80], 'label' => 'Description'])
            ->addIdentifier('media', null, ['label' => 'Fichier'])
            ->add('enabled', null, [
                'label' => 'Activé',
                'editable' => true,
            ])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $object = $showMapper->getAdmin()->getSubject();
        $showMapper
            ->add('createdAt', null, ['label' => 'Date de création'])
            ->add('name', null, ['label' => 'Nom'])
            ->add('content', null, ['label' => 'Description'])
            // ->add('media')
            ->add('media', null, ['label' => 'Url du media', 'template' => '@kohinos/bundles/SonataAdminBundle/Document/url_document.html.twig'])
        ;
    }
}
