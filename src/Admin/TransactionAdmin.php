<?php

namespace App\Admin;

use App\Entity\Flux;
use App\Entity\Transaction;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Sonata\Form\Type\DateTimeRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Administration des transactions.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class TransactionAdmin extends FluxAdmin
{
    protected $translator;
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    ];

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere($query->getRootAliases()[0] . '.parenttype = :parenttype')
                ->setParameter('parenttype', Flux::TYPE_TRANSACTION);

        return $query;
    }

    public function getTotalLabel()
    {
        return $this->translator->trans('Total des transactions');
    }

    public function getTotal()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $query = clone $datagrid->getQuery();
        $query
            ->select('SUM( ' . $query->getRootAlias() . '.montant) as total')
            ->andWhere($query->getRootAlias() . '.parenttype = :type')
            ->setParameter('type', Flux::TYPE_TRANSACTION)
            ->setFirstResult(null)
            ->setMaxResults(null);

        $result = $query->execute([], \Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->add('createdAt', 'datetime', ['label' => 'Date'])
            ->add('type', 'text', ['label' => 'Type', 'template' => '@kohinos/bundles/SonataAdminBundle/Block/translated_value.html.twig'])
            ->add('montant', 'decimal', ['label' => 'Montant', 'attributes' => ['fraction_digits' => 2]])
        ;
        if ($this->security->isGranted('ROLE_CONTROLEUR')) {
            $listMapper
                ->add('expediteur', null, ['label' => 'Expediteur'])
                ->add('destinataire', null, ['label' => 'Destinataire'])
                ->add('operateur', null, ['label' => 'Operateur'])
                ->addIdentifier('reference', null, ['label' => 'Reference'])
            ;
        }
        // @TODO : ajouter le verify uniquement si l'on souhaite (param url)=> sinon c'est beaucoup trop long...
        // $listMapper
        //     ->addIdentifier('verify', null, array('label' => 'Vérifié'))
        // ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('type', StringFilter::class, [
                'label' => 'Type de transaction',
                'advanced_filter' => false,
                'show_filter' => true,
            ], ChoiceType::class, [
                'choices' => Transaction::getAvailableTypes(),
            ])
            ->add('operateur', null, [
                'label' => 'Operateur',
                'advanced_filter' => false,
                'show_filter' => true,
            ])
            ->add('createdAt', 'doctrine_orm_datetime_range', [
                'field_type' => DateTimeRangePickerType::class,
                'label' => 'Date',
            ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'export']);
    }
}
