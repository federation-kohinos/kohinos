<?php

namespace App\Admin;

use App\Entity\User;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Administration des FAQ.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class FaqAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $document = $this->getSubject();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken() ? $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser() : null;
        $formMapper
            ->add('user', HiddenType::class, [
                'data' => $user,
                'data_class' => null,
                'entity_class' => User::class,
                'em' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager(),
            ])
            ->add('name', TextType::class, [
                'label' => 'Titre :',
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Texte',
                'required' => false,
            ])
            ->add('fichier', MediaType::class, [
                'provider' => 'sonata.media.provider.file',
                'context' => 'faq',
                'required' => false,
            ])
            ->add('image', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context' => 'faq',
                'required' => false,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Activé ?',
                'label_attr' => ['class' => 'checkbox-inline'],
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('name', null, ['label' => 'Titre'])
            ->add('content', 'html', ['label' => 'Text', 'sortable' => false])
            ->add('fichier', null, ['label' => 'Fichier'])
            // ->add('image', null, ['label' => 'Image'])
            ->add('image', null, ['label' => 'Icône', 'template' => '@kohinos/bundles/SonataAdminBundle/Image/preview_image_o.html.twig'])
            ->addIdentifier('user', null, ['label' => 'Auteur'])
            ->add('enabled', null, [
                'label' => 'Activé',
                'editable' => true,
            ])
        ;
    }
}
