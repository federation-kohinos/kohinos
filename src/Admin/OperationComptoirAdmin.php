<?php

namespace App\Admin;

use App\Entity\AccountComptoir;
use App\Entity\Comptoir;
use App\Entity\OperationComptoir;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/**
 * Administration des operation des comptoirs.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class OperationComptoirAdmin extends OperationAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('account.accountableObject', null, ['label' => 'Compte Comptoir', 'template' => '@kohinos/bundles/SonataAdminBundle/Block/accountable.html.twig']);
        parent::configureListFields($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $user = $this->security->getUser();
        $query = parent::createQuery($context);
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $operationComptoirtable = $em->getMetadataFactory()->getMetadataFor(OperationComptoir::class)->getTableName();

        if ($user && ($this->security->isGranted('ROLE_GESTION_GROUPE') || $this->security->isGranted('ROLE_CONTACT') || $this->security->isGranted('ROLE_TRESORIER'))) {
            if ($this->hasRequest()) {
                if (empty($this->getRequest()->getSession()->get('_groupegere')) && $this->security->isGranted('ROLE_GESTION_GROUPE')) {
                    $query->andWhere('false = true');
                } elseif (empty($this->getRequest()->getSession()->get('_groupecontactgere')) && $this->security->isGranted('ROLE_CONTACT')) {
                    $query->andWhere('false = true');
                } elseif (empty($this->getRequest()->getSession()->get('_groupetresoriergere')) && $this->security->isGranted('ROLE_TRESORIER')) {
                    $query->andWhere('false = true');
                } else {
                    $groupe = $this->getRequest()->getSession()->get('_groupegere');
                    if (empty($groupe)) {
                        $groupe = $this->getRequest()->getSession()->get('_groupecontactgere');
                        if (empty($groupe)) {
                            $groupe = $this->getRequest()->getSession()->get('_groupetresoriergere');
                        }
                    }
                    $connection = $em->getConnection();
                    $comptoirTable = $em->getMetadataFactory()->getMetadataFor(Comptoir::class)->getTableName();
                    $accountTable = $em->getMetadataFactory()->getMetadataFor(AccountComptoir::class)->getTableName();
                    $statement = $connection->prepare('SELECT f.id FROM ' . $operationComptoirtable . ' f WHERE f.account_id IN  
                        (SELECT a.id FROM ' . $accountTable . ' a WHERE a.comptoir_id IN 
                            (SELECT p.id FROM ' . $comptoirTable . ' p WHERE p.groupe_id = "' . $groupe->getId() . '")
                        )');
                    $statement->execute();
                    $ids = $statement->fetchAll();
                    $query
                        ->andWhere($query->expr()->in($query->getRootAliases()[0] . '.id', ':ids'))
                        ->setParameter('ids', $ids)
                    ;
                }
            }
        }

        return $query;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('account.comptoir', null, [
                'label' => 'Comptoir',
                'advanced_filter' => false,
                'show_filter' => true,
            ])
        ;
    }
}
