<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Import de données.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class ImportAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'importdata';
    protected $baseRouteName = 'importdata';
    protected $datagridValues = [
        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',
        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'createdAt',
        // '_page' => 1,
        // '_per_page' => 32
    ];

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'create', 'show']);
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query
            ->innerJoin($query->getRootAliases()[0] . '.user', 'u')
            ->addSelect('u')
        ;

        return $query;
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        $list['create']['template'] = '@kohinos/bundles/SonataAdminBundle/CRUD/import_button.html.twig';
        // $list['show']['template'] = '@kohinos/bundles/SonataAdminBundle/CRUD/import_button.html.twig';

        return $list;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        parent::configureListFields($listMapper);
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('createdAt', 'datetime', ['label' => 'Date'])
            ->add('user', null, ['label' => 'Utilisateur'])
            ->addIdentifier('type')
            ->add('nbentityadded', null, ['label' => 'Lignes correctes'])
            ->add('nbentityerror', null, ['label' => 'Lignes en erreurs'])
            ->add('media', null, ['label' => 'Fichier'])
            ->add('test', null, ['label' => 'Test ?'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                ],
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $object = $showMapper->getAdmin()->getSubject();
        $showMapper
            ->with('Import')
                ->add('createdAt')
                ->add('test')
                ->add('user')
                ->add('type')
                ->add('media')
                ->add('nbentityadded', null, ['label' => 'Lignes correctes'])
                ->add('nbentityerror', null, ['label' => 'Lignes en erreurs'])
            ->end();
        if (!empty(json_decode($object->getSuccess()))) {
            $showMapper
                ->with('Correct', [
                    'box_class' => 'box box-solid box-success',
                    // 'description' => 'Lorem ipsum',
                ])
                    ->add('success', 'json', ['label' => false])
                ->end()
            ;
        }
        if (!empty(json_decode($object->getWarnings()))) {
            $showMapper
                ->with('Warnings', [
                    'box_class' => 'box box-solid box-warning',
                    // 'description' => 'Lorem ipsum',
                ])
                ->add('warnings', 'json', ['label' => false])
                ->end()
                ;
        }
        if (!empty(json_decode($object->getErrors()))) {
            $showMapper
                ->with('Erreurs', [
                    'box_class' => 'box box-solid box-danger',
                    // 'description' => 'Lorem ipsum',
                ])
                    ->add('errors', 'json', ['label' => false])
                ->end()
            ;
        }
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
}
