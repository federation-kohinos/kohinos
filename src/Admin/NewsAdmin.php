<?php

namespace App\Admin;

use App\Entity\User;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Administration des news.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class NewsAdmin extends AbstractAdmin
{
    protected $translator;
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    // protected function configureDefaultSortValues(array &$sortValues): void
    // {
    //     $sortValues[DatagridInterface::PAGE] = 1;
    //     $sortValues[DatagridInterface::SORT_ORDER] = 'ASC';
    //     $sortValues[DatagridInterface::SORT_BY] = 'position';
    // }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $document = $this->getSubject();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken() ? $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser() : null;
        $formMapper
            ->add('user', HiddenType::class, [
                'data' => $user,
                'data_class' => null,
                'entity_class' => User::class,
                'em' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager(),
            ])
            ->add('media', MediaType::class, [
                'provider' => 'sonata.media.provider.image',
                'context' => 'actualites',
                'label' => 'Image',
            ])
            ->add('name', TextType::class, [
                'label' => 'Titre :',
            ])
            ->add('content', CKEditorType::class, [
                'label' => 'Texte',
                'required' => false,
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Activé ?',
                'required' => false,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
            ->add('visibleByAllGroups', CheckboxType::class, [
                'label' => 'Visible par tous les groupes ?',
                'required' => false,
                'label_attr' => ['class' => 'checkbox-inline'],
            ])
        ;
    }

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->add('move', $this->getRouterIdParameter() . '/move/{position}');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('name', null, ['label' => 'Titre'])
            ->addIdentifier('content', 'html', ['truncate' => ['length' => 80], 'label' => 'Description'])
            ->addIdentifier('media', null, ['label' => 'Fichier'])
            ->addIdentifier('enabled', null, ['label' => 'Activé', 'datatype' => 'App.Document', 'template' => '@kohinos/bundles/SonataAdminBundle/Boolean/editable_boolean.html.twig'])
            ->add('_action', null, [
                'actions' => [
                    'move' => [
                        'template' => '@PixSortableBehavior/Default/_sort.html.twig',
                    ],
                ],
            ]);
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
}
