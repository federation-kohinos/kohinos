<?php

namespace App\Admin;

use App\Enum\HelloassoStateEnum;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\Form\Type\DateTimeRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Gestion des paiements / cotisations HelloAsso.
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class HelloAssoAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'helloasso';
    protected $baseRouteName = 'helloasso';
    protected $datagridValues = [
        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',
        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'createdAt',
        // '_page' => 1,
        // '_per_page' => 32
    ];

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'show']);
        $collection->add('synchro');
        $collection->add('synchrotest');
    }

    public function configureActionButtons($action, $object = null)
    {
        $buttonList = parent::configureActionButtons($action, $object);
        $buttonList['synchro'] = ['template' => '@kohinos/admin/helloasso_synchro_button.html.twig'];
        $buttonList['synchrotest'] = ['template' => '@kohinos/admin/helloasso_synchro_test_button.html.twig'];

        return $buttonList;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        parent::configureListFields($listMapper);
        unset($this->listModes['mosaic']);
        $listMapper
            ->add('createdAt', 'datetime', [
                'label' => 'Date',
            ])
            ->add('helloassoid')
            ->add('type')
            ->add('amount', null, [
                'label' => 'Montant',
            ])
            // ->add('data')
            ->add('fulluser', null, [
                'label' => 'Utilisateur',
            ])
            ->add('fullpayer', null, [
                'label' => 'Payeur',
            ])
            ->add(
                'prestaoradh',
                null,
                [
                    'label' => 'Presta ou Adhérent',
                    'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list_presta_or_adherent.html.twig',
                ]
            )
            ->add('state', null, [
                'label' => 'Etat',
            ])
            ->add('statePayment', null, [
                'label' => 'Etat du paiement',
            ])
            ->add('operationState', null, [
                'label' => 'Etat de l\'opération',
            ])
            ->add('errors', null, [
                'label' => 'Erreurs',
            ])
            ->add('flux')
            ->add('historical', null, [
                'label' => 'Historique ?',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('createdAt', 'doctrine_orm_datetime_range', [
                'field_type' => DateTimeRangePickerType::class,
                'label' => 'Date de création',
            ])
            ->add('state', 'doctrine_orm_string', [
                'advanced_filter' => false,
                'show_filter' => true,
                'label' => 'Etat',
            ], ChoiceType::class, [
                'choices' => [
                    'Waiting' => 'Waiting',
                    'Processed' => 'Processed',
                    'Registered' => 'Registered',
                    'Deleted' => 'Deleted',
                    'Refunded' => 'Refunded',
                    'Unknown' => 'Unknown',
                    'Canceled' => 'Canceled',
                    'Contested' => 'Contested',
                ],
            ])
            ->add('statePayment', 'doctrine_orm_string', [
                'advanced_filter' => false,
                'show_filter' => true,
                'label' => 'Etat du paiement',
            ], ChoiceType::class, [
                'choices' => [
                    'Pending' => 'Pending',
                    'Authorized' => 'Authorized',
                    'Refused' => 'Refused',
                    'Unknown' => 'Unknown',
                    'Registered' => 'Registered',
                    'Error' => 'Error',
                    'Refunded' => 'Refunded',
                    'Refunding' => 'Refunding',
                    'Waiting' => 'Waiting',
                    'Canceled' => 'Canceled',
                    'Contested' => 'Contested',
                    'WaitingBankValidation' => 'WaitingBankValidation',
                    'WaitingBankWithdraw' => 'WaitingBankWithdraw',
                ],
            ])
            ->add('operationState', null, [
                'advanced_filter' => false,
                'show_filter' => true,
                'label' => 'Etat de l\'opération',
            ], ChoiceType::class, [
                'choices' => HelloassoStateEnum::getAvailableTypes(),
                'choice_label' => function ($choice) {
                    return HelloassoStateEnum::getTypeName($choice);
                },
            ])
            ->add('prestataire', null, [
                'label' => 'Prestataire',
                'advanced_filter' => false,
            ])
            ->add('adherent', null, [
                'label' => 'Adhérent',
                'advanced_filter' => false,
            ])
            ->add('historical', null, [
                'label' => 'Historique',
                'advanced_filter' => false,
            ])
        ;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        parent::configure();
        $this->setTemplate('list', '@kohinos/admin/helloasso_list.html.twig');
    }
}
