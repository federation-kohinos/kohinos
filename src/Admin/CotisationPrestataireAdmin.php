<?php

namespace App\Admin;

use App\Entity\Flux;
use App\Entity\GlobalParameter;
use App\Entity\Prestataire;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Administration des cotisations des prestataires.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class CotisationPrestataireAdmin extends CotisationAdmin
{
    protected $baseRouteName = 'cotisation_prestataire';
    protected $baseRoutePattern = 'cotisation_prestataire';

    public function configure()
    {
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $user = $this->security->getUser();
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAliases()[0] . '.operateur', 'u')
              ->andWhere($query->getRootAliases()[0] . ".type='cotisation_prestataire'")
              // ->andWhere('u.prestataire IS NOT NULL')
        ;
        if ($this->hasRequest()) {
            if ($this->security->isGranted('ROLE_GESTION_GROUPE') || $this->security->isGranted('ROLE_CONTACT') || $this->security->isGranted('ROLE_TRESORIER')) {
                if (empty($this->getRequest()->getSession()->get('_groupegere')) && $this->security->isGranted('ROLE_GESTION_GROUPE')) {
                    $query->andWhere('false = true');
                } elseif (empty($this->getRequest()->getSession()->get('_groupecontactgere')) && $this->security->isGranted('ROLE_CONTACT')) {
                    $query->andWhere('false = true');
                } elseif (empty($this->getRequest()->getSession()->get('_groupetresoriergere')) && $this->security->isGranted('ROLE_TRESORIER')) {
                    $query->andWhere('false = true');
                } else {
                    $groupe = $this->getRequest()->getSession()->get('_groupegere');
                    if (empty($groupe)) {
                        $groupe = $this->getRequest()->getSession()->get('_groupecontactgere');
                        if (empty($groupe)) {
                            $groupe = $this->getRequest()->getSession()->get('_groupetresoriergere');
                        }
                    }
                    if (!empty($groupe)) {
                        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
                        $fluxTable = $em->getMetadataFactory()->getMetadataFor(Flux::class)->getTableName();
                        $prestaTable = $em->getMetadataFactory()->getMetadataFor(Prestataire::class)->getTableName();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("SELECT f.id FROM $fluxTable f INNER JOIN $prestaTable a ON (a.id = f.prestataire_id OR a.id = f.prestataire_dest_id) WHERE a.groupe_id = :groupe_id");
                        $statement->bindValue(':groupe_id', $groupe->getId());
                        $statement->execute();
                        $ids = $statement->fetchAll();
                        $query
                            ->andWhere($query->expr()->in($query->getRootAliases()[0] . '.id', ':ids'))
                            ->setParameter('ids', $ids)
                        ;
                    }
                }
            }
        }

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        parent::configureShowFields($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('expediteur', null, ['label' => 'Prestataire'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $cotisation = $this->getSubject();
        $expediteurInfos = [
            'label' => 'Prestataire',
            'class' => Prestataire::class,
            'choices' => $em->getRepository(Prestataire::class)->findDefault(),
            'placeholder' => 'Choisir un prestataire',
            'required' => true,
        ];
        if ($this->hasRequest()) {
            $exp = $this->getRequest()->get('expediteur');
            if (!empty($exp)) {
                $expediteurInfos['data'] = $em->getRepository(Prestataire::class)->findOneById($exp);
            }
        }
        parent::configureFormFields($formMapper);
        $formMapper
            ->with('Cotisation', ['class' => 'col-md-8'])
                ->add('montant', MoneyType::class, [
                    'label' => 'Montant en euro(s)',
                    'required' => true,
                    'scale' => 2,
                    'data' => (null !== $cotisation->getMontant()) ? $cotisation->getMontant() : floatval($em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_PRESTATAIRE)),
                    'help' => 'Montant minimum des cotisations prestataire : ' . floatval($em->getRepository(GlobalParameter::class)->val(GlobalParameter::COTISATION_PRESTATAIRE)) . '€',
                    'constraints' => [
                        new Regex(['pattern' => '/[0-9]{1,}(\.[0-9]{1,2})?/']),
                    ],
                ])
                ->add('reference', TextType::class, [
                    'required' => true,
                ])
                ->add('expediteur', EntityType::class, $expediteurInfos)
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper): void
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('expediteur', null, ['label' => 'Prestataire']);
        parent::configureListFields($listMapper);
    }
}
