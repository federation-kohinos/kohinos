<?php

namespace App\Admin;

use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\FileProvider;

/**
 * Import provider.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class ImportProvider extends FileProvider
{
    /**
     * @param MediaInterface $media
     */
    protected function doTransform(MediaInterface $media)
    {
        // ...
    }

    /**
     * {@inheritdoc}
     */
    public function generatePublicUrl(MediaInterface $media, $format)
    {
        // new logic
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist(MediaInterface $media)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate(MediaInterface $media)
    {
    }
}
