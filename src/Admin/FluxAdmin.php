<?php

namespace App\Admin;

use App\Entity\Flux;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\Form\Type\DateTimeRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as SChoiceType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Administration des Flux.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class FluxAdmin extends AbstractAdmin
{
    protected $translator;
    protected $security;
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
        '_per_page' => 250,
    ];
    protected $maxPerPage = 250;
    protected $perPageOptions = [50, 100, 250, 500, 1000];

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = $this->getModelManager()->createQuery(Flux::class);
        foreach ($this->extensions as $extension) {
            $extension->configureQuery($this, $query, $context);
        }

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'export']);
    }

    protected function configureExportFields(): array
    {
        return [
            $this->trans('Date') => 'createdAt',
            $this->trans('Expediteur') => 'expediteur',
            $this->trans('Destinataire') => 'destinataire',
            $this->trans('Type_parent') => 'parenttype',
            $this->trans('Type') => 'type',
            $this->trans('Montant') => 'montant',
            $this->trans('Moyen') => 'moyen',
            $this->trans('Operateur') => 'operateur',
            $this->trans('Référence') => 'reference',
            $this->trans('Importé') => 'historical',
        ];
    }

    public function getDataSourceIterator()
    {
        $iterator = parent::getDataSourceIterator();
        $iterator->setDateTimeFormat('d/m/Y H:i:s'); //change this to suit your needs

        return $iterator;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->add('createdAt', 'datetime', ['label' => 'Date'])
            ->add('type', 'text', ['label' => 'Type', 'template' => '@kohinos/bundles/SonataAdminBundle/Block/translated_value.html.twig'])
            ->add('montant', 'decimal', ['label' => 'Montant', 'attributes' => ['fraction_digits' => 2]])
            ->add('expediteur', null, ['label' => 'Expediteur'])
            ->add('destinataire', null, ['label' => 'Destinataire'])
            ->add('operateur', null, ['label' => 'Operateur'])
            ->addIdentifier('reference', null, ['label' => 'Reference'])
            // @TODO : ajouter le verify uniquement si l'on souhaite (param url)=> sinon c'est beaucoup trop long...
            // ->addIdentifier('verify', null, array('label' => 'Vérifié'))
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('transfert_or_transaction', 'doctrine_orm_callback', [
                'label' => 'Type',
                'callback' => function ($queryBuilder, $alias, $field, $value) {
                    if (!$value['value']) {
                        return;
                    }
                    $queryBuilder
                        ->where($alias . '.parenttype = :type')
                        ->setParameter('type', $value['value']);

                    return true;
                },
                'advanced_filter' => false,
                'show_filter' => true,
                'field_type' => SChoiceType::class,
                'field_options' => [
                    'choices' => ['Cotisations' => Flux::TYPE_COTISATION, 'Achat de monnaie' => Flux::TYPE_ACHAT, 'Dépôt de billets' => Flux::TYPE_CHANGE, 'Retrait' => Flux::TYPE_RETRAIT, 'Transactions' => Flux::TYPE_TRANSACTION, 'Transferts' => Flux::TYPE_TRANSFERT, 'Vente' => Flux::TYPE_VENTE],
                    'placeholder' => 'Indifférent',
                    'expanded' => true,
                    'multiple' => false,
                ],
            ])
            ->add('type', null, [
                'label' => 'Type plus précis',
                'advanced_filter' => false,
                'show_filter' => true,
            ])
            ->add('operateur', null, [
                'label' => 'Operateur',
                'advanced_filter' => false,
                'show_filter' => true,
            ])
            ->add('createdAt', 'doctrine_orm_datetime_range', [
                'field_type' => DateTimeRangePickerType::class,
                'label' => 'Date',
            ])
        ;
    }

    public function getTemplate($name)
    {
        // if ('list' == $name) {
        //     return '@kohinos/block/base_list_with_total.html.twig';
        // }

        return parent::getTemplate($name);
    }

    public function getTotalLabel()
    {
        return $this->translator->trans('Total des flux');
    }

    public function getTotal()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $query = clone $datagrid->getQuery();
        $query
            ->select('SUM( ' . $query->getRootAlias() . '.montant) as total')
            ->setFirstResult(null)
            ->setMaxResults(null);

        $result = $query->execute([], \Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);

        return $result;
    }

    public function getExportFields()
    {
        return [
            'Date' => 'created_at',
            'Type' => 'type',
            'Montant' => 'montant',
            'Expediteur' => 'expediteur',
            'Destinataire' => 'destinataire',
            'Operateur' => 'operateur',
            'Moyen' => 'moyen',
            'Reference' => 'reference',
        ];
    }
}
