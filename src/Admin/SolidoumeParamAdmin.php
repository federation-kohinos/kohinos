<?php

namespace App\Admin;

use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Administration des paramètres  à Solidoume.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class SolidoumeParamAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'solidoumeparam';
    protected $baseRoutePattern = 'solidoumeparam';
    protected $security;

    public function getTemplate($name)
    {
        if ('list' == $name) {
            return '@kohinos/admin/solidoumeparameter_list.html.twig';
        }

        return parent::getTemplate($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('user', null, ['label' => 'Utilisateur'])
            ->addIdentifier('adherent', null, ['label' => 'Adhérent'])
            ->addIdentifier('amount', null, ['label' => 'Montant'])
            ->addIdentifier('paiementDate', null, ['label' => 'Date de prélèvement'])
            ->addIdentifier('enabled', null, ['label' => 'Activé', 'datatype' => 'App.SolidoumeItem', 'template' => '@kohinos/bundles/SonataAdminBundle/Boolean/editable_boolean.html.twig'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                ],
            ]);
    }

    protected function configureSideMenu(ItemInterface $menu, string $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && in_array($action, ['list'])) {
            return;
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
        $collection->add('redistribution');
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
}
