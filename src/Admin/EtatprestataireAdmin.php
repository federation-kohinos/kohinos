<?php

namespace App\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Security;

/**
 * Administration des tags/états des prestataires (rappel, suspendus, ...).
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class EtatprestataireAdmin extends AbstractAdmin
{
    protected $security;
    protected $baseRouteName = 'tags';
    protected $baseRoutePattern = 'tags';
    protected $datagridValues = [
        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',
        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'name',
    ];

    public function configure()
    {
        parent::configure();
    }

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Informations', ['class' => 'col-md-7'])
                ->add('name', TextType::class, [
                    'label' => 'Nom :',
                    'required' => true,
                ])
                ->add('content', CKEditorType::class, [
                    'label' => 'Description :',
                    // 'config' => ['enterMode' => 'CKEDITOR.ENTER_BR'],
                    'required' => false,
                ])
                ->add('enabled', CheckboxType::class, [
                    'label' => 'Visibilité prestataire ?',
                    'required' => false,
                    'label_attr' => ['class' => 'checkbox-inline'],
                ])
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->addIdentifier('name', null, [
                'label' => 'Nom',
            ])
            ->add('content', null, [
                'label' => 'Description',
                'sortable' => false,
            ])
            ->add('getPrestatairesCount', null, [
                'label' => 'Nombre de prestas',
                'sortable' => false,
            ])
            ->add('enabled', null, [
                'label' => 'Visibilité prestataire',
                'editable' => true,
            ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->remove('delete');
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();
        // unset($actions['list']);
        return $actions;
    }
}
