<?php

namespace App\Admin;

use App\Entity\Siege;
use App\Enum\CurrencyEnum;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Administration des operation du siege.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class OperationSiegeAdmin extends OperationAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('currency', 'doctrine_orm_callback', [
                'callback' => function ($queryBuilder, $alias, $field, $value) {
                    if (!$value['value']) {
                        return;
                    }

                    $queryBuilder
                        ->andWhere($queryBuilder->expr()->in($alias . '.currency', ':types'))
                        ->setParameter('types', $value['value']);

                    return true;
                },
                'label' => 'Devise',
                'advanced_filter' => false,
                'show_filter' => true,
                'field_type' => ChoiceType::class,
                'field_options' => [
                    'choices' => CurrencyEnum::getAvailableTypes(),
                    'choice_label' => function ($choice) {
                        return CurrencyEnum::getTypeName($choice);
                    },
                    'empty_data' => [CurrencyEnum::CURRENCY_EMLC],
                    'data' => [CurrencyEnum::CURRENCY_EMLC],
                    'placeholder' => false,
                    'expanded' => true,
                    'multiple' => true,
                ],
            ])
        ;
    }
}
