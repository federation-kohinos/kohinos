<?php

namespace App\Admin;

use App\Entity\GlobalParameter;
use App\Form\Type\BooleanParameterType;
use App\Repository\GlobalParameterRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Administration des paramètres généraux de l'outil de MLC.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class GlobalParameterAdmin extends AbstractAdmin
{
    protected $translator;
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
        '_per_page' => 500, // Augmenter le nombre de résultats par page
    ];
    
    private $globalParameterRepository;
    
    public function __construct(
        string $code, 
        string $class, 
        string $baseControllerName,
        GlobalParameterRepository $globalParameterRepository
    ) {
        parent::__construct($code, $class, $baseControllerName);
        $this->globalParameterRepository = $globalParameterRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, [
                'label' => 'Nom :',
                'attr' => ['class' => 'text-uppercase'],
            ])
            ->add('description', TextType::class, [
                'label' => 'Description :',
            ])
            ->add('value', TextType::class, [
                'label' => 'Valeur :',
            ])
            ->add('mandatory', HiddenType::class, [
                'empty_data' => false,
                'data' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $listMapper
            ->add('name', null, ['label' => 'Nom'])
            ->add('description', null, ['label' => 'Description'])
            ->add('value', null, ['editable' => true, 'truncate' => ['length' => 80], 'label' => 'Value'])
            ->add('_action', null, [
                'label' => 'Action(s)',
                'actions' => [
                    'edit' => ['template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_edit_gp.html.twig'],
                    'delete' => ['template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_delete_gp.html.twig'],
                ],
            ])
        ;
    }

    public function getTemplate($name)
    {
        if ('list' == $name) {
            return '@kohinos/admin/globalparameter_list.html.twig';
        }

        return parent::getTemplate($name);
    }
    
    /**
     * Récupère tous les paramètres globaux pour l'affichage dans la vue admin
     * 
     * @return GlobalParameter[] Tous les paramètres globaux
     */
    public function getAllParameters()
    {
        return $this->globalParameterRepository->findAllParameters();
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);
        unset($actions['edit']);

        return $actions;
    }
    
    /**
     * Récupère le texte d'aide pour un paramètre donné
     * 
     * @param string $paramName Nom du paramètre
     * @return string|null Texte d'aide ou null si non trouvé
     */
    public function help($paramName)
    {
        $helpTexts = [
            // Configuration MLC
            'MLC_SYMBOL' => '3 caractères maximum',
            
            // Cotisations
            'COTISATION_ADHERENT' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
            'COTISATION_ADHERENT_DEFAULT' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
            'COTISATION_PRESTATAIRE' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
            'RECONVERSION_PRESTATAIRE' => 'Ne pas utiliser de virgule ",", utiliser le point "."',
            
            // Carte
            'MAP_CENTER' => "Pour déterminer le centre et zoom de la carte <a target='_blank' rel='noopener noreferrer' href='https://www.openstreetmap.org/'>Ouvrir OpenStreetMap</a><br/>
                Format: [latitude,longitude] exemple: [45.7811,3.0927]",
            
            // Modules
            'USE_WORDPRESS' => 'Si cette option est activée, le site web public du Kohinos sera désactivé, seul la page de connexion et l\'administration seront accesssible !',
            'USE_PAYZEN' => 'Si cette option est activée, les utilisateurs pourront cotiser et acheter de la monnaie locale par CB sur l\'application (compte Payzen activé obligatoire).',
            'USE_SOLIDOUME' => 'Si cette option est activée, les utilisateurs pourront accéder et participer au programme de Sécurité Sociale alimentaire',
            'USE_HELLOASSO' => 'Si cette option est activée, les utilisateurs pourront cotiser et acheter de la monnaie locale par CB sur l\'application (compte Helloasso activé obligatoire).',
            
            // Autres paramètres
            'ALL_TICKETS' => 'Valeurs séparés par une virgule, le séparateur décimal est le point',
            
            // HelloAsso
            'HELLOASSO_CLIENTID' => 'Vous pouvez le trouver en étant connecté sur helloasso, dans Mon compte > Intégration et API',
            'HELLOASSO_CLIENTSECRET' => 'Vous pouvez le trouver en étant connecté sur helloasso, dans Mon compte > Intégration et API',
            
            // Achat de monnaie
            'ACHAT_MONNAIE_VAL_RADIO_AMOUNT' => 'Valeurs séparées par des virgules',
        ];
        
        return isset($helpTexts[$paramName]) ? $helpTexts[$paramName] : null;
    }

    /**
     * Récupère la configuration des sections et paramètres pour l'affichage
     * 
     * @return array Configuration des sections et paramètres
     */
    public function getParameterSections()
    {
        // Structure de configuration des sections
        $sections = [
            'mlc' => [
                'title' => 'Configuration MLC',
                'color' => 'rgba(0, 123, 255, 0.7)', // primary (bleu)
                'order' => 1,
                'parameters' => [
                    'MLC_URL',
                    'MLC_NAME',
                    'MLC_NAME_SMALL',
                    'MLC_SYMBOL',
                    'ALL_TICKETS',
                    'RECONVERSION_PRESTA',
                    'MLC_CONTACT_EMAIL',
                    'MLC_NOTIF_EMAIL'
                ]
            ],
            'map' => [
                'title' => 'Carte',
                'color' => 'rgba(142, 197, 108, 0.7)', // info (cyan)
                'order' => 2,
                'parameters' => [
                    'MAP_CENTER',
                    'MAP_ZOOM'
                ]
            ],
            'cotisations' => [
                'title' => 'Cotisations',
                'color' => 'rgba(213, 106, 50, 0.7)', // success (vert)
                'order' => 3,
                'parameters' => [
                    'COTISATION_ADHERENT',
                    'COTISATION_ADHERENT_DEFAULT',
                    'COTISATION_FREE_AMOUNT',
                    'COTISATION_PRESTATAIRE',
                    'ACCEPT_DON_ADHERENT_COTISATION',
                    'ACCEPT_DON_PRESTATAIRE_COTISATION'
                ]
            ],
            'achat_monnaie' => [
                'title' => 'Achat de Monnaie',
                'color' => 'rgba(175, 174, 76, 0.7)', // Un autre vert (vert clair)
                'order' => 4,
                'parameters' => [
                    'ACHAT_MONNAIE_SHOW_MANUEL_AMOUNT',
                    'ACHAT_MONNAIE_SHOW_SLIDER_AMOUNT',
                    'ACHAT_MONNAIE_SHOW_RADIO_AMOUNT',
                    'ACHAT_MONNAIE_VAL_RADIO_AMOUNT',
                    'ACCEPT_DON_ADHERENT_ACHAT',
                    'ACCEPT_DON_PRESTATAIRE_ACHAT'
                ]
            ],
            'payments' => [
                'title' => 'Paiements',
                'color' => 'rgba(152, 85, 206 , 0.7)', // warning (jaune)
                'order' => 5,
                'parameters' => [
                    'ACHAT_MONNAIE_CAN_PAY_BY_CHECK_OR_TRANFERT',
                    'CHECK_ORDER',
                    'CHECK_ADDRESS',
                    'IBAN_ASSOCIATION',
                    'IBAN_GUARANTY',
                    'USE_PAYZEN',
                    'USE_HELLOASSO',
                    'HELLOASSO_CLIENTID',
                    'HELLOASSO_CLIENTSECRET',
                    'HELLOASSO_URL_EMLC_ADHERENT',
                    'HELLOASSO_URL_COTISATION_ADHERENT',
                    'HELLOASSO_URL_EMLC_PRESTATAIRE',
                    'HELLOASSO_URL_COTISATION_PRESTATAIRE'
                ]
            ],
            'other' => [
                'title' => 'Autres paramètres',
                'color' => 'rgba(108, 117, 125, 0.7)', // default (gris)
                'order' => 7,
                'parameters' => [
                    'RECONVERSION_PRESTATAIRE',
                    'ADHESION_TEXT',
                    'USE_SOLIDOUME',
                    'USE_WORDPRESS'
                ]
            ]
        ];
        
        // Récupérer tous les paramètres
        $allParameters = $this->globalParameterRepository->findAllParameters();
        $parametersByName = [];
        
        // Créer un tableau associatif des paramètres par nom
        foreach ($allParameters as $parameter) {
            $parametersByName[$parameter->getName()] = $parameter;
        }
        
        // Liste des paramètres déjà assignés à une section
        $assignedParameters = [];
        
        // Assigner les paramètres aux sections
        foreach ($sections as $sectionKey => &$section) {
            $sectionParameters = [];
            
            foreach ($section['parameters'] as $paramName) {
                if (isset($parametersByName[$paramName])) {
                    $sectionParameters[] = $parametersByName[$paramName];
                    $assignedParameters[] = $paramName;
                }
            }
            
            $section['parameters'] = $sectionParameters;
        }
        
        // Ajouter les paramètres non assignés à la section "Autres paramètres"
        foreach ($parametersByName as $name => $parameter) {
            if (!in_array($name, $assignedParameters)) {
                $sections['other']['parameters'][] = $parameter;
            }
        }
        
        // Trier les sections par ordre
        uasort($sections, function($a, $b) {
            return $a['order'] <=> $b['order'];
        });
        
        return $sections;
    }
}
