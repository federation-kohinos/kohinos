<?php

namespace App\Admin;

use App\Entity\AccountPrestataire;
use App\Entity\Adherent;
use App\Entity\ContactPrestataire;
use App\Entity\CotisationAdherent;
use App\Entity\CotisationPrestataire;
use App\Entity\EtatPrestataire;
use App\Entity\GlobalParameter;
use App\Entity\Groupe;
use App\Entity\Prestataire;
use App\Entity\Rubrique;
use App\Entity\User;
use App\Entity\Usergroup;
use App\Enum\CurrencyEnum;
use App\Events\MLCEvents;
use App\Events\PrestataireEvent;
use App\Exporter\CustomDoctrineORMQuerySourceIterator;
use App\Form\Type\ContactEntityFormType;
use App\Form\Type\GeolocPrestataireFormType;
use App\Form\Type\UserFormType;
use Doctrine\ORM\Query;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Model\UserManagerInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Object\Metadata;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\OrderByToSelectWalker;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Sonata\Form\Type\DateTimeRangePickerType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Sonata\MediaBundle\Provider\Pool;
use SpecShaper\GdprBundle\Form\Type\PersonalDataType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Administration des prestataires.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class PrestataireAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'prestataire';
    protected $baseRoutePattern = 'prestataire';
    protected $security;
    protected $eventDispatcher;
    protected $pool;
    protected $datagridValues = [
        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',
        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'raison',
        // '_page' => 1,
        // '_per_page' => 32
    ];

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    public function setPool(Pool $pool)
    {
        $this->pool = $pool;
    }

    public function configure()
    {
        parent::configure();
    }

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $user = $this->security->getUser();
        $query = parent::createQuery($context);

        // if ($user && ($this->security->isGranted('ROLE_GESTION_GROUPE') || $this->security->isGranted('ROLE_CONTACT') || $this->security->isGranted('ROLE_TRESORIER'))) {
        //     if ($this->hasRequest()) {
        //         if (empty($this->getRequest()->getSession()->get('_groupegere'))) {
        //             if (!$this->security->isGranted('ROLE_TRESORIER')) {
        //                 $query->andWhere('false = true');
        //             }
        //         } else {
        //             $query
        //                 ->andWhere($query->getRootAliases()[0] . '.groupe = :group')
        //                 ->setParameter('group', $this->getRequest()->getSession()->get('_groupegere'))
        //             ;
        //         }
        //     }
        // }

        return $query;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }
        $user = $this->security->getUser();
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        $presta = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Prestataire::class)->findOneById($id);
        $users = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(User::class)->findByPrestataire($id);

        if ($this->isGranted('EDIT') && !empty($users)) {
            // @TODO : si plusieurs utilisateurs, mettre un lien par user pour modifier celui-ci ?
            // $menu->addChild("Modifier l'utilisateur", [
            //     'uri' => $this->getConfigurationPool ()->getContainer()->get('router')->generate('admin_app_user_edit', ['id' => $user->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
            // ]);
        }
        // Le prestataire "Monnaie Locale" représentant l'asso recevant les cotisations n'a pas de cotisations lui même !
        if (!$presta->isMlc()) {
            $menu->addChild('Ajouter une cotisation', [
                'uri' => $this->getConfigurationPool()->getContainer()->get('router')->generate('cotisation_prestataire_create', ['expediteur' => $id], UrlGeneratorInterface::ABSOLUTE_URL),
            ]);
            $menu->addChild('Voir les cotisations', [
                'uri' => $this->getConfigurationPool()->getContainer()->get('router')->generate('cotisation_prestataire_list', ['filter' => ['expediteur' => ['value' => $id]]], UrlGeneratorInterface::ABSOLUTE_URL),
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        // Initialize prestataire
        $presta = $this->getSubject();
        // $user = $this->security->getUser();
        $now = new \DateTime();
        $cotisation = null;

        // get the current Image instance
        $imageHelp = null;
        if (!empty($presta) && !empty($presta->getMedia())) {
            $image = $presta->getMedia();
            if ($image && ($webPath = $image->getWebPath())) {
                // get the container so the full path to the image can be set
                $container = $this->getConfigurationPool()->getContainer();
                $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/' . $webPath;
                // add a 'help' option containing the preview's img tag
                $imageHelp = '<img src="' . $fullPath . '" class="admin-preview" />';
            }
        }

        $formMapper
            ->tab('Prestataire')
                ->with('Prestataire', ['class' => 'col-md-6'])
                    ->add('typeprestataire', null, [
                        'label' => 'Type :',
                        'required' => true,
                        'expanded' => true,
                    ])
                    ->add('groupe', ChoiceType::class, [
                        'required' => false,
                        'label' => 'Groupe local :',
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Groupe::class)->findBy(['enabled' => true], ['name' => 'ASC']),
                        'choice_label' => 'name',
                        'placeholder' => 'Choisir un groupe',
                    ])
                    ->add('raison', TextType::class, [
                        'label' => 'Raison :',
                        'required' => true,
                    ])
                    ->add('statut', TextType::class, [
                        'label' => 'Activité :',
                        'required' => false,
                    ])
                    ->add('siret', TextType::class, [
                        'label' => 'SIRET :',
                        'required' => false,
                    ])
                    ->add('iban', PersonalDataType::class, [
                        'label' => 'IBAN :',
                        'required' => false,
                        'attr' => [
                            'placeholder' => 'Entrez un IBAN valide',
                        ],
                    ])
                    ->add('responsable', TextType::class, [
                        'label' => 'Responsable :',
                        'required' => false,
                    ])
                    ->add('metier', TextType::class, [
                        'label' => 'Métier responsable :',
                        'required' => false,
                    ])
                    ->add('horaires', CKEditorType::class, [
                        'label' => 'Horaires',
                        'required' => false,
                    ])
                    ->add('web', UrlType::class, [
                        'label' => 'Site Web',
                        'required' => false,
                    ])
                    ->add('description', CKEditorType::class, [
                        'label' => 'Description',
                        'required' => false,
                    ])
                    ->add('comments', CKEditorType::class, [
                        'label' => 'Observations (privé)',
                        'required' => false,
                    ])
                    ->add('tauxreconversion', PercentType::class, [
                        'label' => 'Taux de reconversion',
                        'required' => false,
                        'scale' => 2,
                        'type' => 'integer',
                        'help' => 'Si la valeur est nulle, par défaut le taux de reconversion sera de ' . $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(GlobalParameter::class)->val(GlobalParameter::RECONVERSION_PRESTATAIRE) . ' %',
                    ])
                    ->add('acceptemlc', null, [
                        'label' => 'Accepte la monnaie électronique ?',
                        'required' => false,
                    ])
                    ->add('enabled', null, [
                        'label' => 'Activé ?',
                        'required' => false,
                    ])
                ->end()
                ->with('Contact(s)', ['class' => 'col-md-6'])
                    ->add('contacts', CollectionType::class, [
                        'entry_type' => ContactEntityFormType::class,
                        'entry_options' => ['label' => false, 'data_class' => ContactPrestataire::class],
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                        'label' => false,
                    ])
                ->end()
                ->with('Adresse(s)', ['class' => 'col-md-6'])
                    ->add('geolocs', CollectionType::class, [
                        'entry_type' => GeolocPrestataireFormType::class,
                        'entry_options' => ['label' => false],
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                        'label' => false,
                    ])
                ->end()
                ->with('Classification', ['class' => 'col-md-6'])
                    ->add('etats', EntityType::class, [
                        'class' => EtatPrestataire::class,
                        'multiple' => true,
                        'required' => false,
                        'by_reference' => false,
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(EtatPrestataire::class)->findBy([]),
                        'label' => 'Tags',
                    ])
                    ->add('rubriques', EntityType::class, [
                        'class' => Rubrique::class,
                        'multiple' => true,
                        'required' => false,
                        'by_reference' => false,
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Rubrique::class)->findBy(['enabled' => true]),
                        'label' => 'Rubriques',
                    ])
                ->end()
                ->with('Image', ['class' => 'col-md-6'])
                    ->add('media', MediaType::class, [
                        'provider' => 'sonata.media.provider.image',
                        'context' => 'prestataire',
                        'help' => $imageHelp,
                        'required' => false,
                    ])
                ->end()
                ->with('Gestionnaire(s)', ['class' => 'col-md-6'])
                    ->add('users', EntityType::class, [
                        // 'mapped' => false,
                        'class' => User::class,
                        'multiple' => true,
                        'required' => false,
                        'label' => 'Associer à un(des) utilisateur(s) existant :',
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(User::class)->findOrderByName(),
                        'choice_label' => function ($user) {
                            return $user->getLastname() . ' ' . $user->getFirstname() . ' (' . $user->getEmail() . ')';
                        },
                        'placeholder' => 'Choisir un utilisateur',
                    ])
                    ->add('newusers', CollectionType::class, [
                        'mapped' => false,
                        'required' => false,
                        'label' => 'Nouvel Utilisateur/Adhérent',
                        'entry_type' => UserFormType::class,
                        'entry_options' => [
                            'required' => false,
                            'label' => false, ],
                        'by_reference' => false,
                        'allow_add' => true,
                        'allow_delete' => true,
                    ])
                ->end()
                ->with('Caissier(s)', ['class' => 'col-md-6'])
                    ->add('caissiers', EntityType::class, [
                        // 'mapped' => false,
                        'class' => User::class,
                        'multiple' => true,
                        'required' => false,
                        'label' => 'Associer à un(des) utilisateur(s) existant :',
                        'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(User::class)->findOrderByName(),
                        'choice_label' => function ($user) {
                            return $user->getLastname() . ' ' . $user->getFirstname() . ' (' . $user->getEmail() . ')';
                        },
                        'placeholder' => 'Choisir un utilisateur',
                    ])
                    ->add('newcaissiers', CollectionType::class, [
                        'mapped' => false,
                        'required' => false,
                        'label' => 'Nouvel Utilisateur/Adhérent',
                        'entry_type' => UserFormType::class,
                        'entry_options' => [
                            'required' => false,
                            'label' => false, ],
                        'by_reference' => false,
                        'allow_add' => true,
                        'allow_delete' => true,
                    ])
                ->end()
            ->end()
        ;
        // @TODO : add tags model transformer if add new from text
        // ->get('etats')
        // ->addModelTransformer(new CallbackTransformer(
        //     function ($tagsAsArray) {
        //         // transform the array to a string
        //         return $tagsAsArray;
        //     },
        //     function ($tagsAsString) {
        //         // transform the string back to an array
        //         return explode(', ', $tagsAsString);
        //     }
        // ))

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper->getFormBuilder()->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $prestataire = $event->getData();
            $users = null;
            /*
                Permet d'ajouter le nouvel utilisateur crée (newusers) aux gestionnaires du presta
                (On crée un compte adhérent en même temps que le prestataire)
             */
            if (null != $event->getForm()->get('users')->getData()) {
                $users = $event->getForm()->get('users')->getData();
                $this->addUsersOrCaissers($users, $prestataire);
            }
            if (null != $event->getForm()->get('caissiers')->getData()) {
                $caissiers = $event->getForm()->get('caissiers')->getData();
                $this->addUsersOrCaissers($caissiers, $prestataire, 'ROLE_CAISSIER');
            }
            if (null != $event->getForm()->get('newusers')->getData()) {
                $newusers = $event->getForm()->get('newusers')->getData();
                $return = $this->addUsersOrCaissers($newusers, $prestataire);
                if (false === $return) {
                    $event->getForm()->get('newusers')->addError(new FormError('Gestionnaires : Courriel(s) déjà utilisé : ' . implode(', ', $return) . '!'));
                }
            }
            if (null != $event->getForm()->get('newcaissiers')->getData()) {
                $newcaissiers = $event->getForm()->get('newcaissiers')->getData();
                $return = $this->addUsersOrCaissers($newcaissiers, $prestataire, 'ROLE_CAISSIER');
                if (count($return) > 0) {
                    $event->getForm()->get('newcaissiers')->addError(new FormError('Caissiers : Courriel(s) déjà utilisé : ' . implode(', ', $return) . '!'));
                }
            }
        });
        parent::configureFormFields($formMapper);
    }

    private function addUsersOrCaissers($users, Prestataire $prestataire, string $role = 'ROLE_PRESTATAIRE')
    {
        $return = [];
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        foreach ($users as $user) {
            $newUser = false;
            if (null == $user || null == $user->getId()) {
                $newUser = true;
                $emailExist = $em->getRepository(User::class)->findBy(['email' => $user->getEmail()]);
                if (count($emailExist) > 0) {
                    $return[] = $emailExist;
                    break;
                }
                $user->setUsername($user->getEmail());
                $adh = new Adherent();
                $user->setAdherent($adh);
                $user->setPassword(md5(random_bytes(10)));
                $groupeAdh = $em->getRepository(Usergroup::class)->findOneByName('Adherent');
                $user->addPossiblegroup($groupeAdh);
            }
            if ('ROLE_PRESTATAIRE' == $role) {
                $prestataire->addUser($user);
                $groupePresta = $em->getRepository(Usergroup::class)->findOneByName('Prestataire');
                if ($newUser) {
                    $this->eventDispatcher->dispatch(MLCEvents::REGISTRATION_PRESTATAIRE, new PrestataireEvent($user, $prestataire, $this->getRequest()));
                }
            } elseif ('ROLE_CAISSIER' == $role) {
                $prestataire->addCaissier($user);
                $groupePresta = $em->getRepository(Usergroup::class)->findOneByName('Caissier');
                if ($newUser) {
                    $this->eventDispatcher->dispatch(MLCEvents::REGISTRATION_CAISSIER, new PrestataireEvent($user, $prestataire, $this->getRequest()));
                }
            }
            $user->addPossiblegroup($groupePresta);
            $this->userManager->updateCanonicalFields($user);
            $em->persist($user);
            if ($newUser) {
                $this->eventDispatcher->dispatch(MLCEvents::REGISTRATION_ADHERENT, new UserEvent($user, $this->getRequest()));
            }
            $em->persist($prestataire);
        }
        $em->flush();

        return $return;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
        ->add('full_text', CallbackFilter::class, [
            'callback' => [$this, 'getFullTextFilter'],
            'field_type' => TextType::class,
            'label' => 'Recherche par nom',
            'show_filter' => true,
            'advanced_filter' => false,
        ])
        ->add('cotisationajour', CallbackFilter::class, [
            'callback' => [$this, 'getCotisationFilter'],
            'field_type' => ChoiceType::class,
            'field_options' => [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ],
            'multiple' => false,
            'label' => 'Cotisation à jour ?',
            'show_filter' => true,
            'advanced_filter' => false,
        ])
        ->add('gestionnaireajour', CallbackFilter::class, [
            'callback' => [$this, 'getGestionnaireFilter'],
            'field_type' => ChoiceType::class,
            'field_options' => [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ],
            'multiple' => false,
            'label' => 'Gestionnaire à jour de sa cotisation ?',
            'show_filter' => true,
            'advanced_filter' => false,
        ])
        ->add('groupe', null, [
            'label' => 'Groupe',
            'show_filter' => true,
            'advanced_filter' => false,
        ])
        ->add('etats', null, [
            'label' => 'Tags',
            'show_filter' => true,
            'advanced_filter' => false,
        ], null, ['expanded' => true, 'multiple' => true,
        ])
        ->add('enabled', null, [
            'label' => 'Activé',
            'advanced_filter' => false,
        ])
        ->add('acceptemlc', null, [
            'label' => 'Accepte e-mlc',
            'advanced_filter' => false,
        ])
        ->add('createdAt', 'doctrine_orm_datetime_range', [
            'field_type' => DateTimeRangePickerType::class,
            'label' => 'Date de création',
        ])
        ->add('updatedAt', 'doctrine_orm_datetime_range', [
            'field_type' => DateTimeRangePickerType::class,
            'label' => 'Date de mise à jour',
        ])
        ->add('typeprestataire', null, [
            'label' => 'Type',
            'advanced_filter' => false,
        ])
        ;
    }

    public function getGestionnaireFilter($queryBuilder, $alias, $field, $value)
    {
        if (null === $value['value']) {
            return false;
        }

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $expr = $em->getExpressionBuilder();

        $a = $em->getRepository(CotisationAdherent::class)
            ->createQueryBuilder('ca')
            ->select('aa.id')
            ->leftJoin('ca.expediteur', 'aa')
            ->leftJoin('ca.cotisationInfos', 'ii')
            ->where('ii.fin > :now')
            ->getQuery()
            ->getDQL();

        if (true === $value['value']) {
            $queryBuilder
                ->leftJoin($alias . '.users', 'uu')
                ->leftJoin('uu.adherent', 'dd')
                ->andWhere(
                    $expr->in(
                        'dd.id',
                        $a
                    )
            );
        } else {
            $queryBuilder
                ->leftJoin($alias . '.users', 'uu')
                ->leftJoin('uu.adherent', 'dd')
                ->andWhere(
                    $expr->notIn(
                        'dd.id',
                        $a
                    )
            );
        }
        $queryBuilder->setParameter('now', new \DateTime('now'));

        return true;
    }

    public function getCotisationFilter($queryBuilder, $alias, $field, $value)
    {
        if (null === $value['value']) {
            return false;
        }

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $expr = $em->getExpressionBuilder();

        $a = $em->getRepository(CotisationPrestataire::class)
            ->createQueryBuilder('c')
            ->select('a.id')
            ->leftJoin('c.expediteur', 'a')
            ->leftJoin('c.cotisationInfos', 'i')
            ->where('i.fin > :now')
            ->getQuery()
            ->getDQL();

        if (true === $value['value']) {
            $queryBuilder->andWhere(
                $expr->in(
                    $alias . '.id',
                    $a
                )
            );
        } else {
            $queryBuilder->andWhere(
                $expr->notIn(
                    $alias . '.id',
                    $a
                )
            );
        }
        $queryBuilder->setParameter('now', new \DateTime('now'));

        return true;
    }

    public function getFullTextFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }

        // Use `andWhere` instead of `where` to prevent overriding existing `where` conditions
        $queryBuilder->andWhere($queryBuilder->expr()->orX(
            $queryBuilder->expr()->like($alias . '.raison', $queryBuilder->expr()->literal('%' . $value['value'] . '%')),
            $queryBuilder->expr()->like($alias . '.statut', $queryBuilder->expr()->literal('%' . $value['value'] . '%')),
            $queryBuilder->expr()->like($alias . '.description', $queryBuilder->expr()->literal('%' . $value['value'] . '%'))
        ));

        return true;
    }

    public function getTemplate($name)
    {
        if ('edit' == $name) {
            return '@kohinos/presta/block/base_edit_prestataires.html.twig';
        }

        return parent::getTemplate($name);
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager): void
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    public function getRouteShowOnFront($object)
    {
        return $this->routeGenerator->generate('show_prestataire', ['slug' => $object->getSlug()]);
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        unset($this->listModes['mosaic']);
        $isWordpressActivated = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(GlobalParameter::class)->val(GlobalParameter::USE_WORDPRESS);
        if ('false' == $isWordpressActivated) {
            $actions = [
                'show' => ['template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_showonfront.html.twig'],
                'edit' => [
                    // You may add custom link parameters used to generate the action url
                    'link_parameters' => [
                        'full' => true,
                    ],
                ],
            ];
            if (null != $this->security->getUser() && ($this->security->isGranted('ROLE_SUPER_ADMIN') || $this->security->isGranted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_ALL') || $this->security->isGranted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_CREATE'))) {
                $actions['addfreecotisationpresta'] = [
                    'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_addfreecotisationpresta.html.twig',
                ];
                $actions['addfreecotisationadh'] = [
                    'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_addfreecotisationadh.html.twig',
                ];
            }
        } else {
            $actions = [
                'edit' => [
                    // You may add custom link parameters used to generate the action url
                    'link_parameters' => [
                        'full' => true,
                    ],
                ],
            ];
            if (null != $this->security->getUser() && ($this->security->isGranted('ROLE_SUPER_ADMIN') || $this->security->isGranted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_ALL') || $this->security->isGranted('ROLE_ADMIN_PRESTATAIRE_COTISATIONS_CREATE'))) {
                $actions['addfreecotisationpresta'] = [
                    'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_addfreecotisationpresta.html.twig',
                ];
                $actions['addfreecotisationadh'] = [
                    'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list__action_addfreecotisationadh.html.twig',
                ];
            }
        }
        $listMapper
        ->addIdentifier('raisonAndIdmlc', 'html', [
            'label' => 'Raison',
        ])
        ->add('groupe', null, [
            'label' => 'Groupe',
            'sortable' => true,
            'sort_field_mapping' => ['fieldName' => 'name'],
            'sort_parent_association_mappings' => [['fieldName' => 'groupe']],
        ])
        // ->addIdentifier('users', null, array('label' => 'Gestionnaires'))
        ->add('rubriques', null)
        ->add('etats', null, [
            'label' => 'Tags',
            // 'editable' => true,
            // 'class' => EtatPrestataire::class,
            // 'multiple' => true,
            // // 'required' => false,
            // // 'by_reference' => false,
            // 'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(EtatPrestataire::class)->findBy(array('enabled' => true))
        ])
        ->add(
            'cotisation',
            null,
            [
                'label' => 'Cotisation à jour',
                'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list_presta_cotisation.html.twig',
            ]
        )
        ->add('users', null, [
            'label' => 'Gestionnaires [Cotisation à jour]',
            'template' => '@kohinos/bundles/SonataAdminBundle/CRUD/list_presta_gestionnaires.html.twig',
        ])
        ->add('enabled', null, [
            'label' => 'Activé',
            'editable' => true,
        ])
        ->add('acceptemlc', null, [
            'label' => 'Accepte e-mlc',
            'editable' => true,
        ])
        ->add('createdAt', 'date', [
            'pattern' => 'dd/MM/YYYY HH:mm',
            'label' => 'Crée le',
        ])
        ->add('updatedAt', 'date', [
            'pattern' => 'dd/MM/YYYY HH:mm',
            'label' => 'Mis à jour le',
        ])
        // You may also specify the actions you want to be displayed in the list
        ->add('_action', null, [
            'actions' => $actions,
        ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection
            ->add('addfreecotisationpresta', $this->getRouterIdParameter() . '/addfreecotisationpresta')
            ->add('addfreecotisationadh', $this->getRouterIdParameter() . '/addfreecotisationadh')
        ;
    }

    public function getObjectMetadata($object)
    {
        if (null != $object->getMedia()) {
            $provider = $this->pool->getProvider($object->getMedia()->getProviderName());

            $url = $provider->generatePublicUrl(
                $object->getMedia(),
                $provider->getFormatName($object->getMedia(), MediaProviderInterface::FORMAT_ADMIN)
            );

            return new Metadata($object->getRaison(), strip_tags($object->getDescription()), $url);
        }

        return new Metadata($object->getRaison(), strip_tags($object->getDescription()));
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    public function preUpdate($prestataire)
    {
        $this->prePersist($prestataire);
    }

    public function prePersist($prestataire)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        $account = $em->getRepository(AccountPrestataire::class)->findOneBy(['prestataire' => $prestataire, 'currency' => CurrencyEnum::CURRENCY_EMLC]);
        if (null == $account) {
            $account = new AccountPrestataire();
            $account
                ->setCurrency(CurrencyEnum::CURRENCY_EMLC)
            ;
            $prestataire->addAccount($account);
            $em->persist($account);
        }
        $em->persist($prestataire);
        $em->flush();
    }

    public function getExportFields()
    {
        return [
            'Id' => 'id',
            'Activé ?' => 'enabled',
            'Raison' => 'raison',
            'Groupe local' => 'groupe.name',
            'Rubriques' => 'rubriquesString',
            'Gestionnaires' => 'usersString',
            'Gestionnaires emails' => 'gestionnairesEmailsString',
            'Tags' => 'etatsString',
            'Crée le' => 'createdAt',
            'Mise à jour le' => 'updatedAt',
        ];
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $fields = [];

        foreach ($this->getExportFields() as $key => $field) {
            // NEXT_MAJOR: Remove the following code in favor of the commented one.
            $label = $this->getTranslationLabel($field, 'export', 'label');
            $transLabel = $this->getTranslator()->trans($label, [], $this->getTranslationDomain());
            if ($transLabel === $label) {
                $fields[$key] = $field;
            } else {
                $fields[$transLabel] = $field;
            }
        }

        return $this->getModelDataSourceIterator($datagrid, $fields);
    }

    /**
     * @return DoctrineORMQuerySourceIterator
     */
    public function getModelDataSourceIterator(DatagridInterface $datagrid, array $fields, $firstResult = null, $maxResult = null)
    {
        $datagrid->buildPager();
        $query = $datagrid->getQuery();

        $query->select('DISTINCT ' . current($query->getRootAliases()));
        $query->setFirstResult($firstResult);
        $query->setMaxResults($maxResult);

        if ($query instanceof ProxyQueryInterface) {
            $sortBy = $query->getSortBy();

            if (!empty($sortBy)) {
                $query->addOrderBy($sortBy, $query->getSortOrder());
                $query = $query->getQuery();
                $query->setHint(Query::HINT_CUSTOM_TREE_WALKERS, [OrderByToSelectWalker::class]);
            } else {
                $query = $query->getQuery();
            }
        }

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $cotisationUtils = $this->getConfigurationPool()->getContainer()->get('app.utils.cotisations');

        $iterator = new CustomDoctrineORMQuerySourceIterator($cotisationUtils, $em, $query, $fields);
        $iterator->setDateTimeFormat('d/m/Y H:i:s'); //change this to suit your needs

        return $iterator;
    }
}
