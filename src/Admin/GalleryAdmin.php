<?php

namespace App\Admin;

use Sonata\MediaBundle\Admin\GalleryAdmin as BaseGalleryAdmin;

/**
 * Administration des galleries de medias (image, document...).
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class GalleryAdmin extends BaseGalleryAdmin
{
    // public function getDashboardActions()
    // {
    //     return [];
    // }
}
