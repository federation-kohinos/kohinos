<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\BooleanFilter;
use Sonata\Form\Type\BooleanType;
use Sonata\Form\Type\DateTimeRangePickerType;
use Sonata\Form\Type\EqualType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Security;

/**
 * Administration des flux 'Demande d'achat de monnaie'.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class AchatMonnaieAConfirmerAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'demande_achat_monnaie';
    protected $baseRoutePattern = 'demande_achat_monnaie';
    protected $security;

    protected $translator;
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
        '_per_page' => 100,
    ];
    protected $maxPerPage = 100;
    protected $perPageOptions = [50, 100, 250, 500, 1000];

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    protected function configureDefaultFilterValues(array &$filterValues)
    {
        $filterValues['reconverti'] = [
            'type' => EqualType::TYPE_IS_EQUAL, // => 1
            'value' => BooleanType::TYPE_NO,     // => 1
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('type', null, [
                'advanced_filter' => false,
                'show_filter' => true,
                'field_type' => ChoiceType::class,
                'field_options' => [
                    'choices' => ['Adhérent' => 'demande_achat_monnaie_adherent', 'Prestataire' => 'demande_achat_monnaie_prestataire'],
                    'placeholder' => 'Indifférent',
                    'expanded' => true,
                    'multiple' => false,
                ],
            ])
            ->add('reconverti', BooleanFilter::class, [
                'advanced_filter' => false,
                'show_filter' => true,
            ])
            ->add('createdAt', 'doctrine_orm_datetime_range', [
                'field_type' => DateTimeRangePickerType::class,
                'label' => 'Date',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $user = $this->security->getUser();
        if ($user && !$user->getCanValidateAchat() && !($this->security->isGranted('ROLE_SUPER_ADMIN') || $this->security->isGranted('ROLE_TRESORIER') || $this->security->isGranted('ROLE_ADMIN_SIEGE'))) {
            $collection->remove('delete');
        }
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        return $list;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);
        $user = $this->security->getUser();
        $listMapper
            ->add('createdAt', 'datetime', ['label' => 'Date'])
            ->add('destinataire', null, ['label' => 'Destinataire'])
            ->add('moyen', null, ['label' => 'Moyen'])
            ->add('montant', 'decimal', ['label' => 'Montant', 'attributes' => ['fraction_digits' => 2]])
            ->add('operateur', null, ['label' => 'Operateur'])
            ->add('reference', null, ['label' => 'Reference'])
        ;
        if ($user && $user->getCanValidateAchat() && ($this->security->isGranted('ROLE_SUPER_ADMIN') || $this->security->isGranted('ROLE_TRESORIER') || $this->security->isGranted('ROLE_ADMIN_SIEGE'))) {
            $listMapper
                ->add('_action', null, [
                    'actions' => [
                        'validate' => ['template' => '@kohinos/bundles/SonataAdminBundle/validate_achat_monnaie.html.twig'],
                        'delete' => [],
                        // 'edit' => [
                        //     // You may add custom link parameters used to generate the action url
                        //     'link_parameters' => [
                        //         'full' => true,
                        //     ],
                        // ],
                    ],
                ])
            ;
        }
    }
}
