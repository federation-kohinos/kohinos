<?php

namespace App\Admin;

use App\Entity\Flux;
use App\Entity\Prestataire;
use App\Entity\Transfert;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\StringFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Administration des transferts.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class TransfertAdmin extends FluxAdmin
{
    protected $security;
    protected $session;
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt',
    ];

    public function setSecurity(Security $security)
    {
        $this->security = $security;
    }

    public function setSession(SessionInterface $session)
    {
        $this->session = $session;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'export']);
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $user = $this->security->getUser();
        $query = parent::createQuery($context);
        $query
            ->andWhere($query->getRootAliases()[0] . '.parenttype = :parenttype')
            ->setParameter('parenttype', Flux::TYPE_TRANSFERT)
        ;

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $fluxtable = $em->getMetadataFactory()->getMetadataFor(Flux::class)->getTableName();
        if ($this->hasRequest()) {
            if ($this->security->isGranted('ROLE_GESTION_GROUPE') || $this->security->isGranted('ROLE_CONTACT') || $this->security->isGranted('ROLE_TRESORIER')) {
                if (empty($this->getRequest()->getSession()->get('_groupegere')) && $this->security->isGranted('ROLE_GESTION_GROUPE')) {
                    $query->andWhere('false = true');
                } elseif (empty($this->getRequest()->getSession()->get('_groupecontactgere')) && $this->security->isGranted('ROLE_CONTACT')) {
                    $query->andWhere('false = true');
                } elseif (empty($this->getRequest()->getSession()->get('_groupetresoriergere')) && $this->security->isGranted('ROLE_TRESORIER')) {
                    $query->andWhere('false = true');
                } else {
                    $groupe = $this->getRequest()->getSession()->get('_groupegere');
                    if (empty($groupe)) {
                        $groupe = $this->getRequest()->getSession()->get('_groupecontactgere');
                        if (empty($groupe)) {
                            $groupe = $this->getRequest()->getSession()->get('_groupetresoriergere');
                        }
                    }
                    $connection = $em->getConnection();
                    $prestatable = $em->getMetadataFactory()->getMetadataFor(Prestataire::class)->getTableName();
                    $statement = $connection->prepare('SELECT f.id FROM ' . $fluxtable . ' f WHERE f.groupe_id = :groupe_id OR (f.prestataire_id IN (SELECT p.id FROM ' . $prestatable . ' p WHERE p.groupe_id = :groupe_id)) OR (f.prestataire_dest_id IN (SELECT p.id FROM ' . $prestatable . ' p WHERE p.groupe_id = :groupe_id))');
                    $statement->bindValue(':groupe_id', $groupe->getId());
                    $statement->execute();
                    $ids = $statement->fetchAll();
                    $query
                        ->andWhere($query->expr()->in($query->getRootAliases()[0] . '.id', ':ids'))
                        ->setParameter('ids', $ids)
                    ;
                }
            } elseif ($this->security->isGranted('ROLE_COMPTOIR')) {
                if (empty($this->getRequest()->getSession()->get('_comptoirgere'))) {
                    $query->andWhere('false = true');
                } else {
                    $comptoir = $this->getRequest()->getSession()->get('_comptoirgere');
                    $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
                    $connection = $em->getConnection();
                    $statement = $connection->prepare('SELECT f.id FROM ' . $fluxtable . ' f WHERE f.comptoir_id = :comptoir_id OR f.user_id = :user_id');
                    $statement->bindValue(':comptoir_id', $comptoir->getId());
                    $statement->bindValue(':user_id', $user->getId());
                    $statement->execute();
                    $ids = $statement->fetchAll();
                    $query
                        ->andWhere($query->expr()->in($query->getRootAliases()[0] . '.id', ':ids'))
                        ->setParameter('ids', $ids)
                    ;
                }
            }
        }

        return $query;
    }

    public function getTotalLabel()
    {
        return $this->translator->trans('Total des transferts');
    }

    public function getTotal()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $query = clone $datagrid->getQuery();
        $query
            ->select('SUM( ' . $query->getRootAlias() . '.montant) as total')
            ->andWhere($query->getRootAlias() . '.parenttype = :type')
            ->setParameter('type', Flux::TYPE_TRANSFERT)
            ->setFirstResult(null)
            ->setMaxResults(null);

        $result = $query->execute([], \Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('type', StringFilter::class, [
                'label' => 'Type de transfert',
                'advanced_filter' => false,
                'show_filter' => true,
            ], ChoiceType::class, [
                'choices' => Transfert::getAvailableTypes(),
            ])
            ->add('operateur', null, [
                'label' => 'Operateur',
                'advanced_filter' => false,
                'show_filter' => true,
            ])
        ;
    }
}
