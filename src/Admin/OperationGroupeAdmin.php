<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;

/**
 * Administration des operation des groupes.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class OperationGroupeAdmin extends OperationAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('account.accountableObject', null, ['label' => 'Compte Groupe', 'template' => '@kohinos/bundles/SonataAdminBundle/Block/accountable.html.twig']);
        parent::configureListFields($listMapper);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        parent::configureDatagridFilters($datagridMapper);
        $datagridMapper
            ->add('account.groupe', null, [
                'label' => 'Groupe',
                'advanced_filter' => false,
                'show_filter' => true,
            ])
        ;
    }
}
