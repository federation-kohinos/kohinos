<?php

namespace App\Events;

/**
 * Tous les évènements.
 *
 * KOHINOS : Outil de gestion de Monnaie Locale Complémentaire
 *
 * @author Julien Jorry <julien.jorry@gmail.com>
 */
class MLCEvents
{
    /* GLOBAL EVENTS*/
    const REGISTRATION_USER = 'mlc.registration_user';
    const REGISTRATION_ADHERENT = 'mlc.registration_adherent';
    const REGISTRATION_PRESTATAIRE = 'mlc.registration_prestataire';
    const REGISTRATION_CAISSIER = 'mlc.registration_caissier';

    /* FLUX EVENTS*/
    const FLUX = 'mlc.flux';
    const VALIDATE_DEMANDE_ACHAT_EMLC = 'mlc.validate.demande.achat.emlc';
    const CHANGE_ADHERENT_COMPTOIR = 'mlc.flux.change.adherent.comptoir';
    const CHANGE_PRESTATAIRE_COMPTOIR = 'mlc.flux.change.prestataire.comptoir';
    const COTISATION_COTISATION = 'mlc.flux.cotisation';
    const COTISATION_COTISATION_ADHERENT = 'mlc.flux.cotisation.adherent';
    const COTISATION_COTISATION_PRESTATAIRE = 'mlc.flux.cotisation.prestataire';
    const TRANSACTION_ADHERENT_ADHERENT = 'mlc.flux.transaction.adherent.adherent';
    const TRANSACTION_ADHERENT_PRESTATAIRE = 'mlc.flux.transaction.adherent.prestataire';
    const TRANSACTION_PRESTATAIRE_ADHERENT = 'mlc.flux.transaction.prestataire.adherent';
    const TRANSACTION_PRESTATAIRE_PRESTATAIRE = 'mlc.flux.transaction.prestataire.prestataire';
    const TRANSFERT_COMPTOIR_GROUPE = 'mlc.flux.transfert.comptoir.groupe';
    const TRANSFERT_GROUPE_COMPTOIR = 'mlc.flux.transfert.groupe.comptoir';
    const TRANSFERT_GROUPE_SIEGE = 'mlc.flux.transfert.groupe.siege';
    const TRANSFERT_SIEGE_GROUPE = 'mlc.flux.transfert.siege.groupe';
    const TRANSFERT_PRESTATAIRE_SIEGE = 'mlc.flux.transfert.prestataire.siege';
    const RETRAIT_COMPTOIR_ADHERENT = 'mlc.flux.retrait.comptoir.adherent';
    const RETRAIT_COMPTOIR_PRESTATAIRE = 'mlc.flux.retrait.comptoir.prestataire';
    const VENTE_COMPTOIR_ADHERENT = 'mlc.flux.vente.comptoir.adherent';
    const VENTE_COMPTOIR_PRESTATAIRE = 'mlc.flux.vente.comptoir.prestataire';
}
