<?php

namespace App\Events;

use App\Entity\Prestataire;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Model\UserInterface;

class PrestataireEvent extends UserEvent
{
    protected $prestataire;
    protected $type;

    public function __construct(UserInterface $user, Prestataire $prestataire, string $type)
    {
        $this->user = $user;
        $this->prestataire = $prestataire;
        $this->type = $type;
    }

    /**
     * Get prestataire.
     *
     * @return Prestataire
     */
    public function getPrestataire(): ?Prestataire
    {
        return $this->prestataire;
    }

    /**
     * Set prestataire.
     *
     * @return $this
     */
    public function setPrestataire(Prestataire $prestataire): self
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * Get type.
     *
     * @return
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @return $this
     */
    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }
}
