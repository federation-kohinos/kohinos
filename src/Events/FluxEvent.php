<?php

namespace App\Events;

use App\Flux\FluxInterface;
use Symfony\Component\EventDispatcher\Event;

class FluxEvent extends Event
{
    protected $flux;

    /**
     * @param FormInterface $form The associated form
     * @param mixed         $data The data
     */
    public function __construct(FluxInterface $flux)
    {
        $this->flux = $flux;
    }

    /**
     * Get flux.
     *
     * @return
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * Set flux.
     *
     * @return $this
     */
    public function setFlux($flux)
    {
        $this->flux = $flux;

        return $this;
    }
}
