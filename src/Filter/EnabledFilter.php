<?php

namespace App\Filter;

use App\Entity\EntityTrait\EnablableEntityTrait;
use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

class EnabledFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $usingTrait = in_array(
            EnablableEntityTrait::class,
            array_keys($targetEntity->reflClass->getTraits())
        );

        if (!$usingTrait || 'Flux' == $targetEntity->reflClass->getShortName()) {
            return '';
        }

        $fieldName = 'enabled';

        // Add the Where clause in the request
        $query = sprintf('(%s.%s = %s OR %s.%s IS NULL)', $targetTableAlias, $fieldName, 'true', $targetTableAlias, $fieldName);

        return $query;
    }
}
