<?php

namespace App\Filter;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Action\SetObjectFieldValueAction;
use Sonata\AdminBundle\Controller\CoreController;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Controller\HelperController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class Configurator.
 */
class Configurator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * onKernelRequest.
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        // when a controller class defines multiple action methods, the controller
        // is returned as [$controllerInstance, 'methodName']
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        $isAdminController =
            $controller instanceof CRUDController ||
            $controller instanceof CoreController ||
            $controller instanceof HelperController ||
            $controller instanceof SetObjectFieldValueAction
        ;

        if ($isAdminController) {
            // @TODO : sur le backoffice, mettre un bouton pour afficher les éléments supprimés
            // (pas vraiment, avec le filtre softdelete) et une variable en session pour activer ou non le filtrer Doctrine
            // $this->em->getFilters()->disable('softdeleteable');
            if ($this->em->getFilters()->isEnabled('enabled_filter')) {
                $this->em->getFilters()->disable('enabled_filter');
            }
        }
    }
}
