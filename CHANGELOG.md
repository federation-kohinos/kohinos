# Update v2.4.12 (2025-02-26)
	- Administration : amélioration / redisng de la vue configuration pour plus de clarté
    - Paramètres globaux : ajout de paramètre pour l'achat de emlc (montant libre, slider, bouton radio et choix possible)
    - Correction de HelloAssoCallbackController pour matcher plus d'adherent et de prestataire
    - Tests: amélioration des tests fonctionnels pour valider l'accessibilité des routes et la fonctionnalité d'authentification

# Update v2.4.10 (2023-04-10)
	- Fix : adherente admin export : add enabled field
	- Achat monnaie : fix maximum to 1000€
	- Fix solidoume form : from 1 to 10 and from 1 to 11 for solidoume@doume.org
	- Fix wording Solidoume params : Date de prélèvement => Date de redistribution

# Update v2.4.9 (2023-02-29)
	- Fix solidoume + admin cotisation prestataire list for super admin

# Update v2.4.8 (2023-01-29)
	- Solidoume: add modal on confirm + add links to buy emlc if balance is inferior
    - Some minor print improvements in index for rubriques and stats
    - Add Solidoume param descriptionCotisation to separate text solidoume 1st page to 2nd page
	- Fix solidoume command
    - Add horaires to comptoir + add some infos on groupepresta in map and liste of groupepresta + fix issue with map
    - Fix other margin problem due to bootstrap update + fetch entities improving
    - Fix fetch problem on user + adherent decreasing performance
    - Fix bootstrap update 2 : fix sliders + margins/padding + fix achat slider value from 1 to 1000 + update bootswatch theme
    - Fix bootstrap data-bs-**** changes on v5
    - Charts : optimize loading on admin + some doctrine lazy loading optimization
    - Fix api platform print html

# Update v2.4.7 (2022-08-01)
	- Chart for stats in admin panel
	    - Add stimulus and Chartjs
	    - Update webpack + bootstrap on front
	    - Add stats number for presta, adherent, comptoirs, groupes, pages, faqs, documents, news
	    - ADd shorcut in admin for validate achat monnaie
	- Add notification for caissier for presta's transactions

# Update v2.4.6 (2022-10-29)
	- Fix ckeditor : all plugin js files was not in build...
	- Adherent : Fix search for adherent to make a transfer
	- Fix prestas count display on map
	- Fix export operation : add expediteur and destinataire
	- API : allow export to csv + fix map poi get all without pagination
	- Fix Solidoume redistribution program : bug in some particular situation
	     Fix SolidoumeCommand bug : if someone has not been yet taken
	- Fix installation bug, global parameter not mandatory
	- Geoloc prestataire : by default visible on map
	- Fix menu bug if no USE_PAYZEN config
	- Fix Don for emlc cotisation
	- Fix impersonating
	- Fix fixtures dev on certain globalparameters boolean
	- Fix API : remove unuse map endpoints
	- Update README.md
	- Fix Solidoume command exit bug
	- Update documentation in doc/ directory
	- Fix errors in installation process : add empty .gitignore in templates/themes/custom to keep directory custom + fix error in.env.dist EMAIL_ERROR with no bracket
	- Add new file
	- ADMIN export : Add simple email into prestataire and solidoume export
	- Solidoume : add send quizz to >3 months participants + fix email layout + fix total amount calculated on taking
	- Fix solidoume command for redistribution don bug + fix solidoume item show
	- Fix bin/console bug

# Update v2.4.5 (2022-08-01)
	- Admin : export prestatataires : ajout d'une colonne avec les gestionnaires (nom + email)
	- Ajout d'un message d'erreur quand un prestatataire essaye de demander une reconversion et qu'il n'est pas à jour de sa cotisation
	- Fix bug sur les formulaires d'achat de monnaie (bug visible uniquement dans certaines conditions)
	- Mise à jour de certains fichiersde configuration (lié aux recettes flex des mise à jours de bundles doctrine et symfony framework-bundle)

# Update v2.4.4 (2022-07-30)
	- ADMIN Adherent liste : fix cotisation à jour ou non (par rapport à son status d'adhérent et non l'utilisateur)
	- Fix récupération de tout les prestataires si aucuns tags assignés (retournait aucun prestataire....)
	- SOLIDOUME:  fix redistribution + add analysis tools for redistribution

# Update v2.4.3 (2022-06-26)
	- Composer update dependencies (simple and quick version)
    - Fix installation bug on tickets value : int => float
    - Fix installation bug on coordinates
	- Solidoume : delete local tests (not use anymore)
	- Solidoume : fix in redistributing money
	- Virement : Autocompletion des adhérents et prestatataires

# Update v2.4.2 (2022-06-02)
	- Fix erreur pour les flux cotisation (avec don = 0)

# Update v2.4.1 (2022-05-06)
	- Fix erreur en virement si don non paramétrer
	- Fix config swiftmailer en dev (technical)
	- Fix monnaie papier nantie : arrondir, pas de virgule possible
	- Fix de l'installation du Kohinos qui renvoyait une erreur
	- Fix Alertes email à chaque transaction ?
	- HelloAsso : fix error in synchronisation

# Update v2.4.0 (2022-04-20)

	- HELLOASSO MODULE
	- Gestion de dons (pour las adherents et prestatataires, pour l'achat et la cotisation)
	- PHP CS FIXER sur tous les fichiers .php
	- Corrections de bugs :
		- Fix : un adhérent/prestataire pas à jour de sa cotisation ne pourra pas faire de virement ou de demande de reconversion
		- Fix import => ajout du type de prestataire par défaut (prestataire)
		- Ajout du bouton pour cotiser pour l'année suivante (si cotisation pour l'année en cours valide)
			- et ajout de la date de fin de validité de la cotisation en cours
	 	- Ajout d'une fonction pour avoir le nom complet d'un adhérent

# Update v2.3.0 (2022-03-29)

## Sécurité sociale alimentaire (Solidoume pour la doume)
	- Nouveau paramètre global USE_SOLIDOUME pour déterminer si on veut utiliser et afficher le programme
	- Interface d'admin avec :
		- configuration des différents paramètres Solidoume (montant min, max, commission...) ainsi que les emails envoyés
		- Liste des participants à Solidoume

# Update v2.2.5 (2022-03-25)
	- ajout d'un paramètre obligatoire MLC_URL qui doit refléter l'url du site kohinos installé (exemple : https://doume.org) => utile pour l'installation de l'application
	- Fix bug sur le rôle contact d'un groupe local => impossible d'afficher l'historique des transactions du groupe
	- ADMIN : Fix bug sur l'ajout d'une cotisation en emlc => le compte est désormais prélevé du montant !
	- PWA Kohinos :
		- utilisation de workbox-webpack-plugin pour la génération du sw.js
		- ajout à la racine des fichiers manifest.json (généré via un twig) et sw.js 
		- modification du layout pour l'ajout du service-worker et des balises meta nécessaires
		- modification du template de la homepage pour simplifier la page en mobile
		- Suppression des assets de langue inutiles pour ckeditor

# Update v2.2.4 (2022-03-22)
	- Fix bug sur l'ajout d'une cotisation sans adhérent ou sans prestataire
	- Admin operation prestataire : add filtre par groupe local

# Update v2.2.3 (2022-03-17)
	- Correction d'un bug sur la cotisation en eMLC qui permettait de cotiser sans avoir le solde de emlc nécessaire
	- Wording pour le comptoir "Reconversion de MLC papier" devient "Dépôt de MLC papier"
	- Fix error sur le choix de la date de début cotisation pour pouvoir choisir une date passée
	- Modification des roles du Trésorier pour exporter les Operations dans l'Administration
	- Fix formulaire de modification des infos du prestatataire et du comptoir loggués
	- Fix cotisation en emlc
	- Cotisations Admin : Wording du label "Expéditeur" => "Adhérent" ou "Prestataire"
	- Fix bug sur la modification du mot de passe de façon erratique + php cs fixer sur les fichiers

# Update v2.2.2 (2022-03-17)
	- Fix page Adherer : ajout de la possibilité de cotiser du montant qu'on souhaite au dessus du montant minimum
	- Fix du soucis au retour de l'adhésion
	- Fix erreur sur la liste des comptoirs
	- Fix sur la configuration de l'envoi d'email
	- Fix monolog configuration (log d'erreurs)
	- Fix page comptoir : la description était mal coupée si trop longue
	- Fix bug sur la demande d'achat de eMLC
	- Mise à jour des assets via yarn
	- Fix @UniqueEntity : modification des messages d'erreur et des erreurs liées aux noms/raisons existant déjà
	- Admin prestataire liste : Ajout du champ "idmlc" sous la raison

# Update v2.2.1 (2022-02-28)

## ADMIN
	- BIG UPDATE ON ADMIN LIST : add export on top + add createdAt and updatedAt filter on different views with datepicker + add cotisation end of validity date instead of 'yes' in list + add free cotisation to manager in admin presta list view
	- fix export reconversion
	- fix export achat monnaie 
	- fix export comptoir : add dates, groupe local and contacts
	- add filters by groupe in operations...
	- DocumentAdmin : add edit, show + url of documents
	- PageAdmin : add URL to list of pages
	- Fix bug for add groupe for contact
	- Admin presta and adherent export : add admin rights to users
	- Sonata Admin : persist filters
	- Presta / Adherent Admin : add cotisation up to date and first cotisation in export

## FRONT
	- Fix cotisation price disabled on adherer page
	- Fix bug order by nom prestataire
	- Update show groupe template + fix liste presta by groupe
	- Contact form : add captcha to avoid spam
	- Prestataire profil : add the possibility to add/remove caissiers + delete lat/lon + better look
	- Fix login button for mobile
	- Prestataire liste : order by groupe or name + show presta by groupe

##[IMPORTANT]
	- Fix bug with multiple flux form submit (fix front + fix csrf token)
	- Fix monolog log for prod
	- Fix gestionnaire groupe local can see every presta and adherent from mlc including operation (can be revert if needed)

# Update v2.2.0 (2022-01-12)

## Security update
	- replace ALL $this->security->getUser()->isGranted(...) BY $this->security->isGranted(...)

## Translation
    - Add Translation command update to fix issue with abstract class
    - Fix translation update so add Translation to Admin menu
    - Update translations files

## Prestataire connected 
	- update personal infos (add horaires, site web and image and remove groupe local)

## FRONT :
	- Prestataire/Partenaire repository
    - fix do not print presta/partner with tags with no print
    - fix : add defaultFilter to all functions to search presta/partner
	- Show one prestataire : update site web (link _blank)

## ADMIN :
	- Admin adherent list :
        - add filter to show if cotisation is updated
	- Admin prestataire list :
        - add column + filter to show if cotisation is updated
        - add column + filter gestionnaires and if cotisation is updated
        - add actions to add free cotisation to presta if cotisation is not up to date
	- Admin prestataire edit :
        - add column comments (private)
        - change 'Statut' to 'Activités' and update field to 255 car

## DIVERS :
	- Update packages.json to update yarn packages to work with webpack compiler
	- Update .env.dist to add EMAIL_USER_FROM & EMAIL_ERROR_FROM
	- Update webpack.config to suppress deprecation warnings
	- Fix issue with Adresse with same name for same presta

# Update v2.1.5 (2021-05-12)

## Correction pour l'affichage des pages de confirmation de paiement

## Suite aux remarques de Stephan : 
	- Correction/complétion du htaccess pour la gestion du cache, des XSS...
	- Suppression des appels aux google font et google apis !
	- Ajout du lien "canonical" pour le référencement

## Différents bugfix :
	- Cotisation à 0euros possible maintenant !
	- Correction des données exportées (sur les flux principalement)
	- Fix 'montant minimum' cotisation + fix last operations + fix link to return home + add error msg to transaction/reconversion
	- Fix operations print on admin siege invalid
	- Fix installation bug on MLC_SYMBOL

# Update v2.1.4 (2021-02-22) :

## Refactorisation et amélioration du code

## Ajout de la possibilité de faire un thème pour le front Kohinos

## Sécurité et gestion de la monnaie :
	- Force HTTPS
	- Changement sur tout la BDD pour plus de sécurité : ID => UUID
	- Ajout de comptes pour :
		- Adherent : compte eMLC
		- Prestataire : compte eMLC
		- Siege : compte eMLC nantie, MLC nantie, billets MLC au siège (plus tard : compte euro de fonctionnement)
		- Groupe : compte billets MLC
		- Comptoir : compte billets MLC
	- Ajout d'opérations sur ces comptes en fonction des actions (flux)
	- Ajout de sécurité pour ne pas avoir la possibilité de faire plusieurs actions sur le même compte en même temps (transactional)
	- Modification des droits en fonction des rôles
	- Ajout d'un hash unique par operations et par compte pour vérifier l'intégrité des données et la non modification directement en BDD !
	- Ajout de commandes Symfony permettant de vérifier l'intégrité des comptes et opérations
	- Ajout de l'achat de monnaie (cb / emlc / espece / autre)
	- Ajout de la demande d'achat de monnaie (cheque / virement)
		=> Ajout d'une case à cocher pour les trésoriers / admin pour valider les demandes d'achat (à la réception du paiement)
	- Comptoir : Ajout de la vente de monnaie numérique aux adhérents et prestas (cb / emlc / espece / autre)
	- Ajout d'un bundle GPDR avec gestion des données sensibles et encryption des données en BDD => pour le moment, uniquement l'Iban prestataire

## Autres améliorations / corrections :

	- Suppression de la possibilité de changer de langue sur l'admin
	- Ajout des notifications emails différentes suivant les flux et le rôle dans l'action expediteur/destinataire
	- Ajout de redirection facultative et paramètrable en fonction de l'action menée
	- Ajout d'un écran permettant de voir toutes les opérations sur un compte
	- Admin siège : ajout de la possibilité d'imprimer/détruire des billets non nantis
	- Front Kohinos : 
		- Ajout de breadcrumb
		- léger arrondi des boutons et des block collapse
		- Loggué en admin : Ajout d'un lien vers la documentation du Kohinos
		- Ajout des statistiques (monnaie émise, en circulation, nantie...)