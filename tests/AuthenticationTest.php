<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Test pour vérifier le fonctionnement de l'authentification
 */
class AuthenticationTest extends WebTestCase
{
    /**
     * Test que la page de login est accessible
     */
    public function testLoginPageIsAccessible()
    {
        $client = static::createClient();
        $client->request('GET', '/login');

        $this->assertNotEquals(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            $client->getResponse()->getStatusCode(),
            'La page de login ne devrait pas retourner une erreur 500'
        );
    }

    /**
     * Test que la soumission des données de login fonctionne
     */
    public function testLoginPostAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');
        
        $this->assertNotEquals(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            $client->getResponse()->getStatusCode(),
            'La page de login ne devrait pas retourner une erreur 500'
        );
        
        // S'assurer que nous avons un formulaire sur la page
        $this->assertGreaterThan(0, $crawler->filter('form')->count(), 'Aucun formulaire trouvé sur la page de login');
        
        // Soumission directe sans utiliser submitForm()
        $client->request(
            'POST',
            '/login_check',
            [
                '_username' => 'test_user',
                '_password' => 'test_password',
                '_csrf_token' => 'test_token',  // La validation CSRF est généralement désactivée en test
            ]
        );
        
        // Vérifier que nous ne recevons pas d'erreur 500
        $this->assertNotEquals(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            $client->getResponse()->getStatusCode(),
            'La soumission du formulaire de login ne devrait pas retourner une erreur 500'
        );
    }

    /**
     * Test que les pages protégées redirigent vers la page de login
     * 
     * @dataProvider securedRoutesProvider
     */
    public function testSecuredRoutesRedirectToLoginPage($url)
    {
        $client = static::createClient();
        $client->request('GET', $url);
        
        $statusCode = $client->getResponse()->getStatusCode();
        
        // Vérifier que nous recevons une redirection (302) ou une erreur d'autorisation (401/403)
        // et non pas une erreur serveur (500)
        $this->assertTrue(
            in_array($statusCode, [
                Response::HTTP_FOUND,            // 302 (redirection)
                Response::HTTP_UNAUTHORIZED,     // 401
                Response::HTTP_FORBIDDEN         // 403
            ]),
            sprintf('La route protégée %s devrait rediriger vers la page de login (302) ou retourner 401/403, mais a retourné %d', $url, $statusCode)
        );
        
        // Si c'est une redirection, vérifier qu'elle pointe vers la page de login
        if ($statusCode === Response::HTTP_FOUND) {
            $location = $client->getResponse()->headers->get('Location');
            $this->assertStringContainsString('/login', $location);
        }
    }

    /**
     * Fournit les routes qui devraient être protégées par l'authentification
     */
    public function securedRoutesProvider()
    {
        yield ['/admin/dashboard'];
        yield ['/admin/app/user/list'];
        yield ['/admin/prestataire/list'];
        yield ['/admin/adherent/list'];
    }
}
