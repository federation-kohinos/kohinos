<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test pour vérifier que les routes critiques de l'application fonctionnent correctement
 * 
 * Ce test se concentre sur les routes les plus importantes de l'application et 
 * vérifie qu'elles retournent un code 200 (pas d'erreur 500).
 */
class CriticalRoutesTest extends WebTestCase
{
    /**
     * Test que les routes critiques retournent un code 200 OK
     * 
     * @dataProvider criticalRoutesProvider
     */
    public function testCriticalRoutesReturnOk($url)
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $statusCode = $client->getResponse()->getStatusCode();
        
        if ($statusCode === Response::HTTP_INTERNAL_SERVER_ERROR) {
            $responseContent = $client->getResponse()->getContent();
            $errorInfo = "Réponse complète: " . substr($responseContent, 0, 500) . "..."; // Afficher seulement les 500 premiers caractères
        } else {
            $errorInfo = "";
        }
        
        $this->assertNotEquals(
            Response::HTTP_INTERNAL_SERVER_ERROR, 
            $statusCode, 
            sprintf('La route critique %s retourne une erreur 500. %s', $url, $errorInfo)
        );
    }

    /**
     * Fournit les routes critiques à tester
     */
    public function criticalRoutesProvider()
    {
        // Page d'accueil
        yield ['/login'];
        
        // Page admin principale
        yield ['/admin/dashboard'];
        
        // Page de liste des prestataires
        yield ['/admin/prestataire/list'];
        
        // Page de liste des adhérents
        yield ['/admin/adherent/list'];
        
        // Routes HelloAsso (utilisées pour les callbacks)
        yield ['/helloasso/verify_payment_success/callback'];
        yield ['/helloasso/notify/callback'];
    }
}
