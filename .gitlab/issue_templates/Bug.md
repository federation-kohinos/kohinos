# Résumé

(Résumé concis du bug)

# Étapes pour reproduire

(Comment reproduire le bug)

# Comportement courant

(ce qu'il se passe et que vous trouver anormal)

# Comportement attendu

(ce qui devrait se passer selon vous ou qui se passait avant la régression)

# Environnement

( Version / environnement / instance, …)  

# Préciser le Compte / Rôle pour se connecter

(fournir les comptes si besoin)

# Logs et/ou captures d'écran pertinent


/label ~BUG